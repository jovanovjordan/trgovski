$(document).ready(function(){
	$(".VebkoLoading").delay( 20 ).fadeOut(0);
	$("header").delay( 40 ).fadeIn(400);
	$("main").delay( 40 ).fadeIn(400);	
	
	//$("#searchBoxBody").toggle();
	$("#vebkoCollapse").click(function(){
            $("#searchBoxBody").toggle();
        });
	//collapse for search -end
	
	
	$('.vebkoMainCategoryOne').css({
	  'height': function() {
		return $(this).width()*3
	  }
	});
	$('.vebkoMainCategoryOne a').css({
	  'padding-top': function() {
		return $(this).height()/2-24
	  }
	});
	//just for small screen
	if($( document ).width() <= 800){
		
		$('.vebkoMainCategoryOne').css({
		  'height': function() {
			return $(this).width()*2
		  }
		});
		$('.vebkoMainCategoryOne a').css({
		  'padding-top': function() {
			return $(this).height()/2-30
		  }
		});
	}
	
	$(".vebkoMessage").delay( 3000 ).fadeOut(1000);
	
	
	
	//vibrate for android start
	$(".btn").vibrate();
	$("a").vibrate();
	//vibrate for android end	
	//gallery start
	$('#gallery').simplegallery({
		galltime : 400,
		gallcontent: '.content',
		gallthumbnail: '.thumbnail',
		gallthumb: '.thumb'
	});
	$('a.goback').click(function(){
		window.history.back();
		return false;
	});
	//gallery end
	var swiper = new Swiper('.swiper-container', {
      zoom: true,
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
});




