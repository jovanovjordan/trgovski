<?php

class Vebko extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Ad_model');
                $this->load->model('Admin_model');
		$this->load->model('User_model');
		$this->load->model('Image_model');
		$this->load->model('Vebko_model');
		$this->load->model('Vehicle_model');
		$this->load->model('Estate_model');
		$this->load->model('Electronics_model');
		$this->load->model('Clothing_model');
		$this->load->model('Service_model');
		$this->load->model('Other_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$timezone = "Europe/Belgrade";
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}
	
	public function index(){
            $ln = $this->uri->segment(3);
            $subdomain_arr = explode('.', $_SERVER['HTTP_HOST'], 2); //creates the various parts
            $subdomain_name = $subdomain_arr[0]; //assigns the first part
            if($subdomain_name!='trgovski'){
                //start store
		$userId = $this->Vebko_model->getUserIdByStoreSubDomain($subdomain_name);
                $mark = 0; //get just publish ads.
                $data["storeAds"] = $this->Vebko_model->getUserAdsМarking($userId,$mark);
                $rules = $data["storeAds"];
                //echo "<pre>";print_r($data["storeAds"]);exit;
               //label
		//data - start			
                $data['storeSubDomain'] = $subdomain_name;
		$data['storeContent'] = $this->Vebko_model->storeContent($userId);
                $data['storeLogo'] = $this->Vebko_model->storeLogo($data['storeContent'][0]->id);
		$data['storeUrl'] = $this->Vebko_model->storeUrl($data['storeContent'][0]->id);
                //data - end
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('store/store',$data);
		$this->load->view('footer',$data);
            }
            else{
                $data["frontMessageMain"]                   = $this->Vebko_model->translateText("frontMessageMain");

                $data["labelCategoryChoiceCategory"]        = $this->Vebko_model->translateText("labelCategoryChoiceCategory");
                $data["real_estate"]                        = $this->Vebko_model->translateText("real_estate");
                $data["vehicle"]                            = $this->Vebko_model->translateText("vehicle");
                $data["electronics"]                        = $this->Vebko_model->translateText("electronics");
                $data["clothing"]                           = $this->Vebko_model->translateText("clothing");
                $data["services"]                           = $this->Vebko_model->translateText("services");
                $data["jobs"]                               = $this->Vebko_model->translateText("jobs");
                $data["allcategories"]                      = $this->Vebko_model->translateText("allcategories");
                $data["other"]                              = $this->Vebko_model->translateText("other");

                $data['checkUserLoginReturnUserData']       = $this->User_model->checkUserLoginReturnUserData();
                $data['title_meta_tag']                     = "Trgovski.com";
                $data['description_meta_tag']               = "Бесплатен интернет огласник";
                $data['image_url_meta_tag']                 = base_url('images/facebookfront.jpg');

                $dataHeaderFooter                           = $this->Vebko_model->dataHeaderFooter();
                $dataHeaderFooter['title_meta_tag']         = $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
                $dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");	
                
                $data['StoresTitle']                    = $this->Vebko_model->translateText("StoresTitle");
                $data['StoresTitleAll']                    = $this->Vebko_model->translateText("StoresTitleAll");
                $stores = $this->Vebko_model->getStores(8);
                foreach ($stores as $store) {
                    $data['storeLogo'][$store->id] = $this->Vebko_model->storeLogo($store->id);
                }
                $data['stores'] = $stores;
                $data['url'] = '.'.substr(base_url(), 8,-1);
                
                $data['ads'] = $this->Vebko_model->getAllAds();
                //echo "<pre>"; print_r($this->Vebko_model->get_all_ads());exit;
     
                $dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
                $this->load->view('header',$dataHeaderFooter);
                $this->load->view('home',$data);
                $this->load->view('store/allBox',$data);
                $this->load->view('ad/all',$data);
                $this->load->view('footer',$data);
            }
	}
        
        
        
        public function get_all_ads(){		
		$queryRowStart = $this->input->post('queryRowStart');
		
		echo(json_encode($this->Vebko_model->get_all_ads($queryRowStart)));
	}
        
        
        
	/*public function store(){
		$storeName = $this->uri->segment(2);
                
		$userId = $this->Vebko_model->getUserIdByStoreName($storeName);
		$data["storeAds"] = $this->Vebko_model->getUserAds($userId);
		
		//label

		//data - start
		$data['storeName'] = $storeName;	
		//data - end
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('store/store',$data);
		$this->load->view('footer',$data);
		
	}
	/*public function category() {		
		$data["labelCategoryChoiceCategory"] = $this->Vebko_model->translateText("labelCategoryChoiceCategory");
		$data["real_estate"] = $this->Vebko_model->translateText("real_estate");
		$data["vehicle"] = $this->Vebko_model->translateText("vehicle");
		$data["electronics"] = $this->Vebko_model->translateText("electronics");
		$data["clothing"] = $this->Vebko_model->translateText("clothing");
		$data["services"] = $this->Vebko_model->translateText("services");
		$data["jobs"] = $this->Vebko_model->translateText("jobs");
		$data["allcategories"] = $this->Vebko_model->translateText("allcategories");
		//$data["getCountry"] = $this->Vebko_model->getCountryID();
		
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/category',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}*/
	public function insert() {
                                 
        $dataHeaderFooter                           = $this->Vebko_model->dataHeaderFooter();
		$dataHeaderFooter['title_meta_tag']       	= $this->Vebko_model->translateText("labelMetaTagInsertAdsTitle");		
		$dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagInsertAdsDescription");		
		$dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/insertads.jpg";		
		
		$data['checkUserLoginReturnUserData']       = $this->User_model->checkUserLoginReturnUserData();		
		$data["labelInsertTitle"]                   = $this->Vebko_model->translateText("labelInsertTitle");
		$data["labelInsertBodyLine1"]               = $this->Vebko_model->translateText("labelInsertBodyLine1");
		$data["labelInsertBodyLine2"]               = $this->Vebko_model->translateText("labelInsertBodyLine2");
		$data["labelInsertBodyLine3"]               = $this->Vebko_model->translateText("labelInsertBodyLine3");
		$data["labelInsertBodyLine4"]               = $this->Vebko_model->translateText("labelInsertBodyLine4");                
		$data["labelInsertCategoryChoiceCategory"]  = $this->Vebko_model->translateText("labelInsertCategoryChoiceCategory");
		$data["real_estate"]                        = $this->Vebko_model->translateText("real_estate");
		$data["vehicle"]                            = $this->Vebko_model->translateText("vehicle");
		$data["electronics"]                        = $this->Vebko_model->translateText("electronics");
		$data["clothing"]                           = $this->Vebko_model->translateText("clothing");
		$data["services"]                           = $this->Vebko_model->translateText("services");
		$data["jobs"]                               = $this->Vebko_model->translateText("jobs");
		$data["allcategories"]                      = $this->Vebko_model->translateText("allcategories");
        $data["other"] 								= $this->Vebko_model->translateText("other");
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/insertcategory',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function contact() {
		$language = $this->Vebko_model->translateText('language');
		$contact = 'content/'.$language.'/contact';
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view($contact,$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
        
        public function setting() {
		$language = $this->Vebko_model->translateText('language');
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
                $data['labelSettingTitle']                              =$this->Vebko_model->translateText('labelSettingTitle');
                $data['labelSettingTitleDescription']                   =$this->Vebko_model->translateText('labelSettingTitleDescription');
                $data['labelSettingTitleChangeLanguage']                =$this->Vebko_model->translateText('labelSettingTitleChangeLanguage');
                $data['labelSettingTitleChangeLanguageMessage']         =$this->Vebko_model->translateText('labelSettingTitleChangeLanguageMessage');
                $data['labelSettingTitleChangeCountry']                 =$this->Vebko_model->translateText('labelSettingTitleChangeCountry');
                $data['labelSettingTitleChangeCountryMessage']          =$this->Vebko_model->translateText('labelSettingTitleChangeCountryMessage');
                $data['labelSettingTitleChangeCurrency']                 =$this->Vebko_model->translateText('labelSettingTitleChangeCurrency');
                $data['labelSettingTitleChangeCurrencyMessage']          =$this->Vebko_model->translateText('labelSettingTitleChangeCurrencyMessage');
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('setting',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
        
	public function account() {
            if($this->User_model->checkUserLoginReturnUserData()==0){
                    $this->session->set_userdata('sessionLoginRedirect','account');
                    redirect('user_authentication/index');
            }
            $data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();

            $dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();

            $data['labelAccountTitle']=$this->Vebko_model->translateText('labelAccountTitle');
            $data['labelAccountBodyPersonalInfoTitle']=$this->Vebko_model->translateText('labelAccountBodyPersonalInfoTitle');
            $data['labelAccountBodyPersonalInfoBody']=$this->Vebko_model->translateText('labelAccountBodyPersonalInfoBody');
            $data['labelAccountBodyMyAdsTitle']=$this->Vebko_model->translateText('labelAccountBodyMyAdsTitle');
            $data['labelAccountSubBodyVehibleTitle']=$this->Vebko_model->translateText('labelAccountSubBodyVehibleTitle');
            $data['labelAccountSubBodyEstateTitle']=$this->Vebko_model->translateText('labelAccountSubBodyEstateTitle');
            $data['labelAccountSubBodyElectronicsTitle']=$this->Vebko_model->translateText('labelAccountSubBodyElectronicsTitle');
            $data['labelAccountSubBodyClothingTitle']=$this->Vebko_model->translateText('labelAccountSubBodyClothingTitle');
            $data['labelAccountSubBodyServiceTitle']=$this->Vebko_model->translateText('labelAccountSubBodyServiceTitle');
            $data['labelAccountSubBodyOtherTitle']=$this->Vebko_model->translateText('labelAccountSubBodyOtherTitle');
            $data['labelAccountStoreInformation']=$this->Vebko_model->translateText('labelAccountStoreInformation');           
            $data['labelAccountStoreInformationStoreName']=$this->Vebko_model->translateText('labelAccountStoreInformationStoreName');           
            $data['labelAccountStoreInformationStoreUrl']=$this->Vebko_model->translateText('labelAccountStoreInformationStoreUrl');           
            $data['labelAccountStoreInformationCreateStoreTitle']=$this->Vebko_model->translateText('labelAccountStoreInformationCreateStoreTitle');              
            $data['labelAccountStoreInformationCreateStoreBody']=$this->Vebko_model->translateText('labelAccountStoreInformationCreateStoreBody');   
            $data['labelAccountStoreInformationCreateStoreButtonTitle']=$this->Vebko_model->translateText('labelAccountStoreInformationCreateStoreButtonTitle');
            $data['labelInsertStore']=$this->Vebko_model->translateText('labelInsertStore');
            
            $data['userData'] = $this->Vebko_model->getUserDate();
            $data['storeData'] = $this->Vebko_model->getStoreDate();
   
            $data['accountMyVehicleData'] 		= $this->Vehicle_model->getMyVehicles();
			$data['accountMyEstateData'] 		= $this->Estate_model->getMyAds();
			$data['accountMyElectronicsData'] 	= $this->Electronics_model->getMyAds();
			$data['accountMyClothingData'] 		= $this->Clothing_model->getMyAds();
			$data['accountMyServiceData'] 		= $this->Service_model->getMyAds();
			$data['accountMyOtherData'] 		= $this->Other_model->getMyAds();

            $content = 'content/all/account';
            $this->load->view('header',$dataHeaderFooter);
            $this->load->view($content,$data);
            $this->load->view('footer',$dataHeaderFooter);
	}
	public function help() {	
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('help',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function about() {	
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();

		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$ln = $dataHeaderFooter[sessionCurrentLanguage];
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('content/'.$ln.'/about',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function notfound(){
		redirect(base_url(), 'refresh');
	}
	public function ln(){
		$ln = 'en';
		$ln = $this->uri->segment(3);
		$cookieLanguage= array(
			'name'   => 'language',
			'value'  => $ln,
			'expire' => '86500000',
		);
		$this->input->set_cookie($cookieLanguage);
                $cookieLanguageSetting= array(
			'name'   => 'languageSetting',
			'value'  => 1,
			'expire' => '86500000',
		);
                
		$this->input->set_cookie($cookieLanguageSetting);
		$redirect = $this->session->userdata('lastURLView');
		
		redirect($redirect);
	}
	public function co(){
		$co = 1;
		$co = $this->uri->segment(3);
		$cookieCountry= array(
			'name'   => 'country',
			'value'  => $co,
			'expire' => '86500000',
		);
                $this->input->set_cookie($cookieCountry);
		$cookieLanguageCountry= array(
			'name'   => 'languageCountry',
			'value'  => 1,
			'expire' => '86500000',
		);
		$this->input->set_cookie($cookieLanguageCountry);
		$redirect = $this->session->userdata('lastURLView');
		redirect($redirect);
	}
        
        public function cu(){
		$ln = 'euro';
		$ln = $this->uri->segment(3);
		$cookieCurrency= array(
			'name'   => 'currency',
			'value'  => $ln,
			'expire' => '86500000',
		);
		$this->input->set_cookie($cookieCurrency);
                
                $cookieCurrencySetting= array(
			'name'   => 'currencySetting',
			'value'  => 1,
			'expire' => '86500000',
		);
                
		$this->input->set_cookie($cookieCurrencySetting);
		$redirect = $this->session->userdata('lastURLView');
		
		redirect($redirect);
	}
        
        
	function search_address_country_region(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($id))));
	}
	function search_address_country_municipality(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality($id))));
	}
        
        public function cron(){
            $address_country_id = 1;
            $subject = "Trgovski.com";
            $body = 'Добар ден, 
<br><br>
Накратко да Bи го преставиме Trgovski.com. 
<br><br>
Trgovski.com е бесплатен и засекогаш ќе остане бесплатен интернет простор за објавување огласи. Достапен во Македонија, Србија и Бугарија, со цел во иднина да се прошири во Хрватска, Црна Гора и Грција. Карактеристиките секој оглас внесен на трговски автоматски се прикажуваат и на избраниот јазик на посетителот на сајтот. 
<br><br>
Новитет на trgovski.com е услугата "Ask Trgovski". "Ask Тrgovski" e услуга кој Ви озвозможува да го прашувате trgovski.com преку facebook fan страницата. За да го сторите ова едноставно поставете прашање што пребарувате во приватна порака на Фан Страницата на Трговски fb.com/trgovskicom повеќе во видеото https://www.youtube.com/watch?v=iWGpTckM2o4 
<br><br>
За почеток посете ја web страницата на trgovski.com и разгледајте ги моменталните огласи, доколку имате сопствен оглас чувстувајте се слободно да го внесете на trgovski.com. 
<br><br>
Со почит<br>
Trgovski.com';
            
            $mailMark = 0;
            
            $mailArray = $this->Admin_model->mailArray($address_country_id,$mailMark);
            
            $this->Admin_model->sendMails($mailArray,$subject,$body);
        }
}
?>