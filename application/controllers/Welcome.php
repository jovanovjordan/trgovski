<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    
    public function __construct() {
		
            parent::__construct();
            $this->load->library(array('session'));
            $this->load->helper(array('url'));
            $this->load->model('user_model');			
			$this->load->model('User_model');
			$this->lang->load("message","macedonian");
		
	}
	public function index()
	{
			$data["language_msg"] = $this->lang->line("msg_first_name");
            $data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$data['title_meta_tag'] = "Trgovski.com";
			$data['description_meta_tag'] = "Бесплатен интернет огласник";
			$data['image_url_meta_tag'] = base_url('images/facebookfront.jpg');
			$this->load->view('header',$data);
            $this->load->view('home',$data);
            $this->load->view('footer');
	}
        
}
