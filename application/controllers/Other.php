<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Other extends CI_Controller {
	public $dbt = "ads_other"; //table for this type of ads
	public $adt = 'other'; //part of url
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Ad_model');
		$this->load->model('User_model');
		$this->load->model('Image_model');
		$this->load->model('Vebko_model');
		$this->load->model('Vehicle_model');
        $this->load->model('Other_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$timezone = "Europe/Belgrade";
		$this->load->library('googlemaps');
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}
	public function _remap($method, $params = array()){
        if (method_exists(__CLASS__, $method)) {
            $this->$method($params);
        } else {
			$adt = $this->adt;
            redirect(base_url($adt), 'refresh');
        }
    }
	public function index() {// search
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$countryId = $this->Vebko_model->getCountryID();
		//label - start
		$data['labelEstate'] 					= $this->Vebko_model->translateText('labelEstate');
		$data['pricefrom'] 						= $this->Vebko_model->translateText('pricefrom');
		$data['country'] 						= $this->Vebko_model->translateText('country');
		$data['region'] 						= $this->Vebko_model->translateText('region');
		$data['municipality'] 					= $this->Vebko_model->translateText('municipality');
		$data['search'] 						= $this->Vebko_model->translateText('search');
		$data['buySell'] 						= $this->Vebko_model->translateText('buySell');
		$data['yearfrom'] 						= $this->Vebko_model->translateText('yearfrom');
		$data['yearto'] 						= $this->Vebko_model->translateText('yearto');
		$data['pricefrom'] 						= $this->Vebko_model->translateText('pricefrom');
		$data['priceto'] 						= $this->Vebko_model->translateText('priceto');
		$data['labelSearch'] 					= $this->Vebko_model->translateText('labelSearch');
		$data['labelSearchDescritpion'] 		= $this->Vebko_model->translateText('labelSearchDescritpion');
		$data['labelSearchOther'] 				= $this->Vebko_model->translateText('labelSearchOther');
		$data['labelSearchOtherDescritpion'] 	= $this->Vebko_model->translateText('labelSearchOtherDescritpion');
		//label - end
		
		//data - start
		$data['adt'] 							= $adt;
		$data['ads_estate_type'] 				= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_other_type','title',1));
		$data['ads_vehicles_price'] 			= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_vehicles_price','title',1));
		$data['ads_vehicles_year'] 				= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_vehicles_year','title',1));
		$data['address_country'] 				= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country());
		$data['address_country_id'] 			= $countryId;
		$data['address_country_region']			= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($countryId));
		$data['address_country_municipality'] 	= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality(1));
		$data['ads_vehicles_bye_sell'] 			= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_other_buy_sell','title',1));

		
		$data['checkUserLoginReturnUserData']	=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$dataHeaderFooter['title_meta_tag'] = $this->Vebko_model->translateText('title_meta_tag_'.$adt);
		$dataHeaderFooter['description_meta_tag'] = $this->Vebko_model->translateText('description_meta_tag_'.$adt);
		$dataHeaderFooter['image_url_meta_tag'] = base_url('images/metatag/search_'.$adt.'.jpg');
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/'.$adt.'/search',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function view(){
		$dbt = $this->dbt;		
		$adt = $this->adt;		
		$id = $this->uri->segment('3');
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);
		$data['checkhAdsOwner'] = $this->Other_model->checkhAdsOwner($id);		
		
		//label - start		
		$data['labelEstateType'] 				= $this->Vebko_model->translateText('labelEstateType');
		$data['labelVehicleNoData'] 			= $this->Vebko_model->translateText('labelVehicleNoData');
		$data['labelVehicleSpecification'] 		= $this->Vebko_model->translateText('labelVehicleSpecification');
		$data['labelVehicleYear'] 				= $this->Vebko_model->translateText('labelVehicleYear');
		$data['labelRoomNumber'] 				= $this->Vebko_model->translateText('labelRoomNumber');
		$data['labelMetro'] 					= $this->Vebko_model->translateText('labelMetro');
		$data['labelContactPerson'] 			= $this->Vebko_model->translateText('labelContactPerson');
		$data['labelContactName'] 				= $this->Vebko_model->translateText('labelContactName');
		$data['labelPhone'] 					= $this->Vebko_model->translateText('labelPhone');
		$data['labelEmail'] 					= $this->Vebko_model->translateText('labelEmail');
		$data['labelLocation'] 					= $this->Vebko_model->translateText('labelLocation');
		$data['labelViewStreet'] 				= $this->Vebko_model->translateText('labelViewStreet');
		$data['labelViewMunicipality'] 			= $this->Vebko_model->translateText('labelViewMunicipality');
		$data['labelViewRegion']				= $this->Vebko_model->translateText('labelViewRegion');
		$data['labelViewCountry'] 				= $this->Vebko_model->translateText('labelViewCountry');
		$data['labelViewShare'] 				= $this->Vebko_model->translateText('labelViewShare');
		$data['labelViewSettings'] 				= $this->Vebko_model->translateText('labelViewSettings');
		$data['labelViewSettingsActivate'] 		= $this->Vebko_model->translateText('labelViewSettingsActivate');
		$data['labelViewSettingsDeaktivate'] 	= $this->Vebko_model->translateText('labelViewSettingsDeaktivate');
		$data['labelViewSettingsChange'] 		= $this->Vebko_model->translateText('labelViewSettingsChange');
		$data['labelViewSettingsRenew'] 		= $this->Vebko_model->translateText('labelViewSettingsRenew');
		$data['labelViewSimilarAds'] 			= $this->Vebko_model->translateText('labelViewSimilarAds');
		$data['labelViewAdd'] 					= $this->Vebko_model->translateText('labelViewAdd');
		$data['labelVehiclePublishedDate'] 		= $this->Vebko_model->translateText('labelVehiclePublishedDate');
		$data['labelMarketingTitle'] 			= $this->Vebko_model->translateText('labelMarketingTitle');
		//label - end
		
		//data - start
		$data['adt'] 				= $adt;	
		$data['ads_id'] = $id;
		$data['ads_marking'] = $this->Vebko_model->get_single_value_from_db ($dbt, $id,'marking');
		if($data['ads_marking']==1){
			$data['ads_marking_decativate']= $this->Vebko_model->translateText('adsDeactive');
		}
		else{
			$data['ads_marking_decativate'] ='';
		}
		$data['ads_title'] 							= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'title');
		$data['ads_date'] 							= date('h:i (d/m/Y)', $this->Vebko_model->get_single_value_from_db ($dbt, $id,'date'));
		$data['ads_date_modify'] 					= date('h:i (d/m/Y)', $this->Vebko_model->get_single_value_from_db ($dbt, $id,'date_modify'));
		$ads_type_id 								= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'ads_type_id');
		$data['ads_type'] 							= $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('ads_other_type', $ads_type_id,'title'));	
		$data['body'] 								= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'body');
		$data['year'] 								= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'year');
		$data['roomNumber'] 						= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'roomNumber');
		$data['metro'] 								= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'metro');
		$data['address_street'] 					= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'address_street');
		$ads_address_country_municipality_id 		= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'address_country_municipality_id');
		$data['ads_address_country_municipality'] 	= $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('address_country_municipality', $ads_address_country_municipality_id,'title'));
		$ads_address_country_region_id 				= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'address_country_region_id');
		$data['ads_address_country_region'] 		= $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('address_country_region', $ads_address_country_region_id,'title'));
		$ads_address_country_id 					= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'address_country_id');
		$data['ads_address_country'] 				= $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('address_country', $ads_address_country_id,'title'));
		$data['price'] 								= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'price');
		$data['priceMkd'] 						= $this->Vebko_model->convertCurrency($data['price'], 'EUR', 'MKD').' '.$this->Vebko_model->translateText($this->input->cookie('currency',true));		
		$data['contactName'] 						= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'contactName');
		$data['contact_phone'] 						= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'contact_phone');
		$data['contact_email'] 						= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'contact_email');
		$data['contact_phone_text'] 				= substr($data['contact_phone'], 0, 3) . '/' . substr($data['contact_phone'],3,3). '-' . substr($data['contact_phone'],6,3);
		
		//maps - start
		$marker = array();
		$data['markerPositionLat'] 						= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'markerPositionLat');
		$data['markerPositionLng'] 						= $this->Vebko_model->get_single_value_from_db ($dbt, $id,'markerPositionLng');
		$marker['position'] =  $data['markerPositionLat'].', '.$data['markerPositionLng'];	
		
		$config = array();
		$config['center']= $marker['position'];	
		$config['zoom'] = 14;		

		$this->googlemaps->initialize($config);	
		$this->googlemaps->add_marker($marker);
		$data['map'] = $this->googlemaps->create_map();
		//maps - end
		
		$ID = sprintf('%08d', $id);
		$directoryPath = 'images/ads/'.$adt.'/ad_'.$ID.'/';		
		
                
                
		//add image to gallery
		$gallery_images = array();
		for($i=1; $i<7; $i++){//set in databese how much colume have for image in table ads_vehicle
			if($this->Vebko_model->get_single_value_from_db ($dbt, $id,'ads_gallery_image_name_'.$i) != 'default.jpg'){
				$data['gallery_image_'.$i] =  base_url($directoryPath.'gallery_image_'.$i.'-'.$this->Vebko_model->get_single_value_from_db ($dbt, $id,'ads_gallery_image_name_'.$i));
				$gallery_images[$i-1] = $data['gallery_image_'.$i];
			}
		}
		$data['gallery_images'] = $gallery_images;
		//
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		//add sugestion to array std object		
		$data['suggestions'] = $this->Other_model->getSearchSugestion();
		$data['marketing'] = $this->Vebko_model->getMarketing();
		$marketingCount = count($data['marketing']);
		$marketingRandomId =rand(0, $marketingCount-1);
		$data['marketingCount'] = $marketingCount;
		$data['marketingSingle'] = $data['marketing'][$marketingRandomId];
		
		//echo "<pre>"; print_r($data); exit;
		//data - end
		
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		
		
		
	
		$dataHeaderFooter['title_meta_tag'] = $data['ads_title'];
		$dataHeaderFooter['description_meta_tag'] = $data['ads_type'].', '.
													$data['price'].' - '.
													$data['ads_address_country_municipality'].', '.
													$data['ads_address_country'];
													
		$dataHeaderFooter['image_url_meta_tag'] = $gallery_images['0'];	
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/'.$adt.'/view', $data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function insert(){
		$dbt = $this->dbt;
		$adt = $this->adt;
		$countryId = $this->Vebko_model->getCountryID();
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		
		// check user login and role - start
		if($this->User_model->checkUserLoginReturnUserData()==0){
			$this->session->set_userdata('sessionLoginRedirect',$adt.'/insert');
			redirect('user_authentication/index');
		}
		if($this->User_model->userRole($userId)==1){
			$this->session->set_userdata('sessionLoginRedirect',$adt.'');
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','accountNotAllowNewAds');
			redirect($adt);
		}
		//check user login and role - end
		
		//maps - start
		$this->load->library('googlemaps');
		$config = array();
		$config['center']= "Macedonia";	
		$config['zoom'] = 8;
		$this->googlemaps->initialize($config);

		$marker = array();
		$marker['position'] = 'Macedonia';
		$marker['draggable'] = true;
		$marker['ondragend'] = 'updateMarkerPosition(event.latLng.lat(), event.latLng.lng());';
		$this->googlemaps->add_marker($marker);
		$data['map'] = $this->googlemaps->create_map();
		//maps - end
		
		//label start
		$data['labelInsertAds'] 						= $this->Vebko_model->translateText('labelInsertAds');
		$data['labelInsertAdsDescription'] 				= $this->Vebko_model->translateText('labelInsertAdsDescription');
		$data['labelInsertAds2'] 						= $this->Vebko_model->translateText('labelInsertAds2');
		$data['labelInsertAdsTitle'] 					= $this->Vebko_model->translateText('labelInsertAdsTitle');
		$data['labelInsertAdsPrice'] 					= $this->Vebko_model->translateText('labelInsertAdsPrice');
		$data['labelInsertAdsBody'] 					= $this->Vebko_model->translateText('labelInsertAdsBody');
		$data['labelInsertInsertImages'] 				= $this->Vebko_model->translateText('labelInsertInsertImages');
		$data['labelInsertInsertImagesDescription'] 	= $this->Vebko_model->translateText('labelInsertInsertImagesDescription');
		$data['labelInsertInsertImagesDelete'] 			= $this->Vebko_model->translateText('labelInsertInsertImagesDelete');
		$data['labelAdsDetails'] 						= $this->Vebko_model->translateText('labelAdsDetails');
		$data['labelType'] 								= $this->Vebko_model->translateText('labelType');
		$data['labelRoomNumber'] 						= $this->Vebko_model->translateText('labelRoomNumber');
		$data['labelMetro'] 							= $this->Vebko_model->translateText('labelMetro');
		$data['labelInsertInsertVehicleYear'] 			= $this->Vebko_model->translateText('labelInsertInsertVehicleYear');
		$data['labelContact'] 							= $this->Vebko_model->translateText('labelContact');
		$data['labelContactName'] 						= $this->Vebko_model->translateText('labelContactName');
		$data['labelContactEmail'] 						= $this->Vebko_model->translateText('labelContactEmail');
		$data['labelCountry'] 							= $this->Vebko_model->translateText('labelCountry');
		$data['labelCountryRegion'] 					= $this->Vebko_model->translateText('labelCountryRegion');
		$data['labelCountryMunicipality'] 				= $this->Vebko_model->translateText('labelCountryMunicipality');
		$data['labelStreet'] 							= $this->Vebko_model->translateText('labelStreet');
		$data['labelPhone'] 							= $this->Vebko_model->translateText('labelPhone');
		$data['labelAdsAgree'] 							= $this->Vebko_model->translateText('labelAdsAgree');
		$data['labelAdsButtonText'] 					= $this->Vebko_model->translateText('labelAdsButtonText');		
		$data['labelAdsCancelButtonText'] 				= $this->Vebko_model->translateText('labelAdsCancelButtonText');
		$data['labelBuySell'] 							= $this->Vebko_model->translateText('labelBuySell');
		//label end
		
		// upload images - start
		$CI = & get_instance();		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$name_array = array();
		$file_name_2 = array();
		$file_name_2[0] = 'default.jpg';
		$file_name_2[1] = 'default.jpg';
		$file_name_2[2] = 'default.jpg';
		$file_name_2[3] = 'default.jpg';
		$file_name_2[4] = 'default.jpg';
		$file_name_2[5] = 'default.jpg';
		$file_name_2[6] = 'default.jpg';
		$counter = 1;
		
		//create director for image
		$nextID = sprintf('%08d', $this->Vebko_model->getNextId($dbt));
		$directoryPath = './images/ads/'.$adt.'/ad_'.$nextID.'/';
		if (!is_dir($directoryPath)) {
			$oldmask = umask(0);
			mkdir($directoryPath, 0777, TRUE);
			umask($oldmask);
		}
		
		foreach ($_FILES as $key => $value)
		{
			//echo "<pre>"; print_r($_FILES); exit;
			$CI->upload->initialize($config);			
			if ($CI->upload->do_upload($key))
			{
				$data = $this->upload->data();			
				if($counter == 1){
					$this -> Image_model -> risizeAndCropImage($data, 1200, 630, $directoryPath,'facebook');
					$this -> Image_model -> risizeAndCropImage($data, 300, 168,$directoryPath,'main');
				}				
				$this -> Image_model -> risizeImage($data, 900, 500,$directoryPath,'gallery_image_'.$counter);
				$file_name_2[$counter] = $data['file_name'];
				$counter++;
				//
			}			
		}
		// upload images - end
		
		$email ='';
		if($this->Vebko_model->get_single_value_from_db_query('users', $userId, 'email')){$email = $this->Vebko_model->get_single_value_from_db_query('users', $userId, 'email');}
		//data - start
		$data['adt'] 								= $adt;
		$data['title_meta_tag'] 					= "";
		$data['description_meta_tag'] 				= "";
		$data['image_url_meta_tag'] 				= "";	
		$data['ads_types'] 							= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_other_type','title',0));
		$data['ads_buy_sell'] 						= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_other_buy_sell','title',0));
		$data['address_country'] 					= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country(1));
		$data['address_country_id'] 				= $countryId;
		$data['address_country_region'] 			= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($countryId,1));
		$regionId                                       = $this->Vebko_model->getFirstRegionIdFromCountryId($countryId);
		$data['address_country_municipality'] 		= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality($regionId,1));
		$data['defaultImage'] 						= base_url('images/defaultcar.jpg');		
		$data['valueUserName'] 						= $this->Vebko_model->get_single_value_from_db_query('users', $userId, 'first_name').' '.$this->Vebko_model->get_single_value_from_db_query('users', $userId, 'last_name');
		$data['valueUserEmail'] 					= $email;
		//data - end
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('adsbody', 'adsbody', 'required|min_length[1]');
		if ($this->form_validation->run() == FALSE) {
			$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
			$this->load->view('header',$dataHeaderFooter);
			$this->load->view('ad/'.$adt.'/insert',$data);
			$this->load->view('footer',$dataHeaderFooter);
		}
		else {
			//echo "<pre>"; print_r($_FILES); exit;
			//Setting values for tabel columns
			
			if($this->input->post('year')){$year = $this->input->post('year');}else{ $year = 0;}
			if($this->input->post('km')){$km = $this->input->post('km');}else{ $km = 0;}
			if($this->input->post('price')){$price = $this->input->post('price');}else{ $price = 0;}
			if($this->input->post('contactEmail')){$contactEmail = $this->input->post('contactEmail');}else{ $contactEmail = '0';}			
			if($this->input->post('phone')){$phone = $this->input->post('phone');}else{ $phone = 0;}
			if($this->input->post('street')){$street = $this->input->post('street');}else{ $street = 0;}
			if($this->input->post('roomNumber')){$roomNumber = $this->input->post('roomNumber');}else{ $roomNumber = 0;} 
            if($this->input->post('metro')){$metro = $this->input->post('metro');}else{ $metro = 0;}         
			$data = array(
				'userId' => $userId,
				'ads_buy_sell_id' 					=> $this->input->post('ads_buy_sell_id'),			
				'ads_type_id' 						=> $this->input->post('ads_type_id'),			
				'title' 							=> $this->input->post('title'),
				'body' 								=> $this->input->post('adsbody'),
				'year' 								=> $year,
				'roomNumber' 						=> $roomNumber,
				'metro' 							=> $metro,
				'address_street' 					=> $street,
				'address_country_id' 				=> $this->input->post('address_country'),
				'address_country_region_id' 		=> $this->input->post('address_country_region'),
				'address_country_municipality_id' 	=> $this->input->post('address_country_municipality'),
				'contact_email' 					=> $contactEmail,
				'contact_phone' 					=> $phone,
				'price' 							=> $price,
				'contactName' 						=> $this->input->post('contactName'),
				'ads_gallery_image_name_1' 			=> $file_name_2[1],
				'ads_gallery_image_name_2'			=> $file_name_2[2],
				'ads_gallery_image_name_3' 			=> $file_name_2[3],
				'ads_gallery_image_name_4' 			=> $file_name_2[4],
				'ads_gallery_image_name_5' 			=> $file_name_2[5],
				'ads_gallery_image_name_6' 			=> $file_name_2[6],
				'date' 								=> time(),
				'date_modify' 						=> time()
			);
			//Transfering data to Model
			$this->Vebko_model->form_insert($dbt,$data);
			$dataHeaderFooter['sessionMessageType'] = "alert alert-success";				
			$dataHeaderFooter['sessionMessageText'] = $this->Vebko_model->translateText($this->session->userdata('sessionMessageText'));
				
			//Loading View;
			$id = $this->Vebko_model->getNextId($dbt)-1;
			$this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','AdsSuccessMessage');
			redirect($adt.'/view/'.$id);
		}
		
	}
	public function update(){	
	$id = $this->uri->segment(3);
	$dbt = $this->dbt;		
	$adt = $this->adt;
	$this->Vebko_model->checkRowExist($dbt, $id, $adt);
	if($this->User_model->checkUserLoginReturnUserData()==0){
		$this->session->set_userdata('sessionLoginRedirect','vehicle/update/'.$id);
		redirect('user_authentication/index');
	}
	if($this->Other_model->checkhAdsOwner($id)!=1){
		$this->session->set_userdata('sessionMessageType','alert-danger');
		$this->session->set_userdata('sessionMessageText','AccessDenied');
		redirect('');
	}
	$this->load->helper('form');
    $this->load->library('form_validation');
    // here it is; I am binding rules 

    $this->form_validation->set_rules('title', 'Course Code', 'required');
    $this->form_validation->set_rules('easiness', 'easiness', 'required');
    $this->form_validation->set_rules('helpfulness', 'helpfulness', 'required');
		
		if ($this->form_validation->run() == FALSE) {
		
		
		//label start
		$data['labelAdsUpdate'] 			= $this->Vebko_model->translateText('labelAdsUpdate');
		$data['labelAdsDescription'] 		= $this->Vebko_model->translateText('labelAdsDescription');
		$data['labelAdsBoxTitle'] 			= $this->Vebko_model->translateText('labelAdsBoxTitle');				
		$data['labelAdsTitle'] 				= $this->Vebko_model->translateText('labelAdsTitle');
		$data['labelAdsPrice'] 				= $this->Vebko_model->translateText('labelAdsPrice');
		$data['labelAdsBody'] 				= $this->Vebko_model->translateText('labelAdsBody');
		$data['labelAdsDetails'] 			= $this->Vebko_model->translateText('labelAdsDetails');
		$data['labelAdsCategory'] 			= $this->Vebko_model->translateText('labelAdsCategory');
		$data['labelAdsBuySell'] 			= $this->Vebko_model->translateText('labelAdsBuySell');
		$data['labelAdsYear'] 				= $this->Vebko_model->translateText('labelAdsYear');
		$data['labelRoomNumber'] 				= $this->Vebko_model->translateText('labelRoomNumber');
		$data['labelMetro'] 					= $this->Vebko_model->translateText('labelMetro');
		$data['labelAdsImages'] 			= $this->Vebko_model->translateText('labelAdsImages');
		$data['labelAdsImagesDescription'] 	= $this->Vebko_model->translateText('labelAdsImagesDescription');
		$data['labelAdsImagesDelete'] 		= $this->Vebko_model->translateText('labelAdsImagesDelete');
		$data['labelContact'] 				= $this->Vebko_model->translateText('labelContact');
		$data['labelContactName'] 			= $this->Vebko_model->translateText('labelContactName');
		$data['labelContactEmail'] 			= $this->Vebko_model->translateText('labelContactEmail');
		$data['labelPhone'] 				= $this->Vebko_model->translateText('labelPhone');
		$data['labelCountry'] 				= $this->Vebko_model->translateText('labelCountry');
		$data['labelCountryRegion'] 		= $this->Vebko_model->translateText('labelCountryRegion');
		$data['labelCountryMunicipality'] 	= $this->Vebko_model->translateText('labelCountryMunicipality');
		$data['labelStreet'] 				= $this->Vebko_model->translateText('labelStreet');
		$data['labelAdsCancelButtonText'] 	= $this->Vebko_model->translateText('labelAdsCancelButtonText');
		$data['labelAdsUpdateButtonText'] 	= $this->Vebko_model->translateText('labelAdsUpdateButtonText');
		$data['labelLocation'] 				= $this->Vebko_model->translateText('labelLocation');
				
		//label end
		
		
		//data - start
		$data['adt'] 								= $adt;
		
		
		for($i = 1; $i<7; $i++){
			$imageName = $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'ads_gallery_image_name_'.$i);
			if($imageName == 'default.jpg'){
				$data['galleryImage'][$i] = base_url('images/defaultcar.jpg');
			}
			else{
				$data['galleryImage'][$i] = base_url('images/ads/'.$adt.'/ad_'.sprintf('%08d', $id).'/gallery_image_'.$i.'-'.$imageName);
			}
		}
		$year 			= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'year'); if($year == 0){$year = '';}
		$contactEmail 	= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'contact_email'); if($contactEmail  == 'f0'){$contactEmail  = '';}
		$phone 			= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'contact_phone'); if($phone  == 0){$phone  = '';}
		$street		 	= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'address_street'); if($street  == '0'){$street  = '';}	
		$roomNumber 	= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'roomNumber'); if($roomNumber  == 0){$roomNumber  = '';}
		$metro		 	= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'metro'); if($metro  == '0'){$metro  = '';}	
		
		
		
		$data['title_meta_tag'] 							= "";
		$data['description_meta_tag'] 						= "";
		$data['image_url_meta_tag'] 						= "";
		$data['ads_id']										= $id;
		$data['title'] 										= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'title');
		$data['body'] 										= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'body');	
		$data['year'] 										= $year;			
		$data['roomNumber'] 								= $roomNumber;
		$data['metro'] 										= $metro;
		$data['ads_type']									= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_other_type', 'title', $addAll=NULL));
		$data['ads_type_id_selected'] 						= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'ads_type_id');
		$data['ads_buy_sell'] 								= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_multiple_value_from_db('ads_other_buy_sell', 'title', $addAll=NULL));
		$data['ads_bye_sell_id_selected'] 					= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'ads_buy_sell_id');
		$data['ads_price'] 									= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'price');
		$data['address_street'] 							= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'address_street');
		
		$data['address_country'] 							= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country(1));
		$data['address_country_id_selected'] 				= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'address_country_id');
		$data['address_country_region'] 					= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($data['address_country_id_selected'],1));
		$data['address_country_region_id_selected'] 		= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'address_country_region_id');
		$data['address_country_municipality'] 				= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality($data['address_country_region_id_selected'],1));
		$data['address_country_municipality_id_selected'] 	= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'address_country_municipality_id');	
		$data['valueUserName'] 								= $this->Vebko_model->get_single_value_from_db_query($dbt, $id, 'contactName');
		$data['street'] 									= $street;
		$data['contactEmail'] 								= $contactEmail;
		$data['phone'] 										= $phone;			
		$data['checkUserLoginReturnUserData']				= $this->User_model->checkUserLoginReturnUserData();
		//data - end
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/'.$adt.'/update',$data);
		$this->load->view('footer',$dataHeaderFooter);
		}
		
		 
	}
	public function updateSave(){
		$dbt = $this->dbt;		
		$adt = $this->adt;		
		$id = $this->input->post('ads_id');
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);
		$idWithZero = sprintf('%08d', $id);
		
		if($this->input->post('year')){$year = $this->input->post('year');}else{ $year = 0;}
		if($this->input->post('price')){$price = $this->input->post('price');}else{ $price = 0;}
		if($this->input->post('contactEmail')){$contactEmail = $this->input->post('contactEmail');}else{ $contactEmail = '0';}			
		if($this->input->post('phone')){$phone = $this->input->post('phone');}else{ $phone = 0;}
		if($this->input->post('street')){$street = $this->input->post('street');}else{ $street = 0;}
		
		
		$dataDB = array(
				'ads_type_id' => $this->input->post('ads_type'),				
				'title' => $this->input->post('title'),
				'body' => $this->input->post('adsbody'),
				'year' => $year,
				'address_country_id' => $this->input->post('address_country'),
				'address_country_region_id' => $this->input->post('address_country_region'),
				'address_country_municipality_id' => $this->input->post('address_country_municipality'),
				'contact_email' => $contactEmail,
				'contact_phone' => $phone,
				'price' => $price,
				'ads_buy_sell_id' => $this->input->post('ads_buy_sell'),
				'contactName' => $this->input->post('contactName')
			);	
		
		$CI = & get_instance();
		//if you are inside controller you can use $this
		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$name_array = array();
		$file_name_2 = array();
		$file_name_2[0] = 'default.jpg';
		$file_name_2[1] = 'default.jpg';
		$file_name_2[2] = 'default.jpg';
		$file_name_2[3] = 'default.jpg';
		$file_name_2[4] = 'default.jpg';
		$file_name_2[5] = 'default.jpg';
		$file_name_2[6] = 'default.jpg';
		$counter = 1;
		$directoryPath = './images/ads/'.$adt.'/ad_'.$idWithZero .'/';
		if (!is_dir($directoryPath)) {
			$oldmask = umask(0);
			mkdir($directoryPath, 0777, TRUE);
			umask($oldmask);
		}
		unset($_FILES['files']);
		//echo "<pre>";print_r($_FILES);exit;
		foreach ($_FILES as $key => $value){
			if($this->input->post('imageDeleteTrue-'.$counter)==0){
					//echo 'ads_gallery_image_name_'.$counter; exit;
					$dataDB['ads_gallery_image_name_'.$counter] = 'default.jpg';
				}
			if($value['error']!=4){
				
				$CI->upload->initialize($config);			
				if ($CI->upload->do_upload($key)){
					$data = $this->upload->data();			
					if($counter == 1){
						$this -> Image_model -> risizeAndCropImage($data, 1200, 630, $directoryPath,'facebook');
						$this -> Image_model -> risizeAndCropImage($data, 300, 168,$directoryPath,'main');
					}				
					$this -> Image_model -> risizeImage($data, 900, 500,$directoryPath,'gallery_image_'.$counter);
					//$file_name_2[$counter] = $data['file_name'];
					
					$dataDB['ads_gallery_image_name_'.$counter] = $data['file_name'];					
				}
				
			}
			
			$counter++;
		}
				
			$this->Vebko_model->updateData($dbt,$id,$dataDB);
			redirect($adt.'/view/'.$id, 'refresh');
	}
	public function activate(){
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$id = $this->uri->segment(3);
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);		
		if($this->Other_model->checkhAdsOwner($id)==1){
			$marking = 0;
			$dataDB = array(
					'marking' => $marking,
				);			
			$this->Vebko_model->updateData($dbt,$id,$dataDB);
			$this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','AdsMarkingActivate');
		}
		else{
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','AccessDenied');
		}		
		redirect($adt.'/view/'.$id, 'refresh');
	}
	public function deactivate(){
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$id = $this->uri->segment(3);
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);
		if($this->Other_model->checkhAdsOwner($id)==1){
			$marking = 1;
			$dataDB = array(
					'marking' => $marking,
				);			
			$this->Vebko_model->updateData($dbt,$id,$dataDB);
			$this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','AdsMarkingDeactivate');
		}
		else{
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','AccessDenied');
		}		
		redirect($adt.'/view/'.$id, 'refresh');
	}
	public function renew(){
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$id = $this->uri->segment(3);
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);
		if($this->User_model->checkUserLoginReturnUserData()==0){
			$this->session->set_userdata('sessionLoginRedirect',''.$dbt.'/update/'.$id);
			redirect('user_authentication/index');
		}
		if($this->Other_model->checkhAdsOwner($id)!=1){
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','AccessDenied');
			redirect('');
		}
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		$allowDay = 1;
		if($this->Vebko_model->checkUserPermissionToRenew($dbt,$id,$allowDay)==1){
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','adsRenewNotAllow');
			redirect($adt.'/view/'.$id);
		}
		
		$dataDB = array(
				'date_modify' => time()
			);
		
		$this->Vebko_model->updateData($dbt,$id,$dataDB);
		$this->session->set_userdata('sessionMessageType','alert-success');
		$this->session->set_userdata('sessionMessageText','adsRenew');
		redirect($adt.'/view/'.$id, 'refresh');
	}
	
	
	
	// search - start
	function get_ads_vehicles(){
		$ads_estate_type_id = $this->input->post('ads_estate_type_id');
		$ads_vehicles_year_start_id = $this->input->post('ads_vehicles_year_start_id');
		$ads_vehicles_year_end_id = $this->input->post('ads_vehicles_year_end_id');
		$ads_vehicles_price_start_id = $this->input->post('ads_vehicles_price_start_id');
		$ads_vehicles_price_end_id = $this->input->post('ads_vehicles_price_end_id');
		$address_country_id = $this->input->post('address_country_id');
		$address_country_region_id = $this->input->post('address_country_region_id');
		$address_country_municipality_id = $this->input->post('address_country_municipality_id');
		$ads_vehicles_bye_sell_id = $this->input->post('ads_vehicles_bye_sell_id');
		$queryRowStart = $this->input->post('queryRowStart');
		
		echo(json_encode($this->Other_model->get_ads_vehicles($ads_estate_type_id,
			$ads_vehicles_year_start_id,$ads_vehicles_year_end_id,
			$ads_vehicles_price_start_id,$ads_vehicles_price_end_id,
			$address_country_id,$address_country_region_id,$address_country_municipality_id,
			$ads_vehicles_bye_sell_id,$queryRowStart
		)));
	}
	// search - end
}