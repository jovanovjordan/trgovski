<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
class Vehicle extends CI_Controller {
	public $dbt = "ads_vehicles"; //table for this type of ads
	public $adt = 'vehicle'; //part of url
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Ad_model');
		$this->load->model('User_model');
		$this->load->model('Image_model');
		$this->load->model('Vebko_model');
		$this->load->model('Vehicle_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$timezone = "Europe/Belgrade";
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}
	public function _remap($method, $params = array())    {
        if (method_exists(__CLASS__, $method)) {
            $this->$method($params);
        } else {
            $this->index();
        }
    }
	public function index() {// search
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$countryId = $this->Vebko_model->getCountryID();
		//label
		$data['vehicle'] = $this->Vebko_model->translateText('vehicle');
		$data['brand'] = $this->Vebko_model->translateText('brand');
		$data['model'] = $this->Vebko_model->translateText('model');
		$data['oil'] = $this->Vebko_model->translateText('oil');
		$data['yearfrom'] = $this->Vebko_model->translateText('yearfrom');
		$data['yearto'] = $this->Vebko_model->translateText('yearto');
		$data['kmfrom'] = $this->Vebko_model->translateText('kmfrom');
		$data['kmto'] = $this->Vebko_model->translateText('kmto');
		$data['pricefrom'] = $this->Vebko_model->translateText('pricefrom');
		$data['priceto'] = $this->Vebko_model->translateText('priceto');
		$data['country'] = $this->Vebko_model->translateText('country');
		$data['region'] = $this->Vebko_model->translateText('region');
		$data['municipality'] = $this->Vebko_model->translateText('municipality');
		$data['search'] = $this->Vebko_model->translateText('search');
		$data['buySell'] = $this->Vebko_model->translateText('buySell');
		$data['labelSearch'] 		= $this->Vebko_model->translateText('labelSearch');
		$data['labelSearchDescritpion'] 		= $this->Vebko_model->translateText('labelSearchDescritpion');
		$data['labelSearchVehicle'] 			= $this->Vebko_model->translateText('labelSearchVehicle');
		$data['labelSearchVehicleDescritpion'] 	= $this->Vebko_model->translateText('labelSearchVehicleDescritpion');
		
		$data['ads_vehicles_type'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_type());
		$data['ads_vehicles_brand'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_brand());
		$data['ads_vehicles_model'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_model());
		
		$data['ads_vehicles_fuel'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_fuel());
		$data['ads_vehicles_year'] = $this->Vehicle_model->get_ads_vehicles_year();
		$data['ads_vehicles_km'] = $this->Vehicle_model->get_ads_vehicles_km();
		$data['ads_vehicles_price'] = $this->Vehicle_model->get_ads_vehicles_price();
		
		$data['address_country'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country());
		$data['address_country_id'] 			= $countryId;
		$data['address_country_region'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($countryId));
		$data['address_country_municipality'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality(1));
		
		$data['get_ads_vehicles'] = $this->Vehicle_model->get_ads_vehicles();
		$data['ads_vehicles_bye_sell'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_bye_sell());
		
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$dataHeaderFooter['title_meta_tag'] = $this->Vebko_model->translateText('title_meta_tag_'.$adt);
		$dataHeaderFooter['description_meta_tag'] = $this->Vebko_model->translateText('description_meta_tag_'.$adt);
		$dataHeaderFooter['image_url_meta_tag'] = base_url('images/metatag/search_'.$adt.'.jpg');
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/car/search',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function view(){
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$id = $this->uri->segment(3);
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);
		
		$data['checkhAdsOwner'] = $this->Vehicle_model->checkhAdsOwner($id);		
		//label - start
		$data['labelVehicleNoData'] = $this->Vebko_model->translateText('labelVehicleNoData');
		$data['labelVehicleSpecification'] = $this->Vebko_model->translateText('labelVehicleSpecification');
		$data['labelVehicleType'] = $this->Vebko_model->translateText('labelVehicleType');
		$data['labelVehicleBrand'] = $this->Vebko_model->translateText('labelVehicleBrand');
		$data['labelVehicleModel'] = $this->Vebko_model->translateText('labelVehicleModel');
		$data['labelVehicleYear'] = $this->Vebko_model->translateText('labelVehicleYear');
		$data['labelVehiclePastKM'] = $this->Vebko_model->translateText('labelVehiclePastKM');
		$data['labelVehicleTypeOil'] = $this->Vebko_model->translateText('labelVehicleTypeOil');
		$data['labelContactPerson'] = $this->Vebko_model->translateText('labelContactPerson');
		$data['labelContactName'] = $this->Vebko_model->translateText('labelContactName');
		$data['labelPhone'] = $this->Vebko_model->translateText('labelPhone');
		$data['labelEmail'] = $this->Vebko_model->translateText('labelEmail');
		$data['labelViewStreet'] = $this->Vebko_model->translateText('labelViewStreet');
		$data['labelViewMunicipality'] = $this->Vebko_model->translateText('labelViewMunicipality');
		$data['labelViewRegion'] = $this->Vebko_model->translateText('labelViewRegion');
		$data['labelViewCountry'] = $this->Vebko_model->translateText('labelViewCountry');
		$data['labelViewShare'] = $this->Vebko_model->translateText('labelViewShare');
		$data['labelViewSettings'] = $this->Vebko_model->translateText('labelViewSettings');
		$data['labelViewSettingsActivate'] = $this->Vebko_model->translateText('labelViewSettingsActivate');
		$data['labelViewSettingsDeaktivate'] = $this->Vebko_model->translateText('labelViewSettingsDeaktivate');
		$data['labelViewSettingsChange'] = $this->Vebko_model->translateText('labelViewSettingsChange');
		$data['labelViewSettingsRenew'] = $this->Vebko_model->translateText('labelViewSettingsRenew');
		$data['labelVehiclePublishedDate'] = $this->Vebko_model->translateText('labelVehiclePublishedDate');
		$data['labelMarketingTitle'] 			= $this->Vebko_model->translateText('labelMarketingTitle');
		//label - end
		
		$data['ads_id'] = $id;
		$data['ads_marking'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles', $id,'marking');
		if($data['ads_marking']==1){
			$data['ads_marking_decativate']= $this->Vebko_model->translateText('adsDeactive');
		}
		else{
			$data['ads_marking_decativate'] ='';
		}
		$data['ads_vehicle_title'] = $this->Vehicle_model->get_ads_vehicle($id,'title');
		$data['ads_vehicle_title'] = ucfirst(strtolower($data['ads_vehicle_title']));
		$data['ads_vehicle_date'] = date('h:i (d/m/Y)', $this->Vehicle_model->get_ads_vehicle($id,'date'));
		$data['ads_vehicle_date_modify'] = date('h:i (d/m/Y)', $this->Vehicle_model->get_ads_vehicle($id,'date_modify'));
		$ads_vehicles_type_id = $this->Vehicle_model->get_ads_vehicle($id,'ads_vehicles_type_id');
		$data['ads_vehicle_type'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('ads_vehicles_type', $ads_vehicles_type_id,'title'));
		$ads_vehicles_brand_id = $this->Vehicle_model->get_ads_vehicle($id,'ads_vehicles_brand_id');
		$data['ads_vehicle_brand'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_brand', $ads_vehicles_brand_id,'title');
		$ads_vehicles_model_id = $this->Vehicle_model->get_ads_vehicle($id,'ads_vehicles_model_id');
		$data['ads_vehicle_model'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_model', $ads_vehicles_model_id,'title');
		$data['body'] = $this->Vehicle_model->get_ads_vehicle($id,'body');
		$data['year'] = $this->Vehicle_model->get_ads_vehicle($id,'year');
		$data['km'] = $this->Vehicle_model->get_ads_vehicle($id,'km');
		$ads_vehicles_fuel_id = $this->Vehicle_model->get_ads_vehicle($id,'vehicles_fuel');
		$data['ads_vehicles_fuel'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('ads_vehicles_fuel', $ads_vehicles_fuel_id,'title'));
		$data['address_street'] = $this->Vehicle_model->get_ads_vehicle($id,'address_street');
		$ads_address_country_municipality_id = $this->Vehicle_model->get_ads_vehicle($id,'address_country_municipality_id');
		$data['ads_address_country_municipality'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('address_country_municipality', $ads_address_country_municipality_id,'title'));
		$ads_address_country_region_id = $this->Vehicle_model->get_ads_vehicle($id,'address_country_region_id');
		$data['ads_address_country_region'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('address_country_region', $ads_address_country_region_id,'title'));
		$ads_address_country_id = $this->Vehicle_model->get_ads_vehicle($id,'address_country_id');
		$data['ads_address_country'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('address_country', $ads_address_country_id,'title'));
		$ads_buy_sell_id = $this->Vehicle_model->get_ads_vehicle($id,'ads_vehicles_bye_sell_id');
		$data['ads_buy_sell'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('ads_vehicles_bye_sell', $ads_buy_sell_id,'title'));
		$data['date'] = $this->Vehicle_model->get_ads_vehicle($id,'date');
		$data['price'] = $this->Vehicle_model->get_ads_vehicle($id,'price');
		$data['priceMkd'] 						= $this->Vebko_model->convertCurrency($data['price'], 'EUR', 'MKD').' '.$this->Vebko_model->translateText($this->input->cookie('currency',true));		
		$data['contactName'] = $this->Vehicle_model->get_ads_vehicle($id,'contactName');
		$data['contact_phone'] = $this->Vehicle_model->get_ads_vehicle($id,'contact_phone');
		$data['contact_email'] = $this->Vehicle_model->get_ads_vehicle($id,'contact_email');
		$data['contact_phone_text'] = substr($data['contact_phone'], 0, 3) . '/' . substr($data['contact_phone'],3,3). '-' . substr($data['contact_phone'],6,3);
		$ads_vehicles_bye_sell_id = $this->Vebko_model->get_single_value_from_db ('ads_vehicles', $id,'ads_vehicles_bye_sell_id');
		$data['ads_vehicles_bye_sell_title_opposite'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('ads_vehicles_bye_sell', $ads_vehicles_bye_sell_id,'title_opposite'));
		$data['ads_vehicles_bye_sell_title_adjective'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db ('ads_vehicles_bye_sell', $ads_vehicles_bye_sell_id,'title_adjective'));
		
		$ID = sprintf('%08d', $id);
		$directoryPath = 'images/ads/car/ad_'.$ID.'/';		
		
		//add image to gallery
		$gallery_images = array();
		for($i=1; $i<7; $i++){//set in databese how much colume have for image in table ads_vehicle
			if($this->Vehicle_model->get_ads_vehicle($id,'ads_gallery_image_name_'.$i) != 'default.jpg'){
				$data['gallery_image_'.$i] =  base_url($directoryPath.'gallery_image_'.$i.'-'.$this->Vehicle_model->get_ads_vehicle($id,'ads_gallery_image_name_'.$i));
				$gallery_images[$i-1] = $data['gallery_image_'.$i];
			}
		}
		$data['gallery_images'] = $gallery_images;
		//
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		//add sugestion to array std object		
		$data['suggestions'] = $this->Vehicle_model->getSearchSugestion();
		$data['marketing'] = $this->Vebko_model->getMarketing();
		$marketingCount = count($data['marketing']);
		$marketingRandomId =rand(0, $marketingCount-1);
		$data['marketingCount'] = $marketingCount;
		$data['marketingSingle'] = $data['marketing'][$marketingRandomId];
		
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		
		
		
	
		$dataHeaderFooter['title_meta_tag'] = $data['ads_vehicle_title'];
		$dataHeaderFooter['description_meta_tag'] = $data['ads_vehicle_model'].', '.
													$data['price'].' - '.
													$data['ads_address_country_municipality'].', '.
													$data['ads_address_country'];
													
		$dataHeaderFooter['image_url_meta_tag'] = $gallery_images['0'];	
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/car/view', $data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	public function insert(){
		$dbt = $this->dbt;
		$adt = $this->adt;
		$countryId = $this->Vebko_model->getCountryID();
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		
		// check user login and role - start
		if($this->User_model->checkUserLoginReturnUserData()==0){
			$this->session->set_userdata('sessionLoginRedirect',$adt.'/insert');
			redirect('user_authentication/index');
		}
		if($this->User_model->userRole($userId)==1){
			$this->session->set_userdata('sessionLoginRedirect',$adt.'');
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','accountNotAllowNewAds');
			redirect($adt);
		}
		//check user login and role - end
		
		
		$CI = & get_instance();
		//if you are inside controller you can use $this
		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$name_array = array();
		$file_name_2 = array();
		$file_name_2[0] = 'default.jpg';
		$file_name_2[1] = 'default.jpg';
		$file_name_2[2] = 'default.jpg';
		$file_name_2[3] = 'default.jpg';
		$file_name_2[4] = 'default.jpg';
		$file_name_2[5] = 'default.jpg';
		$file_name_2[6] = 'default.jpg';
		$counter = 1;
		
		//create director for image
		$nextID = sprintf('%08d', $this->Vebko_model->getNextId('ads_vehicles'));
		$directoryPath = './images/ads/car/ad_'.$nextID.'/';
		if (!is_dir($directoryPath)) {
			$oldmask = umask(0);
			mkdir($directoryPath, 0777, TRUE);
			umask($oldmask);
		}
		
		foreach ($_FILES as $key => $value)
		{
			//echo "<pre>"; print_r($_FILES); exit;
			$CI->upload->initialize($config);			
			if ($CI->upload->do_upload($key))
			{
				$data = $this->upload->data();			
				if($counter == 1){
					$this -> Image_model -> risizeAndCropImage($data, 1200, 630, $directoryPath,'facebook');
					$this -> Image_model -> risizeAndCropImage($data, 300, 168,$directoryPath,'main');
				}				
				$this -> Image_model -> risizeImage($data, 900, 500,$directoryPath,'gallery_image_'.$counter);
				$file_name_2[$counter] = $data['file_name'];
				$counter++;
				//
			}			
		}

		$data['title_meta_tag'] = "Огласник на половни возила";
		$data['description_meta_tag'] = "Голема избор на половни возила од различни брендови и модели.";
		$data['image_url_meta_tag'] = base_url('images/metatag/searchcar.jpg');
		
		$data['ads_vehicles_type'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_type(1));
		$data['ads_vehicles_brand'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_brand(1,1));
		$data['ads_vehicles_model'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_model(1,1));		
		$data['ads_vehicles_fuel'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_fuel(1));
		$data['ads_vehicles_year'] = $this->Vehicle_model->get_ads_vehicles_year();
		$data['ads_vehicles_km'] = $this->Vehicle_model->get_ads_vehicles_km();
		$data['ads_vehicles_price'] = $this->Vehicle_model->get_ads_vehicles_price();
		$data['address_country'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country(1));
		$data['address_country_id'] 			= $countryId;
		$data['address_country_region'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($countryId,1));
		$regionId                                       = $this->Vebko_model->getFirstRegionIdFromCountryId($countryId);
		$data['address_country_municipality'] 		= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality($regionId,1));	
		$data['ads_vehicles_bye_sell'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_bye_sell(1));
		$data['defaultImage'] = base_url('images/defaultcar.jpg');		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('adsbody', 'adsbody', 'required|min_length[1]');
		
		//label start
		$data['labelInsertAds'] = $this->Vebko_model->translateText('labelInsertAds');
		$data['labelInsertAdsDescription'] = $this->Vebko_model->translateText('labelInsertAdsDescription');
		$data['labelInsertAds2'] = $this->Vebko_model->translateText('labelInsertAds2');
		$data['labelInsertAdsTitle'] = $this->Vebko_model->translateText('labelInsertAdsTitle');
		$data['labelInsertAdsPrice'] = $this->Vebko_model->translateText('labelInsertAdsPrice');
		$data['labelInsertAdsBody'] = $this->Vebko_model->translateText('labelInsertAdsBody');
		$data['labelInsertInsertImages'] = $this->Vebko_model->translateText('labelInsertInsertImages');
		$data['labelInsertInsertImagesDescription'] = $this->Vebko_model->translateText('labelInsertInsertImagesDescription');
		$data['labelInsertInsertImagesDelete'] = $this->Vebko_model->translateText('labelInsertInsertImagesDelete');
		$data['labelInsertInsertVehicleDetails'] = $this->Vebko_model->translateText('labelInsertInsertVehicleDetails');
		$data['labelInsertInsertVehicleCategory'] = $this->Vebko_model->translateText('labelInsertInsertVehicleCategory');
		$data['labelInsertInsertVehicleBrand'] = $this->Vebko_model->translateText('labelInsertInsertVehicleBrand');
		$data['labelInsertInsertVehicleModel'] = $this->Vebko_model->translateText('labelInsertInsertVehicleModel');
		$data['labelInsertInsertVehicleBuySell'] = $this->Vebko_model->translateText('labelInsertInsertVehicleBuySell');
		$data['labelInsertInsertVehicleFuel'] = $this->Vebko_model->translateText('labelInsertInsertVehicleFuel');
		$data['labelInsertInsertVehicleYear'] = $this->Vebko_model->translateText('labelInsertInsertVehicleYear');
		$data['labelInsertInsertVehicleКm'] = $this->Vebko_model->translateText('labelInsertInsertVehicleКm');
		$data['labelContact'] = $this->Vebko_model->translateText('labelContact');
		$data['labelContactName'] = $this->Vebko_model->translateText('labelContactName');
		$data['labelContactEmail'] = $this->Vebko_model->translateText('labelContactEmail');
		$data['labelCountry'] = $this->Vebko_model->translateText('labelCountry');
		$data['labelCountryRegion'] = $this->Vebko_model->translateText('labelCountryRegion');
		$data['labelCountryMunicipality'] = $this->Vebko_model->translateText('labelCountryMunicipality');
		$data['labelStreet'] = $this->Vebko_model->translateText('labelStreet');
		$data['labelPhone'] = $this->Vebko_model->translateText('labelPhone');
		$data['labelAdsAgree'] = $this->Vebko_model->translateText('labelAdsAgree');
		$data['labelAdsButtonText'] = $this->Vebko_model->translateText('labelAdsButtonText');		
		$data['labelAdsCancelButtonText'] = $this->Vebko_model->translateText('labelAdsCancelButtonText');
		//label end
		
		
		//sugestion value for contact
		$data['valueUserName'] = $this->Vebko_model->get_single_value_from_db_query('users', $userId, 'first_name').' '.$this->Vebko_model->get_single_value_from_db_query('users', $userId, 'last_name');

		if ($this->form_validation->run() == FALSE) {
			$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
			$this->load->view('header',$dataHeaderFooter);
			$this->load->view('ad/car/insert',$data);
			$this->load->view('footer',$dataHeaderFooter);
		}
		else {
			//echo "<pre>"; print_r($_FILES); exit;
			//Setting values for tabel columns
			
			if($this->input->post('year')){$year = $this->input->post('year');}else{ $year = 0;}
			if($this->input->post('km')){$km = $this->input->post('km');}else{ $km = 0;}
			if($this->input->post('price')){$price = $this->input->post('price');}else{ $price = 0;}
			if($this->input->post('contactEmail')){$contactEmail = $this->input->post('contactEmail');}else{ $contactEmail = '0';}			
			if($this->input->post('phone')){$phone = $this->input->post('phone');}else{ $phone = 0;}
			if($this->input->post('street')){$street = $this->input->post('street');}else{ $street = 0;}
			$table = 'ads_vehicles';
			$data = array(
				'userId' => $userId,
				'ads_vehicles_type_id' => $this->input->post('ads_vehicles_type'),
				'ads_vehicles_brand_id' => $this->input->post('ads_vehicles_brand'),
				'ads_vehicles_model_id' => $this->input->post('ads_vehicles_model'),				
				'title' => $this->input->post('title'),
				'body' => $this->input->post('adsbody'),
				'year' => $year,
				'km' => $km,
				'vehicles_fuel' => $this->input->post('ads_vehicles_fuel'),
				'address_street' => $street,
				'address_country_id' => $this->input->post('address_country'),
				'address_country_region_id' => $this->input->post('address_country_region'),
				'address_country_municipality_id' => $this->input->post('address_country_municipality'),
				'contact_email' => $contactEmail,
				'contact_phone' => $phone,
				'price' => $price,
				'ads_vehicles_bye_sell_id' => $this->input->post('ads_vehicles_bye_sell'),
				'contactName' => $this->input->post('contactName'),
				'ads_gallery_image_name_1' => $file_name_2[1],
				'ads_gallery_image_name_2' => $file_name_2[2],
				'ads_gallery_image_name_3' => $file_name_2[3],
				'ads_gallery_image_name_4' => $file_name_2[4],
				'ads_gallery_image_name_5' => $file_name_2[5],
				'ads_gallery_image_name_6' => $file_name_2[6],
				'date' => time(),
				'date_modify' => time()
			);
			//Transfering data to Model
			$this->Vebko_model->form_insert($table, $data);
			$dataHeaderFooter['sessionMessageType'] = "alert alert-success";				
			$dataHeaderFooter['sessionMessageText'] = $this->Vebko_model->translateText($this->session->userdata('sessionMessageText'));
				
			//Loading View;
			$id = $this->Vebko_model->getNextId('ads_vehicles')-1;
			$this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','AdsSuccessMessage');
			redirect('vehicle/view/'.$id);
		}
		
	}
	public function update(){	
	$dbt = $this->dbt;		
	$adt = $this->adt;
	$id = $this->uri->segment(3);
	$this->Vebko_model->checkRowExist($dbt, $id, $adt);
	if($this->User_model->checkUserLoginReturnUserData()==0){
		$this->session->set_userdata('sessionLoginRedirect','vehicle/update/'.$id);
		redirect('user_authentication/index');
	}
	if($this->Vehicle_model->checkhAdsOwner($id)!=1){
		$this->session->set_userdata('sessionMessageType','alert-danger');
		$this->session->set_userdata('sessionMessageText','AccessDenied');
		redirect('');
	}
	$this->load->helper('form');
    $this->load->library('form_validation');
    // here it is; I am binding rules 

    $this->form_validation->set_rules('title', 'Course Code', 'required');
    $this->form_validation->set_rules('easiness', 'easiness', 'required');
    $this->form_validation->set_rules('helpfulness', 'helpfulness', 'required');
		
		if ($this->form_validation->run() == FALSE) {
		
		
		//label start
		$data['labelUpdateAds'] = $this->Vebko_model->translateText('labelUpdateAds');
		$data['labelInsertAdsDescription'] = $this->Vebko_model->translateText('labelInsertAdsDescription');
		$data['labelInsertAds2'] = $this->Vebko_model->translateText('labelInsertAds2');
		$data['labelUpdateAds2'] = $this->Vebko_model->translateText('labelUpdateAds2');
		$data['labelInsertAdsTitle'] = $this->Vebko_model->translateText('labelInsertAdsTitle');
		$data['labelInsertAdsPrice'] = $this->Vebko_model->translateText('labelInsertAdsPrice');
		$data['labelInsertAdsBody'] = $this->Vebko_model->translateText('labelInsertAdsBody');
		$data['labelInsertInsertImages'] = $this->Vebko_model->translateText('labelInsertInsertImages');
		$data['labelInsertInsertImagesDescription'] = $this->Vebko_model->translateText('labelInsertInsertImagesDescription');
		$data['labelInsertInsertImagesDelete'] = $this->Vebko_model->translateText('labelInsertInsertImagesDelete');
		$data['labelInsertInsertVehicleDetails'] = $this->Vebko_model->translateText('labelInsertInsertVehicleDetails');
		$data['labelInsertInsertVehicleCategory'] = $this->Vebko_model->translateText('labelInsertInsertVehicleCategory');
		$data['labelInsertInsertVehicleBrand'] = $this->Vebko_model->translateText('labelInsertInsertVehicleBrand');
		$data['labelInsertInsertVehicleModel'] = $this->Vebko_model->translateText('labelInsertInsertVehicleModel');
		$data['labelInsertInsertVehicleBuySell'] = $this->Vebko_model->translateText('labelInsertInsertVehicleBuySell');
		$data['labelInsertInsertVehicleFuel'] = $this->Vebko_model->translateText('labelInsertInsertVehicleFuel');
		$data['labelInsertInsertVehicleYear'] = $this->Vebko_model->translateText('labelInsertInsertVehicleYear');
		$data['labelInsertInsertVehicleКm'] = $this->Vebko_model->translateText('labelInsertInsertVehicleКm');
		$data['labelContact'] = $this->Vebko_model->translateText('labelContact');
		$data['labelContactName'] = $this->Vebko_model->translateText('labelContactName');
		$data['labelContactEmail'] = $this->Vebko_model->translateText('labelContactEmail');
		$data['labelCountry'] = $this->Vebko_model->translateText('labelCountry');
		$data['labelCountryRegion'] = $this->Vebko_model->translateText('labelCountryRegion');
		$data['labelCountryMunicipality'] = $this->Vebko_model->translateText('labelCountryMunicipality');
		$data['labelStreet'] = $this->Vebko_model->translateText('labelStreet');
		$data['labelPhone'] = $this->Vebko_model->translateText('labelPhone');
		$data['labelAdsAgree'] = $this->Vebko_model->translateText('labelAdsAgree');
		$data['labelAdsButtonText'] = $this->Vebko_model->translateText('labelAdsButtonText');		
		$data['labelAdsUpdateButtonText'] = $this->Vebko_model->translateText('labelAdsUpdateButtonText');		
		$data['labelAdsCancelButtonText'] = $this->Vebko_model->translateText('labelAdsCancelButtonText');		
		//label end
		
		
		$db_table = 'ads_vehicles';
		$data['title_meta_tag'] = "Огласник на половни возила";
		$data['description_meta_tag'] = "Голема избор на половни возила од различни брендови и модели.";
		$data['image_url_meta_tag'] = base_url('images/metatag/searchcar.jpg');
		$data['ads_id'] = $id;
		$data['title'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'title');
		$data['body'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'body');
		
		for($i = 1; $i<7; $i++){
			$imageName = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_gallery_image_name_'.$i);
			if($imageName == 'default.jpg'){
				$data['galleryImage'][$i] = base_url('images/defaultcar.jpg');
			}
			else{
				$data['galleryImage'][$i] = base_url('images/ads/car/ad_'.sprintf('%08d', $id).'/gallery_image_'.$i.'-'.$imageName);
			}
		}
		
		$year = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'year'); if($year == 0){$year = '';}
		$km = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'km'); if($km  == 0){$km  = '';}
		$contactEmail = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'contact_email'); if($contactEmail  == 0){$contactEmail  = '';}
		$phone = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'contact_phone'); if($phone  == 0){$phone  = '';}
		$street = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_street'); if($street  == '0'){$street  = '';}
		
		$data['ads_vehicles_type'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_type(1));
		$data['ads_vehicles_type_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_type_id');
		$data['ads_vehicles_brand'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_brand($data['ads_vehicles_type_id_selected'],1));
		$data['ads_vehicles_brand_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_brand_id');
		$data['ads_vehicles_model'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_model($data['ads_vehicles_brand_id_selected'],1));
		$data['ads_vehicles_model_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_model_id');
		$data['year'] = $year;
		$data['km'] = $km;
		$data['ads_vehicles_fuel'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_fuel(1));
		$data['ads_vehicles_fuel_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'vehicles_fuel');
		$data['ads_vehicles_bye_sell'] = $this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_bye_sell(1));
		$data['ads_vehicles_bye_sell_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_bye_sell_id');
		$data['ads_vehicles_price'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'price');
		$data['address_street'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_street');
		$data['address_country'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country(1));
		$data['address_country_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_country_id');
		$data['address_country_region'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($data['address_country_id_selected'],1));
		$data['address_country_region_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_country_region_id');
		$data['address_country_municipality'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality($data['address_country_region_id_selected'],1));
		$data['address_country_municipality_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_country_municipality_id');	
		$data['valueUserName'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'contactName');
		$data['street'] = $street;
		$data['contactEmail'] = $contactEmail;
		$data['phone'] = $phone;
		
		
			
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/car/update',$data);
		$this->load->view('footer',$dataHeaderFooter);
		}
		
		 
	}
	public function updateSave(){
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$id = $this->input->post('ads_id');
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);
		$idWithZero = sprintf('%08d', $id);
		
		if($this->input->post('year')){$year = $this->input->post('year');}else{ $year = 0;}
		if($this->input->post('km')){$km = $this->input->post('km');}else{ $km = 0;}
		if($this->input->post('price')){$price = $this->input->post('price');}else{ $price = 0;}
		if($this->input->post('contactEmail')){$contactEmail = $this->input->post('contactEmail');}else{ $contactEmail = '0';}			
		if($this->input->post('phone')){$phone = $this->input->post('phone');}else{ $phone = 0;}
		if($this->input->post('street')){$street = $this->input->post('street');}else{ $street = 0;}
		
		$dataDB = array(
				'ads_vehicles_type_id' => $this->input->post('ads_vehicles_type'),
				'ads_vehicles_brand_id' => $this->input->post('ads_vehicles_brand'),
				'ads_vehicles_model_id' => $this->input->post('ads_vehicles_model'),				
				'title' => $this->input->post('title'),
				'body' => $this->input->post('adsbody'),
				'year' => $year,
				'km' => $km,
				'vehicles_fuel' => $this->input->post('ads_vehicles_fuel'),
				'address_street' => $street,
				'address_country_id' => $this->input->post('address_country'),
				'address_country_region_id' => $this->input->post('address_country_region'),
				'address_country_municipality_id' => $this->input->post('address_country_municipality'),
				'contact_email' => $contactEmail,
				'contact_phone' => $phone,
				'price' => $price,
				'ads_vehicles_bye_sell_id' => $this->input->post('ads_vehicles_bye_sell'),
				'contactName' => $this->input->post('contactName')
			);
			
			
		
		
		$CI = & get_instance();
		//if you are inside controller you can use $this
		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$name_array = array();
		$file_name_2 = array();
		$file_name_2[0] = 'default.jpg';
		$file_name_2[1] = 'default.jpg';
		$file_name_2[2] = 'default.jpg';
		$file_name_2[3] = 'default.jpg';
		$file_name_2[4] = 'default.jpg';
		$file_name_2[5] = 'default.jpg';
		$file_name_2[6] = 'default.jpg';
		$counter = 1;
		$directoryPath = './images/ads/car/ad_'.$idWithZero .'/';
		if (!is_dir($directoryPath)) {
			$oldmask = umask(0);
			mkdir($directoryPath, 0777, TRUE);
			umask($oldmask);
		}
		unset($_FILES['files']);
		//echo "<pre>";print_r($_FILES);exit;
		foreach ($_FILES as $key => $value){
			if($this->input->post('imageDeleteTrue-'.$counter)==0){
					//echo 'ads_gallery_image_name_'.$counter; exit;
					$dataDB['ads_gallery_image_name_'.$counter] = 'default.jpg';
				}
			if($value['error']!=4){
				
				$CI->upload->initialize($config);			
				if ($CI->upload->do_upload($key)){
					$data = $this->upload->data();			
					if($counter == 1){
						$this -> Image_model -> risizeAndCropImage($data, 1200, 630, $directoryPath,'facebook');
						$this -> Image_model -> risizeAndCropImage($data, 300, 168,$directoryPath,'main');
					}				
					$this -> Image_model -> risizeImage($data, 900, 500,$directoryPath,'gallery_image_'.$counter);
					//$file_name_2[$counter] = $data['file_name'];
					
					$dataDB['ads_gallery_image_name_'.$counter] = $data['file_name'];					
				}
				
			}
			
			$counter++;
		}
				
			$this->Vebko_model->updateData($dbt,$id,$dataDB);
			redirect('vehicle/view/'.$id, 'refresh');
	}
	public function activate(){
		$id = $this->uri->segment(3);
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);		
		if($this->Vehicle_model->checkhAdsOwner($id)==1){
			$marking = 0;
			$dataDB = array(
					'marking' => $marking,
				);			
			$this->Vebko_model->updateData($dbt,$id,$dataDB);
			$this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','AdsMarkingActivate');
		}
		else{
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','AccessDenied');
		}		
		redirect('vehicle/view/'.$id, 'refresh');
	}
	public function deactivate(){
		$id = $this->uri->segment(3);
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);		
		if($this->Vehicle_model->checkhAdsOwner($id)==1){
			$marking = 1;
			$dataDB = array(
					'marking' => $marking,
				);			
			$this->Vebko_model->updateData($dbt, $id,$dataDB);
			$this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','AdsMarkingDeactivate');
		}
		else{
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','AccessDenied');
		}		
		redirect('vehicle/view/'.$id, 'refresh');
	}
	public function renew(){	
		$id = $this->uri->segment(3);
		$dbt = $this->dbt;		
		$adt = $this->adt;
		$this->Vebko_model->checkRowExist($dbt, $id, $adt);	
		if($this->User_model->checkUserLoginReturnUserData()==0){
			$this->session->set_userdata('sessionLoginRedirect','vehicle/update/'.$id);
			redirect('user_authentication/index');
		}
		if($this->Vehicle_model->checkhAdsOwner($id)!=1){
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','AccessDenied');
			redirect('');
		}
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		$allowDay = 1;
		if($this->Vebko_model->checkUserPermissionToRenew('ads_vehicles',$id,$allowDay)==1){
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','adsRenewNotAllow');
			redirect('vehicle/view/'.$id);
		}
		
		$dataDB = array(
				'date_modify' => time()
			);
		
		$this->Vebko_model->updateData($dbt,$id,$dataDB);
		$this->session->set_userdata('sessionMessageType','alert-success');
		$this->session->set_userdata('sessionMessageText','adsRenew');
		redirect('vehicle/view/'.$id, 'refresh');
	}
	
	function searchcar_brand(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_brand($id))));
	}
	function searchcar_model(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Vehicle_model->get_ads_vehicles_model($id))));
	}
	function get_ads_vehicles(){
		$ads_vehicles_type_id = $this->input->post('ads_vehicles_type_id');
		$ads_vehicles_brand_id = $this->input->post('ads_vehicles_brand_id');
		$ads_vehicles_model_id = $this->input->post('ads_vehicles_model_id');
		$ads_vehicles_fuel_id = $this->input->post('ads_vehicles_fuel_id');
		$ads_vehicles_year_start_id = $this->input->post('ads_vehicles_year_start_id');
		$ads_vehicles_year_end_id = $this->input->post('ads_vehicles_year_end_id');
		$ads_vehicles_km_start_id = $this->input->post('ads_vehicles_km_start_id');
		$ads_vehicles_km_end_id = $this->input->post('ads_vehicles_km_end_id');
		$ads_vehicles_price_start_id = $this->input->post('ads_vehicles_price_start_id');
		$ads_vehicles_price_end_id = $this->input->post('ads_vehicles_price_end_id');
		$address_country_id = $this->input->post('address_country_id');
		$address_country_region_id = $this->input->post('address_country_region_id');
		$address_country_municipality_id = $this->input->post('address_country_municipality_id');
		$ads_vehicles_bye_sell_id = $this->input->post('ads_vehicles_bye_sell_id');
		$queryRowStart = $this->input->post('queryRowStart');
		
		echo(json_encode($this->Vehicle_model->get_ads_vehicles($ads_vehicles_type_id,$ads_vehicles_brand_id,$ads_vehicles_model_id,$ads_vehicles_fuel_id,
			$ads_vehicles_year_start_id,$ads_vehicles_year_end_id,$ads_vehicles_km_start_id,$ads_vehicles_km_end_id,
			$ads_vehicles_price_start_id,$ads_vehicles_price_end_id,$address_country_id,$address_country_region_id,$address_country_municipality_id,
			$ads_vehicles_bye_sell_id,$queryRowStart
		)));
	}
	
}