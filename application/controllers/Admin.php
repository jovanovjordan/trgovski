<?php

class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Ad_model');
		$this->load->model('User_model');
		$this->load->model('Image_model');
		$this->load->model('Vebko_model');
		$this->load->model('Vehicle_model');		
		$this->load->model('Admin_model');
		$this->load->model('Estate_model');
		$this->load->model('Electronics_model');
		$this->load->model('Clothing_model');
		$this->load->model('Service_model');
		$this->load->model('Other_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$timezone = "Europe/Belgrade";
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}
	
        public function index(){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
			if($userId==1){
				$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
				$dataHeaderFooter['title_meta_tag']       	= $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
				$dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");		
				$dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
				$this->load->view('header',$dataHeaderFooter);
				$this->load->view('admin/admin',$data);               
				$this->load->view('footer',$data);
			}
        }
        public function addMail(){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
			if($userId==1){
            $data['labelCountry'] = $this->Vebko_model->translateText('labelCountry');
            $data['address_country'] = $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country());
            $data['address_country_id'] = $this->Vebko_model->getCountryID();

            $dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
            $dataHeaderFooter['title_meta_tag']       	= $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
            $dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");		
            $dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
            $this->load->view('header',$dataHeaderFooter);
            $this->load->view('admin/addMail',$data);               
            $this->load->view('footer',$data);
			}
        }
        public function saveMail(){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
			if($userId==1){
				$address_country_id = $this->input->post('address_country');
				$mails = $this->input->post('mail');
				$this->Admin_model->putMail($address_country_id, $mails);
				redirect('/admin/addMail', 'refresh');
			}
        }
        public function sendMail(){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
			if($userId==1){
				$data['labelCountry'] 						= $this->Vebko_model->translateText('labelCountry');
				$data['address_country'] 					= $this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country());
				$data['address_country_id'] 				= $this->Vebko_model->getCountryID();
				$dataHeaderFooter							= $this->Vebko_model->dataHeaderFooter();
				$dataHeaderFooter['title_meta_tag']       	= $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
				$dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");		
				$dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
				
				$this->load->view('header',$dataHeaderFooter);
				$this->load->view('admin/sendMail',$data);               
				$this->load->view('footer',$data);
			}
        }
        public function sendMailExe(){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
			if($userId==1){
				$address_country_id = $this->input->post('address_country');
				$subject = $this->input->post('subject');
				$body = $this->input->post('body');
				
				$mailArray = $this->Admin_model->mailArray($address_country_id);
				$this->Admin_model->sendMails($mailArray,$subject,$body);
			}
        }
        
        function getEmails(){
		$id = $this->input->post('id');
		echo(json_encode(implode("<br>",$this->Admin_model->mailArray($id))));
	}
        
        public function test(){
            $this->Admin_model->savetime(date('Y-m-d H:i:s',time()));
        }
}
 ?>
