<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Ad_model');
                $this->load->model('Store_model');
		$this->load->model('User_model');
		$this->load->model('Image_model');
		$this->load->model('Vebko_model');
		$this->load->model('Vehicle_model');
		$this->load->model('Estate_model');
		$this->load->model('Electronics_model');
		$this->load->model('Clothing_model');
		$this->load->model('Service_model');
		$this->load->model('Other_model');
                $this->load->model('Image_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$timezone = "Europe/Belgrade";
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}
	
	public function index(){
		$ln = $this->uri->segment(3);
                
                $data["frontMessageMain"]                   = $this->Vebko_model->translateText("frontMessageMain");                
		$data["labelCategoryChoiceCategory"]        = $this->Vebko_model->translateText("labelCategoryChoiceCategory");
		$data["real_estate"]                        = $this->Vebko_model->translateText("real_estate");
		$data["vehicle"]                            = $this->Vebko_model->translateText("vehicle");
		$data["electronics"]                        = $this->Vebko_model->translateText("electronics");
		$data["clothing"]                           = $this->Vebko_model->translateText("clothing");
		$data["services"]                           = $this->Vebko_model->translateText("services");
		$data["jobs"]                               = $this->Vebko_model->translateText("jobs");
		$data["allcategories"]                      = $this->Vebko_model->translateText("allcategories");
		$data["other"]                              = $this->Vebko_model->translateText("other");
			
		$data['checkUserLoginReturnUserData']       = $this->User_model->checkUserLoginReturnUserData();
		$data['title_meta_tag']                     = "Trgovski.com";
		$data['description_meta_tag']               = "Бесплатен интернет огласник";
		$data['image_url_meta_tag']                 = base_url('images/facebookfront.jpg');
					
		$dataHeaderFooter                           = $this->Vebko_model->dataHeaderFooter();
		$dataHeaderFooter['title_meta_tag']         = $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
		$dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");		
		$dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
		
                $this->load->view('header',$dataHeaderFooter);
		$this->load->view('home',$data);
		$this->load->view('footer',$data);		
	}
	public function storeCheck($storeTitle)        {
                $userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
                $dbt = 'store';
                $colum = 'userId';
                $value = $userId;   
                $storeUserExist = $this->Store_model->existInDb($dbt,$colum,$value);
                if ($storeUserExist > 0){
                        $this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','StoreErrorUserStoreExist');
                        return FALSE;
                }
                
                $dbt = 'store';
                $colum = 'title';
                $value = $storeTitle;   
                $storeTitleExist = $this->Store_model->existInDb($dbt,$colum,$value);              
                if ($storeTitleExist > 0){
                        $this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','StoreErrorStoreNameTaken');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
        public function storeCheckName($storeTitle)        {
                $userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
                $dbt = 'store';
                $colum = 'title';
                $value = $storeTitle;   
                $storeTitleExist = $this->Store_model->existInDb($dbt,$colum,$value,$userId);              
                if ($storeTitleExist > 0){
                        $this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','StoreErrorStoreNameTaken');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
        public function storeCheckURL($subdomain)        {
                $userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
                $dbt = 'store';
                $colum = 'subdomain';
                $value = $subdomain;   
                $storeTitleExist = $this->Store_model->existInDb($dbt,$colum,$value,$userId);              
                if ($storeTitleExist > 0){
                        $this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','StoreErrorStoreUrlTaken');
                        return FALSE;
                }
                if(strpos(trim($subdomain), ' ') !== false){
                    $this->session->set_userdata('sessionMessageType','alert-danger');
                    $this->session->set_userdata('sessionMessageText','StoreErrorStoreUrlMoreWords');
                    return FALSE;
                }
                if($this->Store_model->notAllowWord($subdomain)==0){
                    $this->session->set_userdata('sessionMessageType','alert-danger');
                    $this->session->set_userdata('sessionMessageText','StoreErrorStoreUrlNotAllowWord');
                    return FALSE;
                }
                return TRUE;
        }
        
        public function insert(){
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		// check user login and role - start
		if($this->User_model->checkUserLoginReturnUserData()==0){
			$this->session->set_userdata('sessionLoginRedirect',$adt.'/insert');
			redirect('user_authentication/index');
		}
		if($this->User_model->userRole($userId)==1){
			$this->session->set_userdata('sessionLoginRedirect',$adt.'');
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','accountNotAllowNewAds');
			redirect($adt);
		}
                
                $data['labelInsertAdsDescription']        	= $this->Vebko_model->translateText('labelInsertAdsDescription');

                $data['labelInsertStore']              		= $this->Vebko_model->translateText('labelInsertStore');
                $data['labelInsertStoreBoxTitle']      		= $this->Vebko_model->translateText('labelInsertStoreBoxTitle');
                $data['labelInsertStoreTitle']         		= $this->Vebko_model->translateText('labelInsertStoreTitle');
                $data['labelInsertStoreTitleDescription']       = $this->Vebko_model->translateText('labelInsertStoreTitleDescription');
                $data['labelInsertStoreSubTitle']         	= $this->Vebko_model->translateText('labelInsertStoreSubTitle');
                $data['labelInsertStoreSubTitleDescription']    = $this->Vebko_model->translateText('labelInsertStoreSubTitleDescription');
                $data['labelInsertStoreURL']         		= $this->Vebko_model->translateText('labelInsertStoreURL');
                $data['labelInsertStoreURLDescription']		= $this->Vebko_model->translateText('labelInsertStoreURLDescription');
                $data['labelInsertStoreOpenStore']     		= $this->Vebko_model->translateText('labelInsertStoreOpenStore');
                $data['labelAdsCancelButtonText']      		= $this->Vebko_model->translateText('labelAdsCancelButtonText');
                $data['labelInsertStoreAgree']         		= $this->Vebko_model->translateText('labelInsertStoreAgree');
                
                $data['StoreErrorUserStoreExist']               = $this->Vebko_model->translateText('StoreErrorUserStoreExist');
                $data['StoreErrorStoreNameTaken']               = $this->Vebko_model->translateText('StoreErrorStoreNameTaken');
                $data['StoreSuccessCreated']                    = $this->Vebko_model->translateText('StoreSuccessCreated');
                
                
                
                
                
                $this->load->library('form_validation');
		//$this->form_validation->set_rules('title', 'title', 'required|min_length[4]|callback_storeCheck');
                $this->form_validation->set_rules('subdomain', 'subdomain', 'required|min_length[4]|callback_storeCheckURL');
		if ($this->form_validation->run() == FALSE) {
                    
                    
			$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
			$this->load->view('header',$dataHeaderFooter);
			$this->load->view('store/insert',$data);
			$this->load->view('footer',$dataHeaderFooter);
		}
                else{
                
                // upload images - start
		$CI = & get_instance();		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$counter = 1;
                    $nextID = sprintf('%08d', $this->Vebko_model->getNextId('store'));
                    $directoryPath = './images/store/store_'.$nextID.'/';
                    if (!is_dir($directoryPath)) {
                            $oldmask = umask(0);
                            mkdir($directoryPath, 0777, TRUE);
                            umask($oldmask);
                    }
                //echo $directoryPath;exit;
                
                foreach ($_FILES as $key => $value){
			//echo "<pre>"; print_r($_FILES); exit;
			$CI->upload->initialize($config);			
			if ($CI->upload->do_upload($key)){
				$data = $this->upload->data();
                               
				$this -> Image_model -> risizeImage($data, 120, 120,$directoryPath,'logo');
				$file_name = $data['file_name'];			
			}			
		}
                
                $address_country_id = $this->Vebko_model->getCountryID();
                    $table = 'store';
			$data = array(
                            'userId' => $userId,
                            'title' => $this->input->post('title'),
                            'subtitle' => $this->input->post('subtitle'),
                            'subdomain' => $this->input->post('subdomain'),
                            'logoUrl' =>  substr($directoryPath,2).'logo-'.$file_name,
							'address_country_id' => $address_country_id
			);
			//Transfering data to Model
			$this->Vebko_model->form_insert($table, $data);
                        
                        $this->session->set_userdata('sessionMessageType','alert-success');
			$this->session->set_userdata('sessionMessageText','StoreSuccessCreated');
                        redirect('account', 'refresh');
                }
        }
        
        public function update(){
            $userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
            
            $data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
            
            $data['labelInsertAdsDescription']              = $this->Vebko_model->translateText('labelInsertAdsDescription');                
            $data['labelInsertStore']                       = $this->Vebko_model->translateText('labelInsertStore');
            $data['labelInsertStoreBoxTitle']               = $this->Vebko_model->translateText('labelInsertStoreBoxTitle');
            $data['labelInsertStoreTitle']                  = $this->Vebko_model->translateText('labelInsertStoreTitle');
            $data['labelInsertStoreTitleDescription']       = $this->Vebko_model->translateText('labelInsertStoreTitleDescription');
            $data['labelInsertStoreSubTitle']               = $this->Vebko_model->translateText('labelInsertStoreSubTitle');
            $data['labelInsertStoreSubTitleDescription']    = $this->Vebko_model->translateText('labelInsertStoreSubTitleDescription');
            $data['labelInsertStoreBody']                   = $this->Vebko_model->translateText('labelInsertStoreBody');
            $data['labelInsertStoreOpenStore']              = $this->Vebko_model->translateText('labelInsertStoreOpenStore');
            $data['labelInsertStoreChangeStore']            = $this->Vebko_model->translateText('labelInsertStoreChangeStore');
            $data['labelAdsCancelButtonText']               = $this->Vebko_model->translateText('labelAdsCancelButtonText');
            $data['labelInsertStoreAgree']                  = $this->Vebko_model->translateText('labelInsertStoreAgree');
            $data['StoreErrorUserStoreExist']               = $this->Vebko_model->translateText('StoreErrorUserStoreExist');
            $data['StoreErrorStoreNameTaken']               = $this->Vebko_model->translateText('StoreErrorStoreNameTaken');
            $data['StoreSuccessCreated']                    = $this->Vebko_model->translateText('StoreSuccessCreated');
            $data['labelInsertStoreChange']                 = $this->Vebko_model->translateText('labelInsertStoreChange');
            
            $data['storeDate'] = $this->Vebko_model->getStoreDate();
            
            //print_r($data['storeDate']);exit;
            
            
            
                $this->load->library('form_validation');
                
		$this->form_validation->set_rules('title', 'title', 'required|min_length[4]|callback_storeCheckName');
                $this->form_validation->set_rules('subdomain', 'subdomain', 'required|min_length[4]|callback_storeCheckURL');
		if ($this->form_validation->run() == FALSE) {
			$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
                        $this->load->view('header',$dataHeaderFooter);
                        $this->load->view('store/update',$data);
                        $this->load->view('footer',$dataHeaderFooter);
		}
                else{
                    
                    $dbt = 'store';
                    
                    $dataDB = array(
                        'title' => $this->input->post('title'),
                        'subtitle' => $this->input->post('subtitle'),
                        'subdomain' => $this->input->post('subdomain')
                    ); 
                    
                     // upload images - start
		$CI = & get_instance();		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$counter = 1;
                    $nextID = sprintf('%08d', $this->Vebko_model->getNextId('store'));
                    $directoryPath = './images/store/store_'.$nextID.'/';
                    if (!is_dir($directoryPath)) {
                            $oldmask = umask(0);
                            mkdir($directoryPath, 0777, TRUE);
                            umask($oldmask);
                    }
                //echo $directoryPath;exit;
                //echo "<pre>";print_r($_FILES);exit;
                foreach ($_FILES as $key => $value){
                        if($value['error']!=4){
                            $CI->upload->initialize($config);			
                            if ($CI->upload->do_upload($key)){
                                    $data = $this->upload->data();

                                    $this -> Image_model -> risizeImage($data, 120, 120,$directoryPath,'logo');
                                    $file_name = $data['file_name'];                                    
                                    $dataDB['logoUrl'] = substr($directoryPath,2).'logo-'.$file_name;
                            }
                        }
                    }                      
                    $this->Store_model->storeUpdateData($userId,$dataDB);
                    redirect('account', 'refresh');
                }
                
                
                
            
        }
        
        public function all(){
            $data['StoresTitle']                    = $this->Vebko_model->translateText("StoresTitle");
            $data['StoresSubTitle']                 = $this->Vebko_model->translateText("StoresSubTitle");
            $data['checkUserLoginReturnUserData']   =$this->User_model->checkUserLoginReturnUserData();
            $stores = $this->Vebko_model->getStores();
            foreach ($stores as $store) {
                $data['storeLogo'][$store->id] = $this->Vebko_model->storeLogo($store->id);
            }
            $data['stores'] = $stores;
            $data['url'] = '.'.substr(base_url(), 8,-1);
            $dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
            $this->load->view('header',$dataHeaderFooter);
            $this->load->view('store/all',$data);
            $this->load->view('footer',$dataHeaderFooter);
        }
}