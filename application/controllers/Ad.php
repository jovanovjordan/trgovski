<?php

class Ad extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Ad_model');
		$this->load->model('User_model');
		$this->load->model('Image_model');
		$this->load->model('Vebko_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$timezone = "Europe/Belgrade";
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}	
	public function test(){
		$this->load->library('googlemaps');
		$config = array();
		$config['center']= "Kochani Macedonia";
		$config['zoom'] = 14;
		
		$this->googlemaps->initialize($config);
		
		$marker = array();
		$marker['position']= "Roza Petrova Kochani Macedonia";
		$this ->googlemaps->add_marker($marker);
		
		$marker = array();
		$marker['position']= "Roza Petrova Kochani Macedonia";
		$this ->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();
		$this->load->view('test',$data);
	}
	/*
	public function index() {
			$this->languageSelect();
			$data["real_estate"] = $this->lang->line("real_estate");
			$data["vehicle"] = $this->lang->line("vehicle");
			$data["electronics"] = $this->lang->line("electronics");
			$data["clothing"] = $this->lang->line("clothing");
			$data["services"] = $this->lang->line("real_estate");
			$data["jobs"] = $this->lang->line("jobs");
			$data["allcategories"] = $this->lang->line("allcategories");
			
			
            $data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$data['title_meta_tag'] = "Trgovski.com";
			$data['description_meta_tag'] = "Бесплатен интернет огласник";
			$data['image_url_meta_tag'] = base_url('images/facebookfront.jpg');
			
			
			$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
			$this->languageSelect();
			
			$this->load->view('header',$dataHeaderFooter);
            $this->load->view('home',$data);
            $this->load->view('footer',$data);		
	}
	
	public function languageSelect() {
		if($this->input->cookie('language',true)){
			$language = $this->input->cookie('language',true);
		}
		else{
			$language = 'macedonian';
		}
		$this->lang->load("message",$language);
	}
	
	
	
	//fb user id if is not login return 0
	public function checkUserLoginReturnUserData() {				
		return $this->User_model->checkUserLoginReturnUserData();
		
	}
	/*
	public function category() {
		$this->languageSelect();
		$data["real_estate"] = $this->lang->line("real_estate");
		$data["vehicle"] = $this->lang->line("vehicle");
		$data["electronics"] = $this->lang->line("electronics");
		$data["clothing"] = $this->lang->line("clothing");
		$data["services"] = $this->lang->line("real_estate");
		$data["jobs"] = $this->lang->line("jobs");
		$data["allcategories"] = $this->lang->line("allcategories");
		
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->languageSelect();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/category',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	
	public function add() {				
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->languageSelect();
				$data["real_estate"] = $this->lang->line("real_estate");
		$data["vehicle"] = $this->lang->line("vehicle");
		$data["electronics"] = $this->lang->line("electronics");
		$data["clothing"] = $this->lang->line("clothing");
		$data["services"] = $this->lang->line("real_estate");
		$data["jobs"] = $this->lang->line("jobs");
		$data["allcategories"] = $this->lang->line("allcategories");
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/insertcategory',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	
	
	//car start
	public function insertcar(){
		
		if($this->checkUserLoginReturnUserData()==0){
			$this->session->set_userdata('sessionLoginRedirect','ad/insertcar');
			redirect('user_authentication/index');
		}
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		$CI = & get_instance();
		//if you are inside controller you can use $this
		
		$CI->load->library('upload');
		$config=array();
		$config['upload_path']          = './images/ads/tmp/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 100000000;
		$config['max_width']            = 10240;
		$config['max_height']           = 7608;
		$config['overwrite']           	= false;
		$config['remove_spaces']        = true;
		
		$name_array = array();
		$file_name_2 = array();
		$file_name_2[0] = 'default.jpg';
		$file_name_2[1] = 'default.jpg';
		$file_name_2[2] = 'default.jpg';
		$file_name_2[3] = 'default.jpg';
		$file_name_2[4] = 'default.jpg';
		$file_name_2[5] = 'default.jpg';
		$file_name_2[6] = 'default.jpg';
		$counter = 1;
		
		//create director for image
		$nextID = sprintf('%08d', $this->Ad_model->getNextId('ads_vehicles'));
		$directoryPath = './images/ads/car/ad_'.$nextID.'/';
		if (!is_dir($directoryPath)) {
			$oldmask = umask(0);
			mkdir($directoryPath, 0777, TRUE);
			umask($oldmask);
		}
		
		foreach ($_FILES as $key => $value)
		{
			$CI->upload->initialize($config);
			
			
			if ($CI->upload->do_upload($key))
			{
				$data = $this->upload->data();
				
				
				if($counter == 1){
					$this -> Image_model -> risizeAndCropImage($data, 1200, 630, $directoryPath,'facebook');
					$this -> Image_model -> risizeAndCropImage($data, 300, 168,$directoryPath,'main');
				}				
				$this -> Image_model -> risizeAndCropImage($data, 1200, 630,$directoryPath,'gallery_image_'.$counter);
				

				$file_name_2[$counter] = $data['file_name'];
				$counter++;
				//
			}
			
		}

	
		//
		
		$data['title_meta_tag'] = "Огласник на половни возила";
		$data['description_meta_tag'] = "Голема избор на половни возила од различни брендови и модели.";
		$data['image_url_meta_tag'] = base_url('images/metatag/searchcar.jpg');
		
		$data['ads_vehicles_type'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_type(1));
		$data['ads_vehicles_brand'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_brand(1));
		$data['ads_vehicles_model'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_model(1));		
		$data['ads_vehicles_fuel'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_fuel(1));
		$data['ads_vehicles_year'] = $this->Ad_model->get_ads_vehicles_year();
		$data['ads_vehicles_km'] = $this->Ad_model->get_ads_vehicles_km();
		$data['ads_vehicles_price'] = $this->Ad_model->get_ads_vehicles_price();
		$data['address_country'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_address_country(1));
		$data['address_country_region'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_address_country_region(1));
		$data['address_country_municipality'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_address_country_municipality(1));		
		$data['get_ads_vehicles'] = $this->Ad_model->get_ads_vehicles();
		$data['ads_vehicles_bye_sell'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_bye_sell());
		$data['defaultImage'] = base_url('images/defaultcar.jpg');		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('adsbody', 'adsbody', 'required|min_length[1]');
		
		//label start
		$data['labelInsertAds'] = $this->Vebko_model->translateText('labelInsertAds');
		$data['labelInsertAdsDescription'] = $this->Vebko_model->translateText('labelInsertAdsDescription');
		$data['labelInsertAds2'] = $this->Vebko_model->translateText('labelInsertAds2');
		$data['labelInsertAdsTitle'] = $this->Vebko_model->translateText('labelInsertAdsTitle');
		$data['labelInsertAdsPrice'] = $this->Vebko_model->translateText('labelInsertAdsPrice');
		$data['labelInsertAdsBody'] = $this->Vebko_model->translateText('labelInsertAdsBody');
		$data['labelInsertInsertImages'] = $this->Vebko_model->translateText('labelInsertInsertImages');
		$data['labelInsertInsertImagesDescription'] = $this->Vebko_model->translateText('labelInsertInsertImagesDescription');
		$data['labelInsertInsertImagesDelete'] = $this->Vebko_model->translateText('labelInsertInsertImagesDelete');
		$data['labelInsertInsertVehicleDetails'] = $this->Vebko_model->translateText('labelInsertInsertVehicleDetails');
		$data['labelInsertInsertVehicleCategory'] = $this->Vebko_model->translateText('labelInsertInsertVehicleCategory');
		$data['labelInsertInsertVehicleBrand'] = $this->Vebko_model->translateText('labelInsertInsertVehicleBrand');
		$data['labelInsertInsertVehicleModel'] = $this->Vebko_model->translateText('labelInsertInsertVehicleModel');
		$data['labelInsertInsertVehicleBuySell'] = $this->Vebko_model->translateText('labelInsertInsertVehicleBuySell');
		$data['labelInsertInsertVehicleFuel'] = $this->Vebko_model->translateText('labelInsertInsertVehicleFuel');
		$data['labelInsertInsertVehicleYear'] = $this->Vebko_model->translateText('labelInsertInsertVehicleYear');
		$data['labelInsertInsertVehicleКm'] = $this->Vebko_model->translateText('labelInsertInsertVehicleКm');
		$data['labelContact'] = $this->Vebko_model->translateText('labelContact');
		$data['labelContactName'] = $this->Vebko_model->translateText('labelContactName');
		$data['labelContactEmail'] = $this->Vebko_model->translateText('labelContactEmail');
		$data['labelCountry'] = $this->Vebko_model->translateText('labelCountry');
		$data['labelCountryRegion'] = $this->Vebko_model->translateText('labelCountryRegion');
		$data['labelCountryMunicipality'] = $this->Vebko_model->translateText('labelCountryMunicipality');
		$data['labelStreet'] = $this->Vebko_model->translateText('labelStreet');
		$data['labelPhone'] = $this->Vebko_model->translateText('labelPhone');
		$data['labelAdsAgree'] = $this->Vebko_model->translateText('labelAdsAgree');
		$data['labelAdsButtonText'] = $this->Vebko_model->translateText('labelAdsButtonText');
		
		//label end
		//sugestion value for contact
		$data['valueUserName'] = $this->Vebko_model->get_single_value_from_db_query('users', $userId, 'first_name').' '.$this->Vebko_model->get_single_value_from_db_query('users', $userId, 'last_name');

		if ($this->form_validation->run() == FALSE) {
			$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
			$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
			$this->load->view('header',$dataHeaderFooter);
			$this->load->view('ad/car/insertcar',$data);
			$this->load->view('footer',$dataHeaderFooter);
		}
		else {
			//echo "<pre>"; print_r($_FILES); exit;
			//Setting values for tabel columns
			 
			$data = array(
				'userId' => $userId,
				'ads_vehicles_type_id' => $this->input->post('ads_vehicles_type'),
				'ads_vehicles_brand_id' => $this->input->post('ads_vehicles_brand'),
				'ads_vehicles_model_id' => $this->input->post('ads_vehicles_model'),				
				'title' => $this->input->post('title'),
				'body' => $this->input->post('adsbody'),
				'year' => $this->input->post('year'),
				'km' => $this->input->post('km'),
				'vehicles_fuel' => $this->input->post('ads_vehicles_fuel'),
				'address_street' => $this->input->post('street'),
				'address_country_id' => $this->input->post('address_country'),
				'address_country_region_id' => $this->input->post('address_country_region'),
				'address_country_municipality_id' => $this->input->post('address_country_municipality'),
				'contact_email' => $this->input->post('contactEmail'),
				'contact_phone' => $this->input->post('phone'),
				'price' => $this->input->post('price'),
				'ads_vehicles_bye_sell_id' => $this->input->post('ads_vehicles_bye_sell'),
				'contactName' => $this->input->post('contactName'),
				'ads_gallery_image_name_1' => $file_name_2[1],
				'ads_gallery_image_name_2' => $file_name_2[2],
				'ads_gallery_image_name_3' => $file_name_2[3],
				'ads_gallery_image_name_4' => $file_name_2[4],
				'ads_gallery_image_name_5' => $file_name_2[5],
				'ads_gallery_image_name_6' => $file_name_2[6],
				'date' => time()
			);
			//Transfering data to Model
			$this->Ad_model->form_insert($data);
			$data['message'] = 'Data Inserted Successfully';
			//Loading View;
			$id = $this->Ad_model->getNextId('ads_vehicles')-1;
			redirect('ad/car/'.$id);
		}
		
	}
	public function updatecar(){
			
	$this->load->helper('form');
    $this->load->library('form_validation');


    // here it is; I am binding rules 

    $this->form_validation->set_rules('title', 'Course Code', 'required');
    $this->form_validation->set_rules('easiness', 'easiness', 'required');
    $this->form_validation->set_rules('helpfulness', 'helpfulness', 'required');
		
		if ($this->form_validation->run() == FALSE) {
		$id = $this->uri->segment(3);

		$db_table = 'ads_vehicles';
		$data['title_meta_tag'] = "Огласник на половни возила";
		$data['description_meta_tag'] = "Голема избор на половни возила од различни брендови и модели.";
		$data['image_url_meta_tag'] = base_url('images/metatag/searchcar.jpg');
		$data['ads_id'] = $id;
		$data['title'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'title');
		$data['body'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'body');
		
		for($i = 1; $i<7; $i++){
			$imageName = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_gallery_image_name_'.$i);
			if($imageName == 'default.jpg'){
				$data['galleryImage'][$i] = base_url('images/defaultcar.jpg');
			}
			else{
				$data['galleryImage'][$i] = base_url('images/ads/car/ad_'.sprintf('%08d', $id).'/gallery_image_'.$i.'-'.$imageName);
			}
		}
		
		$data['ads_vehicles_type'] = $this->Ad_model->get_ads_vehicles_type();
		$data['ads_vehicles_type_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_type_id');
		$data['ads_vehicles_brand'] = $this->Ad_model->get_ads_vehicles_brand($data['ads_vehicles_type_id_selected']);
		$data['ads_vehicles_brand_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_brand_id');
		$data['ads_vehicles_model'] = $this->Ad_model->get_ads_vehicles_model($data['ads_vehicles_brand_id_selected']);
		$data['ads_vehicles_model_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_model_id');
		$data['year'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'year');
		$data['km'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'km');
		$data['ads_vehicles_fuel'] = $this->Ad_model->get_ads_vehicles_fuel();
		$data['ads_vehicles_fuel_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'vehicles_fuel');
		$data['ads_vehicles_bye_sell'] = $this->Ad_model->get_ads_vehicles_bye_sell();
		$data['ads_vehicles_bye_sell_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'ads_vehicles_bye_sell_id');
		$data['ads_vehicles_price'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'price');
		$data['address_street'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_street');
		$data['contact_phone'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'contact_phone');
		$data['address_country_region'] = $this->Ad_model->get_address_country_region();
		$data['address_country_region_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_country_region_id');
		$data['address_country_municipality'] = $this->Ad_model->get_address_country_municipality($data['address_country_region_id_selected']);
		$data['address_country_municipality_id_selected'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_country_municipality_id');	
		$data['valueUserName'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'contactName');
		$data['street'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'address_street');
		$data['phone'] = $this->Vebko_model->get_single_value_from_db_query($db_table, $id, 'contact_phone');
		

		
		
			
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/car/updatecar',$data);
		$this->load->view('footer',$dataHeaderFooter);
		}
		
		 
	}
	public function updatecarsave(){
		$data = array(
				'ads_vehicles_type_id' => $this->input->post('ads_vehicles_type'),
				'ads_vehicles_brand_id' => $this->input->post('ads_vehicles_brand'),
				'ads_vehicles_model_id' => $this->input->post('ads_vehicles_model'),				
				'title' =>$this->input->post('title'),
				'body' => $this->input->post('adsbody'),
				'year' => $this->input->post('year'),
				'km' => $this->input->post('km'),
				'vehicles_fuel' => $this->input->post('ads_vehicles_fuel'),
				'address_street' => $this->input->post('street'),
				'address_country_region_id' => $this->input->post('address_country_region'),
				'address_country_municipality_id' => $this->input->post('address_country_municipality'),
				'contact_phone' => $this->input->post('phone'),
				'price' => $this->input->post('price'),
				'ads_vehicles_bye_sell_id' => $this->input->post('ads_vehicles_bye_sell')
			);
			//echo "<pre>"; print_r($data);exit;
			$id = $this->input->post('ads_id');
			$this->Vebko_model->updateData($id,$data);
			redirect('ad/car/'.$id, 'refresh');
	}
	/*
	public function searchcar() {	
		
		//label
		$data['vehicle'] = $this->Vebko_model->translateText('vehicle');
		$data['brand'] = $this->Vebko_model->translateText('brand');
		$data['model'] = $this->Vebko_model->translateText('model');
		$data['oil'] = $this->Vebko_model->translateText('oil');
		$data['yearfrom'] = $this->Vebko_model->translateText('yearfrom');
		$data['yearto'] = $this->Vebko_model->translateText('yearto');
		$data['kmfrom'] = $this->Vebko_model->translateText('kmfrom');
		$data['kmto'] = $this->Vebko_model->translateText('kmto');
		$data['pricefrom'] = $this->Vebko_model->translateText('pricefrom');
		$data['priceto'] = $this->Vebko_model->translateText('priceto');
		$data['country'] = $this->Vebko_model->translateText('country');
		$data['region'] = $this->Vebko_model->translateText('region');
		$data['municipality'] = $this->Vebko_model->translateText('municipality');
		$data['search'] = $this->Vebko_model->translateText('search');
		$data['buySell'] = $this->Vebko_model->translateText('buySell');
		
		$data['ads_vehicles_type'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_type());
		$data['ads_vehicles_brand'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_brand());
		$data['ads_vehicles_model'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_model());
		
		$data['ads_vehicles_fuel'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_fuel());
		$data['ads_vehicles_year'] = $this->Ad_model->get_ads_vehicles_year();
		$data['ads_vehicles_km'] = $this->Ad_model->get_ads_vehicles_km();
		$data['ads_vehicles_price'] = $this->Ad_model->get_ads_vehicles_price();
		
		$data['address_country'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_address_country());
		$data['address_country_region'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_address_country_region(1));
		$data['address_country_municipality'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_address_country_municipality(1));
		
		$data['get_ads_vehicles'] = $this->Ad_model->get_ads_vehicles();
		$data['ads_vehicles_bye_sell'] = $this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_bye_sell());
		
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$dataHeaderFooter['title_meta_tag'] = "Огласник на половни возила";
		$dataHeaderFooter['description_meta_tag'] = "Голема избор на половни возила од различни брендови и модели.";
		$dataHeaderFooter['image_url_meta_tag'] = base_url('images/metatag/searchcar.jpg');
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/car/searchcar',$data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	/*
	function searchcar_brand(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_brand($id))));
	}
	function searchcar_model(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Ad_model->get_ads_vehicles_model($id))));
	}
	/*
	function get_ads_vehicles(){
		$ads_vehicles_type_id = $this->input->post('ads_vehicles_type_id');
		$ads_vehicles_brand_id = $this->input->post('ads_vehicles_brand_id');
		$ads_vehicles_model_id = $this->input->post('ads_vehicles_model_id');
		$ads_vehicles_fuel_id = $this->input->post('ads_vehicles_fuel_id');
		$ads_vehicles_year_start_id = $this->input->post('ads_vehicles_year_start_id');
		$ads_vehicles_year_end_id = $this->input->post('ads_vehicles_year_end_id');
		$ads_vehicles_km_start_id = $this->input->post('ads_vehicles_km_start_id');
		$ads_vehicles_km_end_id = $this->input->post('ads_vehicles_km_end_id');
		$ads_vehicles_price_start_id = $this->input->post('ads_vehicles_price_start_id');
		$ads_vehicles_price_end_id = $this->input->post('ads_vehicles_price_end_id');
		$address_country_id = $this->input->post('address_country_id');
		$address_country_region_id = $this->input->post('address_country_region_id');
		$address_country_municipality_id = $this->input->post('address_country_municipality_id');
		$ads_vehicles_bye_sell_id = $this->input->post('ads_vehicles_bye_sell_id');
		$queryRowStart = $this->input->post('queryRowStart');
		
		echo(json_encode($this->Ad_model->get_ads_vehicles($ads_vehicles_type_id,$ads_vehicles_brand_id,$ads_vehicles_model_id,$ads_vehicles_fuel_id,
			$ads_vehicles_year_start_id,$ads_vehicles_year_end_id,$ads_vehicles_km_start_id,$ads_vehicles_km_end_id,
			$ads_vehicles_price_start_id,$ads_vehicles_price_end_id,$address_country_id,$address_country_region_id,$address_country_municipality_id,
			$ads_vehicles_bye_sell_id,$queryRowStart
		)));
	}/*
	public function car(){
		$id = $this->uri->segment('3');
		$data['ads_id'] = $id;
		$data['ads_vehicle_title'] = $this->Ad_model->get_ads_vehicle($id,'title');
		$data['ads_vehicle_title'] = ucfirst(strtolower($data['ads_vehicle_title']));
		$ads_vehicles_type_id = $this->Ad_model->get_ads_vehicle($id,'ads_vehicles_type_id');
		$data['ads_vehicle_type'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_type', $ads_vehicles_type_id,'title');
		$ads_vehicles_brand_id = $this->Ad_model->get_ads_vehicle($id,'ads_vehicles_brand_id');
		$data['ads_vehicle_brand'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_brand', $ads_vehicles_brand_id,'title');
		$ads_vehicles_model_id = $this->Ad_model->get_ads_vehicle($id,'ads_vehicles_model_id');
		$data['ads_vehicle_model'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_model', $ads_vehicles_model_id,'title');
		$data['body'] = $this->Ad_model->get_ads_vehicle($id,'body');
		$data['year'] = $this->Ad_model->get_ads_vehicle($id,'year');
		$data['km'] = $this->Ad_model->get_ads_vehicle($id,'km');
		$ads_vehicles_fuel_id = $this->Ad_model->get_ads_vehicle($id,'vehicles_fuel');
		$data['ads_vehicles_fuel'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_fuel', $ads_vehicles_fuel_id,'title');
		$data['address_street'] = $this->Ad_model->get_ads_vehicle($id,'address_street');
		$ads_address_country_municipality_id = $this->Ad_model->get_ads_vehicle($id,'address_country_municipality_id');
		$data['ads_address_country_municipality'] = $this->Vebko_model->get_single_value_from_db ('address_country_municipality', $ads_address_country_municipality_id,'title');
		$data['date'] = $this->Ad_model->get_ads_vehicle($id,'date');
		$data['price'] = $this->Ad_model->get_ads_vehicle($id,'price');
		$data['priceMkd'] = $this->Vebko_model->convertCurrency($data['price'], 'EUR', 'MKD');		
		$data['contactName'] = $this->Ad_model->get_ads_vehicle($id,'contactName');
		$data['contact_phone'] = $this->Ad_model->get_ads_vehicle($id,'contact_phone');
		$data['contact_phone_text'] = substr($data['contact_phone'], 0, 3) . '/' . substr($data['contact_phone'],3,3). '-' . substr($data['contact_phone'],6,3);
		$ads_vehicles_bye_sell_id = $this->Vebko_model->get_single_value_from_db ('ads_vehicles', $id,'ads_vehicles_bye_sell_id');
		$data['ads_vehicles_bye_sell_label'] = $this->Vebko_model->get_single_value_from_db ('ads_vehicles_bye_sell', $ads_vehicles_bye_sell_id,'label');
		
		
		
		$ID = sprintf('%08d', $id);
		$directoryPath = 'images/ads/car/ad_'.$ID.'/';		
		
		
		
		//add image to gallery
		$gallery_images = array();
		for($i=1; $i<7; $i++){//set in databese how much colume have for image in table ads_vehicle
			if($this->Ad_model->get_ads_vehicle($id,'ads_gallery_image_name_'.$i) != 'default.jpg'){
				//$gallery_image_1_title = $this->Ad_model->get_ads_vehicle($id,'ads_gallery_image_name_1');
				$data['gallery_image_'.$i] =  base_url($directoryPath.'gallery_image_'.$i.'-'.$this->Ad_model->get_ads_vehicle($id,'ads_gallery_image_name_'.$i));
				$gallery_images[$i-1] = $data['gallery_image_'.$i];
			}
		}
		$data['gallery_images'] = $gallery_images;
		//
		$data['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		//add sugestion to array std object		
		$data['suggestions'] = $this->Ad_model->getSearchSugestion();
		
		$dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
		$dataHeaderFooter['title_meta_tag'] = $data['ads_vehicle_title'];
		$dataHeaderFooter['description_meta_tag'] = $data['ads_vehicles_bye_sell_label'].' '.$data['ads_vehicle_model'].' произведен: '.$data['year'].' година, со поминати '.$data['km'].' километри. Цена: '.$data['price'].' €';
		$dataHeaderFooter['image_url_meta_tag'] = $gallery_images['0'];	
		
		$this->load->view('header',$dataHeaderFooter);
		$this->load->view('ad/car/view', $data);
		$this->load->view('footer',$dataHeaderFooter);
	}
	//car end
	*/
	/*function search_address_country_region(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_region($id))));
	}
	function search_address_country_municipality(){
		$id = $this->input->post('id');
		echo(json_encode($this->Vebko_model->translateTextInArray($this->Vebko_model->get_address_country_municipality($id))));
	}*/
	
}