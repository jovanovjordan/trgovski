<?php
class Bot extends CI_Controller {
    public function __construct(){
        parent::__construct();		
        $this->load->model('Bot_model');
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model('Ad_model');
        $this->load->model('Store_model');
        $this->load->model('User_model');
        $this->load->model('Image_model');
        $this->load->model('Vebko_model');
        $this->load->model('Vehicle_model');
        $this->load->model('Estate_model');
        $this->load->model('Electronics_model');
        $this->load->model('Clothing_model');
        $this->load->model('Service_model');
        $this->load->model('Other_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->library('user_agent');
        $this->load->helper('cookie');
        $this->config->load('facebookchatbot');               
        $timezone = "Europe/Belgrade";
        if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
    }
    
    public function index(){
        $hubVerifyToken = $this->config->item('hubVerifyToken');
        $accessToken = $this->config->item('accessToken');
        // check token at setup
        if ($_REQUEST['hub_verify_token'] === $hubVerifyToken) {
          echo $_REQUEST['hub_challenge'];
          exit;
        }
        // handle bot's anwser
        $input = json_decode(file_get_contents('php://input'), true);
        
        
       $arr = implode(' ', $input['entry'][0]['messaging'][0]);
        $senderId = $input['entry'][0]['messaging'][0]['sender']['id'];
        $this->Bot_model->saveUserSessionId($senderId);
        $ch2 = curl_init('https://graph.facebook.com/v2.6/'.$senderId.'?fields=first_name,last_name,profile_pic&access_token=5'.$accessToken);
        
        
        $messageText = $input['entry'][0]['messaging'][0]['message']['text'];
        $response = null;
        //set Message
        $answer = 0;
        if($messageText) {               
            $messageTextWords = preg_split('/[\ \n\,\.]+/', $messageText);
            $keywords = $this->Bot_model->keywords($messageTextWords);
            $botAnswer = $this->Bot_model->botAnswer($keywords,$senderId);
            if($botAnswer){
                $response = [
                    'recipient' => [ 'id' => $senderId ],
                    'message' => [ 'text' => $botAnswer]
                ];
                $response = json_encode($response);
            }
            else{
                $arrayResults = $this->Bot_model->arrayResults($keywords);		
                $answer = $arrayResults;
                $allelement ='';
                if($answer != 0){
                    $key = 0;
                    
                    foreach ($arrayResults as &$value) {            
                        $type = $value[type];
                        if($value[type]=='vehicles'){$type='vehicle';};
                        $url = base_url().$type.'/view/'.$value[id];
                        $image_url = $this->Vebko_model->getAdsMainImage($value[type],$value[id]);
                        //file_put_contents('t4.txt', '<?php $arr = ' . var_export($value, true) . ';');
                        $allelement .= '{
                                        "title":"'.$value[title].'",
                                        "image_url":"'.$image_url.'",
                                        "subtitle":"'.$value[timeago].' / '.$value[price].' / '.$value[municipality].'",
                                        "default_action": {
                                            "type": "web_url",
                                            "url": "'.$url.'",
                                            "webview_height_ratio": "tall"
                                        },
                                        "buttons":[
                                            {
                                                "type":"web_url",
                                                "url":"'.$url.'",
                                                "title":"Повеќе"
                                            },
                                            {
                                                "type":"phone_number",
                                                "title":"Ѕвони",
                                                "payload":"'.$value[contact_phone].'"
                                            }
                                        ]      
                                    },';            
                         $key++;
                    }

                    $response = '{
                        "recipient":{
                            "id":'.$senderId.'
                        }, 
                        "message":{
                            "attachment":{
                              "type":"template",
                              "payload":{
                                "template_type":"generic",
                                "elements":[
                                    '.$allelement.'
                                ]
                              }
                            }
                          }
                    }';
                }
                else{
                    $answer = "Не се продајдени резлтатити. Обидете се повторно.";
                    //send message to facebook bot
                    $response = [
                            'recipient' => [ 'id' => $senderId ],
                            'message' => [ 'text' => $answer ]
                    ];
                    $response = json_encode($response);
                }
            }
        }         
           
        $ch = curl_init('https://graph.facebook.com/v2.6/me/messages?fields=first_name,last_name,profile_pic&access_token=&access_token='.$accessToken);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        
        if(!empty($messageText)){
            $result = curl_exec($ch);
        }
        $messageText = NULL;
        curl_close($ch);
    }
        
    public function bot(){
        $messageText = "mobilen skopje";
        $messageTextWords = preg_split('/[\ \n\,\.]+/', $messageText);
        $keywords = $this->Bot_model->keywords($messageTextWords);
        $botAnswer = $this->Bot_model->botAnswer($keywords);
        $arrayResults = $this->Bot_model->arrayResults($keywords);
        echo "<pre>"; print_r($arrayResults);exit;
        echo implode(" ",$keywords);
    } 
        
    public function admin(){            
        $data[keywords] = $this->Bot_model->getKeywordFromDB();
        $dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
        $dataHeaderFooter['title_meta_tag']       	= $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
        $dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");		
        $dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
        $this->load->view('header',$dataHeaderFooter);
        $this->load->view('botinsert',$data);
        $this->load->view('botadmin',$data);                
        $this->load->view('footer',$data);
    }
		
    public function keywordinsert(){			
        $category = $this->input->post('category');
        $searchWord = $this->input->post('searchWord');
        $searchWordSuggestion = $this->input->post('searchWordSuggestion');
        //echo $category.'-'.$searchWord.'-'.$searchWordSuggestion;exit;

        $categoryID = $this->Bot_model->getCategoryID($category);
        $searchWordID = $this->Bot_model->getSearchWordID($searchWord,$categoryID);
        $searchWordSuggestionId = $this->Bot_model->putSearchWordSuggestion($searchWordSuggestion,$searchWordID);
        //echo $searchWordSuggestionId; exit;
        redirect('/bot/admin', 'refresh');
    }
    public function sendMessage(){
        $dataHeaderFooter = $this->Vebko_model->dataHeaderFooter();
        $dataHeaderFooter['title_meta_tag']       	= $this->Vebko_model->translateText("labelMetaTagHomeTitle");		
        $dataHeaderFooter['description_meta_tag']   = $this->Vebko_model->translateText("labelMetaTagHomeDescription");		
        $dataHeaderFooter['image_url_meta_tag']     = base_url()."images/metatag/home.jpg";
        $this->load->view('header',$dataHeaderFooter);
        $this->load->view('admin/fbSendMessage',$data);                
        $this->load->view('footer',$data);
    }
    public function sendMessageExe(){
		$botMessageText = $this->input->post('message');
		$buttons = explode(",", $this->input->post('buttons'));
		
        $accessToken = $this->config->item('accessToken');
        //$senderId = '1668837276511981';
        $senderIdArray = $this->Bot_model->getSenderIdArray();
		//print_r($senderIdArray);exit;
        foreach ($senderIdArray as $senderId){
        //$buttons = array('Станови','Куќи','Мобилни', 'Компјутери');
            $this->Bot_model->sendFBChatMessage($accessToken, $senderId, $botMessageText,$buttons);
        }
           exit;    
    }
   
}
?>
