<?php

class VebkoImage extends CI_Controller {
public function risizeImage($upload_data, $widthSize, $heightSize,$directoryPath, $startFileName){
		
		
		$upload_data = $this->upload->data();
		$image_config["image_library"] = "gd2";
		$image_config["source_image"] = $upload_data["full_path"];
		$image_config['create_thumb'] = FALSE;
		$image_config['maintain_ratio'] = TRUE;
		$image_config['new_image'] = $upload_data["file_path"] . 'product.png';
		$image_config['quality'] = "100%";
		$image_config['width'] = $widthSize;
		$image_config['height'] = $heightSize;
		$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
		$image_config['master_dim'] = ($dim > 0)? "height" : "width";
		 
		$this->load->library('image_lib');
		$this->image_lib->initialize($image_config);
		$this->image_lib->resize();


	}
}