<div class="vebkoInsertCar container vebko-padding-0" id="container">
	<div class= "col-md-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5">
		<div class="seacrhBoxContainer col-md-12 col-md-offset-0 vebko-padding-0">        
			<?php $attributes = array("name" => "form");
				echo form_open_multipart("bot/keywordinsert", $attributes);?>
					<div class="vebkoBox col-xs-12">
						<div class="vebkoBoxLabel vebkoBoxTitle col-xs-12 vebko-padding-0">
                                                    <?php echo $labelInsertStore;?>
                                                    <div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
							<?php echo $labelInsertAdsDescription;?>
                                                    </div>                                                
						</div>
					</div>
					
					<div class="vebkoBox col-xs-12 col-md-12">
						<div class="vebkoBoxLabel col-xs-12">
							<?php echo $labelInsertStoreBoxTitle;?>
                                                         
						</div>					
						<div class="form-group col-xs-12 col-md-2 vebko-padding-0">
							category
							<?php echo form_input(array('id' => 'category','class' => 'form-control', 'name' => 'category','required'=>'required')); ?><br />
                                                       
						</div>
						<div class="form-group col-xs-12 col-md-2 vebko-padding-0">
							searchWord
							<?php echo form_input(array('id' => 'searchWord','class' => 'form-control', 'name' => 'searchWord','required'=>'required')); ?><br />
                                                       
						</div>
						<div class="form-group col-xs-12 col-md-8 vebko-padding-0">
							searchWordSuggestion
							<?php echo form_input(array('id' => 'searchWordSuggestion','class' => 'form-control', 'name' => 'searchWordSuggestion','required'=>'required')); ?><br />
                                                       
						</div>
						
							
					</div>
                        <div class="col-xs-12 vebko-padding-0">
						<div class="col-xs-12 vebko-padding-0 col-md-8">
							
						</div>
												
						<div class="col-xs-12 vebko-padding-0 col-md-4">
							<div class="form-group col-xs-12 col-md-6">
								<a class="btn  btn-danger btn-block" href="<?php echo base_url('account'); ?>">Cancel</a>
							</div>							
							<div class="insertButtonSubmit form-group col-xs-12 col-md-6">
								<button name="submit" type="submit" id="insertButton" class="btn  btn-success btn-block">Save</button>
							</div>
						</div>
					</div>
			<?php echo form_close(); ?>
		</div>
	
	</div>
</div>