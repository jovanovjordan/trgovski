<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row col-md-4 col-md-offset-4">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
            
		<div class="">
			<div class="page-header">
				<h1>Најавување</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="username">Корисничко име (Username)</label>
					<input type="text" class="form-control" id="username" name="username" placeholder="Корисничко име">
				</div>
				<div class="form-group">
					<label for="password">Лозинка (Password)</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Лозинка">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-info" value="Најави се">
				</div>
				
				<a href="<?= base_url('index.php/user/register') ?>" class="" role="button">Заборавена лозинка.</a><br>
				<a href="<?= base_url('index.php/user/register') ?>" class="" role="button">Креирај сметна (account)</a>
			
		</div>
	</div><!-- .row -->
</div><!-- .container -->