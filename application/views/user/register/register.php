<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row col-md-4 col-md-offset-4">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="">
			<div class="page-header">
				<h1>Регистирај се</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="username">Корисничко име (Username)</label>
					<input type="text" class="form-control" id="username" name="username" placeholder="Enter a username">
				</div>
				<div class="form-group">
					<label for="email">Електронска пошта (Email)</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email">
				</div>
				<div class="form-group">
					<label for="password">Лозинка (Password)</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Enter a password">
				</div>
				<div class="form-group">
					<label for="password_confirm">Повтори ја лозинката (Password)</label>
					<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm your password">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Креирај сметка">
				</div>
			</form>
		</div>
	</div><!-- .row -->
</div><!-- .container -->