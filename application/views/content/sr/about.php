<div class="container">
	<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxTitle col-xs-12 vebko-padding-0">
				За Трговски
			</div>
		<div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
				trgovski.com
			</div>
	</div>
	<div class="vebkoBox col-xs-12">
		<div class="vebkoBoxLabel col-xs-12">
			Општо
		</div>
		Trgovski.com е бесплатен и засекогаш ќе остане бесплатен интернет простор за објавување огласи.<br>
		Достапен во Македонија, Србија и Бугарија, со цел во иднина да се прошири во Хрватска, Црна Гора и Грција.<br>
		Карактеристиките секој оглас внесен на трговски автоматски се прикажуваат и на избраниот јазик на посетителот на сајтот.<br><br>
	</div>
	<div class="vebkoBox col-xs-12">
		<div class="vebkoBoxLabel col-xs-12">
			Најваување (Логирање)
		</div>
		За да се логирате на Трговски доволно е да кликнете на линкот "Логирај се", ќе бидете логирани со Вашата facebook сметка<br>
		Доколку немате facebook сметка потребно е да креирате, за да може да внесувате огласи.
	</div>
	<div class="vebkoBox col-xs-12">
		<div class="vebkoBoxLabel col-xs-12">
			Внесување и промена на оглас
		</div>
		Само најавени корисници може да внесуваат оглас.<br>
		За да внесете оглас одете на линкот "Внеси оглас" на декторпо верзијата, и на знакот "+" за мобилна верзија.<br>
		Одберете катероија и потполнете ја формата.<br>
		На крај го објавувате огласот.<br>
		На веќе внесен оглас може да ги направите следните опции:<br>
		1. Промена на содржина<br>
		2. Обновување на огласот<br>
		3. Активирање/Деактивирање на оглас<br>
	</div>
	<div class="vebkoBox col-xs-12">
		<div class="vebkoBoxLabel col-xs-12">
			Креирање на продавница
		</div>
		Доколку имате повеќе огласи trgovski.com Ви овозможува да ги прикажете само Вашите огласи на посебед поддомаин<br>
		За да креирате продавница во делот на Вашиот профил, кликнете на копчето "Креирај продавница"<br>
		Потполнете го форумот за продавница и имате Ваш под домаин на trgovski.com пример: mojaprodavnica.trgovski.com
	</div>
	<div class="vebkoBox col-xs-12">
		<div class="vebkoBoxLabel col-xs-12">
			Прашај го трговски (Ask Trvoski)
		</div>
		Ask Trgovski е опција која Ви овозможува да го прашувате и пребарувате огласи преку приватни пораки во fb messanger
		фан страницата на trgovski - fb.com/trgovskicom.<br><br>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/iWGpTckM2o4?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
		
	</div>
       

        <div class="vebkoBox col-xs-12">
			<div class="vebkoBoxTitle col-xs-12 vebko-padding-0">
				Услови за користење
			</div>
		<div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
				trgovski.com
			</div>
	</div>
    <div class="vebkoBox col-xs-12">
		<div class="vebkoBoxLabel col-xs-12">
			Правила и услови за рекламирање
		</div>
		Веб страната trgovski.com е јавно достапна на интернет и секој може да ги виде активните објавени огласи. <br><br>
                Секој користник на социјалната мрежа facebook може да се најави на системот и да постави оглас.<br>
                За содржината и сликите на огласот одговорен е огласувачот. <br><br>
                Огласот не смее да содржи навредлива содржина кон поединец или група.
	</div>
 </div>
<br>
