<div class="container">
	<div class="vebkoAccount col-xs-12 vebko-padding-0">
		<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountTitle; ?>
			</div>
		</div>
		<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountBodyPersonalInfoTitle; ?>
			</div>
			<div class="vebkoBoxBody col-xs-12 vebko-padding-0">
				<div class="vebkoBoxBodyLeft col-xs-12 col-md-1 vebko-padding-0">
					<a href="<?php echo $userData->profile_url; ?>" target ="_blank"><img src="<?php echo $userData->picture_url; ?>" alt="<?php echo $userData->first_name; ?>"></a> 
				</div>
				<div class="vebkoBoxBodyRight col-xs-12 col-md-11 vebko-padding-0">
					<div class="vebkoBoxBodySingle col-xs-12 vebko-padding-0">
						 <a href="<?php echo $userData->profile_url; ?>" target ="_blank"><?php echo $userData->first_name; ?> <?php echo $userData->last_name; ?></a>
					</div>
					<div class="vebkoBoxBodySingle col-xs-12 vebko-padding-0">
						<?php echo $userData->email; ?>
					</div>
				</div>				
			</div>
		</div>
            
                <div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountStoreInformation; ?>
			</div>
                    <?php if ($storeData){?>
			<div class="vebkoBoxBody col-xs-12 vebko-padding-0">
                            <div class="col-xs-12 vebko-padding-0">
                                <?php echo $labelAccountStoreInformationStoreName; ?>
                                <?php echo $storeData->title;?>
                            </div>
                            <div class="col-xs-12 vebko-padding-0">
                                <?php echo $labelAccountStoreInformationStoreUrl; ?>
                                <?php echo anchor('http://'.$storeData->subdomain.'.'.$_SERVER['HTTP_HOST'], $storeData->subdomain.'.'.$_SERVER['HTTP_HOST'], 'class="link-class"') ?>
                            </div>
                            <div class="col-xs-12 vebko-padding-0">
                                <?php echo anchor('store/update', $labelAccountStoreInformationCreateStoreButtonTitle, 'class="btn btn-success"') ?>
                                
                            </div>
			</div>
                    <?php }else{?>
                        <div class="vebkoBoxBody col-xs-12 vebko-padding-0">
                            <div class="col-xs-12 vebko-padding-0">
                                <?php //echo $labelAccountStoreInformationCreateStoreTitle; ?>
                            </div>
                            <div class="col-xs-12 vebko-padding-0">
                                <?php echo $labelAccountStoreInformationCreateStoreBody; ?>
                            </div>
                            <div class="col-xs-12 vebko-padding-0">
                                <?php echo anchor('store/insert', $labelInsertStore, 'class="btn btn-success"') ?>
                                
                            </div>
                        </div>
                    <?php }?>
		</div>
		
		
		<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountBodyMyAdsTitle; ?>
			</div>
			<div class="vebkoBoxBody col-xs-12 vebko-padding-0">
				<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyVehibleTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyVehicleData as $key => $value) {?>							
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<div class="vebkoSubBoxBodySingleLeft col-xs-12 col-md-2 vebko-padding-0">
									<a href="vehicle/view/<?php echo $value->id; ?>">
										<img src="<?php echo base_url("images/ads/car/ad_").sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"> 
									</a>
								</div>
								<div class="vebkoSubBoxBodySingleRight col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
									<a href="vehicle/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
								</div>								
							</div>
						<?php } ?>		
					</div>
				</div>
				<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyEstateTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyEstateData as $key => $value) {?>							
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<div class="vebkoSubBoxBodySingleLeft col-xs-12 col-md-2 vebko-padding-0">
									<a href="estate/view/<?php echo $value->id; ?>">
										<img src="<?php echo base_url("images/ads/estate/ad_").sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"> 
									</a>
								</div>
								<div class="vebkoSubBoxBodySingleRight col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
									<a href="estate/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
								</div>								
							</div>
						<?php } ?>		
					</div>
				</div>

				<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyElectronicsTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyElectronicsData as $key => $value) {?>							
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<div class="vebkoSubBoxBodySingleLeft col-xs-12 col-md-2 vebko-padding-0">
									<a href="electronics/view/<?php echo $value->id; ?>">
										<img src="<?php echo base_url("images/ads/electronics/ad_").sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"> 
									</a>
								</div>
								<div class="vebkoSubBoxBodySingleRight col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
									<a href="electronics/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
								</div>								
							</div>
						<?php } ?>		
					</div>
				</div>

				<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyClothingTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyClothingData as $key => $value) {?>							
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<div class="vebkoSubBoxBodySingleLeft col-xs-12 col-md-2 vebko-padding-0">
									<a href="clothing/view/<?php echo $value->id; ?>">
										<img src="<?php echo base_url("images/ads/clothing/ad_").sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"> 
									</a>
								</div>
								<div class="vebkoSubBoxBodySingleRight col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
									<a href="clothing/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
								</div>								
							</div>
						<?php } ?>		
					</div>
				</div>

				<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyServiceTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyServiceData as $key => $value) {?>							
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<div class="vebkoSubBoxBodySingleLeft col-xs-12 col-md-2 vebko-padding-0">
									<a href="service/view/<?php echo $value->id; ?>">
										<img src="<?php echo base_url("images/ads/service/ad_").sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"> 
									</a>
								</div>
								<div class="vebkoSubBoxBodySingleRight col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
									<a href="service/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
								</div>								
							</div>
						<?php } ?>		
					</div>
				</div>

<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyOtherTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyOtherData as $key => $value) {?>							
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<div class="vebkoSubBoxBodySingleLeft col-xs-12 col-md-2 vebko-padding-0">
									<a href="other/view/<?php echo $value->id; ?>">
										<img src="<?php echo base_url("images/ads/other/ad_").sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"> 
									</a>
								</div>
								<div class="vebkoSubBoxBodySingleRight col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
									<a href="other/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
								</div>								
							</div>
						<?php } ?>		
					</div>
				</div>				
			</div>
		</div>
		
		
		
	</div>
</div>