<div class="container">
	<div class="vebkoAccount col-xs-12 vebko-padding-0">
		<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountTitle; ?>
			</div>
		</div>
		<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountBodyPersonalInfoTitle; ?>
			</div>
			<div class="vebkoBoxBody col-xs-12 vebko-padding-0">
				<div class="vebkoBoxBodyLeft col-xs-12 col-md-1 vebko-padding-0">
					<a href="<?php echo $userData->profile_url; ?>" target ="_blank"><img src="<?php echo $userData->picture_url; ?>" alt="<?php echo $userData->first_name; ?>"></a> 
				</div>
				<div class="vebkoBoxBodyRight col-xs-12 col-md-11 vebko-padding-0">
					<div class="vebkoBoxBodySingle col-xs-12 vebko-padding-0">
						 <a href="<?php echo $userData->profile_url; ?>" target ="_blank"><?php echo $userData->first_name; ?> <?php echo $userData->last_name; ?></a>
					</div>
					<div class="vebkoBoxBodySingle col-xs-12 vebko-padding-0">
						<?php echo $userData->email; ?>
					</div>
				</div>				
			</div>
		</div>
		
		
		<div class="vebkoBox col-xs-12">
			<div class="vebkoBoxLabel col-xs-12">
				<?php echo $labelAccountBodyMyAdsTitle; ?>
			</div>
			<div class="vebkoBoxBody col-xs-12 vebko-padding-0">
				<div class="vebkoSubBox col-xs-12 vebko-padding-0">
					<div class="vebkoSubBoxLabel col-xs-12 vebko-padding-0">
						<?php echo $labelAccountSubBodyVehibleTitle; ?>
					</div>
					<div class="vebkoSubBoxBody col-xs-12 vebko-padding-0">
						<?php foreach ($accountMyVehicleData as $key => $value) {?>
							<div class="vebkoSubBoxBodySingle col-xs-12 vebko-padding-0">
								<a href="vehicle/view/<?php echo $value->id; ?>"><?php echo $value->title; ?></a>
							</div>
						<?php } ?>		
					</div>
				</div>			
			</div>
		</div>
		
		
		
	</div>
</div>