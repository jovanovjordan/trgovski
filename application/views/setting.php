<div class="container">
    <div class="vebkoBox col-xs-12">
                    <div class="vebkoBoxTitle col-xs-12 vebko-padding-0">
                            <?php echo $labelSettingTitle;?>
                    </div>
            <div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
                            <?php echo $labelSettingTitleDescription;?>
                    </div>
    </div>
    <div class="vebkoBox col-xs-12">
        <div class="vebkoBoxLabel col-xs-12">
                <?php echo $labelSettingTitleChangeLanguage;?>
        </div>
        <?php echo $labelSettingTitleChangeLanguageMessage;?><br><br>
        <?php echo anchor('Vebko/ln/mk', 'Македонски', 'class="link-class"') ?>
        <?php echo anchor('Vebko/ln/en', 'English', 'class="link-class"') ?>				
        <?php echo anchor('Vebko/ln/sr', 'Српски', 'class="link-class"') ?>
        <?php echo anchor('Vebko/ln/bg', 'Български', 'class="link-class"') ?><br><br>
    </div>
    
    <div class="vebkoBox col-xs-12">
        <div class="vebkoBoxLabel col-xs-12">
                <?php echo $labelSettingTitleChangeCountry;?>
        </div>
        <?php echo $labelSettingTitleChangeCountryMessage;?><br><br>
        <?php echo anchor('Vebko/co/1', $Macedonia, 'class="link-class vebko-padding-left-5"') ?>
        <?php echo anchor('Vebko/co/2', $Serbia, 'class="link-class vebko-padding-left-5"') ?>
        <?php echo anchor('Vebko/co/3', $Bulgaria, 'class="link-class vebko-padding-left-5"') ?>
        <?php echo anchor('Vebko/co/4', $Greece, 'class="link-class vebko-padding-left-5"') ?><br><br>
    </div>
    
    <div class="vebkoBox col-xs-12">
        <div class="vebkoBoxLabel col-xs-12">
                <?php echo $labelSettingTitleChangeCurrency;?>
        </div>
        <?php echo $labelSettingTitleChangeCurrencyMessage;?><br><br>
        <?php echo anchor('Vebko/cu/eur', $eur, 'class="link-class vebko-padding-left-5"') ?>
        <?php echo anchor('Vebko/cu/mkd', $mkd, 'class="link-class vebko-padding-left-5"') ?>
        <?php echo anchor('Vebko/cu/rsd', $rsd, 'class="link-class vebko-padding-left-5"') ?>				
        <?php echo anchor('Vebko/cu/bgn', $bgn, 'class="link-class vebko-padding-left-5"') ?><br><br>
    </div>
</div>

