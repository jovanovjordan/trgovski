<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html class = "html-page-<?php echo $pageCss;?>" lang="en">
<head >
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:title" content="<?php if (isset($title_meta_tag)){echo $title_meta_tag;}?>"/>
	<meta property="og:description" content="<?php if (isset($description_meta_tag)){echo $description_meta_tag;}?>"/>
	<meta property="og:image" content="<?php if (isset($image_url_meta_tag)){echo $image_url_meta_tag;} ?>"/>
	<title>Trgovski.com</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
        <link rel="icon" href="<?=base_url()?>/images/favicon.jpg" type="image/jpg">

	<!-- css -->
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/css/summernote.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/css/vebko.css') ?>?<?php echo time(); ?>" rel="stylesheet">
	<script src="<?php echo base_url("assets/js/jquery-3.1.1.min.js"); ?>" type="text/javascript"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>	 
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">	
	<script src="<?php echo base_url("assets/js/bootstrap.js"); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url("assets/js/readmore.js"); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url("assets/js/summernote.js"); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url("assets/js/jquery.vibrate.min.js"); ?>" type="text/javascript"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/vebko.js') ?>?<?php echo time(); ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/simplegallery.min.js') ?>"></script>
	
	<!--<script type="text/javascript" src="<?= base_url('assets/js/zepto.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/binaryajax.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/exif.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/canvasResize.js') ?>"></script>-->	
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
        
        <link href="<?= base_url('assets/css/swiper.css') ?>" rel="stylesheet">
        <script src="<?= base_url('assets/js/swiper.js') ?>"></script>
</head>

<body class = "page-<?php echo $pageCss;?>">
<div class = "VebkoLoading col-xs-12 col-md-12">
	<!--<div class = "VebkoLoadingContainer col-xs-12  col-md-12">
		<img src="<?php echo base_url();?>/images/loading.gif" alt="No image" height="auto" width="40px">
	</div>-->
</div>

	<header id="site-header">           
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="headerSelectLanguageCountry">
                        <div class="container">
                            <?php if(!$languageSetting){ ?>
                                <div class = "headerSelectLanguageCountryContainer">
                                        <?php echo anchor('Vebko/ln/en', 'English', 'class="link-class"') ?>
                                        <?php echo anchor('Vebko/ln/mk', 'Македонски', 'class="link-class"') ?>				
                                        <?php echo anchor('Vebko/ln/sr', 'Српски', 'class="link-class"') ?>
                                        <?php echo anchor('Vebko/ln/bg', 'Български', 'class="link-class"') ?>
                                </div>
                            <?php } ?>
                            <?php if((!$languageCountry)&&($languageSetting)){ ?>                        
                                <div class = "headerSelectLanguageCountryContainer">
                                    <?php echo $country; ?>                            
                                    <?php echo anchor('Vebko/co/1', $Macedonia, 'class="link-class vebko-padding-left-5"') ?>
                                    <?php echo anchor('Vebko/co/2', $Serbia, 'class="link-class vebko-padding-left-5"') ?>
                                    <?php echo anchor('Vebko/co/3', $Bulgaria, 'class="link-class vebko-padding-left-5"') ?>				
                                    <?php echo anchor('Vebko/co/4', $Greece, 'class="link-class vebko-padding-left-5"') ?>
                                </div>
                            <?php } ?>
                            <?php if((!$currencySetting)&&($languageCountry)&&($languageSetting)){ ?> 
                                <div class = "headerSelectLanguageCountryContainer">
                                    <?php echo $currency; ?>                                        
                                    <?php echo anchor('Vebko/cu/eur', $eur, 'class="link-class vebko-padding-left-5"') ?>
                                    <?php echo anchor('Vebko/cu/mkd', $mkd, 'class="link-class vebko-padding-left-5"') ?>
                                    <?php echo anchor('Vebko/cu/rsd', $rsd, 'class="link-class vebko-padding-left-5"') ?>
                                    <?php echo anchor('Vebko/cu/bgn', $bgn, 'class="link-class vebko-padding-left-5"') ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
			<div class="container vebko-padding-0">				
				<div class="col-xs-0 col-md-3 vebko-padding-0 hidden-xs">
					<a class="navbar-brand" href="<?= base_url() ?>">Trgovski.com</a>
				</div>
				<div class="col-xs-12 col-md-9 vebko-padding-0">
					<a class="vebkoNav col-xs-2" href="<?= base_url() ?>"><i class="fa fa-home fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $home;?></span></a>
					<a class="vebkoNav col-xs-2" href="<?= base_url('insert') ?>"><i class="fa fa-plus fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $insertAds;?></span></a>
                                        <a class="vebkoNav col-xs-2" href="<?= base_url('setting') ?>"><i class="fa fa-asterisk fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $setting;?></span></a>
					<a class="vebkoNav col-xs-2" href="<?= base_url('about') ?>"><i class="fa fa-info fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $about;?></span></a>
					
					<?php if($checkUserLoginReturnUserData['oauth_uid'] == 0){?>
						<a class="vebkoNav vebkoNav col-xs-2" href="<?= base_url('account') ?>"><i class="fa fa-user fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $labelMyAccount;?></span></a>
						<a class="vebkoNav col-xs-2" href="<?= base_url('user_authentication/index') ?>"><i class="fa fa-sign-in fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $login;?></span></a>
					<?php } else{?>
						<a class="vebkoNav vebkoNavUser col-xs-2" href="<?= base_url('account') ?>"><i class="fa fa-user fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"> <img src="<?php echo $checkUserLoginReturnUserData['picture_url'];?>" alt="Smiley face"> <?php echo $checkUserLoginReturnUserData['first_name'];?></span></a>
						<a class="vebkoNav vebkoNavLogout col-xs-2" href="<?= base_url('user_authentication/logout') ?>"><i class="fa fa-sign-out fa-fw fa-1x hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs"><?php echo $logout;?></span></a>
					<?php }?>
				</div>
			</div><!-- .container-fluid -->
		</nav><!-- .navbar -->
	</header><!-- #site-header -->
	<main id="site-content" role="main">
            
            <div class="vebkoMessage <?php echo $sessionMessageType;?>">
                <div class="vebkoMessageText">
                    <?php echo $sessionMessageText;?>
                </div>			
            </div>