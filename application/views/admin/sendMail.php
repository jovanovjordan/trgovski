<div class="vebkoInsertCar container vebko-padding-0" id="container">
	<div class= "col-md-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5">
		<div class="seacrhBoxContainer col-md-12 col-md-offset-0 vebko-padding-0">        
			<?php $attributes = array("name" => "form");
				echo form_open_multipart("admin/sendMailExe", $attributes);?>
					<div class="vebkoBox col-xs-12">
						<div class="vebkoBoxLabel vebkoBoxTitle col-xs-12 vebko-padding-0">
                                                    <?php echo $labelInsertStore;?>
                                                    <div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
							<?php echo $labelInsertAdsDescription;?>
                                                    </div>                                                
						</div>
					</div>
					
					<div class="vebkoBox col-xs-12 col-md-12">
						<div class="form-group col-xs-12 col-md-3 searchyearend vebko-padding-0 vebko-padding-left-5">
								<div class="col-md-12 vebko-padding-0">
									To
								</div>
								<?php $attributes = 'id="address_country_id" class="form-control"';
								echo form_dropdown('address_country', $address_country, $address_country_id, $attributes); ?>
							</div>
                                            
                                            <div class="form-group col-xs-12 col-md-12 vebko-padding-0">
                                                send to emails:
                                                <div class="form-group col-xs-12 col-md-12 vebko-padding-0" id="mailto">
                                                Izberi drzava
                                            </div>
                                            </div>
                                            
                                            
                                            
                                            
						<div class="form-group col-xs-12 col-md-12 vebko-padding-0">
							Subject
							<?php echo form_input(array('id' => 'subject','class' => 'form-control', 'name' => 'subject','required'=>'required')); ?><br />
                                                       
						</div>
                                            <div class="form-group col-xs-12 col-md-12 vebko-padding-0">
							Body
							<?php 
                                                            $data = array(
                                                                    'name'        => 'body',
                                                                    'id'          => 'body',
                                                                    'class'       => 'form-control'
                                                                );
                                                            echo form_textarea($data);
                                                        ?>
                                                        <br />
                                                       
						</div>
						
							
					</div>
                        <div class="col-xs-12 vebko-padding-0">
						<div class="col-xs-12 vebko-padding-0 col-md-8">
							
						</div>
												
						<div class="col-xs-12 vebko-padding-0 col-md-4">
							<div class="form-group col-xs-12 col-md-6">
								<a class="btn  btn-danger btn-block" href="<?php echo base_url('account'); ?>">Cancel</a>
							</div>							
							<div class="insertButtonSubmit form-group col-xs-12 col-md-6">
								<button name="submit" type="submit" id="insertButton" class="btn  btn-success btn-block">Save</button>
							</div>
						</div>
					</div>
			<?php echo form_close(); ?>
		</div>
	
	</div>
</div>

<script type="text/javascript">
    
    $('#address_country_id').change(function(){
    var address_country_id = $(this).val();
    $("#address_country_municipality_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Admin/getEmails'); ?>",
        data: {id: address_country_id},
        dataType: 'json',
        success:function(data){
            $('#mailto').html(data);
        }
    });
});
</script>

