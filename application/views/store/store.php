<?php ?>
<div class="vebkoStoreOne container  vebko-padding-0">

	<div class="vebkoBox vebkoBoxStoreHeader col-xs-12"> 
            <div class="vebko-padding-0">
                <?php echo $storeLogo;?>
            </div>
            <div class="vebkoStoreOneTitleSubTitle col-xs-12 col-md-10">
                <div class="vebkoStoreOneTitle col-xs-12 vebko-padding-0">
			<?php echo $storeContent[0]->title; ?>
		</div>
                <div class="vebkoStoreOneUrl col-xs-12 vebko-padding-0">
			<?php echo $storeUrl;?>
		</div>
		<div class="vebkoStoreOneSubTitle col-xs-12  vebko-padding-0">
			<?php echo $storeContent[0]->subtitle; ?>
		</div>
            </div>


	</div>
	<div class="vebkoBox searchListAds col-xs-12 vebko-padding-0">
	<?php foreach ($storeAds as $key => $value) {?>
	<div class="col-xs-12 col-md-6 searchSingleAd vebko-padding-0">
                <a class="" href="<?php echo $value["srcSubDomain"];?>">
                    

                    <div class="searchSingleAdMiddle col-md-12 col-xs-12 vebko-padding-0">
                        <div class="searchSingleAdLeft col-xs-4 col-md-3 vebko-padding-0">
                            <img src="<?php echo $value["imageSrc"];?>" alt="No image" height="auto" width="100%">
                        </div>
                        <div class="searchSingleAdMiddleTitle col-xs-8 col-md-9 vebko-padding-0">
                            <?php echo $value["title"];?>
                        </div>
                        <div class="searchSingleAdMiddleDate col-xs-8 col-md-9 vebko-padding-0">
                            <?php echo $value["price"];?>E, <?php echo $value["date_modify"];?>
                        </div>
                        <div class="searchSingleAdMiddleBody vebko-padding-0">
<?php //echo $value["body"];?>

                        </div>					
                    </div>
                </a>						
        </div>
	
	<?php } ?>
	</div>
</div>
