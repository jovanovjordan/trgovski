<div class="vebkoInsertCar container vebko-padding-0" id="container">
    <div class= "col-md-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5">
        <div class="seacrhBoxContainer col-md-12 col-md-offset-0 vebko-padding-0">        
            <?php $attributes = array("name" => "form");
                echo form_open_multipart("store/insert", $attributes);?>
                    <div class="vebkoBox col-xs-12">
                        <div class="vebkoBoxLabel vebkoBoxTitle col-xs-12 vebko-padding-0">
                            <?php echo $labelInsertStore;?>
                            <div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
                                <?php echo $labelInsertAdsDescription;?>
                            </div>                                                
                        </div>
                    </div>
					
                    <div class="vebkoBox col-xs-12 col-md-12">
                        <div class="vebkoBoxLabel col-xs-12">
                            <?php echo $labelInsertStoreBoxTitle;?>
                        </div>					
                        <div class="form-group col-xs-12 col-md-12 vebko-padding-0">
                            <?php echo form_label($labelInsertStoreTitle); ?>
                            <?php echo form_input(array('id' => 'title','class' => 'form-control', 'name' => 'title','required'=>'required')); ?><br />
                            <?php echo $labelInsertStoreTitleDescription;?>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 vebko-padding-0">
                            <?php echo form_label($labelInsertStoreSubTitle); ?>
                            <?php echo form_input(array('id' => 'subtitle','class' => 'form-control', 'name' => 'subtitle')); ?><br />
                            <?php echo $labelInsertStoreSubTitleDescription;?>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 vebko-padding-0">
                            <div class="col-xs-12 col-md-12 vebko-padding-0">
                            <?php echo form_label($labelInsertStoreURL); ?>
                            </div>
                            <div class="col-xs-12 col-md-4 vebko-padding-0">
                                    <?php echo form_input(array('id' => 'subdomain','class' => 'form-control', 'name' => 'subdomain','required'=>'required')); ?>
                            </div>
                            <div class="col-xs-12 col-md-4 vebko-padding-0">
                                    .trgovski.com
                            </div>
                            <div class="form-group col-xs-12 col-md-12 vebko-padding-0">
                                    <?php echo $labelInsertStoreURLDescription;?>
                            </div>
                        </div>
                        <div class="vebkoBox col-xs-12"> <!-- gallery start -->
						<div class="vebkoBoxLabel col-xs-12">
							<?php echo $labelInsertInsertImages;?>
							<div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
								<?php echo $labelInsertInsertImagesDescription;?>
							</div>
						</div>					
						<div class="images col-xs-12 col-md-12 vebko-padding-0">							
							<div class="imageGallery col-xs-12 col-md-12 vebko-padding-0">								
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php for($i = 1; $i < 2; $i ++){?>
									<div id="previewImageBox-<?php echo $i;?>" class="previewImageBox col-xs-6 col-md-2 vebko-padding-0">						 
										<div class="previewImage col-xs-12 col-md-12 vebko-padding-0 vebko-padding-right-5">
											<img id="previewImage-g<?php echo $i;?>" src="<?php echo $defaultImage; ?>" alt=" " />
										</div>
										<div class="uploadImage col-xs-12 col-md-12 vebko-padding-0">
											<input type="file" id="userfile-g<?php echo $i;?>" name="userfile-g<?php echo $i;?>" size="20" title="dd">									
											<button type="button" id="deleteImage-g<?php echo $i;?>" class="btn btn-secondary"><?php echo $labelInsertInsertImagesDelete;?></button>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
					</div> <!-- gallery end -->
                    </div>
                    <div class="col-xs-12 vebko-padding-0">
                        <div class="col-xs-12 vebko-padding-0 col-md-8">
                                <?php echo $labelInsertStoreAgree;?>
                        </div>
                        <div class="col-xs-12 vebko-padding-0 col-md-4">
                            <div class="form-group col-xs-12 col-md-6">
                                <a class="btn  btn-danger btn-block" href="<?php echo base_url('account'); ?>"><?php echo $labelAdsCancelButtonText;?></a>
                            </div>							
                            <div class="insertButtonSubmit form-group col-xs-12 col-md-6">
                                <button name="submit" type="submit" id="insertButton" class="btn  btn-success btn-block"><?php echo $labelInsertStoreOpenStore;?></button>
                            </div>
                        </div>
                    </div>
            <?php echo form_close(); ?>
        </div>	
    </div>
</div>

<script type="text/javascript">
    $( "#deleteImage-g1" ).hide();
$("#userfile-g1").change(function(i){
	
	
		readURLImage1(this);
		//$( '#userfile-g1').hide();
		//$('#deleteImage-g1').show();
		$('#previewImageBox-2').show();
	});

	$("#deleteImage-g1").click(function(){
		$('#previewImage-g1').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g1").hide();
		$( "#userfile-g1").show();
		$( '#userfile-g1').remove();
	});

function readURLImage1(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g1').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
	
}
<!-- start single-->
</script>