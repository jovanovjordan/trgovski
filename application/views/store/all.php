<div class="vebkoStores vebko-padding-0" id="container">
    <div class="container">        
        <div class="vebko-padding-0 col-xs-12 col-md-12">
            <div class="vebkoStoreTitle col-xs-12 vebko-padding-0">
                <?php echo $StoresTitle; ?>
            </div>
            <div class="vebkoBoxLabelDescription col-xs-12 col-md-12  vebko-padding-0">
                <?php //echo $StoresSubTitle; ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-12  vebko-padding-0">
            <?php foreach ($stores as $store) {?>
                <div class="vebkoStore col-xs-3 col-8 vebko-padding-0">
                    <div class="col-xs-12 col-md-12 vebko-padding-0">
                        <?php echo $storeLogo[$store->id];?>
                    </div>
                    <div class="vebkoAllStoreTitle col-xs-12 col-md-12 vebko-padding-0">
                        <?php echo substr($store->title,0,14);?>
                    </div>

                </div>
            <?php } ?>
        </div>
    </div>
</div>