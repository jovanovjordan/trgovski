﻿<div class="container">
	<div  class="col-xs-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5">
		<div class="col-xs-12 vebkoMainCategoryAdd">
						
			<div class="vebkoMainCategory col-xs-12 col-md-12 vebko-padding-0">				
				<div class=" col-xs-12 col-md-6 vebkoMainCategoryContainer vebkoMainCategoryDescription vebko-padding-0">
                                    <h4><i class="" aria-hidden="true"></i><?php echo $labelInsertTitle;?></h4>
                                    <ul>
                                        <li><?php echo $labelInsertBodyLine1;?></li>
                                        <?php if($checkUserLoginReturnUserData==0){?>
                                        <li><?php echo $labelInsertBodyLine2;?></li>
                                        <div class="col-xs-12 col-md-12  vebko-padding-0">
                                                <div class="vebkoLoginBtn col-xs-6 col-md-6  vebko-padding-0 col-xs-offset-3 col-md-offset-3">
                                                        <a href="user_authentication/index" class="btn btn-info btn-block" role="button">
                                                                <?php echo $login;?>
                                                        </a>
                                                </div>
                                        </div>
                                        <?php }?>
                                        <li><?php echo $labelInsertBodyLine3;?></li>
                                        <li><?php echo $labelInsertBodyLine4;?></li>
                                    </ul>
				</div>
				<div class=" col-xs-12 col-md-1 vebkoMainCategoryContainer vebko-padding-0">
				</div>
				<div class=" col-xs-12 col-md-5 vebkoMainCategoryContainer vebko-padding-0">
                                    <div class="col-xs-12 vebko-padding-0">
                                    <h4><?php echo $labelInsertCategoryChoiceCategory;?></h4>
                                    </div>
                                    <div class="col-xs-12 vebko-padding-0">
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4 vebko-padding-0">
                                            <div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
                                                    <a href="../estate/insert" class="btn pagination-centered center-block" role="button">
                                                            <i class="fa fa-building fa-fw fa-2x" aria-hidden="true"></i>
                                                            <br><?php echo $real_estate;?>
                                                    </a>
                                            </div>
                                        </div>
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4 vebko-padding-0">
                                            <div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
                                                    <a href="../vehicle/insert" class="btn pagination-centered center-block" role="button">
                                                            <i class="fa fa-car fa-fw fa-2x" aria-hidden="true"></i>
                                                            <br><?php echo $vehicle;?>
                                                    </a>
                                            </div>
                                        </div>
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4 vebko-padding-0">
                                            <div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
                                                    <a href="../electronics/insert" class="btn pagination-centered center-block" role="button">
                                                            <i class="fa fa-tv fa-fw fa-2x" aria-hidden="true"></i>
                                                            <br><?php echo $electronics;?>
                                                    </a>
                                            </div>
                                        </div>
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4 vebko-padding-0">
                                            <div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
                                                    <a href="../clothing/insert" class="btn pagination-centered center-block" role="button">
                                                            <i class="fa fa-users fa-fw fa-2x" aria-hidden="true"></i>
                                                            <br><?php echo $clothing;?>
                                                    </a>
                                            </div>
                                        </div>
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4 vebko-padding-0">
                                            <div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
                                                    <a href="../service/insert" class="btn pagination-centered center-block" role="button">
                                                            <i class="fa fa-bank fa-fw fa-2x" aria-hidden="true"></i>
                                                            <br><?php echo $services;?>
                                                    </a>
                                            </div>
                                        </div>
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4 vebko-padding-0">
                                            <div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
                                                    <a href="../other/insert" class="btn pagination-centered center-block" role="button">
                                                            <i class="fa fa-cubes fa-fw fa-2x" aria-hidden="true"></i>
                                                            <br><?php echo $other;?>
                                                    </a>
                                            </div>
                                        </div>
                                    </div> 
				</div>
			</div>
		</div>					                                           
	</div>
</div>