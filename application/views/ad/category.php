﻿<div class="container">
	<div  class="col-xs-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5">
		<div class="col-xs-12 vebkoMainCategoryAdd">
			<h4><i class="fa fa-search fa-fw fa-1x" aria-hidden="true"></i><?php echo $labelCategoryChoiceCategory;?></h4>			
			<div class="vebkoMainCategory col-xs-12 col-md-12 vebko-padding-0">
				<div class=" col-xs-12 vebkoMainCategoryContainer vebko-padding-0">	
					<div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
						<a href="../estate" class="btn pagination-centered center-block" role="button">
							<i class="fa fa-building fa-fw fa-2x" aria-hidden="true"></i>
							<br><?php echo $real_estate;?><?php echo $getCountry;?>
						</a>
					</div>
					<div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
						<a href="../vehicle" class="btn pagination-centered center-block" role="button">
							<i class="fa fa-car fa-fw fa-2x" aria-hidden="true"></i>
							<br><?php echo $vehicle;?>
						</a>
					</div>
					<div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
						<a href="ad/searchcar" class="btn pagination-centered center-block not-active" role="button">
							<i class="fa fa-tv fa-fw fa-2x" aria-hidden="true"></i>
							<br><?php echo $electronics;?>
						</a>
					</div>
					<div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
						<a href="ad/searchcar" class="btn pagination-centered center-block not-active" role="button">
							<i class="fa fa-users fa-fw fa-2x" aria-hidden="true"></i>
							<br><?php echo $clothing;?>
						</a>
					</div>
					<div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
						<a href="ad/searchcar" class="btn pagination-centered center-block not-active" role="button">
							<i class="fa fa-bank fa-fw fa-2x" aria-hidden="true"></i>
							<br><?php echo $services;?>
						</a>
					</div>
					<div class="vebkoMainCategoryOne  col-xs-6 col-md-4  vebko-padding-0">
						<a href="ad/searchcar" class="btn pagination-centered center-block not-active" role="button">
							<i class="fa fa-cubes fa-fw fa-2x" aria-hidden="true"></i>
							<br><?php echo $jobs;?>
						</a>
					</div>
				</div>
			</div>
		</div>					                                           
	</div>
</div>