﻿<div class="vebkoInsertCar container vebko-padding-0" id="container">
	<div class= "col-md-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5">
		<div class="seacrhBoxContainer col-md-12 col-md-offset-0 vebko-padding-0">        
			<?php $attributes = array("name" => "form");
				echo form_open_multipart("Vehicle/updateSave", $attributes);?>
				<?php echo form_input(array('id' => 'ads_id','value' => $ads_id,'class' => 'form-control', 'name' => 'ads_id')); ?>
					<div class="vebkoBox col-xs-12">
						<div class="vebkoBoxLabel vebkoBoxTitle col-xs-12 vebko-padding-0">
							<?php echo $labelUpdateAds;?>
							<div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
							<?php echo $labelInsertAdsDescription;?>
							</div>
						</div>
					</div>
					
					<div class="vebkoBox col-xs-12">
						<div class="vebkoBoxLabel col-xs-12">
							<?php echo $labelUpdateAds2;?>
						</div>					
						<div class="form-group col-xs-12 col-md-10 vebko-padding-0">
							<?php echo form_label($labelInsertAdsTitle); ?>
							<?php echo form_error('title'); ?><br />
							<?php echo form_input(array('id' => 'title','value' => $title,'class' => 'form-control', 'name' => 'title','required'=>'required')); ?><br />
						</div>
						<div class="form-group col-xs-12 col-md-2 vebko-padding-0 vebko-padding-left-5">
							<?php echo form_label($labelInsertAdsPrice); ?><br />
							<?php echo form_input(array('id' => 'price','value' => $ads_vehicles_price,'class' => 'form-control', 'name' => 'price','required'=>'required','type'=>'number')); ?><br />
						</div>
						<?php echo form_label($labelInsertAdsBody); ?> <?php echo form_error('adsbody'); ?><br />
						<?php echo form_textarea(array('id' => 'adsbody','value' => $body,'class' => 'form-control', 'name' => 'adsbody','required'=>'required')); ?><br />	
					
					</div>
					
					
					<div class="vebkoBox col-xs-12">
						<div class="vebkoBoxLabel col-xs-12">
							<?php echo $labelInsertInsertVehicleDetails;?>
						</div>
						<div class="form-group col-md-3 vebko-padding-0 vebko-padding-right-5">
							<?php echo form_label($labelInsertInsertVehicleCategory); ?><br />
							<?php $attributes = 'id="ads_vehicles_type_id" class="form-control"';
							echo form_dropdown('ads_vehicles_type', $ads_vehicles_type, $ads_vehicles_type_id_selected, $attributes); ?>
						</div>
						<div class="form-group col-md-3 vebko-padding-0 vebko-padding-right-5">
							<?php echo form_label($labelInsertInsertVehicleBrand); ?><br />
							<?php $attributes = 'id="ads_vehicles_brand_id" class="form-control"';
							echo form_dropdown('ads_vehicles_brand', $ads_vehicles_brand, $ads_vehicles_brand_id_selected, $attributes); ?>
						</div>
						<div class="form-group col-md-3 vebko-padding-0 vebko-padding-left-5">
							<?php echo form_label($labelInsertInsertVehicleModel); ?><br />
							<?php $attributes = 'id="ads_vehicles_model_id" class="form-control" required';
							echo form_dropdown('ads_vehicles_model', $ads_vehicles_model, $ads_vehicles_model_id_selected, $attributes); ?>
						</div>
						<div class="form-group col-md-3 vebko-padding-0 vebko-padding-left-5">
							<?php echo form_label($labelInsertInsertVehicleBuySell); ?><br />
							<?php $attributes = 'id="ads_vehicles_bye_sell_id" class="form-control"';
							echo form_dropdown('ads_vehicles_bye_sell', $ads_vehicles_bye_sell, $ads_vehicles_bye_sell_id_selected, $attributes); ?>
						</div>
						<div class="form-group col-md-3 vebko-padding-0">
						<?php echo form_label($labelInsertInsertVehicleFuel); ?><br />
							<?php $attributes = 'id="ads_vehicles_fuel_id" class="form-control"';
							echo form_dropdown('ads_vehicles_fuel', $ads_vehicles_fuel, $ads_vehicles_fuel_id_selected, $attributes); ?>
						</div>
						
						<div class="form-group col-md-3 vebko-padding-0 vebko-padding-left-5">
							<?php echo form_label($labelInsertInsertVehicleYear); ?><br />
							<?php echo form_input(array('id' => 'year','value' => $year,'class' => 'form-control', 'name' => 'year','type'=>'number')); ?><br />
						</div>
						<div class="form-group col-md-3 vebko-padding-0 vebko-padding-left-5">
							<?php echo form_label($labelInsertInsertVehicleКm); ?><br />
							<?php echo form_input(array('id' => 'km','value' => $km,'class' => 'form-control', 'name' => 'km','type'=>'number')); ?><br />
						</div>
					</div>
					
					
					
					
					<div class="vebkoBox col-xs-12"> <!-- gallery -->
						<div class="vebkoBoxLabel col-xs-12">
							<?php echo $labelInsertInsertImages;?>
							<div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
								<?php echo $labelInsertInsertImagesDescription;?>
							</div>
						</div>					
						<div class="images col-xs-12 col-md-12 vebko-padding-0">
							<div class="imageGallery col-xs-12 col-md-12 vebko-padding-0">								
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php for($i = 1; $i < 7; $i ++){?>
									<div id="previewImageBox-<?php echo $i;?>" class="previewImageBox col-xs-6 col-md-2 vebko-padding-0">						 
										<div class="previewImage col-xs-12 col-md-12 vebko-padding-0 vebko-padding-right-5">
											<img id="previewImage-g<?php echo $i;?>" src="<?php echo $galleryImage[$i]; ?>" alt=" " />
										</div>
										<div class="uploadImage col-xs-12 col-md-12 vebko-padding-0">
											<input type="file" id="userfile-g<?php echo $i;?>" name="userfile-g<?php echo $i;?>" size="20" title="dd" <?php if($i == 1){ /*echo 'required';*/}?>>									
											<button type="button" id="deleteImage-g<?php echo $i;?>" class="btn btn-secondary"><?php echo $labelInsertInsertImagesDelete;?></button>
										</div>
									</div>
									<div class="hidden"><?php echo form_input(array('id' => 'imageDeleteTrue-'.$i,'value' => 1,'class' => 'form-control', 'name' =>'imageDeleteTrue-'.$i,'type'=>'number')); ?></div>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
					<div class="vebkoBox col-xs-12">
						<div class="vebkoBoxLabel col-xs-12">
							<?php echo $labelContact;?>
						</div>						
						<div class="form-group col-xs-12 vebko-padding-0 searchyear">
							<div class="form-group col-xs-12 col-md-4 vebko-padding-0 vebko-padding-0 vebko-padding-left-5">
								<?php echo form_label($labelContactName); ?><br />
								<?php echo form_input(array('id' => 'contactName','value'=>$valueUserName, 'class' => 'form-control', 'name' => 'contactName','required'=>'required')); ?><br />
							</div>
							<div class="form-group col-xs-12 col-md-4 vebko-padding-0 vebko-padding-0 vebko-padding-left-5">
								<?php echo form_label($labelContactEmail); ?><br />
								<?php echo form_input(array('id' => 'contactEmail','value'=>$contactEmail, 'class' => 'form-control', 'name' => 'contactEmail')); ?><br />
							</div>
							<div class="form-group col-xs-12 col-md-4 vebko-padding-0 vebko-padding-0 vebko-padding-left-5">
								<?php echo form_label($labelPhone); ?><br />
								<?php echo form_input(array('id' => 'phone','value'=>$phone,'class' => 'form-control', 'name' => 'phone','type'=>'number')); ?><br />
							</div>
							
							<div class="form-group col-xs-12 col-md-3 searchyearend vebko-padding-0 vebko-padding-left-5">
								<div class="col-md-12 vebko-padding-0">
									<?php echo form_label($labelCountry); ?><br />
								</div>
								<?php $attributes = 'id="address_country_id" class="form-control"';
								echo form_dropdown('address_country', $address_country, $address_country_id_selected, $attributes); ?>
							</div>
							<div class="form-group col-xs-12 col-md-3 searchyearend vebko-padding-0 vebko-padding-left-5">
								<div class="col-md-12 vebko-padding-0">
									<?php echo form_label($labelCountryRegion); ?><br />
								</div>
								<?php $attributes = 'id="address_country_region_id" class="form-control"';
								echo form_dropdown('address_country_region', $address_country_region, $address_country_region_id_selected, $attributes); ?>
							</div>
							<div class="form-group col-xs-12 col-md-3 searchyearend vebko-padding-0 vebko-padding-left-5">
								<div class="col-md-12 vebko-padding-0">
									<?php echo form_label($labelCountryMunicipality); ?><br />
								</div>
								<?php $attributes = 'id="address_country_municipality_id" class="form-control"';
								echo form_dropdown('address_country_municipality', $address_country_municipality, $address_country_municipality_id_selected, $attributes); ?>
							
							</div>							
							
							<div class="form-group col-xs-12 col-md-3 vebko-padding-0 vebko-padding-0 vebko-padding-left-5">
								<?php echo form_label($labelStreet); ?><br />
								<?php echo form_input(array('id' => 'street','value'=>$street,'class' => 'form-control', 'name' => 'street')); ?><br />
							</div>
							
						</div>
					</div>
					<div class="col-xs-12 vebko-padding-0">
						<div class="col-xs-12 vebko-padding-0 col-md-8">
							<?php //echo $labelAdsAgree;?>
						</div>
						<div class="col-xs-12 vebko-padding-0 col-md-4">
							<div class="form-group col-xs-12 col-md-6">
								<a class="btn  btn-danger btn-block" href="<?php echo base_url(); ?>vehicle/view/<?php echo $ads_id; ?>"><?php echo $labelAdsCancelButtonText;?></a>
							</div>							
							<div class="insertButtonSubmit form-group col-xs-12 col-md-6">
							<button name="submit" type="submit" id="insertButton" class="btn  btn-success btn-block"><?php echo $labelAdsUpdateButtonText;?></button>
							</div>
						</div>
					</div>
			<?php echo form_close(); ?>
		</div>
		<div id="fugo">
	</div>
</div>




<script type="text/javascript">
$(document).ready(function() {
	$('#adsbody').summernote({
	  toolbar: [
		// [groupName, [list of button]]
		['style', ['bold', 'italic', 'underline']],
		['para', ['ul', 'ol']]
	  ]
	});
});

$("#insertButton").click(function(){
		//$( ".vebkoInsertCar").hide();
		//$( ".vebkoInsertCarLoading").show();
});


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
	
}


$("#userfile").change(function(){
    readURL(this);
});



$('#userfile-g1').attr('title', '');
//$( "#previewImageBox-2" ).hide();
//$( "#previewImageBox-3" ).hide();
//$( "#previewImageBox-4" ).hide();
//$( "#previewImageBox-5" ).hide();
//$( "#previewImageBox-6" ).hide();
$( "#deleteImage-g1" ).hide();


$("#userfile-g1").change(function(i){
		readURLImage1(this);
		$( '#userfile-g1').hide();
		$('#deleteImage-g1').show();
		$('#previewImageBox-2').show();
	});

	$("#deleteImage-g1").click(function(){
		$('#previewImage-g1').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g1").hide();
		$( "#userfile-g1").show();
	});

function readURLImage1(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g1').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
	
}
<!-- start single-->
//$( "#deleteImage-g2" ).hide();
$("#userfile-g2").change(function(i){
		$('#imageDeleteTrue-2').val('1');
		readURLImage2(this);
		$( '#userfile-g2').hide();
		$('#deleteImage-g2').show();
		$('#previewImageBox-3').show();
	});

	$("#deleteImage-g2").click(function(){
		$('#imageDeleteTrue-2').val('0');
		$('#previewImage-g2').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g2").hide();
		$( "#userfile-g2").show();
	});

function readURLImage2(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }	
}
<!-- end single-->
<!-- start single-->
//$( "#deleteImage-g3" ).hide();
$("#userfile-g3").change(function(i){
		$('#imageDeleteTrue-3').val('1');
		readURLImage3(this);
		$( '#userfile-g3').hide();
		$('#deleteImage-g3').show();
		$('#previewImageBox-4').show();
	});

	$("#deleteImage-g3").click(function(){
		$('#imageDeleteTrue-3').val('0');
		$('#previewImage-g3').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g3").hide();
		$( "#userfile-g3").show();
	});

function readURLImage3(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g3').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }	
}
<!-- end single-->
<!-- start single-->
//$( "#deleteImage-g4" ).hide();
$("#userfile-g4").change(function(i){
		$('#imageDeleteTrue-4').val('1');
		readURLImage4(this);
		$( '#userfile-g4').hide();
		$('#deleteImage-g4').show();
		$('#previewImageBox-5').show();
	});

	$("#deleteImage-g4").click(function(){
		$('#imageDeleteTrue-4').val('0');
		$('#previewImage-g4').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g4").hide();
		$( "#userfile-g4").show();
	});

function readURLImage4(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g4').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }	
}
<!-- end single-->
<!-- start single-->
//$( "#deleteImage-g5" ).hide();
$("#userfile-g5").change(function(i){
		$('#imageDeleteTrue-5').val('1');
		readURLImage5(this);
		$( '#userfile-g5').hide();
		$('#deleteImage-g5').show();
		$('#previewImageBox-6').show();
	});

	$("#deleteImage-g5").click(function(){
		$('#imageDeleteTrue-5').val('0');
		$('#previewImage-g5').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g5").hide();
		$( "#userfile-g5").show();
	});

function readURLImage5(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g5').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }	
}
<!-- end single-->
<!-- start single-->
//$( "#deleteImage-g6" ).hide();
$("#userfile-g6").change(function(i){
		$('#imageDeleteTrue-6').val('1');
		readURLImage6(this);
		$( '#userfile-g6').hide();
		$('#deleteImage-g6').show();
	});

	$("#deleteImage-g6").click(function(){
		$('#imageDeleteTrue-6').val('0');
		$('#previewImage-g6').attr('src', 'http://'+$(location).attr('hostname')+'/images/defaultcar.jpg');
		$( "#deleteImage-g6").hide();
		$( "#userfile-g6").show();
	});

function readURLImage6(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImage-g6').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }	
}
<!-- end single-->




$('#ads_vehicles_type_id').change(function(){
    var ads_vehicles_type_id = $(this).val();
	
    $("#ads_vehicles_brand_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Vehicle/searchcar_brand'); ?>",
        data: {id: ads_vehicles_type_id},
        dataType: 'json',
        success:function(data){
            $.each(data,function(k, v){
                var opt = $('<option />');
                opt.val(k);
                opt.text(v);
                $('#ads_vehicles_brand_id').append(opt);
            });
            //$('#city').html('<option value="0">Select City</option>');
            //$('#state').append('<option value="' + id + '">' + name + '</option>');
        }
    });
});

$('#ads_vehicles_brand_id').change(function(){
    var ads_vehicles_brand_id = $(this).val();
    $("#ads_vehicles_model_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Vehicle/searchcar_model'); ?>",
        data: {id: ads_vehicles_brand_id},
        dataType: 'json',
        success:function(data){
            $.each(data,function(k, v){
                var opt = $('<option />');
                opt.val(k);
                opt.text(v);
                $('#ads_vehicles_model_id').append(opt);
            });
        }
    });
});

$('#address_country_id').change(function(){
    var address_country_id = $(this).val();
    $("#address_country_region_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Vebko/search_address_country_region'); ?>",
        data: {id: address_country_id},
        dataType: 'json',
        success:function(data){
            $.each(data,function(k, v){
                var opt = $('<option />');
                opt.val(k);
                opt.text(v);
                $('#address_country_region_id').append(opt);
            });
        }
    });
});


$('#address_country_region_id').change(function(){
    var address_country_region_id = $(this).val();
    $("#address_country_municipality_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Vebko/search_address_country_municipality'); ?>",
        data: {id: address_country_region_id},
        dataType: 'json',
        success:function(data){
            $.each(data,function(k, v){
                var opt = $('<option />');
                opt.val(k);
                opt.text(v);
                $('#address_country_municipality_id').append(opt);
            });
        }
    });
});

</script>

