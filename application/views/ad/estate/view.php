
<div class="viewcar vebko-padding-0">
	<div class="container  vebko-padding-0">
		<div class="viewCarBox vebko-padding-0 col-xs-12 col-md-9">
			
			<div class="viewCarBoxConteiner col-xs-12">	
				<?php if($ads_marking == 1){?>
				<div class="viewCarBoxMessage col-xs-12 vebko-padding-0">
					<div class="alert alert-warning">
						<strong><?php echo $ads_marking_decativate; ?></strong>
					</div>				
				</div>
				<?php }?>				
				<div class="viewCarBoxFirst col-xs-12 vebko-padding-0">					
                                    <div class="viewCarBoxGallery col-xs-12 col-md-12 hidden-xs1 vebko-padding-0">						
                                            <!--<section id="gallery" class="simplegallery">
                                                    <div class="content col-md-12 vebko-padding-0">												    
                                                            <?php foreach ($gallery_images as $key => $value) {?>
                                                                    <img src="<?php echo $value; ?>" class="image_<?php echo $key; ?>" style="<?php if($key>0){ ?>display:none<?php } ?>" alt="" />
                                                            <?php } ?>
                                                    </div>
                                                    <div class="thumbnail col-md-12 vebko-padding-0">								
                                                            <?php foreach ($gallery_images as $key => $value) {?>
                                                                    <div class="thumb col-xs-2 vebko-padding-0">
                                                                            <a href="#" rel="<?php echo $key; ?>">
                                                                                    <img src="<?php echo $value; ?>" id="thumb_<?php echo $key; ?>" alt="" />
                                                                            </a>
                                                                    </div>    
                                                            <?php } ?>					
                                                    </div>
                                            </section>-->

                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <?php foreach ($gallery_images as $key => $value) {?>        
                                                    <div class="swiper-slide">
                                                        <div class="swiper-zoom-container">
                                                          <img src="<?php echo $value; ?>">
                                                        </div>
                                                    </div>     
                                                <?php } ?>
                                                    <div class="swiper-slide">
                                                        <div class="swiper-zoom-container">
                                                           <div class="vebkoBoxBodyLocationMaps col-xs-12 col-md-12 vebko-padding-0 vebko-padding-right-5">
								<?php echo $map['js']; ?><?php echo $map['html']; ?>
                                                            </div> 
                                                        </div>
                                                    </div> 
                                            </div>
                                            <div class="swiper-pagination"></div>
                                        </div>
                                    </div>
				</div>		
				
				<div class="viewCarBoxSecond col-xs-12 vebko-padding-0">
					<div class="viewCarBoxTitle col-xs-12 vebko-padding-0" data-offset-top="50">					
						<div class="viewCarHeader col-xs-12  vebko-padding-0">
                                                        <div class="col-xs-9 col-md-10 vebko-padding-0">
                                                            <div class="viewCarTitle col-xs-12 col-md-12 vebko-padding-0">
                                                                    <?php echo $ads_title; ?>
                                                            </div>
                                                            <div class="viewCarDate col-xs-12  vebko-padding-0">
                                                                    <?php echo $labelVehiclePublishedDate; ?><?php echo $ads_date_modify; ?>
                                                            </div>
                                                        </div>
                                                        <div class="viewCarPrice col-xs-3 col-md-2 vebko-padding-0">
                                                             <div class="viewCarPriceEuro col-xs-12 col-md-12 vebko-padding-0">
								<?php echo number_format($price); ?> €
                                                            </div>
                                                             <div class="viewCarPriceCurrency col-xs-12 col-md-12 vebko-padding-0">
								(<?php echo $priceMkd; ?>)
                                                            </div>								
							</div>				
                                                       
						</div>
					</div>
					
					<div class="viewCarBody col-xs-12 vebko-padding-0 ">
						<div class="adsCarBodyDescription vebko-padding-0">
							<?php echo $body; ?>
						</div>
						
						
					</div>
				
					
				</div>
				
				
				<div class="viewCarPerformance col-xs-12 col-xs-12 col-md-12 vebko-padding-0">					
					<div class = "subBox  col-xs-12 col-xs-12 vebko-padding-0">
						<div class="vebkoSubTitle col-xs-12 col-md-12 vebko-padding-0">
							<?php echo $labelVehicleSpecification; ?>
						</div>
						<div class="viewCarInfo col-xs-12 col-md-12 vebko-padding-0">
							<div class="col-xs-12 col-md-6 vebko-padding-0">
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php //echo $labelContactName; ?> <b><?php //echo $ads_buy_sell; ?></b>
								</div>
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php echo $labelEstateType; ?> <b><?php echo $ads_type; ?> </b>
								</div>
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php echo $labelRoomNumber; ?> <b><?php echo $roomNumber; ?> </b>
								</div>
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php echo $labelMetro; ?> <b><?php echo $metro; ?> </b>
								</div>
								
							</div>
							<div class="col-xs-12 col-md-6 vebko-padding-0">
								<div class="col-xs-12 col-md-12 vebko-padding-0">
									<?php echo $labelVehicleYear; if($year!=0){echo '<b>'.$year.'</b>';} else {echo '<b>'.$labelVehicleNoData.'</b>';}?>
								</div>
							</div>
						</div>
					</div>
					<div class = "subBox subBox-2 col-xs-12 col-xs-12 vebko-padding-0">
						<div class="vebkoSubTitle col-xs-12 col-md-12 vebko-padding-0">
							<?php echo $labelLocation; ?>
						</div>
						<div class="viewCarInfo col-xs-12 col-md-12 vebko-padding-0">
							
							<div class="vebkoBoxBodyLocationData col-xs-12 col-md-12 vebko-padding-0">
								<div class="col-xs-12 col-md-3 vebko-padding-0">
									<?php echo $labelViewStreet; if($address_street){echo '<b>'.$address_street.'</b>';} else {echo '<b>'.$labelVehicleNoData.'</b>';}?>
								</div>
								<div class="col-xs-12 col-md-3 vebko-padding-0">								
									<?php echo $labelViewMunicipality;?><b><?php echo $ads_address_country_municipality; ?></b>
								</div>
								<div class="col-xs-12 col-md-3 vebko-padding-0">								
									<?php echo $labelViewRegion;?><b><?php echo $ads_address_country_region; ?></b>
								</div>
								<div class="col-xs-12 col-md-3 vebko-padding-0">								
									<?php echo $labelViewCountry;?><b><?php echo $ads_address_country; ?></b>
								</div>
							</div>
                                                        <!--<div class="vebkoBoxBodyLocationMaps col-xs-12 col-md-12 vebko-padding-0 vebko-padding-right-5">
								<?php //echo $map['js']; ?><?php //echo $map['html']; ?>
							</div>-->
						</div>
					</div>
					<div class = "subBox subBox-2 col-xs-12 col-xs-12 vebko-padding-0">							
						<div class="vebkoSubTitle col-xs-12 col-md-12 vebko-padding-0">
							<?php echo $labelContactPerson; ?>
						</div>
						<div class="viewCarInfo col-xs-12 col-md-12 vebko-padding-0">
							<div class="col-xs-12 col-md-12 vebko-padding-0">								
								<div class="col-xs-12 col-md-4 vebko-padding-0">
									<?php //echo $labelContactName; ?> <b><?php echo $contactName; ?></b>
								</div>
								<div class="col-xs-12 col-md-4 vebko-padding-0">
									<?php echo $labelPhone; if($contact_phone!=0){echo '<b>'.$contact_phone_text.'</b>';} else {echo '<b>'.$labelVehicleNoData.'</b>';}?>
								</div>
								<div class="col-xs-12 col-md-4 vebko-padding-0">
									<?php echo $labelEmail; if($contact_email!='0'){echo '<b>'.$contact_email.'</b>';} else {echo '<b>'.$labelVehicleNoData.'</b>';}?>
								</div>
							</div>
							
						</div>															
					</div>
					<div class = "subBox subBox-3 col-xs-12 col-xs-12 vebko-padding-0">
						<div class="vebkoSubTitle col-xs-12 col-md-12 vebko-padding-0">
							<?php echo $labelViewShare; ?>
						</div>
						<div class="vebkoSubBody col-xs-6 vebko-padding-0">
							<!-- AddToAny BEGIN -->
							<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
								<a class="a2a_button_facebook"></a>
								<a class="a2a_button_google_plus"></a>
								<a class="a2a_button_twitter"></a>
							</div>
							<script async src="https://static.addtoany.com/menu/page.js"></script>
							<!-- AddToAny END -->
						</div>
						
						<div class="vebkoSubPhone col-xs-6 vebko-padding-0 vebko-padding-right-5">
							<div class="adsContactPhoneCall visible-xs-block">
								<a class="btn btn-success" href="tel:<?php echo $contact_phone; ?>"><i class="fa fa-phone fa-fw fa-1x" aria-hidden="true"></i> <?php echo $contact_phone_text; ?></a>
							</div>
						</div>						
					</div>
				</div>
				<?php if ($checkhAdsOwner==1){ ?>
				<div class="viewCarBoxSettings col-xs-12 vebko-padding-0">
					<div class="vebkoSubTitle col-xs-12 col-md-12 vebko-padding-0">
						<?php echo $labelViewSettings; ?>
					</div>
					<div class="vebkoSubBody col-xs-12 vebko-padding-0">
						<div class="col-xs-4 col-md-4 vebko-padding-3">
							<div class="col-xs-12 col-md-12 vebko-padding-0">
								<?php if($ads_marking == 1){?>
									<a class="btn btn-info col-xs-12" href="../activate/<?php echo $ads_id; ?>"><?php echo $labelViewSettingsActivate; ?></a>
								<?php }?>
								<?php if($ads_marking == 0){?>
									<a class="btn btn-info col-xs-12" href="../deactivate/<?php echo $ads_id; ?>"><?php echo $labelViewSettingsDeaktivate; ?></a>
								<?php }?>
							</div>
						</div>
						<div class="col-xs-4 col-md-4 vebko-padding-3">
							<div class="col-xs-12 col-md-12 vebko-padding-0">
								<a class="btn btn-info col-xs-12" href="../update/<?php echo $ads_id; ?>"><?php echo $labelViewSettingsChange; ?></a>
							</div>
						</div>
						<div class="col-xs-4 col-md-4 vebko-padding-3">
							<div class="col-xs-12 col-md-12 vebko-padding-0">
								<a class="btn btn-info col-xs-12" href="../renew/<?php echo $ads_id; ?>"><?php echo $labelViewSettingsRenew; ?></a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="viewCarBox vebko-padding-0 col-xs-12 col-md-3">
		<div class="viewCarBoxConteiner col-xs-12">
			<?php if($marketingCount>0){?>
				<div class="vebkoBox col-xs-12">
					<span class="vebkoBoxLabelMarketingTitle col-xs-12 vebko-padding-0"><?php echo $labelMarketingTitle; ?></span>
					<a href="<?php echo $marketingSingle->url; ?>" target="_blank">
						<img src="<?php echo $marketingSingle->image; ?>" alt="HTML tutorial" style="width:100%;height:auto;border:0;">
					</a>
				</div>
			<?php }?>
			<div class="vebkoBox col-xs-12">
				
					<a class=" btn btn-info col-xs-12" href="<?php echo base_url(); ?><?php echo $adt;?>/insert"><?php echo $labelViewAdd;?></a>
				
			</div>
			<div class="vebkoBox col-xs-12">
				<div class="vebkoBoxLabel col-xs-12">
					<?php echo $labelViewSimilarAds;?>
				</div>
				<div class="vebkoBoxBody col-xs-12 vebko-padding-0">
					<?php foreach ($suggestions as $key => $value) {?>							
							<div class="vebkoSeachSingleBox col-xs-12 col-md-12 vebko-padding-0">
								<a href="<?php echo base_url(); ?><?php echo $adt;?>/view/<?php echo $value->id; ?>"> 
									<div class="col-xs-4 col-md-4 vebko-padding-0">
										<img src="<?php echo base_url(); ?>images/ads/<?php echo $adt;?>/ad_<?php echo sprintf('%08d',$value->id); ?>/main-<?php echo $value->ads_gallery_image_name_1; ?>"/>
										<?php //echo $value->id; ?>
									</div>
									<div class="vebkoSeachSingleBoxTitle col-xs-8 col-md-8 vebko-padding-0 vebko-padding-left-5">
										<?php echo $value->title; ?>
									</div>
								</a>
							</div>							
					<?php } ?>
				</div>
			</div>
		</div></div>
	</div>
</div>

  <!-- Initialize Swiper -->
 <script>
    
</script>



 