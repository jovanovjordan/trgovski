﻿<?php ?>

<div class="seacrhHeader">
	<div class="container">
		<!--<div class="seacrhHeaderTitle">
			<h3 col-md-4 col-md-offset-2><i class="fa fa-car fa-fw fa-1x" aria-hidden="true"></i> Возила</h3>
		</div>-->
	</div>
</div>
<div class="seacrhMain">
	<div id="loading" class="vebko-padding-0">
		<img src="<?php echo base_url();?>/images/loading.gif" alt="No image" height="auto" width="100%">				
	</div>			
	<div class="container">
		<!--<div id="seacrhBoxShow">
			<div id="seacrhBoxShowContainer">
				<i class="fa fa-search hidden-md hidden-lg"></i>
			</div>
		</div>-->
		<div id="search" class="vebkoBox seacrhMainLeft col-xs-12 col-md-12 ">			
			<div id="seacrhBox" class="col-xs-12 col-md-12 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 ">
				<div class="seacrhBoxContainer">        
						<div id="searchBoxTitle" class="vebkoSearchBoxLabel col-md-12  vebko-padding-0">
                                                    <div class="searchBoxLeft col-xs-11 vebko-padding-0">						
                                                            <?php echo $labelSearchEstate;?>
                                                            <div class="vebkoBoxLabelDescription col-xs-12 vebko-padding-0">
                                                                    <?php echo $labelSearchEstateDescritpion;?>
                                                            </div>
                                                    </div>
                                                    <div class="searchBoxRight col-xs-1 vebko-padding-0 ">
                                                            <div id="vebkoCollapse" class="btn btn-info pull-right"><i class="fa fa-search" aria-hidden="true"></i></div>
                                                    </div>
							<div id="queryRowStart">0</div>
                                                        

						</div>
						<div id="searchBoxBody" class="vebkoSearchBoxBody col-xs-12 vebko-padding-0 scrollmenu">
						<?php $attributes = array("name" => "form");
							echo form_open("", $attributes);?>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
								<div class="col-md-12 vebko-padding-0">
									<?php echo $country;?>
								</div>
								<?php $attributes = 'id="address_country_id" class="form-control"';
								echo form_dropdown('address_country', $address_country, $address_country_id, $attributes); ?>
							</div>
                                                        <div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
								<div class="col-md-12 vebko-padding-0">
									<?php echo $region;?>
								</div>
								<?php $attributes = 'id="address_country_region_id" class="form-control"';
								echo form_dropdown('address_country_region', $address_country_region, set_value('address_country_region'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
								<div class="col-md-12 vebko-padding-0">
									<?php echo $municipality;?>
								</div>
								<?php $attributes = 'id="address_country_municipality_id" class="form-control"';
								echo form_dropdown('address_country_municipality', $address_country_municipality, set_value('address_country_municipality'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
								<?php echo $labelEstate;?>
								<?php $attributes = 'id="ads_estate_type_id" class="form-control"';
								echo form_dropdown('ads_estate_type', $ads_estate_type, set_value('ads_estate_type'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
								<div class="col-xs-12 col-md-12 vebko-padding-0">
										<?php echo $buySell;?>
								</div>
								<?php $attributes = 'id="ads_vehicles_bye_sell_id" class="form-control"';
								echo form_dropdown('ads_vehicles_bye_sell', $ads_vehicles_bye_sell, set_value('ads_vehicles_bye_sell'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
							<?php echo $yearfrom;?>
								<?php $attributes = 'id="ads_vehicles_year_start_id" class="form-control"';
								echo form_dropdown('ads_vehicles_year', $ads_vehicles_year, set_value('ads_vehicles_year'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
							<?php echo $yearto;?>
								<?php $attributes = 'id="ads_vehicles_year_end_id" class="form-control"';
								echo form_dropdown('ads_vehicles_year', $ads_vehicles_year, set_value('ads_vehicles_year'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
							<?php echo $pricefrom;?>
								<?php $attributes = 'id="ads_vehicles_price_start_id" class="form-control"';
								echo form_dropdown('ads_vehicles_price', $ads_vehicles_price, set_value('ads_vehicles_price'), $attributes); ?>
							</div>
							<div class="scrollMenuSingle col-md-2 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 vebko-padding-top-5 vebko-padding-bottom-5">
							<?php echo $priceto;?>
								<?php $attributes = 'id="ads_vehicles_price_end_id" class="form-control"';
								echo form_dropdown('ads_vehicles_price', $ads_vehicles_price, set_value('ads_vehicles_price'), $attributes); ?>
							</div>
												
							<!--<div class="col-xs-6 col-md-6 vebko-padding-0 vebko-padding-left-5 vebko-padding-top-5 vebko-padding-bottom-5">
								<div class="col-xs-12 col-md-4 pull-right vebko-padding-0 vebko-padding-left-5 vebko-padding-top-5 vebko-padding-bottom-5">
									<button name="submit" type="submit" id="serachButton" class="btn btn-info btn-block"><?php echo $search;?></button>
								</div>
							</div>-->
							<?php echo form_close(); ?>
						</div>
				</div>
			</div>
		</div>
		
		<div id = "results" class="seacrhMainRight col-md-12 col-xs-12">
			<div id="searchListAds" class="searchListAds vebko-padding-0">
									
			</div>		
		</div>
		
		
		
		
	</div>
	
</div>




<script src="<?php echo base_url("assets/js/jquery-3.1.1.min.js"); ?>" type="text/javascript"></script>
<script type="text/javascript">



$('#address_country_id').change(function(){
    var address_country_id = $(this).val();
    $("#address_country_region_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Vebko/search_address_country_region'); ?>",
        data: {id: address_country_id},
        dataType: 'json',
        success:function(data){
            $.each(data,function(k, v){
                var opt = $('<option />');
                opt.val(k);
                opt.text(v);
                $('#address_country_region_id').append(opt);
            });
        }
    });
});
$('#address_country_region_id').change(function(){
    var address_country_region_id = $(this).val();
    $("#address_country_municipality_id > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('Vebko/search_address_country_municipality'); ?>",
        data: {id: address_country_region_id},
        dataType: 'json',
        success:function(data){
            $.each(data,function(k, v){
                var opt = $('<option />');
                opt.val(k);
                opt.text(v);
                $('#address_country_municipality_id').append(opt);
            });
        }
    });
});


//print search results - start
reload_Search_List();
$("#serachButton").click(reload_Search_List);
$(".seacrhBoxContainer div").on('change', function() {
  reload_Search_List();
})
function reload_Search_List() {
	$('#queryRowStart').html(function(i, val) { return 0 });// set counter to zero
	var address_country_region_id = 1;
	
	
	//variable from search
	var ads_estate_type_id 				= $( "#ads_estate_type_id" ).val();
	var ads_vehicles_year_start_id 		= $( "#ads_vehicles_year_start_id" ).val();
	var ads_vehicles_year_end_id		= $( "#ads_vehicles_year_end_id" ).val();
	var ads_vehicles_price_start_id 	= $( "#ads_vehicles_price_start_id" ).val();
	var ads_vehicles_price_end_id 		= $( "#ads_vehicles_price_end_id" ).val();
	var address_country_id 				= $( "#address_country_id" ).val();
	var address_country_region_id		= $( "#address_country_region_id" ).val();
	var address_country_municipality_id = $( "#address_country_municipality_id" ).val();
	var ads_vehicles_bye_sell_id		= $( "#ads_vehicles_bye_sell_id" ).val();
	
	

$(".searchListAds").empty();
$('.searchListAds').html('<div id="searchSingleAdHidden"></div>');
    $.ajax({
           type: "POST",
           url: "<?php echo site_url($adt.'/get_ads_vehicles'); ?>",
           data: {
				ads_estate_type_id: 				ads_estate_type_id,
				ads_vehicles_year_start_id: 		ads_vehicles_year_start_id,
				ads_vehicles_year_end_id: 			ads_vehicles_year_end_id,
				ads_vehicles_price_start_id: 		ads_vehicles_price_start_id,
				ads_vehicles_price_end_id: 			ads_vehicles_price_end_id,
				address_country_id: 				address_country_id,
				address_country_region_id: 			address_country_region_id,
				address_country_municipality_id: 	address_country_municipality_id,
				ads_vehicles_bye_sell_id: 			ads_vehicles_bye_sell_id
			},
		   dataType: 'json',
		   async: false,
        cache: false,
        timeout: 30000,
		   success:function(data){
				$.each(data,function(id, dataAd){
					
					var opt ='<div class="col-xs-12 col-md-6 searchSingleAd vebko-padding-0">'+
						'<a class="" href="<?php echo $adt;?>/view/'+dataAd[0]+'">'+
                                                    '<div class=" col-xs-12 vebko-padding-0">'+
                                                        '<div class="searchSingleAdMiddleTitle col-xs-9 col-md-10 vebko-padding-0">'+
                                                            dataAd[2]+
                                                        '</div>'+
                                                        '<div class="searchSingleAdRightPrice col-xs-3 col-md-2 vebko-padding-0">'+
                                                            dataAd[3]+' €'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="searchSingleAdMiddleDate col-xs-12 col-md-12 vebko-padding-0">'+
                                                        dataAd[4]+' / '+dataAd[5]+' / '+dataAd[7]+' / '+dataAd[8]+
                                                     '</div>'+
                                                    '<div class="searchSingleAdMiddle col-md-12 col-xs-12 vebko-padding-0">'+
                                                        '<div class="searchSingleAdLeft col-xs-5 col-md-4 vebko-padding-0">'+
                                                            '<img src="'+dataAd[1]+'" alt="No image" height="auto" width="100%">'+ 
                                                        '</div>'+
                                                        '<div class="searchSingleAdMiddleBody vebko-padding-0">'+
                                                                dataAd[6]+
                                                        '</div>'+					
                                                    '</div>'+
						'</a>'+							
					'</div>';
					
					$( "#startQueryRowId" ).text(dataAd[9]+8);
					$('#searchSingleAdHidden').append(opt);
					
				});
				
				
			}
		 });

	//$('html, body').animate({scrollTop: $('#searchListAds').offset().top - 60 }, 1200);
	
	 return false; // avoid to execute the actual submit of the form.
};
var counter = 0;
function counter(){
	counter++;
	$( "#startQueryRowId" ).text(dataAd[9]);
}

$('#loading').hide();	
$( document ).ajaxStart(function() {
    $( "#loading" ).show();
});
$( document ).ajaxStop(function() {
    $("#loading").hide();
});

$( document ).ready(function() {    
    $(window).scroll(function(){		
        if($(window).scrollTop() >= ($(document).height()-$(window).height())- 60)
        {
            reload_Search_List_Add();
        }
    });
});

function reload_Search_List_Add(){
	$('#queryRowStart').html(function(i, val) { return val*1+16 });
	//variable from search
	var ads_estate_type_id 				= $( "#ads_estate_type_id" ).val();
	var ads_vehicles_year_start_id 		= $( "#ads_vehicles_year_start_id" ).val();
	var ads_vehicles_year_end_id		= $( "#ads_vehicles_year_end_id" ).val();
	var ads_vehicles_price_start_id 	= $( "#ads_vehicles_price_start_id" ).val();
	var ads_vehicles_price_end_id 		= $( "#ads_vehicles_price_end_id" ).val();
	var address_country_id 				= $( "#address_country_id" ).val();
	var address_country_region_id		= $( "#address_country_region_id" ).val();
	var address_country_municipality_id = $( "#address_country_municipality_id" ).val();
	var ads_vehicles_bye_sell_id		= $( "#ads_vehicles_bye_sell_id" ).val();
	var queryRowStart = $( "#queryRowStart" ).text();
	

	
	
    $.ajax({
           type: "POST",
           url: "<?php echo site_url($adt.'/get_ads_vehicles'); ?>",
           data: {
				ads_estate_type_id: 				ads_estate_type_id,
				ads_vehicles_year_start_id: 		ads_vehicles_year_start_id,
				ads_vehicles_year_end_id: 			ads_vehicles_year_end_id,
				ads_vehicles_price_start_id: 		ads_vehicles_price_start_id,
				ads_vehicles_price_end_id: 			ads_vehicles_price_end_id,
				address_country_id: 				address_country_id,
				address_country_region_id: 			address_country_region_id,
				address_country_municipality_id: 	address_country_municipality_id,
				ads_vehicles_bye_sell_id: 			ads_vehicles_bye_sell_id,
				queryRowStart: queryRowStart
			},
		   dataType: 'json',
		   async: false,
        cache: false,
        timeout: 30000,
		   success:function(data){
				$.each(data,function(id, dataAd){
					
					var opt ='<div class="col-xs-12 col-md-6 searchSingleAd vebko-padding-0">'+
						'<a class="" href="<?php echo $adt;?>/view/'+dataAd[0]+'">'+
                                                    '<div class=" col-xs-12 vebko-padding-0">'+
                                                            '<div class="searchSingleAdMiddleTitle col-xs-9 col-md-10 vebko-padding-0">'+
                                                                dataAd[2]+
                                                            '</div>'+
                                                            '<div class="searchSingleAdRightPrice col-xs-3 col-md-2 vebko-padding-0">'+
                                                                dataAd[3]+' €'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="searchSingleAdMiddleDate col-xs-12 col-md-12 vebko-padding-0">'+
                                                        dataAd[4]+' / '+dataAd[5]+' / '+dataAd[7]+' / '+dataAd[8]+
                                                     '</div>'+
                                                    '<div class="searchSingleAdMiddle col-md-12 col-xs-12 vebko-padding-0">'+
                                                        '<div class="searchSingleAdLeft col-xs-5 col-md-4 vebko-padding-0">'+
                                                            '<img src="'+dataAd[1]+'" alt="No image" height="auto" width="100%">'+ 
                                                        '</div>'+
                                                        '<div class="searchSingleAdMiddleBody vebko-padding-0">'+
                                                                dataAd[6]+
                                                        '</div>'+					
                                                    '</div>'+
						'</a>'+							
					'</div>';
					
					$('#searchSingleAdHidden').append(opt);
				});
			}
		 });
}
</script>
