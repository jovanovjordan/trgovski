<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	</main><!-- #site-content -->

	<footer id="site-footer" role="contentinfo text-center">
		<div class = "container">
			<div class = "footerFirst col-xs-12 col-md-12 vebko-padding-0">
				<div class = "col-xs-12 col-md-4 vebko-padding-0">
					<?php echo anchor('Vebko/ln/mk', 'Македонски', 'class="link-class"') ?>
					<?php echo anchor('Vebko/ln/en', 'English', 'class="link-class"') ?>				
					<?php echo anchor('Vebko/ln/sr', 'Српски', 'class="link-class"') ?>
					<?php echo anchor('Vebko/ln/bg', 'Български', 'class="link-class"') ?>
				</div>
				<div class = "col-xs-12 col-md-4 vebko-padding-0">
					<?php //echo $country; ?>
					<?php echo anchor('Vebko/co/1', $Macedonia, 'class="link-class vebko-padding-left-5"') ?>
					<?php echo anchor('Vebko/co/2', $Serbia, 'class="link-class vebko-padding-left-5"') ?>
					<?php echo anchor('Vebko/co/3', $Bulgaria, 'class="link-class vebko-padding-left-5"') ?>
                                    <?php echo anchor('Vebko/co/4', $Greece, 'class="link-class vebko-padding-left-5"') ?>
				</div>
                                
                                <div class = "col-xs-12 col-md-4 vebko-padding-0">
					<?php //echo $currency; ?>                                        
					<?php echo anchor('Vebko/cu/eur', $eur, 'class="link-class vebko-padding-left-5"') ?>
					<?php echo anchor('Vebko/cu/mkd', $mkd, 'class="link-class vebko-padding-left-5"') ?>
					<?php echo anchor('Vebko/cu/rsd', $rsd, 'class="link-class vebko-padding-left-5"') ?>				
                                        <?php echo anchor('Vebko/cu/bgn', $bgn, 'class="link-class vebko-padding-left-5"') ?>
				</div>
                            
			</div>
			<div class = "footerCopyright">
				<?php echo $footerMessage;?>
			</div>
		</div>
	</footer><!-- #site-footer -->

	<!-- js -->
	
	<!--<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>-->
	
	<script type="text/javascript">
		$("#seacrhMainBottomBox").click(seacrhMainBottomBox);
		function seacrhMainBottomBox() {			
			
		}
		   
			$(".bottomButton").hide();
		   $(window).scroll(function() {

		if ($(this).scrollTop()>200)
			 {
				$('.bottomButton').fadeIn();
			 }
			else
			 {
			  $('.bottomButton').fadeOut();
			 }
		 });
	</script>
	
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-52945874-1', 'auto');
	  ga('send', 'pageview');

	</script>

</body>
</html>