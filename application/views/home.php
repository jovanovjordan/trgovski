<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="homepage">
    <div class="container vebko-padding-right-5 vebko-padding-left-5">
		<div class="vebkoMainText col-xs-12 col-md-7 vebko-padding-0 vebko-padding-right-5">
			<div class="vebkoMainTextLabel col-xs-12 col-md-12 vebko-padding-0">
				
			</div>
			<!--<div clas-s="vebkoMainBody col-xs-12 col-md-12 vebko-padding-0">			
				Креирај свој оглас или<br>
				пребарувај од веќе внесените огласи
			</div>-->
			<div class="vebkoMainTextImage col-xs-12 hidden-xs hidden-ms col-md-8 vebko-padding-0 vebko-padding-left-5 vebko-padding-right-5 text-center">	                             
				<div class = "col-xs-12 vebko-padding-10">
					<img src="images/map.gif" alt="">
				</div>            
				<div class = "col-xs-12 vebko-padding-10">
					<?php //echo $frontMessageMain;?>
				</div>
			</div>
		</div>
		<div class="vebkoMainCategory col-xs-12 col-md-5 vebko-padding-0">
			<div class=" col-xs-12 vebkoMainCategoryContainer vebko-padding-0">
                            
                            <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4  vebko-padding-0">
                                <div class="vebkoMainCategoryOne vebko-padding-0">
					<a href="estate" class="btn pagination-centered center-block" role="button">
						<i class="fa fa-building fa-fw fa-2x" aria-hidden="true"></i>
						<br><?php echo $real_estate;?>
					</a>
				</div>
                            </div>
				
			<div class="vebkoMainCategoryOneMain col-xs-6 col-md-4  vebko-padding-0">
                                <div class="vebkoMainCategoryOne vebko-padding-0">
					<a href="vehicle" class="btn pagination-centered center-block" role="button">
						<i class="fa fa-car fa-fw fa-2x" aria-hidden="true"></i>
						<br><?php echo $vehicle;?>
					</a>
				</div></div>
			
                            <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4  vebko-padding-0">
                                <div class="vebkoMainCategoryOne vebko-padding-0">
					<a href="electronics" class="btn pagination-centered center-block" role="button">
						<i class="fa fa-tv fa-fw fa-2x" aria-hidden="true"></i>
						<br><?php echo $electronics;?>
					</a>
				</div></div>
			
                                <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4  vebko-padding-0">
                                <div class="vebkoMainCategoryOne vebko-padding-0">
					<a href="clothing" class="btn pagination-centered center-block" role="button">
						<i class="fa fa-users fa-fw fa-2x" aria-hidden="true"></i>
						<br><?php echo $clothing;?>
					</a>
				</div></div>
			
                                    <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4  vebko-padding-0">
                                <div class="vebkoMainCategoryOne vebko-padding-0">
					<a href="service" class="btn pagination-centered center-block" role="button">
						<i class="fa fa-bank fa-fw fa-2x" aria-hidden="true"></i>
						<br><?php echo $services;?>
					</a>
				</div></div>
			
                                        <div class="vebkoMainCategoryOneMain col-xs-6 col-md-4  vebko-padding-0">
                                <div class="vebkoMainCategoryOne vebko-padding-0">
					<a href="other" class="btn pagination-centered center-block" role="button">
						<i class="fa fa-cubes fa-fw fa-2x" aria-hidden="true"></i>
						<br><?php echo $other;?>
					</a>
				</div></div>
			</div>			                                           
			<div class=" col-xs-12 vebkoMainCategoryAll vebko-padding-0">	
				<div class="vebkoMainCategoryAllContainer  col-xs-6 col-md-4  vebko-padding-0 col-xs-offset-3 col-md-offset-4">
					<a href="insert" class="btn pagination-centered center-block" role="button">
						<?php echo $insertAds;?>
					</a>
				</div>
			</div>
		</div>
		<div class="vebkoMainTextBoddom col-xs-12 col-md-12 vebko-padding-0">
				
				<!--Trgovski.com интернет огласник-->
			</div			
		
    </div><!-- .container -->
</div>

