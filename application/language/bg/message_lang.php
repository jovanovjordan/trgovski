<?php
$lang["language"] = "mk";

//front message
$lang["frontMessageMain"] 	= "Рекламирането на trgovski.com е безплатно и винаги ще остане безплатно.";

//menu
$lang["home"] 			= "Начало";
$lang["search"] 		= "Търси";
$lang["category"] 		= "Категории";
$lang["insertAds"] 		= "Поставете обява";
$lang["help"] 			= "Помощ";
$lang["setting"]                = "Обстановка";
$lang["about"]			= "Помогне";
$lang["contact"] 		= "Контакти";
$lang["login"] 			= "Вход";
$lang["labelMyAccount"] = "Моята сметка";
$lang["logout"] 		= "Изход";

//footer
$lang["footerMessage"] 						= "trgovski.com създаден от vebko.com - Всички права запазени";
$lang["labelCategoryChoiceCategory"] 		= "Изберете категория за търсен";
$lang["labelInsertCategoryChoiceCategory"] 	= "Изберете категория, в която ще внесите обявас";
$lang["real_estate"] 						= "Недвижими имоти";
$lang["vehicle"] 							= "превозни средства";
$lang["electronics"] 						= "електроника";
$lang["clothing"] 							= "дрехи";
$lang["services"] 							= "услуги";
$lang["jobs"] 								= "работа";
$lang["allcategories"] 						= "всички категории";

//time
$lang["before"] 	= "Преди";
$lang["second"] 	= "секунда";
$lang["seconds"] 	= "секунди";
$lang["minute"] 	= "минута";
$lang["minutes"] 	= "минути";
$lang["hour"] 		= "час";
$lang["hours"] 		= "часа";
$lang["day"] 		= "ден";
$lang["days"] 		= "дни";
$lang["week"] 		= "седмица";
$lang["weeks"] 		= "седмици";
$lang["month"] 		= "месец";
$lang["months"] 	= "месеца";
$lang["year"] 		= "година";
$lang["years"] 		= "години";

$lang["vehicle"] 	= "Возилa";
$lang["brand"] 		= "Бранд";
$lang["model"] 		= "Модел";
$lang["oil"] 		= "Гориво";
$lang["yearfrom"] 	= "Година от";
$lang["yearto"] 	= "Година до";
$lang["kmfrom"] 	= "Километри от";
$lang["kmto"] 		= "Километри до";
$lang["pricefrom"] 	= "Цена от";
$lang["priceto"] 	= "Цена до";
$lang["all"] 		= "Всички";
$lang["All"] 		= "Всички";
$lang["diesel"] 	= "Дизел";
$lang["oil"] 		= "Бензин";
$lang["gas/petrol"] = "Газ/Бензин";
$lang["electric"] 	= "Електрическа";
$lang["hybrid"] 	= "Хибрид";
$lang["other"] 		= "Други";

//meta tags - start
$lang["labelMetaTagEuro"] 			= "Евра";
$lang["labelMetaTagProdcutionYear"] = " год.";
$lang["sell_adjective"] 			= "Се продава";
$lang["buy_adjective"] 				= "Изисква за покупка ";

$lang["title_meta_tag_vehicle"] 			= "Употребявани автомобили";
$lang["description_meta_tag_vehicle"] 		= "Изборът на употребявани автомобили от различни марки и модели";

$lang["title_meta_tag_estate"] 				= "Оферта на недвижими имоти";
$lang["description_meta_tag_estate"] 		= "Изборът на нови и употребявани недвижими имоти";

$lang["title_meta_tag_electronics"] 		= "Електрониски устройства";
$lang["description_meta_tag_electronics"] 	= "Изборът на телефони, телевизори и други електронни устройства";

$lang["title_meta_tag_clothing"] 			= "Продажба на дрехи";
$lang["description_meta_tag_clothing"] 		= "Изборът на дрехи и обувки";

$lang["title_meta_tag_service"] 			= "Услуги";
$lang["description_meta_tag_service"] 		= "Съобщения на услуги";

$lang["title_meta_tag_other"] 				= "Обяви";
$lang["description_meta_tag_other"] 		= "Обяви";


$lang["labelMetaTagInsertAdsTitle"] 		= "Поставете обява";
$lang["labelMetaTagInsertAdsDescription"] 	= "Прозвучаване на trgovski.com е безплатно и завинаги ще остане безплатно";
$lang["labelMetaTagHomeTitle"] 				= "Интернет огласник";
$lang["labelMetaTagHomeDescription"] 		= "Прозвучаване на trgovski.com е безплатно и завинаги ще остане безплатно";
//meta tags - end

//location countrys, regionas and municipality - start
$lang["country"] 		= "Државa";
$lang["All country"] 	= "Всички щати";
$lang["region"] 		= "Регион";
$lang["All regions"] 	= "Всички Региони";
$lang["municipality"] 	= "Община";
//location countrys, regionas and municipality - end

$lang["buySell"] 				= "Куп./Прод.";
$lang["buy"] 					= "Купувам";
$lang["sell"] 					= "Продавам";
$lang["search"] 				= "Търсене";
$lang["labelSearch"]                            = "Търсене";
$lang["Rent"]                                   = "Наем";
$lang["labelSearchDescritpion"] = "Изберете желаните характеристики";

//ADS label - start
$lang["labelAdsUpdate"] 					= "Изменение на обява";
$lang["labelAdsDescription"] 				= "полоњата отбелязани с * са задължителни";
$lang["labelAdsBoxTitle"] 					= "Обява";
$lang["labelAdsTitle"] 						= "Заглавие*";
$lang["labelAdsPrice"] 						= "Цена (в евро)*";
$lang["labelAdsBody"] 						= "Съдържание*";
$lang["labelAdsCategory"] 					= "Категорија*";
$lang["labelAdsBuySell"] 					= "Продавам / Купувам*";
$lang["labelAdsYear"] 						= "Година";
$lang["labelAdsImages"] 					= "Снимки";
$lang["labelAdsImagesDescription"] 			= "Въвеждайте хоризонтални снимки. Първата снимка е задожителна, максималния брой разрешени снимки е 6.";
$lang["labelAdsImagesDelete"] 				= "Изтриване";
$lang["labelViewSimilarAds"] 				= "Подобни";
$lang["labelViewAdd"] 						= "Поставете обява";
$lang["labelBuySell"] 						= "Купувам / Продавам";
$lang["labelType"] 							= "Категория";
$lang["labelAdsDetails"] 					= "Характеристики";
$lang["labelInsertAds"] 					= "Поставете обява";
$lang["labelUpdateAds"] 					= "Промени Обявата";
$lang["labelInsertAdsDescription"] 			= "*задължителни полета";
$lang["labelInsertAds2"] 					= "Обявата";
$lang["labelUpdateAds2"] 					= "Промени Обявата";
$lang["labelInsertAdsTitle"] 				= "Заглавие*";
$lang["labelInsertAdsPrice"] 				= "Цена (в евро)*";
$lang["labelInsertAdsBody"] 				= "Съдържание*";
$lang["labelInsertInsertImages"] 			= "Качи снимки";
$lang["labelInsertInsertImagesDescription"] = "Въвеждайте хоризонтални снимки. Първата снимка е задожителна, максималния брой разрешени снимки е 6.";
$lang["labelInsertInsertImagesDelete"] 		= "Изтриване";
$lang["labelInsertInsertVehicleDetails"] 	= "Детайли за автомобила";
$lang["labelInsertInsertVehicleCategory"] 	= "Тип превозно средство*";
$lang["labelInsertInsertVehicleBrand"] 		= "Бранд*";
$lang["labelInsertInsertVehicleModel"] 		= "Модел*";
$lang["labelInsertInsertVehicleBuySell"] 	= "Купувам/Продавам*";
$lang["labelInsertInsertVehicleFuel"] 		= "Гориво";
$lang["labelInsertInsertVehicleYear"] 		= "Година на производство";
$lang["labelInsertInsertVehicleКm"] 		= "Километра пробег";
$lang["labelContact"] 						= "Контакт данни*";
$lang["labelContactName"] 					= "Име и фамилия*";
$lang["labelContactEmail"] 					= "Електронен адре (email)";
$lang["labelCountry"] 						= "Държава*";
$lang["labelCountryRegion"] 				= "Регион*";
$lang["labelCountryMunicipality"] 			= "Община*";
$lang["labelStreet"] 						= "Улица и номер";
$lang["labelPhone"] 						= "Телефон";
$lang["labelAdsAgree"] 						= "С претискање на Зареди обява се съгласите с правилата.";
$lang["labelAdsButtonText"] 				= "Публикуване обява";
$lang["labelAdsUpdateButtonText"] 			= "Запиши промените";
$lang["labelAdsCancelButtonText"] 			= "Отказ";

//type of vehicle start
$lang["car"] 	= "Автомобил";
$lang["trunk"] 	= "Камион";
$lang["other"] 	= "Друго";
//type of vehicle end

//view start
$lang["labelVehicleNoData"] 		= "няма информация";
$lang["labelVehicleSpecification"] 	= "Спецификации";
$lang["labelVehicleType"] 			= "Тип";
$lang["labelVehicleBrand"] 			= "Бранд";
$lang["labelVehicleModel"] 			= "Модел";
$lang["labelVehicleYear"] 			= "Година на производство: ";
$lang["labelVehiclePastKM"] 		= "Километра пробег: ";
$lang["labelVehicleTypeOil"] 		= "Тип на задвижване";
$lang["labelContactPerson"]			= "Информация за контакти";
$lang["labelContactName"] 			= "Име";
$lang["labelPhone"] 				= "Телефон: ";
$lang["labelEmail"] 				= "Имейл: ";
$lang["labelViewStreet"] 			= "Улица:";
$lang["labelViewMunicipality"] 		= "Община: ";
$lang["labelViewRegion"] 			= "Регион: ";
$lang["labelViewCountry"] 			= "Държава: ";
$lang["labelViewShare"] 			= "Сподели в социалните мрежи";
$lang["labelViewSettings"] 			= "Сетирање на обява";
$lang["labelViewSettingsActivate"] 	= "Активиране";
$lang["labelViewSettingsDeaktivate"] = "Деактивиране";
$lang["labelViewSettingsChange"] 	= "Промени";
$lang["labelViewSettingsRenew"] 	= "Обнови";
$lang["labelVehiclePublishedDate"] 	= "Публикувано в: ";
//view end

//estate - start
$lang["labelSearchEstate"] 				= "Недвижими имоти";
$lang["labelSearchEstateDescritpion"] 	= "Къщи, Апартаменти, Ниви и др.";
//estate - end
//Electronics - start
$lang["labelSearchElectronics"] 		= "Електрически устройства";
$lang["labelSearchElectronicsDescritpion"] 	= "Мобилни телефони, Телевизори, Компютри, Машизни за домакинства и др.";
//Electronics - end
//vehicle - start
$lang["labelSearchVehicle"] 			= "Превозни средства";
$lang["labelSearchVehicleDescritpion"] 	= "Автомобили, Камиони, Машини и др.";
//vehicle - end
//Clothing - start
$lang["labelSearchClothing"] 			= "Облекло";
$lang["labelSearchClothingDescritpion"] = "Обувки и дрехи";
//Clothing - end
//Service - start
$lang["labelSearchService"] 			= "Услуги";
$lang["labelSearchServiceDescritpion"] = "Транспорт, Избелване и т.н.";
//Service - end
//Clothing - start
$lang["labelSearchOther"] 			= "Останото";
$lang["labelSearchOtherDescritpion"] = "Други категории, които не са включени в други";
//Clothing - end
//insert and update ads - end

//alert message - start
$lang["AdsSuccessMessage"] 		= "Обявата е публикувана";
$lang["loginSuccessMessage"] 	= "Успечно влезете";
$lang["AdsMarkingActivate"] 	= "Съобщението е активиран";
$lang["AdsMarkingDeactivate"] 	= "Съобщението е деактивирана - маркирани като реализирала";
$lang["AccessDenied"] 			= "Незаконна дейност";
$lang["adsDeactive"] 			= "Съобщението е завършен";
$lang["adsInsertNotAllow"] 		= "Можете да въведете само една обява в 3 дни";
$lang["adsRenewNotAllow"] 		= "Можете да възстановявате обявата веднъж дневно";
$lang["adsRenew"] 				= "Съобщението е възстановена с днешна дата";
$lang["accountNotAllowNewAds"] 	= "Временно не може да влезе нови обяви";
//alert message - end

//my account - start
$lang["labelAccountTitle"] 					= "Моята сметка";
$lang["labelAccountBodyPersonalInfoTitle"] 	= "Лични данни (Facebook)";
$lang["labelAccountBodyMyAdsTitle"] 		= "Моят обяви";
$lang["labelAccountSubBodyVehibleTitle"] 	= "Автомобили";
//my account - end

// estate - start
$lang["labelEstate"] 		= "Имот";
$lang["labelEstateType"] 	= "Вид";
$lang["labelRoomNumber"] 	= "Брой помещения";
$lang["labelMetro"] 		= "Квадратни метра";
$lang["labelLocation"] 		= "Местоположение (Адрес)";

//category
$lang["House"] 			= "Къщи";
$lang["Appartments"] 	= "Апартаменти";
$lang["Cottages"] 		= "Вили";
$lang["Rooms"] 			= "Стаи";
$lang["Stores"] 		= "Магазини";
$lang["Offices"] 		= "Офиси";
$lang["Plots"] 			= "Плацеви";
$lang["Fields"] 		= "Ниви";
$lang["Others"] 		= "Останало";
// estate - end

// service - start
$lang["Тransport"]                      = "Транспортна";
$lang["Home help and cleaning"]         = "Помощ за дома и почистване";
$lang["Craftsmen"]                      = "Занаятчиите";
$lang["Lessons and Teaching"]           = "Уроци и преподаване";
$lang["Bookkeeping / Finance / Law"]    = "Счетоводство/Финанси/Закон";
$lang["Preparation and Food Service"] 	= "Подготовка и хранене";
// service - end
// other - start
$lang["Furniture"] 		= "Мебели";
$lang["Job"]                    = "Работа";
$lang["Sports equipment"] 	= "Спортна екипировка";
$lang["Medical equipment"] 	= "Медицинско оборудване";
// other - end   

//insert page - start
$lang["labelInsertTitle"]                   = "Зареди Обявата";
$lang["labelInsertBodyLine1"]               = "Прозвучаване на trgovski.com е безплатно и засекога ще остане безплатно";
$lang["labelInsertBodyLine2"]               = "За да въведете обява е необходимо да влезете с вашия Facebook акаунт (account)";
$lang["labelInsertBodyLine3"]               = "Броят на обяви не е ограничен, всички съмнителни сметки ще бъдат спрени";
$lang["labelInsertBodyLine4"]               = "За начало изберете категория, към която принадлежи вашата обява";
// insert page - end
//my account page - start
$lang["labelAccountSubBodyEstateTitle"] 		= "Недвижими имоти";
$lang["labelAccountSubBodyElectronicsTitle"] 	= "Електрически устройства";
$lang["labelAccountSubBodyClothingTitle"] 		= "Облекло";
$lang["labelAccountSubBodyServiceTitle"] 		= "Услуги";
$lang["labelAccountSubBodyOtherTitle"] 			= "Останало";
$lang["labelAccountStoreInformation"]                   = "Информации за продавница";
$lang["labelAccountStoreInformationStoreName"] 		= "Име на продавница:";
$lang["labelAccountStoreInformationStoreUrl"]          = "Линк (URL) до продавницата:";
// my account page - end

//store - start 
$lang["storeNotExist"] 			= "Магазинът няма";
//store - end

//variable from Database
//location countrys, regionas and municipality - start
$lang["country"] 	= "Државa: ";
$lang["Macedonia"] 	= "Македония";
$lang["Serbia"] 	= "Сърбия";
$lang["Bulgaria"] 	= "България";

//Macedonia - start
$lang["Skopje City"] 		= "Скопски Регион (град Скопје)";
$lang["Northeast Region"] 	= "Североизточен Регион";
$lang["East Region"] 		= "Източен Регион";
$lang["Southeast Region"] 	= "Югоизточен Регион";
$lang["Vardar Region"] 		= "Вардарски Регион";
$lang["Pelagonian Region"] 	= "Пелагониски Регион";
$lang["Polog Region"] 		= "Полошки Регион";
$lang["Southwest Region"] 	= "Югозападен Регион";
$lang["municipality"] 		= "Община";

$lang["Aerodrom"] 				= "Аеродром";
$lang["Butel"] 					= "Бутел";
$lang["Gazi Baba"] 				= "Гази Баба";
$lang["Gjorgje Petrov"] 		= "Гьорче Петров";
$lang["Karposh"] 				= "Карпош";
$lang["Kisela Voda"] 			= "Кисела Вода";
$lang["Centar"] 				= "Център";
$lang["Chair"] 					= "Чаир";
$lang["Shuto Orizari"] 			= "Шуто Оризари";
$lang["Arachinovo"] 			= "Арачиново";
$lang["Zelenikovo"] 			= "Зелениково";
$lang["Ilinden"] 				= "Илинден";
$lang["Petrovec"] 				= "Петровец";
$lang["Sopishte"] 				= "Сопище";
$lang["Studenichani"] 			= "Студеничани";
$lang["Chucher Sandevo"] 		= "Чучер Сандево";

$lang["Kratovo"] 				= "Кратово";
$lang["Kumanovo"] 				= "Куманово";
$lang["Kriva Palanka"] 			= "Крива Паланка";
$lang["Lipkovo"] 				= "Липково";
$lang["Rankovce"] 				= "Ранковце";
$lang["Staro Nagorichane"] 		= "Старо Нагоричане";

$lang["Berovo"] 				= "Верово";
$lang["Vinica"] 				= "Виница";
$lang["Delchevo"] 				= "Длечево";
$lang["Zrnovci"] 				= "Зърновци";
$lang["Karbinci"] 				= "Карбинци";
$lang["Kochani"] 				= "Кочани";
$lang["Makedonska Kamenica"] 	= "Македонска Каменица";
$lang["Pehchevo"] 				= "Пехчево";
$lang["Cheshinovo - Obleshevo"] = "Чешиново - Облешево";
$lang["Shtip"] 					= "Щип";
$lang["Probishtip"] 			= "Пробищип";

$lang["Bogdanci"] 				= "Боданци";
$lang["Bosilovo"] 				= "Босилево";
$lang["Vasilevo"] 				= "Василево";
$lang["Valandovo"] 				= "Валандово";
$lang["Gevgelija"] 				= "Гевгелија";
$lang["Konche"] 				= "Конче";
$lang["Dojran"] 				= "Дойран";
$lang["Novo Selo"] 				= "Ново Село";
$lang["Radovish"] 				= "Радовиш";
$lang["Strumica"] 				= "Струмица";

$lang["Veles"] 					= "Велес";
$lang["Gratsko"] 				= "Гратско";
$lang["Demir Kapija"] 			= "Демир Капия";
$lang["Kavadarci"] 				= "Кавадарци";
$lang["Lozovo"] 				= "Лозово";
$lang["Negotino"] 				= "Неготино";
$lang["Rosoman"]				= "Росоман";
$lang["Sveti Nikole"] 			= "Свети Николе";
$lang["Chashka"] 				= "Чашка";

$lang["Bitola"] 				= "Битола";
$lang["Demir Hisar"] 			= "Демир Хисар";
$lang["Krivogashtani"] 			= "Кривогащани";
$lang["Krushevo"] 				= "Крушево";
$lang["Mogila"] 				= "Могила";
$lang["Prilep"] 				= "Прилеп";
$lang["Resen"] 					= "Ресен";
$lang["Novaci"] 				= "Новаци";
$lang["Dolneni"] 				= "Долнени";

$lang["Bogovinje"] 				= "Боговиње";
$lang["Brvenica"] 				= "Брвеница";
$lang["Vrapchishte"] 			= "Врапчиште";
$lang["Gostivar"] 				= "Гостивар";
$lang["Zhelino"] 				= "Желино";
$lang["Jagunovce"] 			 	= "Јагуновце";
$lang["Mavrovo i Rostushe"] 	= "Маврово и Ростуше";
$lang["Tearce"] 				= "Теарце";
$lang["Tetovo"] 				= "Тетово";

$lang["Vevchani"] 				= "Вевчани";
$lang["Debar"] 					= "Дебър";
$lang["Debarca"] 				= "Дебарца";
$lang["Kichevo"] 				= "Кичево";
$lang["Makedonski Brod"] 		= "Македонски Брод";
$lang["Ohrid"] 					= "Охрид";
$lang["Plasnica"] 				= "Пласница";
$lang["Struga"] 				= "Струга";
$lang["Centar Zupa"] 			= "Центар Жупа";
//Macedonia - end

//Bulgaria - start
$lang["Sofia City"] 			= "София-град";
$lang["Blagoevgrad"] 			= "Благоевград";
$lang["Burgas"] 				= "Бургас";
$lang["Dobrich"] 				= "Добрич";
$lang["Gabrovo"] 				= "Габрово";
$lang["Haskovo"] 				= "Хасково";
$lang["Kardzhali"] 				= "Кърджали";
$lang["Kyustendil"] 			= "Кюстендил";
$lang["Lovech"] 				= "Ловеч";
$lang["Montana"] 				= "Монтана";
$lang["Pazardzhik"] 			= "Пазарджик";
$lang["Pernik"] 				= "Перник";
$lang["Plovdiv"] 				= "Пловдив";
$lang["Razgrad"] 				= "Разград";
$lang["Ruse"] 					= "Русе";
$lang["Shumen"] 				= "Шумен";
$lang["Silistra"] 				= "Силистра";
$lang["Sliven"] 				= "Сливен";
$lang["Smolyan"] 				= "Смолян";
$lang["Sofia (province)"] 		= "Софийска област";
$lang["Stara Zagora"] 			= "Стара Загора";
$lang["Targovishte"] 			= "Търговище";
$lang["Varna"] 					= "Варна";
$lang["Veliko Tarnovo"] 		= "Велико Търново";
$lang["Vidin"] 					= "Видин";
$lang["Vratsa"] 				= "Враца";
$lang["Yambol"] 				= "Ямбол";
$lang["Pleven"] 				= "Плевен";
$lang["Targovishte"] 			= "Търговище";

$lang["Bansko"] 				= "Банско";
$lang["Belitsa"] 				= "Белица";
$lang["Blagoevgrad"] 			= "Благоевград";
$lang["Garmen"] 				= "Гърмен";
$lang["Gotse Delchev"] 			= "Гоце Делчев";
$lang["Hadzhidimovo"] 			= "Хаджидимово";
$lang["Kresna"] 				= "Кресна";
$lang["Petrich"] 				= "Петрич";
$lang["Razlog"] 				= "Разлог";
$lang["Sandanski"] 				= "Сандански";
$lang["Satovcha"] 				= "Сатовча";
$lang["Simitli"] 				= "Симитли";
$lang["Strumyani"] 				= "Струмяни";
$lang["Yakoruda"] 				= "Якоруда";

$lang["Aytos"] 					= "Айтос";
$lang["Burgas"] 				= "Бургас";
$lang["Kameno"] 				= "Камено";
$lang["Karnobat"] 				= "Карнобат";
$lang["Malko Tarnovo"] 			= "Малко Търново";
$lang["Nesebar"] 				= "Несебър";
$lang["Pomorie"] 				= "Поморие";
$lang["Primorsko"] 				= "Приморско";
$lang["Ruen"] 					= "Сандански";
$lang["Sozopol"] 				= "Созопол";
$lang["Sredets"] 				= "Средец";
$lang["Sungurlare"] 			= "Сунгурларе";
$lang["Tsarevo"] 				= "Царево";

$lang["Balchik"] 				= "Балчик";
$lang["Dobrich (city)"] 		= "Добрич-Град";
$lang["Dobrichka (rural)"] 		= "Добричка";
$lang["General Toshevo"] 		= "Банско";
$lang["Kavarna"] 				= "Генерал Тошево";
$lang["Krushari"] 				= "Каварна";
$lang["Shabla"] 				= "Шабла";
$lang["Tervel"] 				= "Тервел";

$lang["Dryanovo"] 				= "Дряново";
$lang["Gabrovo"] 				= "Габрово";
$lang["Sevlievo"] 				= "Севлиево";
$lang["Tryavna"] 				= "Трявна";

$lang["Dimitrovgrad"] 			= "Димитровград";
$lang["Harmanli"] 				= "Харманли";
$lang["Haskovo"] 				= "Хасково";
$lang["Ivaylovgrad"] 			= "Ивайловград";
$lang["Lyubimets"] 				= "Любимец";
$lang["Madzharovo"] 			= "Маджарово";
$lang["Mineralni bani"] 		= "Минерални бани";
$lang["Simeonovgrad"] 			= "Симеоновград";
$lang["Stambolovo"] 			= "Стамболово";
$lang["Svilengrad"] 			= "Свиленград";
$lang["Topolovgrad"] 			= "Тополовград";

$lang["Ardino"] 				= "Ардино";
$lang["Chernoochene"] 			= "Черноочене";
$lang["Dzhebel"] 				= "Джебел";
$lang["Kardzhali"] 				= "Кърджали";
$lang["Kirkovo"] 				= "Кирково";
$lang["Krumovgrad"] 			= "Крумовград";
$lang["Momchilgrad"] 			= "Момчилград";

$lang["Boboshevo"] 				= "Бобошево";
$lang["Bobov dol"] 				= "Бобов дол";
$lang["Dupnitsa"] 				= "Дупница";
$lang["Kocherinovo"] 			= "Кочериново";
$lang["Kyustendil"] 			= "Кюстендил";
$lang["Nevestino"] 				= "Невестино";
$lang["Rila"] 					= "Рила";
$lang["Sapareva banya"] 		= "Сапарева баня";
$lang["Treklyano"] 				= "Трекляно";

$lang["Apriltsi"] 				= "Априлци";
$lang["Letnitsa"] 				= "Летница";
$lang["Lovech"] 				= "Ловеч";
$lang["Lukovit"] 				= "Луковит";
$lang["Teteven"] 				= "Тетевен";
$lang["Troyan"] 				= "Троян";
$lang["Ugarchin"] 				= "Угърчин";
$lang["Yablanitsa"] 			= "Ябланица";

$lang["Berkovitsa"] 			= "Берковица";
$lang["Boychinovtsi"] 			= "Бойчиновци";
$lang["Brusartsi"] 				= "Брусарци";
$lang["Chiprovtsi"] 			= "Чипровци";
$lang["Georgi Damyanovo"] 		= "Георги Дамяново";
$lang["Lom"] 					= "Лом";
$lang["Medkovets"] 				= "Медковец";
$lang["Montana"] 				= "Монтана";
$lang["Valchedram"] 			= "Вълчедръм";
$lang["Varshets"] 				= "Вършец";
$lang["Yakimovo"] 				= "Якимово";

$lang["Pazardzhik"] 			= "Пазарджик";
$lang["Velingrad"] 				= "Велинград";
$lang["Septemvri"] 				= "Септември";
$lang["Panagyurishte"] 			= "Панагюрище";
$lang["Peshtera"] 				= "Пещера";
$lang["Rakitovo"] 				= "Ракитово";
$lang["Bratsigovo"] 			= "Брацигово";
$lang["Belovo"] 				= "Белово";
$lang["Batak"] 					= "Батак";
$lang["Lesichovo"] 				= "Лесичово";
$lang["Strelcha"] 				= "Стрелча";
$lang["Sarnitsa"] 				= "Сърница";

$lang["Belene"] 				= "Белене";
$lang["Gulyantsi"] 				= "Гулянци";
$lang["Dolna Mitropoliya"] 		= "Долна Митрополия";
$lang["Dolni Dabnik"] 			= "Долни Дъбник";
$lang["Levski"] 				= "Левски";
$lang["Nikopol"] 				= "Никопол";
$lang["Iskar"] 					= "Искър";
$lang["Pleven"] 				= "Плевен";
$lang["Pordim"] 				= "Пордим";
$lang["Cherven Bryag"] 			= "Червен бряг";
$lang["Knezha"] 				= "Кнежа";

$lang["Asenovgrad"] 			= "Асеновград";
$lang["Brezovo"] 				= "Брезово";
$lang["Hisarya"] 				= "Хисаря";
$lang["Kaloyanovo"] 			= "Калояново";
$lang["Karlovo"] 				= "Карлово";
$lang["Krichim"] 				= "Кричим";
$lang["Kuklen"] 				= "Куклен";
$lang["Laki"] 					= "Лъки";
$lang["Maritsa (Plovdiv rural)"]= "Марица";
$lang["Perushtitsa"] 			= "Перущица";
$lang["Plovdiv (city)"] 		= "Пловдив";
$lang["Parvomay"] 				= "Първомай";
$lang["Rakovski"] 				= "Раковски";
$lang["Rodopi (Plovdiv rural)"] = "Родопи";
$lang["Sadovo"] 				= "Садово";
$lang["Sopot"] 					= "Сопот";
$lang["Stamboliyski"] 			= "Стамболийски";
$lang["Saedinenie"] 			= "Съединение";

$lang["Isperih"] 				= "Исперих";
$lang["Kubrat"] 				= "Кубрат";
$lang["Loznitsa"] 				= "Лозница";
$lang["Razgrad"] 				= "Разград";
$lang["Samuil"] 				= "Самуил";
$lang["Tsar Kaloyan"] 			= "Цар Калоян";
$lang["Zavet"] 					= "Завет";

$lang["Borovo"] 				= "Борово";
$lang["Byala"] 					= "Бяла";
$lang["Vetovo"] 				= "Ветово";
$lang["Dve Mogili"] 			= "Две могили";
$lang["Ivanovo"] 				= "Иваново";
$lang["Ruse"] 					= "Русе";
$lang["Slivo Pole"] 			= "Сливо поле";
$lang["Tsenovo"] 				= "Ценово";

$lang["Venets"] 				= "Венец";
$lang["Varbitsa"] 				= "Върбица";
$lang["Hitrino"] 				= "Хитрино";
$lang["Kaolinovo"] 				= "Каолиново";
$lang["Kaspichan"] 				= "Каспичан";
$lang["Nikola Kozlevo"] 		= "Никола Козлево";
$lang["Novi Pazar"] 			= "Нови пазар";
$lang["Veliki Preslav"] 		= "Велики Преслав";
$lang["Smyadovo"] 				= "Смядово";
$lang["Shumen"] 				= "Шумен";

$lang["Alfatar"] 				= "Алфатар";
$lang["Glavinitsa"] 			= "Главиница";
$lang["Dulovo"] 				= "Дулово";
$lang["Kaynardzha"] 			= "Кайнарджа";
$lang["Silistra"] 				= "Силистра";
$lang["Sitovo"] 				= "Ситово";
$lang["Tutrakan"] 				= "Тутракан";

$lang["Kotel"] 					= "Котел";
$lang["Nova Zagora"] 			= "Нова Загора";
$lang["Sliven"] 				= "Сливен";
$lang["Tvarditsa"] 				= "Твърдица";

$lang["Banite"] 				= "Баните";
$lang["Borino"] 				= "Борино";
$lang["Chepelare"] 				= "Чепеларе";
$lang["Devin"] 					= "Девин";
$lang["Dospat"] 				= "Доспат";
$lang["Madan"] 					= "Мадан";
$lang["Nedelino"] 				= "Неделино	";
$lang["Rudozem"] 				= "Рудозем";
$lang["Smolyan"] 				= "Смолян";
$lang["Zlatograd"] 				= "Златоград";

$lang["Bankya"] 				= "Банкя";
$lang["Buhovo"] 				= "Бухово";
$lang["Novi Iskar"] 			= "Нови Искъ";
$lang["Sofia"] 					= "София";

$lang["Anton"] 					= "Антон";
$lang["Botevgrad"] 				= "Ботевград";
$lang["Bozhurishte"] 			= "Божурище";
$lang["Chavdar"] 				= "Чавдар";
$lang["Chelopech"] 				= "Челопеч";
$lang["Dolna Banya"] 			= "Долна баня";
$lang["Dragoman"] 				= "Драгоман";
$lang["Elin Pelin"] 			= "Елин Пелин";
$lang["Etropole"] 				= "Етрополе";
$lang["Godech"] 				= "Годеч";
$lang["Gorna Malina"] 			= "Горна Малина";
$lang["Ihtiman"] 				= "Ихтиман";
$lang["Koprivshtitsa"] 			= "Копривщица";
$lang["Kostenets"] 				= "Костенец";
$lang["Kostinbrod"] 			= "Костинброд";
$lang["Mirkovo"] 				= "Мирково";
$lang["Pirdop"] 				= "Пирдоп";
$lang["Pravets"] 				= "Правец";
$lang["Samokov"] 				= "Самоков";
$lang["Slivnitsa"] 				= "Сливница";
$lang["Svoge"] 					= "Своге";
$lang["Zlatitsa"] 				= "Златица";

$lang["Bratya"] 				= "Братя Даскалови";
$lang["Chirpan"] 				= "Чирпан";
$lang["Gurkovo"] 				= "Гурково";
$lang["Galabovo"] 				= "Гълъбово";
$lang["Kazanlak"] 				= "Казанлък";
$lang["Maglizh"] 				= "Мъглиж";
$lang["Nikolaevo"] 				= "Николаево";
$lang["Opan"] 					= "Опан";
$lang["Pavel Banya"] 			= "Павел баня";
$lang["Radnevo"] 				= "Раднево";
$lang["Stara Zagora"] 			= "Стара Загора";

$lang["Targovishte"] 			= "Търговище";
$lang["Popovo"] 				= "Попово";
$lang["Omurtag"] 				= "Омуртаг";
$lang["Antonovo"] 				= "Антоново";
$lang["Opaka"] 					= "Опака";

$lang["Avren"] 					= "Аврен";
$lang["Aksakovo"] 				= "Аксаково";
$lang["Beloslav"] 				= "Белослав";
$lang["Byala"] 					= "Бяла";
$lang["Varna"] 					= "Варна";
$lang["Vetrino"] 				= "Ветрино";
$lang["Valchi Dol"] 			= "Вълчи дол";
$lang["Dolni Chiflik"] 			= "Долни чифлик";
$lang["Devnya"] 				= "Девня";
$lang["Dalgopol"] 				= "Дългопол";
$lang["Provadiya"] 				= "Провадия";
$lang["Suvorovo"] 				= "Суворово";

$lang["Elena"] 					= "Елена";
$lang["Gorna Oryahovitsa"] 		= "Горна Оряховица";
$lang["Lyaskovets"] 			= "Лясковец";
$lang["Pavlikeni"] 				= "Павликени";
$lang["Polski Trambesh"] 		= "Полски Тръмбеш";
$lang["Strazhitsa"] 			= "Стражица";
$lang["Suhindol"] 				= "Сухиндол";
$lang["Svishtov"] 				= "Свищов";
$lang["Veliko Tarnovo"] 		= "Велико Търново";
$lang["Zlataritsa"] 			= "Златарица";

$lang["Belogradchik"] 			= "Белоградчик";
$lang["Boynitsa"] 				= "Бойница";
$lang["Bregovo"] 				= "Брегово";
$lang["Vidin"] 					= "Видин";
$lang["Gramada"] 				= "Грамада";
$lang["Dimovo"] 				= "Димово";
$lang["Kula"] 					= "Кула";
$lang["Makresh"] 				= "Макреш";
$lang["Novo Selo"] 				= "Ново Село";
$lang["Ruzhintsi"] 				= "Ружинци";
$lang["Chuprene"] 				= "Чупрене";

$lang["Borovan"] 				= "Борован";
$lang["Byala Slatina"] 			= "Бяла Слатина";
$lang["Vratsa"] 				= "Враца";
$lang["Kozloduy"] 				= "Козлодуй";
$lang["Krivodol"] 				= "Криводол";
$lang["Mezdra"] 				= "Мездра";
$lang["Mizia"] 					= "Мизия";
$lang["Oryahovo"] 				= "Оряхово";
$lang["Roman"] 					= "Роман";
$lang["Hayredin"] 				= "Хайредин";

$lang["Bolyarovo"] 				= "Болярово";
$lang["Elhovo"] 				= "Елхово";
$lang["Straldzha"] 				= "Стралджа";
$lang["Tundzha (rural)"] 		= "Тунджа";
$lang["Yambol (city)"] 				= "Ямбол";
//Bulgaria - end

//setting start
$lang["labelSettingTitle"]                          =   "Подесување";
$lang["labelSettingTitleDescription"]               =   "Подесување на јазик, Држава и валута.";
$lang["labelSettingTitleChangeLanguage"]            =   "Промени Јазик";
$lang["labelSettingTitleChangeLanguageMessage"]     =   "Со кликнување на линкот соодветно променете го јазикот";
$lang["labelSettingTitleChangeCountry"]             =   "Промена на Држава";
$lang["labelSettingTitleChangeCountryMessage"]      =   "Со кликнување на имато на Државата ја подесувате основната Држава за пребарување";
$lang["labelSettingTitleChangeCurrency"]            =   "Промена на валута";
$lang["labelSettingTitleChangeCurrencyMessage"]     =   "Со кликнување на валутата ја подесувата основната валута";
//setting end