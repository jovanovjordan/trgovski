<?php
$lang["language"] = "mk";

//front message
$lang["frontMessageMain"] 	= "Advertise on trgovski.com is free and will always remain free.";

//menu
$lang["home"] 			= "Home";
$lang["search"] 		= "Search";
$lang["category"] 		= "Category";
$lang["insertAds"] 		= "Insert";
$lang["help"] 			= "Help";
$lang["contact"] 		= "Contact";
$lang["setting"]                = "Settings";
$lang["about"]			= "About";
$lang["login"] 			= "Login";
$lang["labelMyAccount"] = "My account";
$lang["logout"] 		= "Logout";

//footer
$lang["footerMessage"] 						= "trgovski.com by vebko.com - All rights reserved";
$lang["labelCategoryChoiceCategory"] 		= "Choise category";
$lang["labelInsertCategoryChoiceCategory"] 	= "Choise category for your advertisement";
$lang["real_estate"] 						= "Real estate";
$lang["vehicle"] 							= "vehicle";
$lang["electronics"] 						= "electronics";
$lang["clothing"] 							= "clothing";
$lang["services"] 							= "services";
$lang["jobs"] 								= "jobs";
$lang["allcategories"] 						= "all categories";

//time
$lang["before"] 	= "Before";
$lang["second"] 	= "second";
$lang["seconds"] 	= "seconds";
$lang["minute"] 	= "minute";
$lang["minutes"] 	= "minutes";
$lang["hour"] 		= "hour";
$lang["hours"]		= "hours";
$lang["day"] 		= "day";
$lang["days"] 		= "days";
$lang["week"] 		= "week";
$lang["weeks"] 		= "weeks";
$lang["month"] 		= "month";
$lang["months"] 	= "months";
$lang["year"] 		= "year";
$lang["years"] 		= "years";

$lang["vehicle"] 	= "Vehicle";
$lang["brand"] 		= "Brand";
$lang["model"] 		= "Model";
$lang["oil"] 		= "Oil";
$lang["yearfrom"] 	= "Year from";
$lang["yearto"] 	= "Year to";
$lang["kmfrom"] 	= "Kilometers from";
$lang["kmto"] 		= "Kilometers to";
$lang["pricefrom"] 	= "Price from";
$lang["priceto"] 	= "Price to";
$lang["all"] 		= "All";
$lang["All"] 		= "All";
$lang["diesel"] 	= "Diesel";
$lang["oil"] 		= "Oil";
$lang["gas/petrol"] = "Gas/Petrol";
$lang["electric"] 	= "Electric";
$lang["hybrid"] 	= "Hibrid";
$lang["other"] 		= "Other";
$lang["Other"] 		= "Other";

//meta tags - start
$lang["labelMetaTagEuro"] 			= "Euros";
$lang["labelMetaTagProdcutionYear"] = " years";
$lang["sell_adjective"]				= "Selling ";
$lang["buy_adjective"] 				= "Loking for buy ";

$lang["title_meta_tag_vehicle"] 			= "Used car";
$lang["description_meta_tag_vehicle"] 		= "A lot of used cars";

$lang["title_meta_tag_estate"] 				= "Real estate";
$lang["description_meta_tag_estate"] 		= "Lot of choice real estate";

$lang["title_meta_tag_electronics"] 		= "Elctronic device";
$lang["description_meta_tag_electronics"] 	= "Phones, Computers etc.";

$lang["title_meta_tag_clothing"] 			= "Clothing";
$lang["description_meta_tag_clothing"] 		= "Cloting sell";

$lang["title_meta_tag_service"] 			= "Service";
$lang["description_meta_tag_service"] 		= "Find service";

$lang["title_meta_tag_other"] 				= "Other";
$lang["description_meta_tag_other"] 		= "Other";


$lang["labelMetaTagInsertAdsTitle"] 		= "Insert Advertisement";
$lang["labelMetaTagInsertAdsDescription"] 	= "Advertisement on trgovski.com is free and will be free forever";
$lang["labelMetaTagHomeTitle"] 				= "Internet Advertisement";
$lang["labelMetaTagHomeDescription"] 		= "Advertisement on trgovski.com is free and will be free forever";
//meta tags - end

//location countrys, regionas and municipality - start
$lang["country"] 		= "Country";
$lang["All country"] 	= "All country";
$lang["region"] 		= "Регион";
$lang["All regions"] 	= "Сите Региони";
$lang["municipality"] 	= "Municipality";
//location countrys, regionas and municipality - end

$lang["buySell"] 				= "Buy Sell";
$lang["buy"] 					= "Buy";
$lang["sell"] 					= "Sell";
$lang["search"] 				= "Seacrh";
$lang["labelSearch"]                            = "Seacrh";
$lang["Rent"]                                   = "Rent";
$lang["labelSearchDescritpion"] = "Select search types";

//ADS label - start
$lang["labelAdsUpdate"] 					= "Change Advertisement";
$lang["labelAdsDescription"] 				= "*required";
$lang["labelAdsBoxTitle"] 					= "Advertisement";
$lang["labelAdsTitle"] 						= "Title*";
$lang["labelAdsPrice"] 						= "Price (in euro)*";
$lang["labelAdsBody"] 						= "Body*";
$lang["labelAdsCategory"] 					= "Category*";
$lang["labelAdsBuySell"] 					= "Buy / Sell*";
$lang["labelAdsYear"] 						= "Year";
$lang["labelAdsImages"] 					= "GAllery";
$lang["labelAdsImagesDescription"] 			= "Insert landscape images. First image is required, max number of image is 6.";
$lang["labelAdsImagesDelete"] 				= "Delete";
$lang["labelViewSimilarAds"] 				= "Similar";
$lang["labelViewAdd"] 						= "Insert Advertisement";
$lang["labelBuySell"] 						= "Buy / Sell*";
$lang["labelType"] 							= "Category";
$lang["labelAdsDetails"] 					= "Specification";
$lang["labelInsertAds"] 					= "Insert Advertisement";
$lang["labelUpdateAds"] 					= "Update Vehicle";
$lang["labelInsertAdsDescription"] 			= "* required";
$lang["labelInsertAds2"] 					= "Advertisement*";
$lang["labelUpdateAds2"] 					= "Update Advertisement";
$lang["labelInsertAdsTitle"] 				= "Title*";
$lang["labelInsertAdsPrice"] 				= "Price (in euro)*";
$lang["labelInsertAdsBody"] 				= "Body*";
$lang["labelInsertInsertImages"] 			= "Insert images";
$lang["labelInsertInsertImagesDescription"] = "Insert landscape images. First image is required, max number of image is 6.";
$lang["labelInsertInsertImagesDelete"] 		= "Delete";
$lang["labelInsertInsertVehicleDetails"]	= "Details for Vehicle";
$lang["labelInsertInsertVehicleCategory"] 	= "Type of Vehicle*";
$lang["labelInsertInsertVehicleBrand"] 		= "Brand*";
$lang["labelInsertInsertVehicleModel"] 		= "Model*";
$lang["labelInsertInsertVehicleBuySell"] 	= "Buy/Sell*";
$lang["labelInsertInsertVehicleFuel"] 		= "Type Fuel*";
$lang["labelInsertInsertVehicleYear"] 		= "Year production*";
$lang["labelInsertInsertVehicleКm"] 		= "Kilimetears*";
$lang["labelContact"] 						= "Contact";
$lang["labelContactName"] 					= "First and Last Name*";
$lang["labelContactEmail"] 					= "Email*";
$lang["labelCountry"] 						= "Country*";
$lang["labelCountryRegion"] 				= "Region*";
$lang["labelCountryMunicipality"] 			= "Municipality*";
$lang["labelStreet"] 						= "Street and Street Number*";
$lang["labelPhone"] 						= "Phone*";
$lang["labelAdsAgree"] 						= "With press the button you are agree with rules. ";
$lang["labelAdsButtonText"] 				= "Publish advertisement";
$lang["labelAdsUpdateButtonText"] 			= "Save";
$lang["labelAdsCancelButtonText"] 			= "Cancel";

//type of vehicle start
$lang["car"] = "Car";
$lang["trunk"] = "Trunk";
$lang["other"] = "Other";
//type of vehicle end

//view start
$lang["labelVehicleNoData"] 		= "No informaion";
$lang["labelVehicleSpecification"] 	= "Specification";
$lang["labelVehicleType"] 			= "Type";
$lang["labelVehicleBrand"] 			= "Brand";
$lang["labelVehicleModel"] 			= "Model";
$lang["labelVehicleYear"] 			= "Year: ";
$lang["labelVehiclePastKM"] 		= "Km: ";
$lang["labelVehicleTypeOil"] 		= "Type of oil";
$lang["labelContactPerson"]			= "Contact Information";
$lang["labelContactName"] 			= "Name";
$lang["labelPhone"] 				= "Phone: ";
$lang["labelEmail"] 				= "Email: ";
$lang["labelViewStreet"] 			= "Street:";
$lang["labelViewMunicipality"] 		= "Mumicipality: ";
$lang["labelViewRegion"] 			= "Retion: ";
$lang["labelViewCountry"] 			= "Country: ";
$lang["labelViewShare"] 			= "Share";
$lang["labelViewSettings"] 			= "Settings";
$lang["labelViewSettingsActivate"] 	= "Activate";
$lang["labelViewSettingsDeaktivate"] = "Deactivate";
$lang["labelViewSettingsChange"] 	= "Change";
$lang["labelViewSettingsRenew"] 	= "Renew";;
$lang["labelVehiclePublishedDate"] 	= "Publish on: ";;
//view end

//estate - start
$lang["labelSearchEstate"] 				= "Estate";
$lang["labelSearchEstateDescritpion"] 	= "House, Appartments etc.";;
//estate - end
//Electronics - start
$lang["labelSearchElectronics"] 		= "Elctronic device";
$lang["labelSearchElectronicsDescritpion"] 	= "Phone, mobile, computers etc";;
//Electronics - end
//vehicle - start
$lang["labelSearchVehicle"] 			= "Vehicle";
$lang["labelSearchVehicleDescritpion"] 	= "Car, Trunks etc.";;
//vehicle - end
//Clothing - start
$lang["labelSearchClothing"] 			= "Clothing";
$lang["labelSearchClothingDescritpion"] = "Clothing";;
//Clothing - end
//Service - start
$lang["labelSearchService"] 			= "Service";
$lang["labelSearchServiceDescritpion"] = "Trasport etc";;
//Service - end
//Clothing - start
$lang["labelSearchOther"] 			= "Others";
$lang["labelSearchOtherDescritpion"] = "Others cateogry who is not include in previews";
//Clothing - end
//insert and update ads - end

//alert message - start
$lang["AdsSuccessMessage"] 		= "Advertisement is publish";
$lang["loginSuccessMessage"] 	= "You are login";
$lang["AdsMarkingActivate"] 	= "Advertisement is actived";
$lang["AdsMarkingDeactivate"] 	= "Advertisment is deactived - market like DONE";
$lang["AccessDenied"] 			= "Access is not allowed";
$lang["adsDeactive"] 			= "Advertisement is DONE";
$lang["adsInsertNotAllow"] 		= "You can inserct just 3 advetisments";
$lang["adsRenewNotAllow"] 		= "You can renew advertisement just one time per day";
$lang["adsRenew"] 				= "Advertisement is renewed";
$lang["accountNotAllowNewAds"] 	= "You can not insert advertisements";
//alert message - end

//my account - start
$lang["labelAccountTitle"] 					= "My account";
$lang["labelAccountBodyPersonalInfoTitle"] 	= "Personal informaion (Facebook)";
$lang["labelAccountBodyMyAdsTitle"] 		= "My Advertisement";
$lang["labelAccountSubBodyVehibleTitle"] 	= "Cars";
//my account - end

// estate - start
$lang["labelEstate"] 		= "Estate";
$lang["labelEstateType"] 	= "Type";
$lang["labelRoomNumber"] 	= "Number of rooms";
$lang["labelMetro"] 		= "m2";
$lang["labelLocation"] 		= "Location (Address)";

//category
/*
$lang["House"] 			= "Куќи";
$lang["Appartments"] 	= "Станови";
$lang["Cottages"] 		= "Викендици";
$lang["Rooms"] 			= "Соби";
$lang["Stores"] 		= "Продавници";
$lang["Offices"] 		= "Канцеларии";
$lang["Plots"] 			= "Плацвои";
$lang["Fields"] 		= "Ниви";
$lang["Others"] 		= "Останато";
// estate - end*/

//insert page - start
$lang["labelInsertTitle"]                   = "Insert Advertisement";
$lang["labelInsertBodyLine1"]               = "Advertisement on trgovski.com is free and will be free forever";
$lang["labelInsertBodyLine2"]               = "For insert advertisement you must to login with your Facebook account";
$lang["labelInsertBodyLine3"]               = "Number of advertisement is not limited, all suspicious accounts will be suspend";
$lang["labelInsertBodyLine4"]               = "For beginin chouse category for your Advertisement";
// insert page - end
//my account page - start
$lang["labelAccountSubBodyEstateTitle"] 		= "Estate";
$lang["labelAccountSubBodyElectronicsTitle"] 	= "Elctronic device";
$lang["labelAccountSubBodyClothingTitle"] 		= "Clothing";
$lang["labelAccountSubBodyServiceTitle"] 		= "Service";
$lang["labelAccountSubBodyOtherTitle"] 			= "Other";
$lang["labelAccountStoreInformation"]                   = "Iformation about store";
$lang["labelAccountStoreInformationStoreName"] 		= "Name of store:";
$lang["labelAccountStoreInformationStoreUrl"]          = "Store URL:";
// my account page - end

$lang["country"] 	= "Country: ";

//store - start
$lang["labelAccountStoreInformation"]                       = "Информации за продавница";
$lang["labelAccountStoreInformationStoreName"]              = "Име на продавница:";
$lang["labelAccountStoreInformationStoreUrl"]               = "Линк (URL) до продавницата:";
$lang["labelAccountStoreInformationCreateStoreTitle"]       = "Отвори продавница";
$lang["labelInsertStoreChange"]                             = "Измени";
$lang["labelInsertStoreChangeStore"]                        = "Запиши промени";
$lang["labelAccountStoreInformationCreateStoreBody"]        = "Трговски Ви овозможува да отворите сопствена продавница каде ќе бидат преставени само Вашите проиводи.";
$lang["labelAccountStoreInformationCreateStoreButtonTitle"] = "Промени податоци";
$lang["labelInsertStore"]                                   = "Креирај продавнца";
$lang["storeNotExist"]                                      = "Продавницата не постои";
$lang["labelInsertStore"]                                   = "Креирај продавнца";
$lang["labelInsertStoreBoxTitle"]                           = "Податоци за продавницата";
$lang["labelInsertStoreTitle"]                              = "Име на продавницата*";
$lang["labelInsertStoreTitleDescription"]                   = "Внесете име на продавницата";
$lang["labelInsertStoreSubTitle"]                           = "Објаснување на продавницата*";
$lang["labelInsertStoreSubTitleDescription"]                = "Внесете дејност на продавницата, пример, агенција за продажба на недвижности, плац за продажба на половни автомобили итн.";
$lang["labelInsertStoreURL"]                           		= "Url*";
$lang["labelInsertStoreURLDescription"]                		= "Внесете само еден збор на латиница кој ќе биде суб домаин нa trgovski.com за Вашата продавница.";
$lang["labelInsertStoreBody"]                               = "Името на продавницата ќе биде и URL, затоа за име внесете само еден збор и на латиница.";
$lang["labelInsertStoreOpenStore"]                          = "Креирај Продавница";
$lang["labelInsertStoreAgree"]                              = "Со кликнување на креирај продавница се согласувате со услувите на трговски.цом";
$lang["StoreErrorUserStoreExist"]                           = "Веќе имате креирано сопствена продавница";
$lang["StoreErrorStoreNameTaken"]                           = "Името е зафатено обидете се со друго име";
$lang["StoreSuccessCreated"]                                = "Успешно креирана продавница";
$lang["StoresTitle"]                                        = "Stores";
$lang["StoresTitleAll"]                                     = "All";
$lang["StoresSubTitle"]                                     = "Trgovski.com продавници";
//store - end


//setting start
$lang["labelSettingTitle"]                          =   "Setting";
$lang["labelSettingTitleDescription"]               =   "Settings - language, country and currency.";
$lang["labelSettingTitleChangeLanguage"]            =   "Change language";
$lang["labelSettingTitleChangeLanguageMessage"]     =   "Click on language what you wnat to use";
$lang["labelSettingTitleChangeCountry"]             =   "Change Country";
$lang["labelSettingTitleChangeCountryMessage"]      =   "Chouse default country";
$lang["labelSettingTitleChangeCurrency"]            =   "Change currency";
$lang["labelSettingTitleChangeCurrencyMessage"]     =   "Chouse defaul currency";
//setting end