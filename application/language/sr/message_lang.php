<?php
$lang["language"] = "sr";

//front message
$lang["frontMessageMain"] 	= "Огласување на trgovski.com је бесплатно и увјек ќе остати бесплатно.";

//menu
$lang["home"] 			= "Почетна";
$lang["search"] 		= "Тражи";
$lang["category"] 		= "Категории";
$lang["insertAds"] 		= "Унесите оглас";
$lang["help"]			= "Помоч";
$lang["setting"]                = "Подешавање";
$lang["about"]			= "Помоч";
$lang["contact"] 		= "Контакт";
$lang["login"] 			= "Пријави се";
$lang["labelMyAccount"] = "Мој налог";
$lang["logout"] 		= "Одјави се";

//footer
$lang["footerMessage"] 						= "trgovski.com креирао vebko.com -  Сва права задржана";
$lang["labelCategoryChoiceCategory"] 		= "Изаберите категорију претраге";
$lang["labelInsertCategoryChoiceCategory"] 	= "Изаберите категорију за унос огласа";
$lang["real_estate"] 						= "Некретнине";
$lang["vehicle"] 							= "Возила";
$lang["electronics"] 						= "Eлекторника";
$lang["clothing"] 							= "Oдећа";
$lang["services"] 							= "Услуге";
$lang["jobs"] 								= "Посао";
$lang["allcategories"] 						= "Све категорије";

//time
$lang["before"]   =  "Пре";
$lang["second"]   =   "друго";
$lang["seconds"]   =   "секунди";
$lang["minute"]   =   "минут";
$lang["minutes"]   =   "минута";
$lang["hour"]   =   "сат";
$lang["hours"]   =   "сати";
$lang["day"]   =   "дан";
$lang["days"]   =   "дани";
$lang["week"]   =   "недеља";
$lang["weeks"]   =   "недељама";
$lang["month"]   =   "месец";
$lang["months"]   =   "месеци";
$lang["year"]   =   "година";
$lang["years"]   =   "године";
     
$lang["vehicle"]   =   "Возило";
$lang["brand"]   =   "Бренд";
$lang["model"]   =   "Модел";
$lang["oil"]   =   "Гориво";
$lang["yearfrom"]   =   "Година од";
$lang["yearto"]   =   "Година до";
$lang["kmfrom"]   =   "Километри од";
$lang["kmto"]   =   "Километри до";
$lang["pricefrom"]   =   "Цена од";
$lang["priceto"]   =   "Цена до";
$lang["all"]   =   "све";
$lang["All"]   =   "Све";
$lang["diesel"]   =   "Дизел";
$lang["oil"]   =   "Бензин";
$lang["gas/petrol"] =  "Гас / бензин";
$lang["electric"]   =   "Електрични";
$lang["hybrid"]   =   "Хибрид";
$lang["other"]   =   "преостало";
$lang["Other"]   =   "Преостало";
     
//meta tags - start     
$lang["labelMetaTagEuro"]   =   "Еуро";
$lang["labelMetaTagProdcutionYear"] = "година";
$lang["sell_adjective"]   =   "Продаја";
$lang["buy_adjective"]   =   "Обавезно купити";
     
$lang["title_meta_tag_vehicle"]   =   "Коришћена возила";
$lang["description_meta_tag_vehicle"]   =   "Избор рабљених возила из различитих брендова и модела";
     
$lang["title_meta_tag_estate"]   =   "Понуда некретнина";
$lang["description_meta_tag_estate"]   =   "Избор нових и коришћених некретнина";
     
$lang["title_meta_tag_electronics"]   =   "Електронски уређаји";
$lang["description_meta_tag_electronics"]   =   "Избор телефона, телевизора и других електронских уређаја";
     
$lang["title_meta_tag_clothing"]   =   "Продаја одеће";
$lang["description_meta_tag_clothing"]   =   "Одабир одеће и обуће";
     
$lang["title_meta_tag_service"]   =   "Услуге";
$lang["description_meta_tag_service"]   =   "Рекламне услуге";
     
$lang["title_meta_tag_other"]   =   "Огласи";
$lang["description_meta_tag_other"]   =   "Огласи";
     
     
$lang["labelMetaTagInsertAdsTitle"]   =   "Убаци оглас";
$lang["labelMetaTagInsertAdsDescription"]   =   "Оглашавање на трговски.цом је бесплатан и увијек ће остати бесплатан";
$lang["labelMetaTagHomeTitle"]   =   "Интернет обавештење";
$lang["labelMetaTagHomeDescription"]   =   "Оглашавање на трговски.цом је бесплатан и увијек ће остати бесплатан";
//meta tags - end     
     
//location countrys, regionas and municipality - start     
$lang["country"]   =   "Држава";
$lang["All country"]   =   "Све државе";
$lang["region"]   =   "Регион";
$lang["All regions"]   =   "Сви региони";
$lang["municipality"]   =   "Општина";
//location countrys, regionas and municipality - end     
     
$lang["buySell"]   =   "Купите";
$lang["buy"]   =   "Купите";
$lang["sell"]   =   "Продајем";
$lang["search"]   =   "Тражи";
$lang["labelSearch"]                            =  "Тражи";
$lang["Rent"]                                   = "Изнајмување";
$lang["labelSearchDescritpion"]                 =  "Изаберите жељене функције";
     
//ADS label - start     
$lang["labelAdsUpdate"]   =   "Измена на оглас";
$lang["labelAdsDescription"]   =   "траже маркице означене са *";
$lang["labelAdsBoxTitle"]   =   "Оглас";
$lang["labelAdsTitle"]   =   "Наслов *";
$lang["labelAdsPrice"]   =   "Цена (у еврима) *";
$lang["labelAdsBody"]   =   "Садржај *";
$lang["labelAdsCategory"]   =   "Категорија *";
$lang["labelAdsBuySell"]   =   "Продај / купи *";
$lang["labelAdsYear"]   =   "Година";
$lang["labelAdsImages"]   =   "Галерија";
$lang["labelAdsImagesDescription"]   =   "Убаците хоризонталне слике. Прва слика је потребна, максимални број дозвољених слика је 6.";
$lang["labelAdsImagesDelete"]   =   "Избриши";
$lang["labelViewSimilarAds"]   =   "Слично";
$lang["labelViewAdd"]   =   "Убаци оглас";
$lang["labelBuySell"]   =   "Купи / Продај";
$lang["labelType"]   =   "Категорија";
$lang["labelAdsDetails"]   =   "Карактеристике";
$lang["labelInsertAds"]   =   "Унесите оглас";
$lang["labelUpdateAds"]   =   "Промени оглас";
$lang["labelInsertAdsDescription"]   =   "* обавезна поља";
$lang["labelInsertAds2"]   =   "Адвертисемент";
$lang["labelUpdateAds2"]   =   "Промени оглас";
$lang["labelInsertAdsTitle"]   =   "Наслов *";
$lang["labelInsertAdsPrice"]   =   "Цена (у еврима) *";
$lang["labelInsertAdsBody"]   =   "Садржај огласа";
$lang["labelInsertInsertImages"]   =   "Убаци слике";
$lang["labelInsertInsertImagesDescription"]   =   "Убаците хоризонталне слике. Прва слика је потребна, максимални број дозвољених слика је 6.";
$lang["labelInsertInsertImagesDelete"]   =   "Избриши";
$lang["labelInsertInsertVehicleDetails"]   =   "Детаљи возила";
$lang["labelInsertInsertVehicleCategory"]   =   "Врста возила *";
$lang["labelInsertInsertVehicleBrand"]   =   "Бранд *";
$lang["labelInsertInsertVehicleModel"]   =   "Модел *";
$lang["labelInsertInsertVehicleBuySell"]   =   "Купи / Продај *";
$lang["labelInsertInsertVehicleFuel"]   =   "Гориво";
$lang["labelInsertInsertVehicleYear"]   =   "Година производње";
$lang["labelInsertInsertVehicleКm"]   =   "Прошли километри";
$lang["labelContact"]   =   "Контакт подаци *";
$lang["labelContactName"]   =   "Име и презиме *";
$lang["labelContactEmail"]   =   "Е-маил адреса";
$lang["labelCountry"]   =   "Земља *";
$lang["labelCountryRegion"]   =   "Регион *";
$lang["labelCountryMunicipality"]   =   "Општина *";
$lang["labelStreet"]   =   "Улица и број";
$lang["labelPhone"]   =   "Телефон";
$lang["labelAdsAgree"]   =   "Ако одбијете унос огласа, слажете се са правилима.";
$lang["labelAdsButtonText"]   =   "Објави оглас";
$lang["labelAdsUpdateButtonText"]   =   "Напишите измене";
$lang["labelAdsCancelButtonText"]   =   "Откажи";
     
//type of vehicle start     
$lang["car"]   =   "Кола";
$lang["trunk"]   =   "Камион";
$lang["other"]   =   "Остало";
//type of vehicle end     
     
//view start     
$lang["labelVehicleNoData"]   =   "нема података";
$lang["labelVehicleSpecification"]   =   "Спецификације";
$lang["labelVehicleType"]   =   "Тип";
$lang["labelVehicleBrand"]   =   "Бренд";
$lang["labelVehicleModel"]   =   "Модел";
$lang["labelVehicleYear"]   =   "Година производње:";
$lang["labelVehiclePastKM"]   =   "Пређена километража:";
$lang["labelVehicleTypeOil"]   =   "Врста погона";
$lang["labelContactPerson"]  =   "Контакт информације";
$lang["labelContactName"]   =   "Име";
$lang["labelPhone"]   =   "Телефон:";
$lang["labelEmail"]   =   "E-mail:";
$lang["labelViewStreet"]   =   "Улица:";
$lang["labelViewMunicipality"]   =   "Општина:";
$lang["labelViewRegion"]   =   "Регион:";
$lang["labelViewCountry"]   =   "Земља:";
$lang["labelViewShare"]   =   "Дељење на друштвеним мрежама";
$lang["labelViewSettings"]   =   "Оглас ";
$lang["labelViewSettingsActivate"]   =   "Активирај";
$lang["labelViewSettingsDeaktivate"]  =   "Деактивирај";
$lang["labelViewSettingsChange"]   =   "Промене";
$lang["labelViewSettingsRenew"]   =   "Ажурирај" ;;
$lang["labelVehiclePublishedDate"]   =   "Објављено у:" ;;
//view end     
     
//estate - start     
$lang["labelSearchEstate"]   =   "Некретнине";
$lang["labelSearchEstateDescritpion"]   =   "Куће, станови, парцеле, нива, итд." ;;
//estate - end     
//Electronics - start     
$lang["labelSearchElectronics"]   =   "Електрични уређаји";
$lang["labelSearchElectronicsDescritpion"]   =   "Мобилни телефони, телевизори, рачунари, кућни апарати, итд." ;;
//Electronics - end     
//vehicle - start     
$lang["labelSearchVehicle"]   =   "Возила";
$lang["labelSearchVehicleDescritpion"]   =   "Аутомобили, камиони, машине за рад, итд" ;;
//vehicle - end     
//Clothing - start     
$lang["labelSearchClothing"]   =   "Одјећа";
$lang["labelSearchClothingDescritpion"] =   "Обућа и одећа" ;
//Clothing - end     
//Service - start     
$lang["labelSearchService"]   =   "Услуге";
$lang["labelSearchServiceDescritpion"] =  "Транспорт, бељење и слично" ;
//Service - end     
//Clothing - start     
$lang["labelSearchOther"]   =   "Остатак";
$lang["labelSearchOtherDescritpion"] =   "Друге категорије које нису укључене у друге";
//Clothing - end     
//insert and update ads - end     
     
//alert message - start     
$lang["AdsSuccessMessage"]   =   "Оглас је објављен.";
$lang["loginSuccessMessage"]   =   "Успешно пријављивање";
$lang["AdsMarkingActivate"]   =   "Оглас је активиран";
$lang["AdsMarkingDeactivate"]   =   "Оглас је Деактивиран - означен као имплементиран";
$lang["AccessDenied"]   =   "Неовлашћена активност";
$lang["adsDeactive"]   =   "Оглас је завршен";
$lang["adsInsertNotAllow"]   =   "Можете унети само један оглас за 3 дана";
$lang["adsRenewNotAllow"]   =   "Можете да ажурирате оглас једном дневно";
$lang["adsRenew"]   =   "Оглас је ажуриран пуним датумом";
$lang["accountNotAllowNewAds"]   =   "Не можете привремено да уносите нове огласе";
//alert message - end     
     
//my account - start     
$lang["labelAccountTitle"]   =   "Мој налог";
$lang["labelAccountBodyPersonalInfoTitle"]   =   "Лични подаци (Фацебоок)";
$lang["labelAccountBodyMyAdsTitle"]   =   "Моји огласи";
$lang["labelAccountSubBodyVehibleTitle"]   =   "Аутомобили";
//my account - end     
     
// estate - start     
$lang["labelEstate"]   =   "Категорија";
$lang["labelEstateType"]   =   "Поглед";
$lang["labelRoomNumber"]   =   "Број соба";
$lang["labelMetro"]   =   "m2";
$lang["labelLocation"]   =   "Локација (адреса)";
// estate - end     
     
// service - start
$lang["Тransport"]                      = "Транспорт";
$lang["Home help and cleaning"]         = "Помоћ у кући и чишћење";
$lang["Craftsmen"]                      = "Занатлије";
$lang["Lessons and Teaching"]           = "Лекције и Настава";
$lang["Bookkeeping / Finance / Law"]    = "Књиговодство/Финансије/Закон";
$lang["Preparation and Food Service"] 	= "Припрема и услуга хране ";
// service - end
// other - start
$lang["Furniture"] 		= "Мебел";
$lang["Job"]                    = "Работа";
$lang["Sports equipment"] 	= "Спортска опрема";
$lang["Medical equipment"] 	= "Медицинска опрема";
// other - end   
//      
//category     
$lang["House"]   =   "Куће";
$lang["Appartments"]   =   "Станови";
$lang["Cottages"]   =   "Викенд";
$lang["Rooms"]   =   "Собе";
$lang["Stores"]   =   "Продавнице";
$lang["Offices"]   =   "Канцеларије";
$lang["Plots"]   =   "Плоче";
$lang["Fields"]   =   "Нивои";
$lang["Others"]   =   "Преостало";
// category - end     
     
//electornics - start     
$lang["Phones"]             = "Мобилни";
$lang["Computers"]          = "Рачунари";
// electornics - end     
     
//insert page - start     
$lang["labelInsertTitle"]                   =  "Унесите оглас";
$lang["labelInsertBodyLine1"]               =   "Оглашавање на трговски.цом је бесплатан и увек ће бити бесплатно бесплатно";
$lang["labelInsertBodyLine2"]               =   "Да бисте унели оглас, потребно је да се пријавите на свој Фацебоок налог (налог)";
$lang["labelInsertBodyLine3"]               =   "Број обавештења није ограничен, све сумњиве рачуне ће бити обустављене";
$lang["labelInsertBodyLine4"]               =   "Изаберите категорију којој први оглас припада";
// insert page - end     
//my account page - start     
$lang["labelAccountSubBodyEstateTitle"]                     = "Некретнине";
$lang["labelAccountSubBodyElectronicsTitle"]                = "Електрични уређаји";
$lang["labelAccountSubBodyClothingTitle"]                   = "Одјећа";
$lang["labelAccountSubBodyServiceTitle"]                    = "Услуге";
$lang["labelAccountSubBodyOtherTitle"]                      = "Преостало";
     
// my account page - end     
     
//test from Database
//location countrys, regionas and municipality - start     
$lang["country"]   =   "Земља:";
$lang["Macedonia"]   =   "Македонија";
$lang["Serbia"]   =   "Србија";
$lang["Bulgaria"]   =   "Бугарска";
     
     
$lang["Skopje City"]   =   "Скопски регион";
$lang["Northeast Region"]   =   "Сјевероисточни регион";
$lang["East Region"]   =   "Источна регија";
$lang["Southeast Region"]   =   "Југоисточни регион";
$lang["Vardar Region"]   =   "Регион Вардара";
$lang["Pelagonian Region"]   =   "Регион Пелагоније";
$lang["Polog Region"]   =   "Полог Регион";
$lang["Southwest Region"]   =   "Југозападни регион";
$lang["municipality"]   =   "Општина";
     
$lang["Aerodrom"]   =   "Аеродром";
$lang["Butel"]   =   "Бутел";
$lang["Gazi Baba"]   =   "Гази Баба";
$lang["Gjorgje Petrov"]   =   "Ђорче Петров";
$lang["Karposh"]   =   "Карпош";
$lang["Kisela Voda"]   =   "Кисела Вода";
$lang["Centar"]   =   "Центар";
$lang["Chair"]   =   "Чаир";
$lang["Shuto Orizari"]   =   "Шуто Оризари";
$lang["Arachinovo"]   =   "Арачиново";
$lang["Zelenikovo"]   =   "Зелениково";
$lang["Ilinden"]   =   "Илинден";
$lang["Petrovec"]   =   "Петровец";
$lang["Sopishte"]   =   "Сопиште";
$lang["Studenichani"]   =   "Студеничани";
$lang["Chucher Sandevo"]   =   "Чучер Сандево";
 
$lang["Kratovo"]   =   "Кратово";
$lang["Kumanovo"]   =   "Куманово";
$lang["Kriva Palanka"]   =   "Крива Паланка";
$lang["Lipkovo"]   =   "Липково";
$lang["Rankovce"]   =   "Ранковце";
$lang["Staro Nagorichane"]   =   "Старо Нагоричане";
  
$lang["Berovo"]   =   "Берово";
$lang["Vinica"]   =   "Виница";
$lang["Delchevo"]   =   "Делчево";
$lang["Zrnovci"]   =   "Зрновци";
$lang["Karbinci"]   =   "Карбинци";
$lang["Kochani"]   =   "Кочани";
$lang["Makedonska Kamenica"]   =   "Македонска Каменица";
$lang["Pehchevo"]   =   "Пехчхево";
$lang["Cheshinovo - Obleshevo"] = "Чешиново - Облешево";
$lang["Shtip"]   =   "Штип";
$lang["Probishtip"]   =   "Пробиштип";
 
$lang["Bogdanci"]   =   "Богданци";
$lang["Bosilovo"]   =   "Босилево";
$lang["Vasilevo"]   =   "Василево";
$lang["Valandovo"]   =   "Валандово";
$lang["Gevgelija"]   =   "Гевгелија";
$lang["Konche"]   =   "Конче";
$lang["Dojran"]   =   "Дојран";
$lang["Novo Selo"]   =   "Ново Село";
$lang["Radovish"]   =   "Радовиш";
$lang["Strumica"]   =   "Струмица";
  
$lang["Veles"]   =   "Велес";
$lang["Gratsko"]   =   "Гратско";
$lang["Demir Kapija"]   =   "Демир Капија";
$lang["Kavadarci"]   =   "Кавадарци";
$lang["Lozovo"]   =   "Лозово";
$lang["Negotino"]   =   "Неготино";
$lang["Rosoman"]  =   "Росоман";
$lang["Sveti Nikole"]   =   "Свети Николе";
$lang["Chashka"]   =   "Чашка";
  
$lang["Bitola"]   =   "Битола";
$lang["Demir Hisar"]   =   "Демир Хисар";
$lang["Krivogashtani"]   =   "Кривогастани";
$lang["Krushevo"]   =   "Крушево";
$lang["Mogila"]   =   "Гроб";
$lang["Prilep"]   =   "Прилеп";
$lang["Resen"]   =   "Ресен";
$lang["Novaci"]   =   "Новинари";
$lang["Dolneni"]   =   "Долнени";
  
$lang["Bogovinje"]   =   "Боговиње";
$lang["Brvenica"]   =   "Брвеница";
$lang["Vrapchishte"]   =   "Врапцисте";
$lang["Gostivar"]   =   "Гостивар";
$lang["Zhelino"]   =   "Зелино";
$lang["Jagunovce"]   =   "Јагуновце";
$lang["Mavrovo i Rostushe"]   =   "Маврово и Ростусхе";
$lang["Tearce"]   =   "Теарце";
$lang["Tetovo"]   =   "Тетово";
 
$lang["Vevchani"]   =   "Вевцани";
$lang["Debar"]   =   "Дебар";
$lang["Debarca"]   =   "Дебарца";
$lang["Kichevo"]   =   "Кичево";
$lang["Makedonski Brod"]   =   "Македонски Брод";
$lang["Ohrid"]   =   "Охрид";
$lang["Plasnica"]   =   "Пласница";
$lang["Struga"]   =   "Струга";
$lang["Centar Zupa"]   =   "Центар Жупа";

$lang["Vojvodina"]              =   "Војводина";
$lang["Belgrade"]               =   "Београд";
$lang["Sumadija and Western Serbia"]   =   "Шумадија и Западна Србија";
$lang["Southern and Eastern Serbia"]   =   "Јужна и Источна Србија";
$lang["Kosovo and Metohija"]   =   "Косово и Метохија";


$lang["Zrenjanin"]   =   "Зрењанин";
$lang["Subotica"]   =   "Суботица";
$lang["Kikinda"]   =   "Кикинда";
$lang["Novi Sad"]   =   "Нови Сад";
$lang["Pancevo"]   =   "Панчево";
$lang["Sremska Mitrovica"]   =   "Сремска Митровица";
$lang["Sombor"]   =   "Сомбор";
$lang["Barajevo"]   =   "Барајево";
$lang["Cukarica"]   =   "Цукарица";
$lang["Grocka"]   =   "Гроцка";

$lang["Lazarevac"]   =   "Лазаревац";
$lang["Mladenovac"]   =   "Младеновац";
$lang["Novi Beograd"]   =   "Нови Београд";
$lang["Obrenovac"]   =   "Обреновац";
$lang["Palilula"]   =   "Палилула";
$lang["Rakovica"]   =   "Раковица";
$lang["Savski Venac"]   =   "Савски Венац";
$lang["Sopot"]   =   "Сопот";
$lang["Stari Grad"]   =   "Стари Град";
$lang["Surcin"]   =   "Сурцин";

$lang["Vozdovac"]   =   "Воздовац";
$lang["Vracar"]   =   "Врацар";
$lang["Zemun"]   =   "Земун";
$lang["Zvezdara"]   =   "Звездара";
$lang["Kragujevac"]   =   "Крагујевац";
$lang["Krusevac"]   =   "Крусевац";
$lang["Kraljevo"]   =   "Краљево";
$lang["Sabac"]   =   "Шабац";
$lang["Cacak"]   =   "Чацак";
$lang["Novi Pazar"]   =   "Нови Пазар";

$lang["Valjevo"]   =   "Ваљево";
$lang["Loznica"]   =   "Лозница";
$lang["Uzice"]   =   "Узице";
$lang["Jagodina"]   =   "Јагодина";
$lang["Paraцcin"]   =   "Параћин";
$lang["Arangjelovac"]   =   "Арангјеловац";
$lang["Gornji Milanovac"]   =   "Горњи Милановац";
$lang["Trstenik"]   =   "Трстеник";
$lang["Nis"]   =   "Нис";
$lang["Leskovac"]   =   "Лесковац";

$lang["Smederevo"]   =   "Смедерево";
$lang["Vranje"]   =   "Врање";
$lang["Pozarevac"]   =   "Позаревац";
$lang["Zajecar"]   =   "Зајецар";
$lang["Pirot"]   =   "Пирот";
$lang["Aleksinac"]   =   "Алексинац";
$lang["Smederevska Palanka"]   =   "Смедеревска Паланка";
$lang["Bor"]   =   "Бор";
$lang["Prokuplje"]   =   "Прокупље";
$lang["Velika Plana"]   =   "Велика Плана";

$lang["Pristina"]   =   "Приштина";
$lang["Gnjilane"]   =   "Гњилане";
$lang["Kosovska Mitrovica"]   =   "Косовска Митровица";
$lang["Pec"]   =   "Пец";
$lang["Prizren"]   =   "Призрен";

//store - start  =  #ВАЛУЕ!
$lang["labelAccountStoreInformation"]                       = "Информације о продавници";
$lang["labelAccountStoreInformationStoreName"]              = "Име трговине:";
$lang["labelAccountStoreInformationStoreUrl"]               = "Веза (УРЛ) у радњу:";
$lang["labelAccountStoreInformationCreateStoreTitle"]       = "Отвори продавница";
$lang["labelInsertStoreChange"]                             = "Уреди";
$lang["labelInsertStoreChangeStore"]                        = "Напиши измене";
$lang["labelAccountStoreInformationCreateStoreBody"]        = "Трговац вам омогућава да отворите сопствену продавницу у којој ће бити достављени само ваши производи.";
$lang["labelAccountStoreInformationCreateStoreButtonTitle"] = "Промени податке";
$lang["labelInsertStore"]                                   = "Креирај продавца";
$lang["storeNotExist"]                                      = "Продавница не постоји";
$lang["labelInsertStore"]                                   = "Креирај продавца";
$lang["labelInsertStoreBoxTitle"]                           = "Податоци за продавницу"; 
$lang["labelInsertStoreTitle"]                              = "Име продавнице *";
$lang["labelInsertStoreBody"]                               = "Име продавнице ће такође бити УРЛ адреса, тако да унесете само једну реч на латиничну абецеду за име.";
$lang["labelInsertStoreOpenStore"]                          = "Креирај продавницу";
$lang["labelInsertStoreAgree"]                              = "Кликом на креирај продавницу слажете се са услугама трговца.";
$lang["StoreErrorUserStoreExist"]                           = "Већ сте креирали сопствену продавницу";
$lang["StoreErrorStoreNameTaken"]                           = "Име је заузет покушати друго име";
$lang["StoreSuccessCreated"]                                = "Успешно креирана продавница";
$lang["StoresTitle"]                                        = "Продавници";
$lang["StoresTitleAll"]                                     = "Сите";
$lang["StoresSubTitle"]                                     = "Trgovski.com продавници";
//store - end 

//curency start
$lang["currency"]   =   "Валута:";
$lang["eur"]   =   "Евро";
$lang["mkd"]   =   "Денари";
$lang["rsd"]   =   "Динари";
$lang["bgn"]   =   "Лева";
//curency end



//setting start
$lang["labelSettingTitle"]                          =   "Подесување";
$lang["labelSettingTitleDescription"]               =   "Поставка језика, Земља и валута.";
$lang["labelSettingTitleChangeLanguage"]            =   "Промените језик";
$lang["labelSettingTitleChangeLanguageMessage"]     =   "Кликните на везу да бисте промијенили језик у складу с тим.";
$lang["labelSettingTitleChangeCountry"]             =   "Промени земљу";
$lang["labelSettingTitleChangeCountryMessage"]      =   "Ако кликнете на име државе, ви подразумевате државу претраживања";
$lang["labelSettingTitleChangeCurrency"]            =   "Промена валуте";
$lang["labelSettingTitleChangeCurrencyMessage"]     =   "Клик на валуту је прилагодио основну валуту.";
//setting end