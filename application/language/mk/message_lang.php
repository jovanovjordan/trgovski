<?php
$lang["language"]                               = "mk";

//front message
$lang["frontMessageMain"]                       = "Огласувањето на trgovski.com е бесплатно и секогаш ќе остане бесплатно.";

//menu
$lang["home"]                                   = "Почетна";
$lang["search"]                                 = "Пребарувај";
$lang["category"]                               = "Категории";
$lang["insertAds"]                                  = "Внеси оглас";
$lang["help"]			= "Помош";
$lang["setting"]                                  = "Подесување";
$lang["about"]			= "Помош";
$lang["contact"] 		= "Контакт";
$lang["login"] 			= "Логирај се";
$lang["labelMyAccount"] = "Моја сметка";
$lang["logout"] 		= "Одлогирај се";

//footer
$lang["footerMessage"] 						= "trgovski.com креирано од vebko.com - Сите права задржани";
$lang["labelCategoryChoiceCategory"] 		= "Изберете категорија за пребарување";
$lang["labelInsertCategoryChoiceCategory"] 	= "Изберете категорија во која ќе внесите оглас";
$lang["real_estate"] 						= "Недвижности";
$lang["estate"] 						= "Недвижности";
$lang["vehicle"] 							= "возила";
$lang["vehicles"] 							= "Возила";
$lang["electronics"] 						= "електорника";
$lang["clothing"] 							= "облека";
$lang["services"] 							= "услуги";
$lang["service"] 							= "Услуги";
$lang["jobs"] 								= "работа";
$lang["allcategories"] 						= "сите категории";

//time
$lang["before"] 	= "Пред";
$lang["second"] 	= "секунда";
$lang["seconds"] 	= "секунди";
$lang["minute"] 	= "минута";
$lang["minutes"] 	= "минути";
$lang["hour"] 		= "час";
$lang["hours"] 		= "часа";
$lang["day"] 		= "ден";
$lang["days"] 		= "дена";
$lang["week"] 		= "седмица";
$lang["weeks"] 		= "седмици";
$lang["month"] 		= "месец";
$lang["months"] 	= "месеци";
$lang["year"] 		= "година";
$lang["years"] 		= "години";

$lang["vehicle"] 	= "Возилa";
$lang["brand"] 		= "Бранд";
$lang["model"] 		= "Модел";
$lang["oil"] 		= "Гориво";
$lang["yearfrom"] 	= "Година од";
$lang["yearto"] 	= "Година до";
$lang["kmfrom"] 	= "Километри од";
$lang["kmto"] 		= "Километри до";
$lang["pricefrom"] 	= "Цена од";
$lang["priceto"] 	= "Цена до";
$lang["all"] 		= "Сите";
$lang["All"] 		= "Сите";
$lang["diesel"] 	= "Дизел";
$lang["oil"] 		= "Бензин";
$lang["gas/petrol"] = "Плин/Бензин";
$lang["electric"] 	= "Електричен";
$lang["hybrid"] 	= "Хибрид";
$lang["other"] 		= "Останато";
$lang["Other"] 		= "Останато";

//meta tags - start
$lang["labelMetaTagEuro"] 			= "Евра";
$lang["labelMetaTagProdcutionYear"] = " год.";
$lang["sell_adjective"] 			= "Се продава ";
$lang["buy_adjective"] 				= "Се бара за купување ";

$lang["title_meta_tag_vehicle"] 			= "Половни автомобили";
$lang["description_meta_tag_vehicle"] 		= "Избор на половни возила од различни брендови и модели";

$lang["title_meta_tag_estate"] 				= "Понуда на недвижности";
$lang["description_meta_tag_estate"] 		= "Избор на нови и користени недвижности";

$lang["title_meta_tag_electronics"] 		= "Електрониски уреди";
$lang["description_meta_tag_electronics"] 	= "Избор на телефони, телевизори и други електронски уреди";

$lang["title_meta_tag_clothing"] 			= "Продажба на облека";
$lang["description_meta_tag_clothing"] 		= "Избор на облека и обувки";

$lang["title_meta_tag_service"] 			= "Услуги";
$lang["description_meta_tag_service"] 		= "Огласување на услуги";

$lang["title_meta_tag_other"] 				= "Огласи";
$lang["description_meta_tag_other"] 		= "Огласи";


$lang["labelMetaTagInsertAdsTitle"] 		= "Внеси оглас";
$lang["labelMetaTagInsertAdsDescription"] 	= "Огласувањето на trgovski.com е бесплатно и засекогаш ќе остане бесплатно";
$lang["labelMetaTagHomeTitle"] 				= "Интернет огласник";
$lang["labelMetaTagHomeDescription"] 		= "Огласувањето на trgovski.com е бесплатно и засекогаш ќе остане бесплатно";
//meta tags - end

//location countrys, regionas and municipality - start
$lang["country"] 		= "Државa";
$lang["All country"] 	= "Сите Држави";
$lang["region"] 		= "Регион";
$lang["All regions"] 	= "Сите Региони";
$lang["municipality"] 	= "Municipality";
//location countrys, regionas and municipality - end

$lang["buySell"] 				= "Купувам";
$lang["buy"] 					= "Купувам";
$lang["sell"] 					= "Продавам";
$lang["search"] 				= "Пребарај";
$lang["labelSearch"]                            = "Пребарај";
$lang["Rent"]                                   = "Изнајмување";
$lang["rent"]                                   = "Изнајмување";
$lang["labelSearchDescritpion"]                 = "Изберете ги посакуваните карактеристики";

//ADS label - start
$lang["labelAdsUpdate"] 					= "Измена на оглас";
$lang["labelAdsDescription"] 				= "полоњата означени со * се задолжителни";
$lang["labelAdsBoxTitle"] 					= "Оглас";
$lang["labelAdsTitle"] 						= "Наслов*";
$lang["labelAdsPrice"] 						= "Цена (во евра)*";
$lang["labelAdsBody"] 						= "Содржина*";
$lang["labelAdsCategory"] 					= "Категорија*";
$lang["labelAdsBuySell"] 					= "Продавам / Купувам*";
$lang["labelAdsYear"] 						= "Година";
$lang["labelAdsImages"] 					= "Галерија";
$lang["labelAdsImagesDescription"] 			= "Внесувајте хоризонтални слики. Првата слика е задожителна, максималниот број на дозволени слики е 6.";
$lang["labelAdsImagesDelete"] 				= "Избриши";
$lang["labelViewSimilarAds"] 				= "Слични";
$lang["labelViewAdd"] 						= "Внеси оглас";
$lang["labelBuySell"] 						= "Купувам/Продавам";
$lang["labelType"] 							= "Категорија";
$lang["labelAdsDetails"] 					= "Карактеристики";
$lang["labelInsertAds"] 					= "Внеси Оглас";
$lang["labelUpdateAds"] 					= "Промени Оглас";
$lang["labelInsertAdsDescription"] 			= "* задолжителни полиња";
$lang["labelInsertAds2"] 					= "Оглас";
$lang["labelUpdateAds2"] 					= "Промени оглас";
$lang["labelInsertAdsTitle"] 				= "Наслов*";
$lang["labelInsertAdsPrice"] 				= "Цена (во евра)*";
$lang["labelInsertAdsBody"] 				= "Содржина на оглас*";
$lang["labelInsertInsertImages"] 			= "Внеси слики";
$lang["labelInsertInsertImagesDescription"] = "Внесувајте хоризонтални слики. Првата слика е задожителна, максималниот број на дозволени слики е 6.";
$lang["labelInsertInsertImagesDelete"] 		= "Избриши";
$lang["labelInsertInsertVehicleDetails"] 	= "Детали за возилото";
$lang["labelInsertInsertVehicleCategory"] 	= "Тип на возило*";
$lang["labelInsertInsertVehicleBrand"] 		= "Бранд*";
$lang["labelInsertInsertVehicleModel"] 		= "Модел*";
$lang["labelInsertInsertVehicleBuySell"] 	= "Купувам/Продавам*";
$lang["labelInsertInsertVehicleFuel"] 		= "Гориво";
$lang["labelInsertInsertVehicleYear"] 		= "Година на производство";
$lang["labelInsertInsertVehicleКm"] 		= "Изминати километри";
$lang["labelContact"] 						= "Контакт податоци*";
$lang["labelContactName"] 					= "Име и презиме*";
$lang["labelContactEmail"] 					= "Електронска адреса (email)";
$lang["labelCountry"] 						= "Држава*";
$lang["labelCountryRegion"] 				= "Регион*";
$lang["labelCountryMunicipality"] 			= "Општина*";
$lang["labelStreet"] 						= "Улица и број";
$lang["labelPhone"] 						= "Телефон";
$lang["labelAdsAgree"] 						= "Со претискање на Внеси оглас се сложувате со правилата.";
$lang["labelAdsButtonText"] 				= "Објави оглас";
$lang["labelAdsUpdateButtonText"] 			= "Запиши ги промените";
$lang["labelAdsCancelButtonText"] 			= "Откажи";

//type of vehicle start
$lang["car"] 	= "Автомобил";
$lang["trunk"] 	= "Камион";
$lang["other"] 	= "Друго";
//type of vehicle end

//view start
$lang["labelVehicleNoData"] 		= "нема информации";
$lang["labelVehicleSpecification"] 	= "Спецификации";
$lang["labelVehicleType"] 			= "Тип";
$lang["labelVehicleBrand"] 			= "Бранд";
$lang["labelVehicleModel"] 			= "Модел";
$lang["labelVehicleYear"] 			= "Година на производство: ";
$lang["labelVehiclePastKM"] 		= "Поминати километри: ";
$lang["labelVehicleTypeOil"] 		= "Вид на погон";
$lang["labelContactPerson"]			= "Контакт информации";
$lang["labelContactName"] 			= "Име";
$lang["labelPhone"] 				= "Телефон: ";
$lang["labelEmail"] 				= "Емаил: ";
$lang["labelViewStreet"] 			= "Улица:";
$lang["labelViewMunicipality"] 		= "Општина: ";
$lang["labelViewRegion"] 			= "Регион: ";
$lang["labelViewCountry"] 			= "Држава: ";
$lang["labelViewShare"] 			= "Сподели на социјалните мрежи";
$lang["labelViewSettings"] 			= "Сетирање на оглас";
$lang["labelViewSettingsActivate"] 	= "Активирај";
$lang["labelViewSettingsDeaktivate"] = "Деактивирај";
$lang["labelViewSettingsChange"] 	= "Промени";
$lang["labelViewSettingsRenew"] 	= "Обнови";
$lang["labelVehiclePublishedDate"] 	= "Објавено во: ";
$lang["labelMarketingTitle"] 		= "Рекламa";
//view end

//estate - start
$lang["labelSearchEstate"] 				= "Недвижности";
$lang["labelSearchEstateDescritpion"] 	= "Куќи, Станови, Плацови, Ниви и сл.";;
//estate - end
//Electronics - start
$lang["labelSearchElectronics"] 		= "Електрични уреди";
$lang["labelSearchElectronicsDescritpion"] 	= "Мобилни телефони, Телевизори, Компјутери, Машизни за домакинство и сл.";;
//Electronics - end
//vehicle - start
$lang["labelSearchVehicle"] 			= "Возила";
$lang["labelSearchVehicleDescritpion"] 	= "Автомобили, Камиони, Работни машини и сл.";;
//vehicle - end
//Clothing - start
$lang["labelSearchClothing"] 			= "Облека";
$lang["labelSearchClothingDescritpion"] = "Обувки и облека";;
//Clothing - end
//Service - start
$lang["labelSearchService"] 			= "Услуги";
$lang["labelSearchServiceDescritpion"] = "Превоз, Белење и слично";;
//Service - end
//Clothing - start
$lang["labelSearchOther"] 			= "Останото";
$lang["labelSearchOtherDescritpion"] = "Останати категории кои не се вклучени во другите";;
//Clothing - end
//insert and update ads - end

//alert message - start
$lang["AdsSuccessMessage"] 		= "Огласот е објавен.";
$lang["loginSuccessMessage"] 	= "Успечно најавување";
$lang["AdsMarkingActivate"] 	= "Огласот е Активиран";
$lang["AdsMarkingDeactivate"] 	= "Огласот е Деактивиран - Означен како реализирано";
$lang["AccessDenied"] 			= "Недозволена активност";
$lang["adsDeactive"] 			= "Огласот е завршен";
$lang["adsInsertNotAllow"] 		= "Може да внесете само еден оглас во 3 дена";
$lang["adsRenewNotAllow"] 		= "Може да го обновувате огласот еднаш дневно";
$lang["adsRenew"] 				= "Огласот е обновен со денешна дата";
$lang["accountNotAllowNewAds"] 	= "Привремено не може да внесувате нови огласи";
//alert message - end

//my account - start
$lang["labelAccountTitle"] 					= "Моја сметка";
$lang["labelAccountBodyPersonalInfoTitle"] 	= "Лични податоци (Facebook)";
$lang["labelAccountBodyMyAdsTitle"] 		= "Мој огласи";
$lang["labelAccountSubBodyVehibleTitle"] 	= "Автомобили";
//my account - end

// estate - start
$lang["labelEstate"] 		= "Категорија";
$lang["labelEstateType"] 	= "Вид";
$lang["labelRoomNumber"] 	= "Број на простории";
$lang["labelMetro"] 		= "Метри Квадратни";
$lang["labelLocation"] 		= "Локација (Адреса)";
// estate - end

// service - start
$lang["Тransport"]                      = "Транспорт";
$lang["Home help and cleaning"]         = "Домашна помош и чистење";
$lang["Craftsmen"]                      = "Занаетчии";
$lang["Lessons and Teaching"]           = "Часови и Подучување";
$lang["Bookkeeping / Finance / Law"]    = "Книговодство/Финансии/Право";
$lang["Preparation and Food Service"] 	= "Припрема на храна и услуга";
// service - end
// other - start
$lang["Furniture"] 		= "Мебел";
$lang["Job"]                    = "Работа";
$lang["Sports equipment"] 	= "Спортска опрема";
$lang["Medical equipment"] 	= "Медицинска опрема";
// other - end
// 
//category
$lang["House"] 			= "Куќи";
$lang["Appartments"] 	= "Станови";
$lang["Cottages"] 		= "Викендици";
$lang["Rooms"] 			= "Соби";
$lang["Stores"] 		= "Продавници";
$lang["Offices"] 		= "Канцеларии";
$lang["Plots"] 			= "Плацови";
$lang["Fields"] 		= "Ниви";
$lang["Others"] 		= "Останато";
// category - end

//electornics - start
$lang["Phones"]                 = "Мобилни";
$lang["Computers"]              = "Компјутери";
$lang["TV Audio Video"]         = "ТВ Аудио Видео";
$lang["White goods"]            = "Бела Техника";
$lang["Household appliances"]   = "Кујнски Апарати";
$lang["Cameras"]                = "Фото апарати и камери";
$lang["Cooling and heating"]    = "Ладење и греење";
// electornics - end

//insert page - start
$lang["labelInsertTitle"]                   = "Внеси Оглас";
$lang["labelInsertBodyLine1"]               = "Огласувањето на trgovski.com  е бесплатно и засекога ќе остане бесплатно";
$lang["labelInsertBodyLine2"]               = "За да внесете оглас потребно е да се логирате со Вашата Facebook сметка (account)";
$lang["labelInsertBodyLine3"]               = "Бројот на огласи не е ограничен, сите сомнителни сметки ќе бидат суспендирани";
$lang["labelInsertBodyLine4"]               = "За почеток изберете категорија на која припаѓа Вашиот Оглас";
// insert page - end
//my account page - start
$lang["labelAccountSubBodyEstateTitle"]                     = "Недвижности";
$lang["labelAccountSubBodyElectronicsTitle"]                = "Електрични уреди";
$lang["labelAccountSubBodyClothingTitle"]                   = "Облека";
$lang["labelAccountSubBodyServiceTitle"]                    = "Услуги";
$lang["labelAccountSubBodyOtherTitle"]                      = "Останато";

// my account page - end

//test from Database
//location countrys, regionas and municipality - start
$lang["country"] 	= "Државa: ";
$lang["Macedonia"] 	= "Македонија";
$lang["Serbia"] 	= "Србија";
$lang["Bulgaria"] 	= "Бугарија";


$lang["Skopje City"] 		= "Скопски Регион";
$lang["Northeast Region"] 	= "Североисточен Регион";
$lang["East Region"] 		= "Источен Регион";
$lang["Southeast Region"] 	= "Југоисточен Регион";
$lang["Vardar Region"] 		= "Вардарски Регион";
$lang["Pelagonian Region"] 	= "Пелагониски Регион";
$lang["Polog Region"] 		= "Полошки Регион";
$lang["Southwest Region"] 	= "Југозападен Регион";
$lang["municipality"] 		= "Општина";

$lang["Aerodrom"] 				= "Аеродром";
$lang["Butel"] 					= "Бутел";
$lang["Gazi Baba"] 				= "Гази Баба";
$lang["Gjorgje Petrov"] 		= "Ѓорче Петров";
$lang["Karposh"] 				= "Карпош";
$lang["Kisela Voda"] 			= "Кисела Вода";
$lang["Centar"] 				= "Центар";
$lang["Chair"] 					= "Чаир";
$lang["Shuto Orizari"] 			= "Шуто Оризари";
$lang["Arachinovo"] 			= "Арачиново";
$lang["Zelenikovo"] 			= "Зелениково";
$lang["Ilinden"] 				= "Илинден";
$lang["Petrovec"] 				= "Петровец";
$lang["Sopishte"] 				= "Сопиште";
$lang["Studenichani"] 			= "Студеничани";
$lang["Chucher Sandevo"] 		= "Чучер Сандево";

$lang["Kratovo"] 				= "Кратово";
$lang["Kumanovo"] 				= "Куманово";
$lang["Kriva Palanka"] 			= "Крива Паланка";
$lang["Lipkovo"] 				= "Липково";
$lang["Rankovce"] 				= "Ранковце";
$lang["Staro Nagorichane"] 		= "Старо Нагоричане";

$lang["Berovo"] 				= "Верово";
$lang["Vinica"] 				= "Виница";
$lang["Delchevo"] 				= "Длечево";
$lang["Zrnovci"] 				= "Зрновци";
$lang["Karbinci"] 				= "Карбинци";
$lang["Kochani"] 				= "Кочани";
$lang["Makedonska Kamenica"] 	= "Македонска Каменица";
$lang["Pehchevo"] 				= "Пехчево";
$lang["Cheshinovo - Obleshevo"] = "Чешиново - Облешево";
$lang["Shtip"] 					= "Штип";
$lang["Probishtip"] 			= "Пробиштип";

$lang["Bogdanci"] 				= "Боданци";
$lang["Bosilovo"] 				= "Босилево";
$lang["Vasilevo"] 				= "Василево";
$lang["Valandovo"] 				= "Валандово";
$lang["Gevgelija"] 				= "Гевгелија";
$lang["Konche"] 				= "Конче";
$lang["Dojran"] 				= "Дојран";
$lang["Novo Selo"] 				= "Ново Село";
$lang["Radovish"] 				= "Радовиш";
$lang["Strumica"] 				= "Струмица";

$lang["Veles"] 					= "Велес";
$lang["Gratsko"] 				= "Гратско";
$lang["Demir Kapija"] 			= "Демир Капија";
$lang["Kavadarci"] 				= "Кавадарци";
$lang["Lozovo"] 				= "Лозово";
$lang["Negotino"] 				= "Неготино";
$lang["Rosoman"]				= "Росоман";
$lang["Sveti Nikole"] 			= "Свети Николе";
$lang["Chashka"] 				= "Чашка";

$lang["Bitola"] 				= "Битола";
$lang["Demir Hisar"] 			= "Демир Хисар";
$lang["Krivogashtani"] 			= "Кривогаштани";
$lang["Krushevo"] 				= "Крушево";
$lang["Mogila"] 				= "Могила";
$lang["Prilep"] 				= "Прилеп";
$lang["Resen"] 					= "Ресен";
$lang["Novaci"] 				= "Новаци";
$lang["Dolneni"] 				= "Долнени";

$lang["Bogovinje"] 				= "Боговиње";
$lang["Brvenica"] 				= "Брвеница";
$lang["Vrapchishte"] 			= "Врапчиште";
$lang["Gostivar"] 				= "Гостивар";
$lang["Zhelino"] 				= "Желино";
$lang["Jagunovce"] 			 	= "Јагуновце";
$lang["Mavrovo i Rostushe"] 	= "Маврово и Ростуше";
$lang["Tearce"] 				= "Теарце";
$lang["Tetovo"] 				= "Тетово";

$lang["Vevchani"] 				= "Вевчани";
$lang["Debar"] 					= "Дебар";
$lang["Debarca"] 				= "Дебарца";
$lang["Kichevo"] 				= "Кичево";
$lang["Makedonski Brod"] 		= "Македонски Брод";
$lang["Ohrid"] 					= "Охрид";
$lang["Plasnica"] 				= "Пласница";
$lang["Struga"] 				= "Струга";
$lang["Centar Zupa"] 			= "Центар Жупа";


$lang["Vojvodina"]              =   "Војводина";
$lang["Belgrade"]               =   "Београд";
$lang["Sumadija and Western Serbia"]   =   "Шумадија и Западна Србија";
$lang["Southern and Eastern Serbia"]   =   "Јужна и Источна Србија";
$lang["Kosovo and Metohija"]   =   "Косово и Метохија";


$lang["Zrenjanin"]   =   "Зрењанин";
$lang["Subotica"]   =   "Суботица";
$lang["Kikinda"]   =   "Кикинда";
$lang["Novi Sad"]   =   "Нови Сад";
$lang["Pancevo"]   =   "Панчево";
$lang["Sremska Mitrovica"]   =   "Сремска Митровица";
$lang["Sombor"]   =   "Сомбор";
$lang["Barajevo"]   =   "Барајево";
$lang["Cukarica"]   =   "Цукарица";
$lang["Grocka"]   =   "Гроцка";

$lang["Lazarevac"]   =   "Лазаревац";
$lang["Mladenovac"]   =   "Младеновац";
$lang["Novi Beograd"]   =   "Нови Београд";
$lang["Obrenovac"]   =   "Обреновац";
$lang["Palilula"]   =   "Палилула";
$lang["Rakovica"]   =   "Раковица";
$lang["Savski Venac"]   =   "Савски Венац";
$lang["Sopot"]   =   "Сопот";
$lang["Stari Grad"]   =   "Стари Град";
$lang["Surcin"]   =   "Сурцин";

$lang["Vozdovac"]   =   "Воздовац";
$lang["Vracar"]   =   "Врацар";
$lang["Zemun"]   =   "Земун";
$lang["Zvezdara"]   =   "Звездара";
$lang["Kragujevac"]   =   "Крагујевац";
$lang["Krusevac"]   =   "Крусевац";
$lang["Kraljevo"]   =   "Краљево";
$lang["Sabac"]   =   "Шабац";
$lang["Cacak"]   =   "Чацак";
$lang["Novi Pazar"]   =   "Нови Пазар";

$lang["Valjevo"]   =   "Ваљево";
$lang["Loznica"]   =   "Лозница";
$lang["Uzice"]   =   "Узице";
$lang["Jagodina"]   =   "Јагодина";
$lang["Paraцcin"]   =   "Параћин";
$lang["Arangjelovac"]   =   "Арангјеловац";
$lang["Gornji Milanovac"]   =   "Горњи Милановац";
$lang["Trstenik"]   =   "Трстеник";
$lang["Nis"]   =   "Нис";
$lang["Leskovac"]   =   "Лесковац";

$lang["Smederevo"]   =   "Смедерево";
$lang["Vranje"]   =   "Врање";
$lang["Pozarevac"]   =   "Позаревац";
$lang["Zajecar"]   =   "Зајецар";
$lang["Pirot"]   =   "Пирот";
$lang["Aleksinac"]   =   "Алексинац";
$lang["Smederevska Palanka"]   =   "Смедеревска Паланка";
$lang["Bor"]   =   "Бор";
$lang["Prokuplje"]   =   "Прокупље";
$lang["Velika Plana"]   =   "Велика Плана";

$lang["Pristina"]   =   "Приштина";
$lang["Gnjilane"]   =   "Гњилане";
$lang["Kosovska Mitrovica"]   =   "Косовска Митровица";
$lang["Pec"]   =   "Пец";
$lang["Prizren"]   =   "Призрен";

//bulgaris start
$lang["Sofia City"]         =   "Софија (град)";
$lang["Plovdiv"]            =   "Пловдив";
$lang["Varna"]              =   "Варна";
$lang["Burgas"]             =   "Бургас";
$lang["Stara Zagora"]       =   "Стара Загора";
$lang["Blagoevgrad"]        =   "Благоевград";
$lang["Pleven"]             =   "Плевел";
$lang["Pazardzhik"]         =   "Пазарџик";
$lang["Veliko Tarnovo"]     =   "Велико Трново";
$lang["Sofia (province)"]   =   "Софија (околина)";

$lang["Ruse"]               =   "Русе";
$lang["Vratsa"]             =   "Врашта";
$lang["Sliven"]             =   "Силвен";
$lang["Dobrich"]            =   "Добрич";
$lang["Shumen"]             =   "Шумен";
$lang["Montana"]            =   "Монтана";
$lang["Lovech"]             =   "Ловец";
$lang["Kardzhali"]          =   "Карџали";
$lang["Kyustendil"]         =   "Ќустендил";
$lang["Yambol"]             =   "Јамбол";

$lang["Razgrad"]            =   "Разград";
$lang["Pernik"]             =   "Перник";
$lang["Gabrovo"]            =   "Габрово";
$lang["Silistra"]           =   "Силишта";
$lang["Smolyan"]            =   "Шмолиан";
$lang["Targovishte"]        =   "Тарговиште";
$lang["Vidin"]              =   "Видин";
$lang["Haskovo"]            =   "Хасково";

$lang["Bankya"]   			=   "Банкуа";
$lang["Buhovo"]   			=   "Бухово";
$lang["Novi Iskar"]   		=   "Нови Искар";
$lang["Sofia"]   			=   "Софиа";
$lang["Asenovgrad"]   		=   "Асеновград";
$lang["Brezovo"]   			=   "Брезово";
$lang["Hisarya"]   			=   "Хисара";
$lang["Kaloyanovo"]   		=   "Калојаво";
$lang["Karlovo"]   			=   "Карлово";
$lang["Krichim"]   			=   "Кричим";

$lang["Kuklen"]   			=   "Куклен";
$lang["Laki"]   			=   "Лаки";
$lang["Maritsa (Plovdiv rural)"]   =   "Мориста (Пловдив провинција)";
$lang["Perushtitsa"]   		=   "Перуштица";
$lang["Plovdiv (city)"]   	=   "Пловдив - град";
$lang["Parvomay"]   		=   "Парвомај";
$lang["Rakovski"]   		=   "Раковски";
$lang["Rodopi (Plovdiv rural)"]   =   "Радопи (Пловдив провинција)";
$lang["Sadovo"]   			=   "Садово";
$lang["Sopot"]   			=   "Сопот";

$lang["Stamboliyski"]   	=   "Стамболски";
$lang["Saedinenie"]   		=   "Соединение";
$lang["Avren"]   			=   "Аврен";
$lang["Aksakovo"]   		=   "Аксаково";
$lang["Beloslav"]   		=   "Велослав";
$lang["Byala"]  			=   "Бела";
$lang["Varna"]   			=   "Варна";
$lang["Vetrino"]   			=   "Ветрино";
$lang["Valchi Dol"]   		=   "Валчи Дол";
$lang["Dolni Chiflik"]   	=   "Долник Чивлик";

$lang["Devnya"]   			=   "Девниа";
$lang["Dalgopol"]   		=   "Далгопол";
$lang["Provadiya"]   		=   "Правадија";
$lang["Suvorovo"]   		=   "Сиворово";
$lang["Aytos"]   			=   "Аитос";
$lang["Burgas"]   			=   "Бургас";
$lang["Kameno"]   			=   "Камено";
$lang["Karnobat"]   		=   "Ларнобат";
$lang["Malko Tarnovo"]   	=   "Малко Тарново";
$lang["Nesebar"]   			=   "Несебар";

$lang["Pomorie"]   			=   "Поморие";
$lang["Primorsko"]   		=   "Приморско";
$lang["Ruen"]   			=   "Руен";
$lang["Sozopol"]   			=   "Созопол";
$lang["Sredets"]   			=   "Средец";
$lang["Sungurlare"]   		=   "Сунгуларе";
$lang["Tsarevo"]   			=   "Царево";
$lang["Bratya"]   			=   "Братиа";
$lang["Chirpan"]   			=   "Чирпан";
$lang["Gurkovo"]   			=   "Гурково";

$lang["Galabovo"]   		=   "Галабово";
$lang["Kazanlak"]   		=   "Казанлак";
$lang["Maglizh"]   			=   "Маглиз";
$lang["Nikolaevo"]   		=   "Николаево";
$lang["Opan"]   			=   "Опан";
$lang["Pavel Banya"]   		=   "Павел Бања";
$lang["Radnevo"]   			=   "Раднево";
$lang["Stara Zagora"]   	=   "Стара Загора";
$lang["Bansko"]   			=   "Банско";
$lang["Belitsa"]   			=   "Белица";

$lang["Blagoevgrad"]   		=   "Благоевград";
$lang["Garmen"]   			=   "Гермен";
$lang["Gotse Delchev"]   	=   "Гоце Делчев";
$lang["Hadzhidimovo"]   	=   "Хаџидимово";
$lang["Kresna"]  			=   "Крешна";
$lang["Petrich"]   			=   "Петрич";
$lang["Razlog"]   			=   "Разлог";
$lang["Sandanski"]   		=   "Сандански";
$lang["Satovcha"]   		=   "Саточа";
$lang["Simitli"]   			=   "Симитли";

$lang["Strumyani"]   		=   "Струмиани";
$lang["Yakoruda"]   		=   "Џакоруда";
$lang["Belene"]   			=   "Белане";
$lang["Gulyantsi"]   		=   "Гуланци";
$lang["Dolna Mitropoliya"]  =   "Долна Митрополија";
$lang["Dolni Dabnik"]   	=   "Левски";
$lang["Nikopol"]   			=   "Никопол";
$lang["Iskar"]   			=   "Искра";
$lang["Pleven"]   			=   "Плевен";

$lang["Pordim"]   			=   "Пордим";
$lang["Cherven Bryag"]   	=   "Червен Браг";
$lang["Knezha"]   			=   "Кнеша";
$lang["Pazardzhik"]   		=   "Пазарџик";
$lang["Velingrad"]   		=   "Велинград";
$lang["Septemvri"]   		=   "Семтеври";
$lang["Panagyurishte"]   	=   "Пангуриште";
$lang["Peshtera"]   		=   "Пештера";
$lang["Rakitovo"]   		=   "Ракитово";
$lang["Bratsigovo"]   		=   "Братсигово";

$lang["Belovo"]   			=   "Белово";
$lang["Batak"]   			=   "Батак";
$lang["Lesichovo"]   		=   "Лесичово";
$lang["Strelcha"]   		=   "Стрелча";
$lang["Sarnitsa"]   		=   "Сарница";
$lang["Elena"]   			=   "Елена";
$lang["Gorna Oryahovitsa"]  =   "Горна Орхаховица";
$lang["Lyaskovets"]   		=   "Луасковец";
$lang["Pavlikeni"]   		=   "Павликени";
$lang["Polski Trambesh"]   	=   "Полски Трабеш";

$lang["Strazhitsa"]   		=   "Стражиште";
$lang["Suhindol"]   		=   "Сухидол";
$lang["Svishtov"]   		=   "Свиштов";
$lang["Veliko Tarnovo"]   	=   "Велико Трново";
$lang["Zlataritsa"]   		=   "Златарица";
$lang["Dimitrovgrad"]   	=   "Димитровград";
$lang["Harmanli"]   		=   "Хамали";
$lang["Haskovo"]   			=   "Хасково";
$lang["Ivaylovgrad"]   		=   "Иваловград";
$lang["Lyubimets"]   		=   "Љубиметц";

$lang["Madzharovo"]   		=   "Маџхарово";
$lang["Mineralni bani"]   	=   "Минерални бањи";
$lang["Simeonovgrad"]   	=   "Симеоновград";
$lang["Stambolovo"]   		=   "Стамболово";
$lang["Svilengrad"]   		=   "Свелинград";
$lang["Topolovgrad"]   		=   "Тополовград";
$lang["Anton"]   			=   "Антон";
$lang["Botevgrad"]   		=   "Ботевград";
$lang["Bozhurishte"]   		=   "Боџуриште";
$lang["Chavdar"]   			=   "Чавдар";
$lang["Chelopech"]   		=   "Челопеч";
$lang["Dolna Banya"]   		=   "Долна Бања";
$lang["Dragoman"]   		=   "Драгоман";
$lang["Elin Pelin"]   		=   "Елин Пелин";
$lang["Etropole"]   		=   "Етрополо";
$lang["Godech"]   			=   "Годеч";
$lang["Gorna Malina"]   	=   "Горна Малина";
$lang["Ihtiman"]   			=   "Итиман";
$lang["Koprivshtitsa"]   	=   "Копривиштица";
$lang["Kostenets"]   		=   "Костенец";
$lang["Kostinbrod"]   		=   "Костинброд";
$lang["Mirkovo"]   			=   "Марково";
$lang["Pirdop"]   			=   "Пирдоп";
$lang["Pravets"]   			=   "Правец";
$lang["Samokov"]   			=   "Самоков";
$lang["Slivnitsa"]   		=   "Сливница";
$lang["Svoge"]   			=   "Своге";
$lang["Zlatitsa"]   		=   "Златица";
$lang["Borovo"]   			=   "Борово";
$lang["Byala"]   			=   "Бања";
$lang["Vetovo"]   			=   "Вотово";
$lang["Dve Mogili"]   		=   "Две Могили";
$lang["Ivanovo"]   			=   "Иваново";
$lang["Ruse"]   			=   "Русе";
$lang["Slivo Pole"]   		=   "Сиво Поле";
$lang["Tsenovo"]   			=   "Ценово";
$lang["Borovan"]   			=   "Борован";
$lang["Byala Slatina"]   	=   "Бела Слатина";
$lang["Vratsa"]   			=   "Враца";
$lang["Kozloduy"]   		=   "Козлодои";
$lang["Krivodol"]   		=   "Криводол";
$lang["Mezdra"]   			=   "Мезда";
$lang["Mizia"]   			=   "Мизиа";
$lang["Oryahovo"]   		=   "Орѕахово";
$lang["Roman"]   			=   "Роман";
$lang["Hayredin"]   		=   "Хајредин";
$lang["Kotel"]   			=   "Котел";
$lang["Nova Zagora"]   		=   "Нова Загора";
$lang["Sliven"]   			=   "Сливен";
$lang["Tvarditsa"]   		=   "Твардица";
$lang["Balchik"]   			=   "Балчик";
$lang["Dobrich (city)"]   	=   "Добрич (град)";
$lang["Dobrichka (rural)"] 	=   "Добричка (Рураллно)";
$lang["General Toshevo"]   	=   "Генерал Тошево";
$lang["Kavarna"]   			=   "Каварна";
$lang["Krushari"]   		=   "Кришари";
$lang["Shabla"]   			=   "Шабла";
$lang["Tervel"]   			=   "Тервел";
$lang["Venets"]   			=   "Венец";
$lang["Varbitsa"]   		=   "Варбипшта";
$lang["Hitrino"]   			=   "Хитрино";
$lang["Kaolinovo"]   		=   "Каолиново";
$lang["Kaspichan"]   		=   "Кашпичан";
$lang["Nikola Kozlevo"]   	=   "Никола Козлево";
$lang["Novi Pazar"]   		=   "Нови Пазар";
$lang["Veliki Preslav"]   	=   "Велики Преслав";
$lang["Smyadovo"]   		=   "Смијадово";
$lang["Shumen"]   			=   "Шумен";
$lang["Berkovitsa"]   		=   "Берковица";
$lang["Boychinovtsi"]   	=   "Бојчиновци";
$lang["Brusartsi"]   		=   "Брусарици";
$lang["Chiprovtsi"]   		=   "Чипровитци";
$lang["Georgi Damyanovo"]   =   "Геоги Дамјанов";
$lang["Lom"]   				=   "Лом";
$lang["Medkovets"]   		=   "Медковец";
$lang["Montana"]   			=   "Монтана";
$lang["Valchedram"]   		=   "Велчедрам";
$lang["Varshets"]   		=   "Верштец";
$lang["Yakimovo"]   		=   "Јакимово";
$lang["Lovech"]   			=   "Ловец";
$lang["Troyan"]   			=   "Тројан";
$lang["Teteven"]   			=   "Тетевен";
$lang["Lukovit"]   			=   "Луковит";
$lang["Ugarchin"]   		=   "Угарчин";
$lang["Yablanitsa"]   		=   "Јабланица";
$lang["Letnitsa"]   		=   "Летница";
$lang["Apriltsi"]   		=   "Априлци";
$lang["Kardzhali"]   		=   "Карџали";
$lang["Kirkovo"]   			=   "Кирково";
$lang["Krumovgrad"]   		=   "Крумовград";
$lang["Momchilgrad"]   		=   "Момчилград";
$lang["Ardino"]   			=   "Ардино";
$lang["Chernoochene"]   	=   "Черноочене";
$lang["Dzhebel"]   			=   "Чебел";
$lang["Kyustendil"]   		=   "Кустендил";
$lang["Dupnitsa"]   		=   "Дупница";
$lang["Bobov dol"]   		=   "Бобов дол";
$lang["Sapareva banya"]   	=   "Сапарева бања";
$lang["Kocherinovo"]   		=   "Кочериново";
$lang["Rila"]   			=   "Рила";
$lang["Boboshevo"]   		=   "Бобошеово";
$lang["Nevestino"]   		=   "Невестино";
$lang["Treklyano"]   		=   "Треклиано";
$lang["Tundzha (rural)"]   	=   "Тунџа (рурал)";
$lang["Yambol (city)"]   	=   "Јамбол (град)";
$lang["Straldzha"]   		=   "Стралча";
$lang["Elhovo"]   			=   "Елхово";
$lang["Bolyarovo"]   		=   "Болјарово";
$lang["Razgrad"]   			=   "Разград";
$lang["Isperih"]   			=   "Испрерич";
$lang["Kubrat"]   			=   "Кубрат";
$lang["Tsar Kaloyan"]   	=   "Цар Калијан";
$lang["Zavet"]   			=   "Завет";
$lang["Loznitsa"]   		=   "Лозница";
$lang["Samuil"]   			=   "Самуил";
$lang["Pernik"]   			=   "Перник";
$lang["Gabrovo"]   			=   "Габрово";
$lang["Sevlievo"]   		=   "Севлиево";
$lang["Tryavna"]   			=   "Трајавна";
$lang["Dryanovo"]   		=   "Друјаново";
$lang["Silistra"]   		=   "Силишта";
$lang["Tutrakan"]   		=   "Тутракан";
$lang["Dulovo"]   			=   "Дулово";
$lang["Glavinitsa"]   		=   "Главница";
$lang["Alfatar"]   			=   "Алфатар";
$lang["Sitovo"]   			=   "Ситово";
$lang["Kaynardzha"]  		=   "Кајнарциха";
$lang["Smolyan"]   			=   "Смолјан";
$lang["Zlatograd"]   		=   "Златоград";
$lang["Devin"]   			=   "Девин";
$lang["Madan"]   			=   "Мадан";
$lang["Chepelare"]   		=   "Чепеларе";
$lang["Rudozem"]   			=   "Рудозем";
$lang["Dospat"]   			=   "Доспат";
$lang["Borino"]   			=   "Борино";
$lang["Banite"]   			=   "Баните";
$lang["Targovishte"]		=   "Тарговиште";
$lang["Popovo"]   			=   "Попово";
$lang["Omurtag"]   			=   "Оморутаг";
$lang["Opaka"]   			=   "Опака";
$lang["Antonovo"]   		=   "Антоново";
$lang["Vidin"]   			=   "Видин";
$lang["Belogradchik"]   	=   "Белограднчик";
$lang["Kula"]   			=   "Кула";
$lang["Bregovo"]   			=   "Брегово";
$lang["Gramada"]   			=   "Грамада";
$lang["Dimovo"]   			=   "Димово";

//bulgaris end

//greece start
$lang["Greece"]   			=   "Грција";
$lang["Thrace"]   			=   "Тракија";
$lang["Thessaly"]   			=   "Тесалија";
$lang["Epirus"]   			=   "Епир";
$lang["Central Greece"]   		=   "Централна Грција";
$lang["Peloponnese"]   			=   "Пелопонез";
$lang["Aegean Islands"]   		=   "Егејските Острови";
$lang["Ionian Islands"]   		=   "Јонските Острови";
$lang["Crete"]   			=   "Крит";

$lang["Alexandroupoli"]   			=   "Александруполи";
$lang["Komotini"]   			=   "Комотини";
$lang["Xanthi"]   			=   "Ксанти";
$lang["Orestiada"]   			=   "Орестиада";
$lang["Didymoteicho"]   			=   "Дидимитихо";
$lang["Thessaloniki"]   			=   "Солун";
$lang["Kavala"]   			=   "Кавала";
$lang["Katerini"]   			=   "Катерини";
$lang["Serres"]   			=   "Серес";
$lang["Drama"]   			=   "Драма";

$lang["Kozani"]   			=   "Кожани";
$lang["Veria"]   			=   "Верија";
$lang["Giannitsa"]   			=   "Гианишта";
$lang["Ptolemaida"]   			=   "Птолемаида";
$lang["Kilkis"]   			=   "Килкис";
$lang["Naoussa"]   			=   "Науса";
$lang["Aridaia"]   			=   "Аридаја";
$lang["Alexandria"]   			=   "Александрија";
$lang["Edessa"]   			=   "Воден";
$lang["Nea Moudania"]   		=   "Неа Муданија";

$lang["Florina"]   			=   "Флорина";
$lang["Kastoria"]   			=   "Костур";
$lang["Grevena"]   			=   "Гревена";
$lang["Polygyros"]   			=   "Полигирос";
$lang["Skydra"]   			=   "Скадра";
$lang["Kardítsa"]   			=   "Кардица";
$lang["Lárisa"]   			=   "Лариса";
$lang["Néa Ionía"]   			=   "Неа Лониа";
$lang["Orestiada"]   			=   "Орестида";
$lang["Tríkala"]   			=   "Трикала";

$lang["Vólos"]   			=   "Волос";
$lang["Elassona"]   			=   "Еласона";
$lang["Farsala"]   			=   "Фарсала";
$lang["Ioannina"]   			=   "Јанина";
$lang["Arta"]   			=   "Арта";
$lang["Preveza"]   			=   "Превеза";
$lang["Igoumenitsa"]   			=   "Игуменица";
$lang["Zitsa"]   			=   "Зитца";
$lang["Ziros"]   			=   "Зирос";
$lang["Nikolaos Skoufas"]   		=   "Николаос Скуфас";

$lang["Parga"]   			=   "Парга";
$lang["Souli"]   			=   "Соули";
$lang["Dodoni"]   			=   "Додони";
$lang["Pogoni"]   			=   "Погони";
$lang["Filiates"]   			=   "Филиати";
$lang["Konitsa"]   			=   "Коница";
$lang["Metsovo"]   			=   "Мецово";
$lang["Central Tzoumerka"]   		=   "Централна Џумерка";
$lang["Georgios Karaiskakis"]   	=   "Георгиос Караискакис";
$lang["North Tzoumerka"]   		=   "Северна Цумерка";

$lang["Zagori"]   			=   "Загори";
$lang["Athens"]   			=   "Атина";
$lang["Lamia"]   			=   "Ламија";
$lang["Agrinio"]   			=   "Агринио";
$lang["Chalkida"]   			=   "Халкида";
$lang["Thebes"]   			=   "Теба";
$lang["Livadeia"]   			=   "Ливедија";
$lang["Patras"]   			=   "Патра";
$lang["Kalamata"]   			=   "Каламата";
$lang["Corinth"]   			=   "Коринт";

$lang["Tripoli"]   			=   "Триполи";
$lang["Argos"]   			=   "Аргос";
$lang["Pyrgos"]   			=   "Пиргос";
$lang["Aigio"]   			=   "Аидо";
$lang["Sparta"]   			=   "Спарта";
$lang["Nafplio"]   			=   "Нафплио";
$lang["Heraklion"]   			=   "Хераклион";
$lang["Chania"]   			=   "Чаниа";
$lang["Rethymno"]   			=   "Ретимно";
$lang["Ierapetra"]   			=   "Ирапетра";
$lang["Agios Nikolaos"]   		=   "Агиос Николаос";
$lang["Sitia"]   			=   "Ситиа";
//greece end


//store - start
$lang["labelAccountStoreInformation"]                       = "Информации за продавница";
$lang["labelAccountStoreInformationStoreName"]              = "Име на продавница:";
$lang["labelAccountStoreInformationStoreUrl"]               = "Линк (URL) до продавницата:";
$lang["labelAccountStoreInformationCreateStoreTitle"]       = "Отвори продавница";
$lang["labelInsertStoreChange"]                             = "Измени";
$lang["labelInsertStoreChangeStore"]                        = "Запиши промени";
$lang["labelAccountStoreInformationCreateStoreBody"]        = "Трговски Ви овозможува да отворите сопствена продавница каде ќе бидат преставени само Вашите проиводи.";
$lang["labelAccountStoreInformationCreateStoreButtonTitle"] = "Промени податоци";
$lang["labelInsertStore"]                                   = "Креирај продавнца";
$lang["storeNotExist"]                                      = "Продавницата не постои";
$lang["labelInsertStore"]                                   = "Креирај продавнца";
$lang["labelInsertStoreBoxTitle"]                           = "Податоци за продавницата";
$lang["labelInsertStoreTitle"]                              = "Име на продавницата*";
$lang["labelInsertStoreTitleDescription"]                   = "Внесете име на продавницата";
$lang["labelInsertStoreSubTitle"]                           = "Објаснување на продавницата*";
$lang["labelInsertStoreSubTitleDescription"]                = "Внесете дејност на продавницата, пример, агенција за продажба на недвижности, плац за продажба на половни автомобили итн.";
$lang["labelInsertStoreURL"]                           		= "Url*";
$lang["labelInsertStoreURLDescription"]                		= "Внесете само еден збор на латиница кој ќе биде суб домаин нa trgovski.com за Вашата продавница.";
$lang["labelInsertStoreBody"]                               = "Името на продавницата ќе биде и URL, затоа за име внесете само еден збор и на латиница.";
$lang["labelInsertStoreOpenStore"]                          = "Креирај Продавница";
$lang["labelInsertStoreAgree"]                              = "Со кликнување на креирај продавница се согласувате со услувите на трговски.цом";
$lang["StoreErrorUserStoreExist"]                           = "Веќе имате креирано сопствена продавница";
$lang["StoreErrorStoreNameTaken"]                           = "Името е зафатено обидете се со друго име";
$lang["StoreSuccessCreated"]                                = "Успешно креирана продавница";
$lang["StoresTitle"]                                        = "Продавници";
$lang["StoresTitleAll"]                                     = "Сите";
$lang["StoresSubTitle"]                                     = "Trgovski.com продавници";
//store - end

//curency start
$lang["currency"]   =   "Валута:";
$lang["eur"]   =   "Евро";
$lang["mkd"]   =   "Денари";
$lang["rsd"]   =   "Динари";
$lang["bgn"]   =   "Лева";
//curency end

//bot start
$lang["hi"]   =   "Здраво";
$lang["bye, see you."]   =   "Пријатно, се гледаме. :)";
$lang["hi"]   =   "Здраво";
$lang["hi"]   =   "Здраво";
//bot end

//setting start
$lang["labelSettingTitle"]                          =   "Подесување";
$lang["labelSettingTitleDescription"]               =   "Подесување на јазик, Држава и валута.";
$lang["labelSettingTitleChangeLanguage"]            =   "Промени Јазик";
$lang["labelSettingTitleChangeLanguageMessage"]     =   "Со кликнување на линкот соодветно променете го јазикот";
$lang["labelSettingTitleChangeCountry"]             =   "Промена на Држава";
$lang["labelSettingTitleChangeCountryMessage"]      =   "Со кликнување на имато на Државата ја подесувате основната Држава за пребарување";
$lang["labelSettingTitleChangeCurrency"]            =   "Промена на валута";
$lang["labelSettingTitleChangeCurrencyMessage"]     =   "Со кликнување на валутата ја подесувата основната валута";
//setting end