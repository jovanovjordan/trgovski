<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Vebko_model extends CI_Model{
	public function __construct(){		
		parent::__construct();
		
		$this->load->helper('cookie');
		$this->load->database();
	}
	//function for data in database start
	function form_insert($table=NULL,$data){
		if($table==NULL){
			$table = 'ads_vehicles';
		}
		$this->db->insert($table, $data);
	}
	public function updateData($table=NULL,$id,$data){
		if($table==NULL){
			$table = 'ads_vehicles';
		}
		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}
	public function get_single_value_from_db($db_table, $id, $parametar){
		$this->db->select('*');
		$this->db->from($db_table);
		$this->db->where('id', $id);
		return $this->db->get()->row()->$parametar;
	}
	public function get_single_value_from_db_query($db_table, $id, $parametar){
		$queryAll = "SELECT * FROM ".$db_table." WHERE id =".$id;
		return $this->db->query($queryAll)->row()->$parametar;
	}
	public function get_multiple_value_from_db($db_table, $parametar, $addAll=NULL){
		$id = array();
		$name = array();
		if($addAll == 1){
			$id = array('0');
			$name = array('All');
		}
		$result = $this->db->get($db_table)->result();
        for ($i=0; $i<count($result); $i++)
        {
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->$parametar);
        }
        return array_combine($id, $name);
	}
	function checkRowExist($db_table, $id, $redirect){
		if(!is_numeric($id)){
			$this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','adsNotExist');
			redirect(base_url($redirect), 'refresh');
		}
		 $query = $this->db->query("SELECT id FROM ".$db_table." WHERE id = ".$id." limit 1");
		 if ($query->num_rows() == 0){
			 $this->session->set_userdata('sessionMessageType','alert-danger');
			$this->session->set_userdata('sessionMessageText','adsNotExist');
			redirect(base_url($redirect), 'refresh');
		 }
		 return 0;
	}
	//function for data in database end
	
	//functions for translate start
	public function translateText($string){
            
		$this->languageSelect();
		if($this->lang->line($string)){
			$returnString = $this->lang->line($string);
		}
		else{
			$returnString = $string;
		}
		return $returnString;
	}
	public function translateTextInArray($array){
		$this->languageSelect();
		foreach ($array as $key => $field) {
			if($this->lang->line($field)){
				$returnArray[$key] = $this->lang->line($field);
			}
			else{
				$returnArray[$key] = $field;
			}
		}
		return $returnArray;
	}
	public function languageSelect(){
		if(!$this->input->cookie('language')){			
			$ln = 'mk';			
			//if($this->Vebko_model->location()->country == 'RS'){$ln = 'sr';}
			//if($this->Vebko_model->location()->country == 'BG'){$ln = 'bg';}
			$cookie= array(
				'name'   => 'language',
				'value'  => $ln,
				'expire' => '86500',
			);
			$this->input->set_cookie($cookie);
		}
		else{			
			$ln = $this->input->cookie('language',true);
		}
		$this->lang->load("message",$ln);
	}
	function location(){	
		$ip = $this->input->ip_address();
		$json       = file_get_contents("http://ipinfo.io/".$ip);
		$details    = json_decode($json);
		//print_r($details); echo'gg' ; exit;
		return $details;
	}
	public function getCountryID(){
		
		if($this->input->cookie('country')){
			$country = $this->input->cookie('country',true);
		}
		else{			
			$country = 1;
			//if($this->Vebko_model->location()->country == 'RS'){$country = 2;}
			//if($this->Vebko_model->location()->country == 'BG'){$country = 3;}
			$cookieCountry= array(
				'name'   => 'country',
				'value'  => $country,
				'expire' => '86500',
			);		
			$this->input->set_cookie($cookieCountry);
			
		}
		return $country;
	}
	function getFirstRegionIdFromCountryId($countryId){
            $this->db->select('*');
            $this->db->from('address_country_region');
            $this->db->where('address_country_id',$countryId);
            $reault_array = $this->db->get()->result_array();
            return $reault_array[0]['id'];
        }
        
        function getViewIfExist($viewName){
		$language = $this->Vebko_model->translateText('language');
		$contentView = 'content/'.$language.'/'.$viewName;
		
		if (!file_exists('application/views/' . $contentView)){
			$contentView = 'content/mk/'.$viewName;
		}
		echo $return;
	}
	//functions for translate end
	
	public function getUserIdFromFacebookIdFromSession(){
		$var = $this->session->userdata;
		if(isset($var['userData'])){
			$sessionFBID = $var['userData']['oauth_uid'];
			$queryAll = "SELECT id FROM users WHERE oauth_uid =".$sessionFBID;
			$returnValue = $this->db->query($queryAll)->row()->id;
		}
		else{
			$returnValue = 0;
		}
		return $returnValue;		
	}
	public function getUserDate(){
		$userID = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id', $userID);
		
		$userData = array();
		$userData = $this->db->get()->row();
		return $userData;
	}
        
        //store start
	public function getUserIdByStoreName($storename=NULL){
		//storename is same like username
		$userID = 0;
		if($storename!=NULL){
			$this->db->select('id, userId');
			$this->db->where('title', $storename);
			$query = $this->db->get('store');
			if ($query->num_rows()==1){
				$userID = $query->row()->userId;
			}
			else{
				 $this->session->set_userdata('sessionMessageType','alert-danger');
				$this->session->set_userdata('sessionMessageText','storeNotExist');
				redirect(base_url(), 'refresh');
			}
		}
		return $userID;
	}
	
	public function getUserIdByStoreSubDomain($subdomain){
		//storename is same like username
		$userID = 0;
		if($subdomain!=NULL){
			$this->db->select('userId');
			$this->db->where('subdomain', $subdomain);
			$query = $this->db->get('store');
			if ($query->num_rows()==1){
				$userID = $query->row()->userId;
			}
			else{
				 $this->session->set_userdata('sessionMessageType','alert-danger');
				$this->session->set_userdata('sessionMessageText','storeNotExist');
				redirect(base_url(), 'refresh');
			}
		}
		return $userID;
	}
	 function getStores($limit=NULL){
		$address_country_id = $this->getCountryID();
		$this->db->select('*');
		$this->db->from('store');
		$this->db->where('address_country_id',$address_country_id);
        $this->db->order_by("orderId", "desc");
        if($limit){$this->db->limit($limit);}
		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
	}
	public function storeContent($userId) {
            $this->db->select('*');
            $this->db->from('store');
            $this->db->where('userId',$userId);
            $query = $this->db->get();
        return $query->result();
        }
        public function storeUrl($storeId){
            $storeUrl = $this->get_single_value_from_db("store", $storeId, "subdomain");
            $storeUrl =$storeUrl.'.'.substr(base_url(), 8,-1);
            return $storeUrl;
        }
        public function storeLogo($storeId){
            $logoUrl = $this->get_single_value_from_db("store", $storeId, "logoUrl");
            $storeUrl = $this->get_single_value_from_db("store", $storeId, "subdomain");
            $storeTitle = $this->get_single_value_from_db("store", $storeId, "title");
             $logoDiv ='<div class="storeLogoMain">';
            $logoDiv .='<a href="http://'.$storeUrl.'.'.substr(base_url(), 8,-1).'">';
                $logoDiv .='<div class="storeLogo storeLogo-'.$storeTitle[0].'">';
                if (file_exists($logoUrl)) {
                    $logoDiv .='<img src="'.base_url($logoUrl).'" alt="image">';
                }
                else{
                    $logoDiv .='<div class = "logoStoreNotExist">';
                    $logoDiv .= $storeTitle[0];
                    $logoDiv .='</div>';
                }
                $logoDiv .='</div>';
            $logoDiv .='</a>';
            $logoDiv .='</div>';
            return $logoDiv;
        }
	
	
        public function getStoreDate(){
		$userID = $this->Vebko_model->getUserIdFromFacebookIdFromSession();
		$this->db->select('*');
		$this->db->from('store');
		$this->db->where('userId', $userID);
		
		$storeData = array();
		$storeData = $this->db->get()->row();
		return $storeData;
	}
        //store end
	public function getNextId($tableName){
		$queryAll = "SELECT AUTO_INCREMENT
					FROM information_schema.tables
					WHERE table_name = '".$tableName."'";
		$parametar = "AUTO_INCREMENT";
		return $this->db->query($queryAll)->row()->$parametar;
	}
	
	public function dataHeaderFooter(){
		//set language and country - start
                $this->Vebko_model->languageSelect();
                $dataHeaderFooter['sessionCurrentLanguage']     = $this->input->cookie('language',true);
                $dataHeaderFooter['languageSetting']            = $this->input->cookie('languageSetting',true);
                $dataHeaderFooter['languageCountry']            = $this->input->cookie('languageCountry',true);
                $dataHeaderFooter['currency']                   = $this->input->cookie('currency',true);
                $dataHeaderFooter['currencySetting']            = $this->input->cookie('currencySetting',true);
                //$this->Vebko_model->location();
		//set language - end
		$this->session->set_userdata('sessionLoginRedirect',base_url(uri_string())); 
		$this->session->set_userdata('lastURLView',base_url(uri_string()));
		//get message alert start
		$dataHeaderFooter['sessionMessageType'] = "";
		$dataHeaderFooter['sessionMessageText'] = "";
		//for testing
		/*$this->session->set_userdata('sessionMessageType','alert-success');
		$this->session->set_userdata('sessionMessageText','AdsSuccessMessage');
		$this->session->unset_userdata('sessionMessageType');
		$this->session->unset_userdata('sessionMessageText');
		*/
		if(($this->session->userdata('sessionMessageType')!== FALSE)||($this->session->userdata('sessionMessageText')!== FALSE)){
			
				$dataHeaderFooter['sessionMessageType'] = 'alert '.$this->session->userdata('sessionMessageType');				
				$dataHeaderFooter['sessionMessageText'] = $this->Vebko_model->translateText($this->session->userdata('sessionMessageText'));
				$this->session->unset_userdata('sessionMessageType');
				$this->session->unset_userdata('sessionMessageText');
						
		}
		else{
			$dataHeaderFooter['sessionMessageType'] = "";
			$dataHeaderFooter['sessionMessageText'] = "";
		}
		//get message alert end
		
		$dataHeaderFooter['checkUserLoginReturnUserData']=$this->User_model->checkUserLoginReturnUserData();
		
		$dataHeaderFooter['home'] 			= $this->Vebko_model->translateText('home');
		$dataHeaderFooter['search'] 		= $this->Vebko_model->translateText('search');
		$dataHeaderFooter['category'] 		= $this->Vebko_model->translateText('category');
		$dataHeaderFooter['insertAds'] 		= $this->Vebko_model->translateText('insertAds');
                $dataHeaderFooter['setting'] 		= $this->Vebko_model->translateText('setting');
                $dataHeaderFooter['about'] 		= $this->Vebko_model->translateText('about');
		$dataHeaderFooter['help'] 			= $this->Vebko_model->translateText('help');
		$dataHeaderFooter['contact'] 		= $this->Vebko_model->translateText('contact');
		$dataHeaderFooter['login'] 			= $this->Vebko_model->translateText('login');
		$dataHeaderFooter['logout'] 		= $this->Vebko_model->translateText('logout');
		$dataHeaderFooter['labelMyAccount'] = $this->Vebko_model->translateText('labelMyAccount');
		$dataHeaderFooter['footerMessage']	= $this->Vebko_model->translateText('footerMessage');
		$dataHeaderFooter['country']		= $this->Vebko_model->translateText('country');
		$dataHeaderFooter['Macedonia']		= $this->Vebko_model->translateText('Macedonia');
		$dataHeaderFooter['Serbia']		= $this->Vebko_model->translateText('Serbia');
		$dataHeaderFooter['Bulgaria']		= $this->Vebko_model->translateText('Bulgaria');
                $dataHeaderFooter['Greece']		= $this->Vebko_model->translateText('Greece');
                $dataHeaderFooter['currency']		= $this->Vebko_model->translateText('currency');
		$dataHeaderFooter['eur']		= $this->Vebko_model->translateText('eur');
		$dataHeaderFooter['mkd']		= $this->Vebko_model->translateText('mkd');
		$dataHeaderFooter['rsd']		= $this->Vebko_model->translateText('rsd');
                $dataHeaderFooter['bgn']		= $this->Vebko_model->translateText('bgn');
		
		
		$dataHeaderFooter['pageCss'] = $this->uri->segment(1);
		if($this->uri->segment(2)){$dataHeaderFooter['pageCss'] .= '-'.$this->uri->segment(2);}
		return $dataHeaderFooter;		
	}
	
	function convertCurrency($amount, $from, $to) {
            if($this->input->cookie('currency',true)){$to = $this->input->cookie('currency',true);}
            $to = $this->input->cookie('currency',true);
            $url = 'http://finance.google.com/finance/converter?a=' . $amount . '&from=' . $from . '&to=' . $to;
            $data = file_get_contents($url);
            preg_match_all("/<span class=bld>(.*)<\/span>/", $data, $converted);
            $final = preg_replace("/[^0-9.]/", "", $converted[1][0]);
            if(!$final){$final=$amount;}
            return number_format($final,0,"."," ");;
        }

	//Address start
	function get_address_country($addAll=NULL){
		$id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
        $result = $this->db->get('address_country')->result();
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_address_country_region($cid=NULL,$addAll=NULL){
		$id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
		$result = $this->db->where('address_country_id', $cid)->get('address_country_region')->result();
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_address_country_municipality($cid=NULL,$addAll=NULL){
		$id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
		$result = $this->db->where('address_country_id', $cid)->get('address_country_municipality')->result();
            for ($i = 0; $i < count($result); $i++){
                array_push($id, $result[$i]->id);
                array_push($name, $result[$i]->title);
            }
            return array_combine($id, $name);
        }
	//Address end
	
	public function ago($time){
		$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$periods = $this->Vebko_model->translateTextInArray($periods);
		$lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

		   $difference     = $now - $time;
		   $tense         = "ago";

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		   $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
		   $periods = array("seconds", "minutes", "hours", "days", "weeks", "months", "years", "decade");
		   $periods = $this->Vebko_model->translateTextInArray($periods);
	   }

	   return " ".$this->Vebko_model->translateText('before')." $difference $periods[$j]";
	}
	public function checkUserLastInsertInTable($db_table,$userId){
		$parametar = 'date';
		$queryAll = "SELECT * FROM ".$db_table." WHERE userId =".$userId." ORDER BY date DESC";
		$rowcount = $this->db->query($queryAll)->num_rows();
		if ($rowcount > 0){
			$return = $this->db->query($queryAll)->row()->$parametar;
		}
		else{
			$return = 0;
		}
		return $return;
	}
	public function checkUserInsertAdsInDays($db_table,$userId,$numberOfDays){
		$lastInsertDate = $this->Vebko_model->checkUserLastInsertInTable($db_table,$userId);
		$allowDate = time() - ($numberOfDays * 86400);
		$return = 0;
		if($lastInsertDate > $allowDate){
			$return = 1;
		}
		
		return $return;
	}
	public function checkUserPermissionToRenew($db_table,$adsId,$numberOfDays){
		$parametar = "date_modify";
		$lastDateModified = $this->Vebko_model->get_single_value_from_db_query($db_table, $adsId, $parametar);
		//echo date('d/m/Y h:i', $lastDateModified); exit;
		$allowDate = time() - ($numberOfDays * 86400);
		$return = 0;
		if($lastDateModified > $allowDate){
			$return = 1;
		}
		//echo date('d/m/Y h:i', $lastDateModified).'<br>'.date('d/m/Y h:i', time()).'<br>'.date('d/m/Y h:i', $allowDate).'<br>'.$return; exit;
		return $return;
	}
	
        
        public function getAllAds(){
		$userAdsArray = array();
		$tableArray = array('ads_estate','ads_vehicles','ads_electronics','ads_clothing','ads_service','ads_other');
		$counter = 0;
		foreach ($tableArray as $key => $value){
			$tableArrayAdsId = $this->Vebko_model->getAdsTable($value);			
			foreach ($tableArrayAdsId as $key2 => $value2){				
				$singleID = $value2->id;
				$userAdsArray[$counter] = $this->Vebko_model->gatAdsShortData($value, $singleID);
				$counter ++;
			}			
		}
		
		return $userAdsArray;
	}
        function getAdsTable($tableName){		
            $this->db->select('id');
            $this->db->from($tableName);		
            $query = $this->db->get();
            $result = $query->result();		
            return $result;
        }
        
        public function get_all_ads($queryRowStart=NULL){
			$address_country_id = $this->getCountryID();
			$userAdsArray = array();
			$tableArray = array('ads_estate','ads_vehicles','ads_electronics','ads_clothing','ads_service','ads_other');
			$counter = 0;
			foreach ($tableArray as $key => $value){
				$this->db->select('*');
				$this->db->from($value);
				$this->db->where('address_country_id',$address_country_id);
				$this->db->where('marking',0);
				$query = $this->db->get();
				$result = $query->result();
				$src = substr($value, 4);
				$srcImg = substr($value, 4);
				if($value=="ads_vehicles"){
					$src = "vehicle";
					$srcImg = "car";
				}
                                $type = substr($value, 4);
				foreach ($result as $key2 => $value2){
					
					$arrayReturn[$counter]['id'] = $value2->id;
					$arrayReturn[$counter]['title'] = $this->substrwords($this->removeQuotes($value2->title),'100','...');
					$arrayReturn[$counter]['price'] =$value2->price;
					$arrayReturn[$counter]['body'] = $this->substrwords($this->removeQuotes($value2->body),'200','...');
                                        $arrayReturn[$counter]['marking'] =$value2->marking;
					$arrayReturn[$counter]['date_modify'] = $this->Vebko_model->ago($value2->date_modify);
                                        $arrayReturn[$counter]['date_modify_long'] = $value2->date_modify;
					$arrayReturn[$counter]['src'] =base_url().$src.'/view/'.$value2->id;
					$arrayReturn[$counter]['imageSrc'] =base_url().'images/ads/'.$srcImg.'/ad_'.sprintf('%08d', $arrayReturn[$counter]['id']).'/main-'.$value2->ads_gallery_image_name_1;;
					$arrayReturn[$counter]['municipality'] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('address_country_municipality', $value2->address_country_municipality_id, 'title'));
                                        $arrayReturn[$counter]['type'] = $this->Vebko_model->translateText($type);
                                        $counter++;
				}
			}
			
			foreach ($arrayReturn as $key => $row) {
				$date_modify_long[$key] = $row['date_modify_long'];
			}
			array_multisort($date_modify_long, SORT_DESC, $arrayReturn);
                        if($queryRowStart==NULL){
                            $queryRowStart = 0;
                        }
                        $arrayReturn = array_slice($arrayReturn,$queryRowStart,16);
 
			//print_r($arrayReturn);exit;
            return $arrayReturn;
        }
        
	public function getUserAds($userId=NULL){
		
		if($userId==NULL){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession(); 
		}
		$userAdsArray = array();
		$tableArray = array('ads_estate','ads_vehicles','ads_electronics','ads_clothing','ads_service','ads_other');
		$counter = 0;
		foreach ($tableArray as $key => $value){
			$tableArrayAdsId = $this->Vebko_model->getAds($value, $userId);			
			foreach ($tableArrayAdsId as $key2 => $value2){				
				$singleID = $value2->id;
				$userAdsArray[$counter] = $this->Vebko_model->gatAdsShortData($value, $singleID);
				$counter ++;
			}			
		}
		
		return $userAdsArray;
	}
        public function getUserAdsМarking($userId,$mark){
            
            $returnArray = $this->getUserAds($userId);          
            
            foreach ($returnArray as $key => $rule) {
                if ($rule['marking'] == 1) {
                    unset($returnArray[$key]);
                   }
              }
            return $returnArray;
        }
        
	function getAds($tableName, $userId=NULL){
		if($userId==NULL){
			$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession(); 
		}
		
		$this->db->select('id');
		$this->db->from($tableName);
		$this->db->where('userID',$userId);		
		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
	}
	function gatAdsShortData($tableName, $id){
		//echo $id; exit;
		$arrayReturn = array();
		
		$this->db->select('id');
		$this->db->select('title');
		$this->db->select('price');
		$this->db->select('body');
        $this->db->select('marking');
		$this->db->select('date_modify');
		$this->db->select('ads_gallery_image_name_1');
		$this->db->from($tableName);
		$this->db->where('id',$id);		
		$query = $this->db->get();
		$result = $query->result();
		//print_r($result); exit;
		foreach ($result as $key => $value){
			$src = substr($tableName, 4);
			$srcImg = substr($tableName, 4);
			if($tableName=="ads_vehicles"){
				$src = "vehicle";
				$srcImg = "car";
			}
			$arrayReturn['id'] =$value->id;
			$arrayReturn['title'] =$value->title;
			$arrayReturn['price'] =$value->price;
			$arrayReturn['body'] = $this->substrwords($this->removeQuotes($value->body),'300','...');
                        $arrayReturn['marking'] =$value->marking;
			//$arrayReturn['date_modify'] = $this->Vebko_model->ago($value->date_modify); //problem with cookies
                        $arrayReturn['date_modify_long'] = $value->date_modify;
			$arrayReturn['src'] = base_url().$src.'/view/'.$value->id;
                        $arrayReturn['srcSubDomain'] = 'http://'.$_SERVER['HTTP_HOST'].'/'.$src.'/view/'.$value->id;
			$arrayReturn['imageSrc'] =base_url().'images/ads/'.$srcImg.'/ad_'.sprintf('%08d', $arrayReturn['id']).'/main-'.$value->ads_gallery_image_name_1;;
		}
		return $arrayReturn;
	}
        function getAdsMainImage($adsType, $adsId){
            $adsTypeNew=$adsType;
            if($adsType=='vehicles'){$adsTypeNew='car';}
            return base_url().'images/ads/'.$adsTypeNew.'/ad_'.sprintf('%08d', $adsId).'/main-'.$this->get_single_value_from_db('ads_'.$adsType, $adsId, 'ads_gallery_image_name_1');
        }
        public function removeQuotes($string){
             $string = str_replace('"','',$string);
             $string = str_replace("'","",$string);
			 $string = str_replace("\n","",$string);
             return $string;
        }
        
        function substrwords($text, $maxchar, $end='...') {
        if (strlen($text) > $maxchar) {
            return substr($text, 0, strrpos(substr($text, 0, $maxchar + 1), ' ')) . $end;
            }
        return $text;
    }
    public function getMarketing($address_country_id=NULL){
		$address_country_id = $this->getCountryID();
		$this->db->select('*');
		$this->db->from('marketing');
		$this->db->where('enable',0);
		if($address_country_id){$this->db->where('address_country_id',$address_country_id);}
		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
		
		
		
	}
   


}