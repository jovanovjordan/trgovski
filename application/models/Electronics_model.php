<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Electronics_model extends CI_Model{
	public function _construct(){		
		parent::__construct();

		$this->load->database();
		$this->load->model('Vebko_model');		
	}
	
	function get_ads_vehicles($ads_estate_type_id=NULL,
							$ads_vehicles_year_start_id=NULL,$ads_vehicles_year_end_id=NULL,
							$ads_vehicles_price_start_id=NULL,$ads_vehicles_price_end_id=NULL,
							$address_country_id=NULL,$address_country_region_id=NULL,$address_country_municipality_id=NULL,
							$ads_vehicles_bye_sell_id=NULL,$queryRowStart=NULL
							){
		$db = 'ads_electronics';		
		
		$this->db->select('*');
		$this->db->from($db);
		if($ads_estate_type_id != 0){$this->db->where('ads_type_id', $ads_estate_type_id);}
		if($ads_vehicles_bye_sell_id != 0){$this->db->where('ads_buy_sell_id', $ads_vehicles_bye_sell_id);}		
		if($ads_vehicles_year_start_id != 0){
			$ads_vehicles_year_start_title = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_year', $ads_vehicles_year_start_id, 'title');
			$this->db->where('year >=', $ads_vehicles_year_start_title);
		}
		if($ads_vehicles_year_end_id != 0){
			$ads_vehicles_year_start_end = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_year', $ads_vehicles_year_end_id, 'title');
			$this->db->where('year <=', $ads_vehicles_year_start_end);
		}		
		if($ads_vehicles_price_start_id != 0){
			$ads_vehicles_price_start_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_price', $ads_vehicles_price_start_id, 'value');
			$this->db->where('price >=', $ads_vehicles_price_start_value);
		}
		if($ads_vehicles_price_end_id != 0){
			$ads_vehicles_price_end_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_price', $ads_vehicles_price_end_id, 'value');
			$this->db->where('price <=', $ads_vehicles_price_end_value);
		}		
		if($address_country_id != 0){$this->db->where('address_country_id', $address_country_id);}
		if($address_country_region_id != 0){$this->db->where('address_country_region_id', $address_country_region_id);}
		if($address_country_municipality_id != 0){$this->db->where('address_country_municipality_id', $address_country_municipality_id);}
		 $this->db->where('marking =', '0');
		if($queryRowStart==NULL){
			$this->db->limit(16,0);
		}
		else{
			$this->db->limit(16,$queryRowStart);
		}
		$this->db->order_by('date_modify','desc');
		
		$query = $this->db->get();
		$result = $query->result();
		
		
		
        $id = array();
        $title = array();
		$adsSearchList = array();
        for ($i = 0; $i < count($result); $i++){
			$ID = sprintf('%08d', $result[$i]->id);
			$directoryPath = 'images/ads/electronics/ad_'.$ID.'/';
            array_push($id, $result[$i]->id);
            array_push($title, $result[$i]->title);
			$adsSearchList[$i]["id"] = $result[$i]->id;
			$adsSearchList[$i]["imageUrl"] = base_url($directoryPath.'main-'.$this->Vebko_model->get_single_value_from_db_query($db , $ID, 'ads_gallery_image_name_1'));		
			$adsSearchList[$i]["title"] = $result[$i]->title;
			$adsSearchList[$i]["price"] = number_format($result[$i]->price);
			$adsSearchList[$i]["date"] = $this->Vebko_model->ago ($result[$i]->date_modify);
			$adsSearchList[$i]["municipality"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('address_country_municipality', $result[$i]->address_country_municipality_id, 'title'));
                        $adsSearchList[$i]["body"] = $this->Vebko_model->substrwords(strip_tags($result[$i]->body),260,' ...');
                        $adsSearchList[$i]["type"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('ads_electronics_type', $result[$i]->ads_type_id, 'title'));
                        $adsSearchList[$i]["sell"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('ads_electronics_buy_sell', $result[$i]->ads_buy_sell_id, 'title'));
                        
        }
        return $adsSearchList;
    }
	
	function checkhAdsOwner($id){
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession(); 
		$adsVehicleUserId = $this->Vebko_model->get_single_value_from_db_query('ads_electronics', $id,'userId');
		$return = 0;
		if($userId==$adsVehicleUserId){
			$return = 1;
		}
		return $return;
	}
	
	public function getSearchSugestion(){
		$this->db->select('id');
		$this->db->select('title');
		$this->db->select('ads_gallery_image_name_1');
		$this->db->from('ads_electronics');
		$this->db->where('marking',0);
		$this->db->limit(16,0);
		$this->db->order_by('date','desc');		
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	function getMyAds(){
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession(); 
		$this->db->select('id');
		$this->db->select('date_modify');
		$this->db->select('title');
		$this->db->select('ads_gallery_image_name_1');
		$this->db->from('ads_electronics');
		$this->db->where('userID',$userId);
		$this->db->order_by('date_modify','desc');		
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}