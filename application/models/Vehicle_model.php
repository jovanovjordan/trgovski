<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model extends CI_Model{
	public function _construct(){		
		parent::__construct();

		$this->load->database();
		$this->load->model('Vebko_model');		
	}
	function get_ads_vehicles_type($addAll=NULL){
        
		$id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
		$result = $this->db->get('ads_vehicles_type')->result();		
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_ads_vehicles_brand($cid=NULL,$addAll=NULL){
        
        $id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
		$result = $this->db->get('ads_vehicles_brand')->result();
        for ($i=0; $i<count($result); $i++)
        {
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_ads_vehicles_model($cid=NULL,$addAll=NULL){
		 $id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
        $result = $this->db->where('ads_vehicles_brand_id', $cid)->get('ads_vehicles_model')->result();
        for ($i=0; $i<count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_ads_vehicles_fuel($addAll=NULL){
            $id = array();
            $name = array();
            if($addAll == NULL){
                    $id = array('0');
                    $name = array('All');
            }
            $result = $this->db->get('ads_vehicles_fuel')->result();
            for ($i = 0; $i < count($result); $i++){
                array_push($id, $result[$i]->id);
                array_push($name, $result[$i]->title);
            }
            return array_combine($id, $name);
        }
	function get_ads_vehicles_year(){
        $result = $this->db->get('ads_vehicles_year')->result();;
        $id = array('0');
        $name = array('-');
        for ($i = 0; $i < count($result); $i++)
        {
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_ads_vehicles_km(){
        $result = $this->db->get('ads_vehicles_km')->result();;
        $id = array('0');
        $name = array('-');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_ads_vehicles_price(){
        $result = $this->db->get('ads_vehicles_price')->result();;
        $id = array('0');
        $name = array('-');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_ads_vehicles_bye_sell($addAll = NULL){
		$id = array();
		$name = array();
		if($addAll == NULL){
			$id = array('0');
			$name = array('All');
		}
        $result = $this->db->get('ads_vehicles_bye_sell')->result();
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	public function get_ads_vehicle($id,$parametar){
		$this->db->select('*');
		$this->db->from('ads_vehicles');
		$this->db->where('id', $id);
		return $this->db->get()->row()->$parametar;
	}
	function get_ads_vehicles($ads_vehicles_type_id=NULL,$ads_vehicles_brand_id=NULL,$ads_vehicles_model_id=NULL, $ads_vehicles_fuel_id=NULL,
							$ads_vehicles_year_start_id=NULL,$ads_vehicles_year_end_id=NULL,$ads_vehicles_km_start_id=NULL,$ads_vehicles_km_end_id=NULL,
							$ads_vehicles_price_start_id=NULL,$ads_vehicles_price_end_id=NULL,$address_country_id=NULL,$address_country_region_id=NULL,
							$address_country_municipality_id=NULL,$ads_vehicles_bye_sell_id=NULL,$queryRowStart=NULL
							){
		
		
		
		$this->db->select('*');
		$this->db->from('ads_vehicles');
		
		if($ads_vehicles_type_id != 0){$this->db->where('ads_vehicles_type_id', $ads_vehicles_type_id);}
		if($ads_vehicles_brand_id != 0){$this->db->where('ads_vehicles_brand_id', $ads_vehicles_brand_id);}
		if($ads_vehicles_model_id != 0){$this->db->where('ads_vehicles_model_id', $ads_vehicles_model_id);}
		if($ads_vehicles_fuel_id != 0){$this->db->where('vehicles_fuel', $ads_vehicles_fuel_id);}
		
		
		if($ads_vehicles_year_start_id != 0){
			$ads_vehicles_year_start_title = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_year', $ads_vehicles_year_start_id, 'title');
			$this->db->where('year >=', $ads_vehicles_year_start_title);
		}
		if($ads_vehicles_year_end_id != 0){
			$ads_vehicles_year_start_end = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_year', $ads_vehicles_year_end_id, 'title');
			$this->db->where('year <=', $ads_vehicles_year_start_end);
		}
		if($ads_vehicles_km_start_id != 0){
			$ads_vehicles_km_start_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_km', $ads_vehicles_km_start_id, 'value');
			$this->db->where('km >=', $ads_vehicles_km_start_value);
		}
		if($ads_vehicles_km_end_id != 0){
			$ads_vehicles_km_end_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_km', $ads_vehicles_km_end_id, 'value');
			$this->db->where('km <=', $ads_vehicles_km_end_value);
		}
		if($ads_vehicles_price_start_id != 0){
			$ads_vehicles_price_start_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_price', $ads_vehicles_price_start_id, 'value');
			$this->db->where('price >=', $ads_vehicles_price_start_value);
		}
		if($ads_vehicles_price_end_id != 0){
			$ads_vehicles_price_end_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_price', $ads_vehicles_price_end_id, 'value');
			$this->db->where('price <=', $ads_vehicles_price_end_value);
		}
		
		
		if($address_country_id != 0){$this->db->where('address_country_id', $address_country_id);}
		if($address_country_region_id != 0){$this->db->where('address_country_region_id', $address_country_region_id);}
		if($address_country_municipality_id != 0){$this->db->where('address_country_municipality_id', $address_country_municipality_id);}
		if($ads_vehicles_bye_sell_id != 0){$this->db->where('ads_vehicles_bye_sell_id', $ads_vehicles_bye_sell_id);}
		
		$this->db->where('marking =', '0');
		if($queryRowStart==NULL){
			$this->db->limit(16,0);
		}
		else{
			$this->db->limit(16,$queryRowStart);
		}
		$this->db->order_by('date_modify','desc');
		
		$query = $this->db->get();
		$result = $query->result();
		
		
		
        $id = array();
        $title = array();
		$adsVehicles = array();
        for ($i = 0; $i < count($result); $i++){
			$ID = sprintf('%08d', $result[$i]->id);
			$directoryPath = 'images/ads/car/ad_'.$ID.'/';
            array_push($id, $result[$i]->id);
            array_push($title, $result[$i]->title);
			$adsVehicles[$i]["id"] = $result[$i]->id;
			$adsVehicles[$i]["title"] = $result[$i]->title;
			$adsVehicles[$i]["region"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('address_country_region', $result[$i]->address_country_region_id, 'title'));
			$adsVehicles[$i]["municipality"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('address_country_municipality', $result[$i]->address_country_municipality_id, 'title'));
			$adsVehicles[$i]["year"] = $result[$i]->year;
			$adsVehicles[$i]["km"] = $result[$i]->km;
			$adsVehicles[$i]["price"] = number_format($result[$i]->price);
			$adsVehicles[$i]["fuel"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('ads_vehicles_fuel', $result[$i]->vehicles_fuel, 'title'));
			$adsVehicles[$i]["sell"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('ads_vehicles_bye_sell', $result[$i]->ads_vehicles_bye_sell_id, 'title'));
			$adsVehicles[$i]["imageUrl"] = base_url($directoryPath.'main-'.$this->Vebko_model->get_single_value_from_db_query('ads_vehicles', $ID, 'ads_gallery_image_name_1'));		
			$adsVehicles[$i]["body"] = $this->Vebko_model->substrwords(strip_tags($result[$i]->body),260,' ...');
			$adsVehicles[$i]["date"] = $this->Vebko_model->ago ($result[$i]->date_modify);
                        $adsVehicles[$i]["type"] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db_query('ads_vehicles_brand', $result[$i]->ads_vehicles_type_id, 'title'));

		}
        return $adsVehicles;
    }
	function checkhAdsOwner($id){
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession(); 
		$adsVehicleUserId = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles', $id,'userId');
		$return = 0;
		if($userId==$adsVehicleUserId){
			$return = 1;
		}
		return $return;
	}
	
	public function getSearchSugestion(){
		$this->db->select('id');
		$this->db->select('title');
		$this->db->select('ads_gallery_image_name_1');
		$this->db->from('ads_vehicles');
		$this->db->where('marking',0);
		$this->db->limit(16,0);
		$this->db->order_by('date','desc');		
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	function getMyVehicles(){
		$userId = $this->Vebko_model->getUserIdFromFacebookIdFromSession(); 
		$this->db->select('id');
		$this->db->select('date_modify');
		$this->db->select('title');
		$this->db->select('ads_gallery_image_name_1');
		$this->db->from('ads_vehicles');
		$this->db->where('userID',$userId);
		$this->db->order_by('date_modify','desc');		
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}