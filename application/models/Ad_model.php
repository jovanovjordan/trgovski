<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_model extends CI_Model{

	public function __construct(){		
		parent::__construct();

		$this->load->database();
		$this->load->model('Vebko_model');		
	}	
	
	function form_insert($data){
		$this->db->insert('ads_vehicles', $data);
	}

	public function create_ad($adTitle) {		
		$data = array(
            'id'   => 58,
			'adTitle'   => $adTitle,			
		);		
		return $this->db->insert('ads', $data);		
	}

	/*function get_ads_vehicles_type(){
        $result = $this->db->get('ads_vehicles_type')->result();
        $id = array('0');
        $name = array('All');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	function get_ads_vehicles_brand($cid=NULL){
        $result = $this->db->where('ads_vehicles_type_id', $cid)->get('ads_vehicles_brand')->result();
        $id = array('0');
        $name = array('All');
        for ($i=0; $i<count($result); $i++)
        {
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	function get_ads_vehicles_model($cid=NULL){
        $result = $this->db->where('ads_vehicles_brand_id', $cid)->get('ads_vehicles_model')->result();
        $id = array('0');
        $name = array('All');
        for ($i=0; $i<count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	function get_ads_vehicles_fuel(){
        $result = $this->db->get('ads_vehicles_fuel')->result();;
        $id = array('0');
        $name = array('all');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	function get_ads_vehicles_year(){
        $result = $this->db->get('ads_vehicles_year')->result();;
        $id = array('0');
        $name = array('-');
        for ($i = 0; $i < count($result); $i++)
        {
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	function get_ads_vehicles_km(){
        $result = $this->db->get('ads_vehicles_km')->result();;
        $id = array('0');
        $name = array('-');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	function get_ads_vehicles_price(){
        $result = $this->db->get('ads_vehicles_price')->result();;
        $id = array('0');
        $name = array('-');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	
	
	function get_ads_vehicles_bye_sell(){
        $result = $this->db->get('ads_vehicles_bye_sell')->result();;
        $id = array('0');
        $name = array('All');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	
	
	function get_address_country(){
        $result = $this->db->get('address_country')->result();;
        $id = array('0');
        $name = array('All country');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_address_country_region($cid=NULL){
        //$result = $this->db->get('address_country_region')->result();
		$result = $this->db->where('address_country_id', $cid)->get('address_country_region')->result();
        $id = array('0');
        $name = array('All regions');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }
	function get_address_country_municipality($cid=NULL){
		$result = $this->db->where('address_country_id', $cid)->get('address_country_municipality')->result();
        $id = array('0');
        $name = array('all');
        for ($i = 0; $i < count($result); $i++){
            array_push($id, $result[$i]->id);
            array_push($name, $result[$i]->title);
        }
        return array_combine($id, $name);
    }	
	*/
	function get_ads_vehicles($ads_vehicles_type_id=NULL,$ads_vehicles_brand_id=NULL,$ads_vehicles_model_id=NULL, $ads_vehicles_fuel_id=NULL,
							$ads_vehicles_year_start_id=NULL,$ads_vehicles_year_end_id=NULL,$ads_vehicles_km_start_id=NULL,$ads_vehicles_km_end_id=NULL,
							$ads_vehicles_price_start_id=NULL,$ads_vehicles_price_end_id=NULL,$address_country_id=NULL,$address_country_region_id=NULL,
							$address_country_municipality_id=NULL,$ads_vehicles_bye_sell_id=NULL,$queryRowStart=NULL
							){
		
		
		
		$this->db->select('*');
		$this->db->from('ads_vehicles');
		
		if($ads_vehicles_type_id != 0){$this->db->where('ads_vehicles_type_id', $ads_vehicles_type_id);}
		if($ads_vehicles_brand_id != 0){$this->db->where('ads_vehicles_brand_id', $ads_vehicles_brand_id);}
		if($ads_vehicles_model_id != 0){$this->db->where('ads_vehicles_model_id', $ads_vehicles_model_id);}
		if($ads_vehicles_fuel_id != 0){$this->db->where('vehicles_fuel', $ads_vehicles_fuel_id);}
		
		
		if($ads_vehicles_year_start_id != 0){
			$ads_vehicles_year_start_title = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_year', $ads_vehicles_year_start_id, 'title');
			$this->db->where('year >=', $ads_vehicles_year_start_title);
		}
		if($ads_vehicles_year_end_id != 0){
			$ads_vehicles_year_start_end = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_year', $ads_vehicles_year_end_id, 'title');
			$this->db->where('year <=', $ads_vehicles_year_start_end);
		}
		if($ads_vehicles_km_start_id != 0){
			$ads_vehicles_km_start_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_km', $ads_vehicles_km_start_id, 'value');
			$this->db->where('km >=', $ads_vehicles_km_start_value);
		}
		if($ads_vehicles_km_end_id != 0){
			$ads_vehicles_km_end_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_km', $ads_vehicles_km_end_id, 'value');
			$this->db->where('km <=', $ads_vehicles_km_end_value);
		}
		if($ads_vehicles_price_start_id != 0){
			$ads_vehicles_price_start_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_price', $ads_vehicles_price_start_id, 'value');
			$this->db->where('price >=', $ads_vehicles_price_start_value);
		}
		if($ads_vehicles_price_end_id != 0){
			$ads_vehicles_price_end_value = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_price', $ads_vehicles_price_end_id, 'value');
			$this->db->where('price <=', $ads_vehicles_price_end_value);
		}
		
		
		if($address_country_id != 0){$this->db->where('address_country_id', $address_country_id);}
		if($address_country_region_id != 0){$this->db->where('address_country_region_id', $address_country_region_id);}
		if($address_country_municipality_id != 0){$this->db->where('address_country_municipality_id', $address_country_municipality_id);}
		if($ads_vehicles_bye_sell_id != 0){$this->db->where('ads_vehicles_bye_sell_id', $ads_vehicles_bye_sell_id);}
		
		
		if($queryRowStart==NULL){
			$this->db->limit(10,0);
		}
		else{
			$this->db->limit(10,$queryRowStart);
		}
		$this->db->order_by('date','desc');
		
		$query = $this->db->get();
		$result = $query->result();
		
		
		
        $id = array();
        $title = array();
		$adsVehicles = array();
        for ($i = 0; $i < count($result); $i++){
			$ID = sprintf('%08d', $result[$i]->id);
			$directoryPath = 'images/ads/car/ad_'.$ID.'/';
            array_push($id, $result[$i]->id);
            array_push($title, $result[$i]->title);
			$adsVehicles[$i][0] = $result[$i]->id;
			$adsVehicles[$i][1] = $result[$i]->title;
			$adsVehicles[$i][2] = $this->Vebko_model->get_single_value_from_db_query('address_country_region', $result[$i]->address_country_region_id, 'title');
			$adsVehicles[$i][3] = $this->Vebko_model->get_single_value_from_db_query('address_country_municipality', $result[$i]->address_country_municipality_id, 'title');
			$adsVehicles[$i][4] = $result[$i]->year;
			$adsVehicles[$i][5] = $result[$i]->km;
			$adsVehicles[$i][6] = $result[$i]->price;
			$adsVehicles[$i][7] = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_fuel', $result[$i]->vehicles_fuel, 'title');
			$adsVehicles[$i][8] = $this->Vebko_model->get_single_value_from_db_query('ads_vehicles_bye_sell', $result[$i]->ads_vehicles_bye_sell_id, 'title');
			$adsVehicles[$i][9] = base_url($directoryPath.'main-'.$this->Vebko_model->get_single_value_from_db_query('ads_vehicles', $ID, 'ads_gallery_image_name_1'));		
			$adsVehicles[$i][10] = $result[$i]->body;
			$adsVehicles[$i][11] = $this -> ago ($result[$i]->date);
		}
        return $adsVehicles;
    }
	
	
	
	public function get_municipality($id=NULL){
		$this->db->select('title');
		$this->db->from('address_country_municipality');
		$this->db->where('id', $id);
		return $this->db->get()->row()->title;
	}
	
	//car
	public function get_ads_vehicle($id,$parametar){
		$this->db->select('*');
		$this->db->from('ads_vehicles');
		$this->db->where('id', $id);
		return $this->db->get()->row()->$parametar;
	}
	
	
	
	
	public function getNextId($tableName){
		$queryAll = "SELECT AUTO_INCREMENT
					FROM information_schema.tables
					WHERE table_name = '".$tableName."'";
		$parametar = "AUTO_INCREMENT";
		return $this->db->query($queryAll)->row()->$parametar;
	}
	public function ago($time){
		$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$periods = $this->Vebko_model->translateTextInArray($periods);
		$lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

		   $difference     = $now - $time;
		   $tense         = "ago";

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		   $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
		   $periods = array("seconds", "minutes", "hours", "days", "weeks", "months", "years", "decade");
		   $periods = $this->Vebko_model->translateTextInArray($periods);
	   }

	   return " ".$this->Vebko_model->translateText('before')." $difference $periods[$j]";
	}
	
	
	
	public function test($d){
		$data=array('title'=>$d);
		$this->db->where('id',12);
		$this->db->update('ads_vehicles',$data);
	}
}
