<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model{

	public function __construct(){		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->model('Vebko_model');		
	}
	public function putMail($country_id, $mails){
		$mails = explode("	", $mails);
		$mails = $this->getJustEmail($mails);
		//echo"<pre>";print_R($mails);exit;
		$mails = array_unique($mails);
		foreach ($mails as $mail){
			$id = 0;
			$this->db->select('*');
			$this->db->from('admin_mails');
			$this->db->where('title', $mail);
			$id = $this->db->get()->row()->id;
			if($id == NULL){
				$data = array(
					'country_id' => $country_id,
					'title' => $mail
				);
				$this->db->insert('admin_mails', $data);
			}
		}
	}
        public function getJustEmail($words){
            $i=0;
            foreach($words as $word){
                if (strpos($word, '@') !== false) {
                    $mails[$i]=$word;
                    $i++;
                }
            }
            return $mails;
        }
        public function mailArray($country_id,$mark){
            $mailArray = array();
            
            $this->db->select('*');
            $this->db->from('admin_mails');
            $this->db->where('country_id', $country_id);
            $this->db->where('mark', $mark);
            //$this->db->order_by("dateLastSend", "asc");
            $this->db->order_by("id", "desc");
            $this->db->limit(100, 0);
            $query = $this->db->get();
            $counter =1;
           foreach ($query->result() as $row){
               array_push($mailArray, $row->title); 
              
               $counter++;
           }
           //array_push($mailArray, "jovanovjoce+".date("Ymdhisa")."@gmail.com");
           
           return $mailArray;
        }
        public function sendMails($mailArray,$subject,$body){
            $mailArray = array_unique($mailArray);
            
            $result = count($mailArray);
            foreach($mailArray as $mail) {

                $to      =  $mail;
                $subject = $subject;
				
                $message = $body;
                $this->updateTimeSendMail($to);
                $this->increaceMark($to);				
                $this->sendMailCode($to, $subject, $message);				
                
                $conuter++;
            }
            //echo "<pre>"; print_r($mailArray);exit;
        }
		
		public function sendMailCode($to, $subject, $message){
                    $this->load->library('email');
                    $this->email->set_mailtype("html");
                    $this->email->from('no-replay@trgovski.com', 'Trgovski.com');
                    $this->email->to($to);
                    $this->email->cc('');
                    $this->email->bcc('');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    $this->email->send();
		}
		public function updateTimeSendMail($email){
			
                    $data=array(
                        'dateLastSend'=>date('Y-m-d H:i:s',time())
                        );
                    $this->db->where('title',$email);
                    $this->db->update('admin_mails',$data);
			
		}
		public function increaceMark($email){
                    $this->db->where('title', $email);
                    $this->db->set('mark', 'mark+1', FALSE);
                    $this->db->update('admin_mails');
		}
}