<?php

class Image_model extends CI_Model {
public function risizeImage($upload_data, $widthSize, $heightSize, $directorySavePath, $startFileName){
		$upload_data = $this->upload->data();
		$image_config["image_library"] = "gd2";
		$image_config["source_image"] = $upload_data["full_path"];
		$image_config['create_thumb'] = FALSE;
		$image_config['maintain_ratio'] = TRUE;
		$image_config['new_image'] = $directorySavePath.$startFileName.'-'. $upload_data['file_name'];
		$image_config['quality'] = "90%";
		$image_config['width'] = $widthSize;
		$image_config['height'] = $heightSize;
		$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
		$image_config['master_dim'] = ($dim > 0)? "height" : "width";
		
		//echo "<pre>"; print_r($image_config); exit;
		$this->load->library('image_lib');
		$this->image_lib->initialize($image_config);
		$this->image_lib->resize();
	}
public function risizeAndCropImage($upload_data, $widthSize, $heightSize,$directorySavePath, $startFileName){
	$this->load->library('image_lib');  
	$config = array(
			'source_image'      =>  $upload_data["full_path"], 
			'create_thumb'      => false,
			'new_image'         =>  './images/ads/tmp/'.$startFileName.'-temp-'.$upload_data['file_name'],
			'maintain_ratio'    => true,
			'quality'         	=>  '100%',
			'width'             => $widthSize,
			'height'            => $heightSize,
			);
			
	$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($config['width'] / $config['height']);
	$config['master_dim'] = ($dim > 0)? "height" : "width"; 
	$this->image_lib->initialize($config);
	$this->image_lib->resize();

	$config = array(
		'image_library'      =>  'gd2', 
		'source_image'      =>  './images/ads/tmp/'.$startFileName.'-temp-'.$upload_data['file_name'],
		'new_image'         =>  $directorySavePath.'/'.$startFileName.'-'.$upload_data['file_name'],
		'maintain_ratio'    => false,
		'quality'         =>  '70%',
		'width'             => $widthSize,
		'height'            => $heightSize,
		'x_axis'    => '0',
		'y_axis'    => '0',
		);
		
	$this->image_lib->clear();
	$this->image_lib->initialize($config); 
	$this->image_lib->crop();
}
}	