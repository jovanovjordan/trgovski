<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Bot_model extends CI_Model{

	public function __construct(){		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->model('Vebko_model');		
	}
	public function keywords($arrayText){
		$key = 0;
		foreach ($arrayText as $value) {
			$search_word_title = $this->getSearchWordFromWordSuggestion($value);
			if($search_word_title){
				$arryaKeywordId[$key] = $search_word_title;
				$key++;
			}			
		}
		$arryaKeywordId = array_unique($arryaKeywordId);
		$key = 0;
		foreach ($arryaKeywordId as $value) {
			$categoryId = $this -> Vebko_model -> get_single_value_from_db('bot_search_word', $value, 'search_word_category_id');
			$categoryTitle = $this -> Vebko_model -> get_single_value_from_db('bot_search_word_category', $categoryId, 'title');
			$keyword = $this -> Vebko_model -> get_single_value_from_db('bot_search_word', $value, 'title');
			$arryaKeywords[$categoryTitle] = $keyword;
			
		}
		return $arryaKeywords;
	}
        public function botAnswer($keywords,$senderId){
            $botAnswer = 0;
            if (array_key_exists('message', $keywords)) {
                    if($keywords['message']=='hi'){
                        $botAnswer = $this->translateText('hi',$this->language($senderId));
                    }
                    if($keywords['message']=='bye'){
                        $botAnswer = $this->translateText('hi',$this->language($senderId));
                    }
                    if($keywords['message']=='help'){
                        $botAnswer = 'Едноставно поставете прашање што пребарувате. На пример "Барам стан под кирија во Центар" или "Купувам автомобил"';
                    }
					if($keywords['message']=='en'){
						$langauge = 'en';
						$this->setLanguage($langauge, $senderId);
                        $botAnswer = 'Langauge is change to English.';
                    }
					if($keywords['message']=='mk'){
						$langauge = 'mk';
						$this->setLanguage($langauge, $senderId);
                        $botAnswer = 'Супер, сега ќе зборуваме на македоснки.';
						}
					if($keywords['message']=='sr'){
						$langauge = 'sr';
						$this->setLanguage($langauge, $senderId);
                        $botAnswer = 'Супер, сега ќе зборуваме на српски.';
					}
					if($keywords['message']=='bg'){
						$langauge = 'bg';
						$this->setLanguage($langaugeId, $senderId);
                        $botAnswer = 'Langauge is change to English.';
                    }
            }
            return $botAnswer;
        }
	public function setLanguage($langaugeId, $senderId){
			$this->db->where('title', $senderId);
			$q = $this->db->get('bot_users_session');

			if ( $q->num_rows() > 0 ) 
			{	
					$data = array(
						'languageId' => $langaugeId
					);

					$this->db->where('title', $senderId);
					$this->db->update('bot_users_session', $data);
					return "ok";
			}
			else{
				$data = array(
						'languageId' => $langaugeId
					);

					$this->db->where('title', $senderId);
					$this->db->insert('bot_users_session', $data);
					return "ok";
			}
	}
	
	
	public function arrayResults($keywords){
		//print_r($keywords); exit;
                $type = 'estate';
                if (array_key_exists('estate', $keywords)) {
                    $type = 'estate';
                }
                if (array_key_exists('vehicles_type', $keywords)||array_key_exists('vehicles_brand', $keywords)||array_key_exists('vehicles_model', $keywords)) {
                    $type = 'vehicles';
                }
                if (array_key_exists('electronics_type', $keywords)) {
                    $type = 'electronics';
                }
                if (array_key_exists('clothing_type', $keywords)) {
                    $type = 'clothing';
                }                
                if (array_key_exists('service_type', $keywords)) {
                    $type = 'service';
                }
                if (array_key_exists('other', $keywords)) {
                    $type = 'other';
                }                
                //echo $type;exit;
		$this->db->select('*');
		$this->db->from('address_country');
		$this->db->where('title', $keywords[country]);
		$countryId = $this->db->get()->row()->id;
		
		$this->db->select('*');
		$this->db->from('address_country_region');
		$this->db->where('title', $keywords[region]);
		$regionId = $this->db->get()->row()->id;
		
		$this->db->select('*');
		$this->db->from('address_country_municipality');
		$this->db->where('title', $keywords[municipality]);
		$munipilityId = $this->db->get()->row()->id;
		
                if($type == 'estate'){
                    $estateId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_estate_type');
                    $this->db->where('title', $keywords[estate]);
                    $estateId = $this->db->get()->row()->id;
                    
                    $estateBuySellId = 0;
                    if (array_key_exists('buy_sell', $keywords)) {
                        $estateBuySellId = 0;
                        $this->db->select('*');
                        $this->db->from('ads_estate_buy_sell');
                        $this->db->where('title', $keywords[buy_sell]);
                        $estateBuySellId = $this->db->get()->row()->id;
                    }
                    //echo $estateBuySellId; exit;
                }
                
                if($type == 'vehicles'){
                    $vehicleTypeId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_vehicles_type');
                    $this->db->where('title', $keywords[vehicles_type]);
                    $vehicleTypeId = $this->db->get()->row()->id;
                    
                    $vehicleBrandId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_vehicles_brand');
                    $this->db->where('title', $keywords[vehicles_brand]);
                    $vehicleBrandId = $this->db->get()->row()->id;
                    
                    $vehicleModelId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_vehicles_model');
                    $this->db->where('title', $keywords[vehicles_model]);
                    $vehicleModelId = $this->db->get()->row()->id;
                }
                
                if($type == 'electronics'){
                    $electronicsId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_electronics_type');
                    $this->db->where('title', $keywords[electronics_type]);
                    $electronicsId = $this->db->get()->row()->id;
                }
                
                if($type == 'clothing'){
                    $clothingId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_clothing_type');
                    $this->db->where('title', $keywords[clothing]);
                    $clothingId = $this->db->get()->row()->id;
                }
                
                 if($type == 'service'){
                    $serviceId = 0;
                    $this->db->select('*');
                    $this->db->from('ads_service_type');
                    $this->db->where('title', $keywords[clothing]);
                    $serviceId = $this->db->get()->row()->id;
                }
                
        	$this->db->select('*');
		$this->db->from('ads_'.$type);
		if($countryId){$this->db->where('address_country_id', $countryId);}
		if($regionId){$this->db->where('address_country_region_id', $regionId);}
		if($munipilityId){$this->db->where('address_country_municipality_id', $munipilityId);}
                if($estateId){$this->db->where('ads_type_id', $estateId);}
                if($estateBuySellId){$this->db->where('ads_buy_sell_id', $estateBuySellId);}                
                if($vehicleTypeId){$this->db->where('ads_vehicles_type_id', $vehicleTypeId);}
                if($vehicleBrandId){$this->db->where('ads_vehicles_brand_id', $vehicleBrandId);}
                if($vehicleModelId){$this->db->where('ads_vehicles_model_id', $vehicleModelId);}
                if($electronicsId){$this->db->where('ads_type_id', $electronicsId);}
                if($clothingId){$this->db->where('ads_type_id', $clothingId);}
                if($serviceId){$this->db->where('ads_type_id', $serviceId);}
		$this->db->where('marking', '0');
                $this->db->limit(10);
		$this->db->order_by("date_modify", "desc");
		$query = $this->db->get();
		for($i=0;$i<$query->num_rows();$i++){
                        $data[$i][type] = $type; 
			$data[$i][id] = $query->row($i)->id;
                        $data[$i][title] = $this->Vebko_model->removeQuotes($query->row($i)->title);
                        $data[$i][price] = $query->row($i)->price.'€';
                        $data[$i][municipality] = $this->Vebko_model->translateText($this->Vebko_model->get_single_value_from_db('address_country_municipality', $query->row($i)->address_country_municipality_id, 'title'));
                        $data[$i][timeago] = $this->Vebko_model->ago($query->row($i)->date_modify);
                        $data[$i][contact_phone] = $query->row($i)->contact_phone;
                        $data[$i][ads_gallery_image_name_1] = $query->row($i)->ads_gallery_image_name_1;
		}

		$returnResults = $data;
		return $returnResults;
	}
	public function getSearchWordFromWordSuggestion($suggestionWord){
		
		$search_word_id = 0;
		$this->db->select('*');
		$this->db->from('bot_search_word_suggestion');
		$this->db->where('title', $suggestionWord);
		$search_word_id = $this->db->get()->row()->search_word_id;
		if($search_word_id !=0)		{
			$this->db->select('*');
			$this->db->from('bot_search_word');
			$this->db->where('id', $search_word_id);
			$search_word_title = $this->db->get()->row()->title;
		}
		return $search_word_id;
	}
        
        public function getKeywordFromDB(){
            $this->db->select('*');
            $this->db->from('bot_search_word');
			$this->db->order_by("id", "desc");
            $query = $this->db->get();
            
            for($i=0;$i<$query->num_rows();$i++){
                $data[$i][id] = $query->row($i)->id;
                $data[$i][title] = $query->row($i)->title;               
                $search_word_category_id = $query->row($i)->search_word_category_id;
                
                $this->db->select('*');        
				$this->db->from('bot_search_word_category');                
				$this->db->where('id', $search_word_category_id);
                $data[$i][category] = $this->db->get()->row()->title;
                
                
                $this->db->select('*');
                $this->db->from('bot_search_word_suggestion');
                $this->db->where('search_word_id', $data[$i][id]);
                $query2 = $this->db->get();
                
                for($i2=0;$i2<$query2->num_rows();$i2++){                    
                    $data[$i][suggestion][$i2] = $query2->row($i2)->title; 
                }
            }
            return $data;
        }
	public function getCategoryID($category){
		$categoryId = 0;
		$this->db->select('*');
        $this->db->from('bot_search_word_category');
		$this->db->where('title', $category);
		$categoryId = $this->db->get()->row()->id;
		if($categoryId == NULL){
			$data = array(
			   'title' => $category
			);
			$this->db->insert('bot_search_word_category', $data);
			$categoryId = $this->db->insert_id();
		}
		return $categoryId;
	}
	public function getSearchWordID($searchWord,$categoryID){
		$searchWordId = 0;
		$this->db->select('*');
                $this->db->from('bot_search_word');
		$this->db->where('title', $searchWord);
		$searchWordId = $this->db->get()->row()->id;
		if($searchWordId == NULL){
			$data = array(
				'search_word_category_id' => $categoryID,
				'title' => $searchWord
			);
			$this->db->insert('bot_search_word', $data);
			$searchWordId = $this->db->insert_id();
		}
		return $searchWordId;
	}
	
	public function putSearchWordSuggestion($searchWordSuggestions,$searchWordID){
		$searchWordSuggestions = explode(" ", $searchWordSuggestions);
		$searchWordSuggestions = array_unique($searchWordSuggestions);
		foreach ($searchWordSuggestions as $searchWordSuggestion){
			$searchWordSuggestionId = 0;
			$this->db->select('*');
			$this->db->from('bot_search_word_suggestion');
			$this->db->where('title', $searchWordSuggestion);
			$searchWordSuggestionId = $this->db->get()->row()->id;
			
			if($searchWordSuggestionId == NULL){
				$data = array(
					'search_word_id' => $searchWordID,
					'title' => $searchWordSuggestion
				);
				$this->db->insert('bot_search_word_suggestion', $data);
				$searchWordId = $this->db->insert_id();
			}
		}
		return $searchWordSuggestionId;
	}
	public function saveUserSessionId($botUserSessionId){
            $title = 0;
            $this->db->select('*');
            $this->db->from('bot_users_session');
            $this->db->where('title', $botUserSessionId);
            $title = $this->db->get()->row()->id;

            if($title == NULL){
                    $data = array(
                        'title' => $botUserSessionId,
                        'time' => date('m/d/Y h:i:s', time())
                    );
                    $this->db->insert('bot_users_session', $data);
            }
            else{
                 $data = array(
                        'time' => date('m/d/Y h:i:s', time())
                    );
                $this->db->where('title', $botUserSessionId);
                $this->db->update('bot_users_session',$data);
            }
        }
        public function getSenderIdArray(){          
            $this->db->select('*');
            $this->db->from('bot_users_session');
            $query = $this->db->get();
            for($i=0;$i<$query->num_rows();$i++){
                $senderIdArray[$i] = $query->row($i)->title;
            }
            //echo"<pre>"; print_r($senderIdArray);exit;
            return $senderIdArray;
        }
        public function sendFBChatMessage($accessToken, $senderId, $botMessageText, $buttons=NULL){
                  
            //send message to facebook bot
            /*$response = [
                    'recipient' => [ 'id' => $senderId ],
                    'message' => [ 'text' => $botMessageText ]
            ];
            $response = json_encode($response);*/
            
            if($buttons){
                foreach ($buttons as &$button) {
                    $allelement .= '{
                                  "content_type":"text",
                                  "title":"'.$button.'",
                                  "payload":"<POSTBACK_PAYLOAD>"
                                },'; 
                }
                $response = '{
                        "recipient":{
                            "id":'.$senderId.'
                        }, 
                        "message":{
                            "text": "'.$botMessageText.'",
                                "quick_replies":[
                                '.$allelement.'
                              ]
                          }
                    }';
            }
            $ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token='.$accessToken);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);               
            if(empty($messageText)){
                $result = curl_exec($ch);
                 $answer = 'dddd';
            }
            $messageText = NULL;
            curl_close($ch);
        }
        
	public function language($senderId){
		$this->db->select('*');
		$this->db->where('title', $senderId);
		$this->db->limit(1);// only apply if you have more than same id in your table othre wise comment this line
		$query = $this->db->get('bot_users_session');
		$row = $query->row();
		return $row->languageId;
	}
	public function translateText($string, $ln){
		
		$this->lang->load("message",$ln);
		if($this->lang->line($string)){
			$returnString = $this->lang->line($string);
		}
		else{
			$returnString = $string;
		}
		return $returnString;
	}
       
}