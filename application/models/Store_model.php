<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Store_model extends CI_Model{
    public function _construct(){		
            parent::__construct();

            $this->load->database();
            $this->load->model('Vebko_model');		
    }
    function existInDb($db,$colum,$value,$userId){       
        $this->db->where($colum,$value);
        $this->db->where('userId !=',$userId);
        $query = $this->db->get($db);
        if ($query->num_rows() > 0){
            return $query->num_rows();
        }
        else{
            return 0;
        }
    }
    function storeUserExist($userId)    {
       
        $this->db->where('userId',$userId);
        $query = $this->db->get('store');
        if ($query->num_rows() > 0){
            return $query->num_rows();
        }
        else{

            return 0;
        }
    }
    public function notAllowWord($subdomain){
        $return = 1;
        $notAllowWords = array('www','html','com.mk','facebook','instagram','linkedin',',','.');
        foreach ($notAllowWords as $value){
            if (strpos($subdomain, $value) !== false) {
                $return = 0;
            }
        }
        return $return;
    }
    public function storeUpdateData($userId,$data){		
		$this->db->where('userId', $userId);
		$this->db->update('store', $data);
	}
}

